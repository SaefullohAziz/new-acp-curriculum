<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login', ['title' => 'Login']);
});

// Route::get('/register/success', function () {
//     $view = [
//         'title' => 'Thanks for Registration!',
//     ];
//     return view('welcome', $view);
// })->name('register.success');

Auth::routes();
Route::post('/login/submit', 'Auth\LoginController@Login')->name('login.submit');

// School
Route::prefix('school')->name('school.')->group(function () {
    Route::get('register', 'SchoolController@create')->name('register');
    Route::post('/', 'SchoolController@store')->name('store');
});

Route::prefix('get')->name('get.')->group( function() {
    Route::post('city', 'GetController@city')->name('city');
    Route::post('chart', 'GetController@adminChartSchool')->name('chart');
    Route::post('chartReqJava', 'GetController@chartReqJava')->name('chartReqJava');
    Route::post('chartReqOutJava', 'GetController@chartReqOutJava')->name('chartReqOutJava');
    Route::post('schoolStatus', 'GetController@schoolStatus')->name('schoolStatus');
    Route::post('studentStatus', 'GetController@studentStatus')->name('studentStatus');
    Route::post('regency', 'GetController@regency')->name('regency');
    Route::post('school', 'GetController@school')->name('school');
    Route::post('generation', 'GetController@generation')->name('generation');
    Route::post('unitScore', 'GetController@unitScore')->name('unitScore');
    Route::post('studentAchievement', 'GetController@studentAchievement')->name('studentAchievement');
    Route::post('schoolAchievement', 'GetController@schoolAchievement')->name('schoolAchievement');
    Route::post('student', 'GetController@student')->name('student');
    Route::post('schoolAcademy', 'GetController@schoolAcademy')->name('schoolAcademy');
    Route::post('schoolClassIntensif', 'GetController@schoolClassIntensif')->name('schoolClassIntensif');
    Route::post('talentPool', 'GetController@talentPool')->name('talentPool');
    Route::post('academy', 'GetController@academy')->name('academy');
});


// default guard
Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('user', 'UserController')->except(['destroy']);
    Route::prefix('user')->name('user.')->group(function(){
        Route::post('list', 'UserController@list')->name('list');
        Route::post('reset', 'UserController@reset')->name('reset');
        Route::delete('destroy', 'UserController@destroy')->name('destroy');
    });
    Route::resource('psychotest-result', 'PsychologicalTestResultController')->except(['list', 'show']);
    Route::prefix('psychotest-result')->name('psychotest-result.')->group(function(){
        Route::post('mbti_list', 'PsychologicalTestResultController@mbti_list')->name('mbti_list');
        Route::get('{psychologicalTest}/attempt/{psychologicalTestAttempt}/result', 'PsychologicalTestResultController@mbti_show')->name('mbti_show');

        Route::post('disc_list', 'PsychologicalTestResultController@disc_list')->name('disc_list');
        Route::get('{user}/result', 'PsychologicalTestResultController@disc_show')->name('disc_show');
    });
});

// guard Student
Route::middleware(['auth:student'])->group(function () {
    Route::namespace('Student')->prefix('student')->name('student.')->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::post('/home/store', 'HomeController@store')->name('home.store');
        
        // StudentLesson
        Route::prefix('studentLesson')->name('studentLesson.')->group(function(){
            Route::post('list', 'StudentLessonController@list')->name('list');
            Route::post('reset', 'StudentLessonController@reset')->name('reset');
            Route::delete('destroy', 'StudentLessonController@destroy')->name('destroy');
            Route::post('preCreate', 'StudentLessonController@preCreate')->name('preCreate');
        });
        Route::resource('studentLesson', 'StudentLessonController')->except(['destroy']);

        // profile
        Route::prefix('profile')->name('profile.')->group(function(){
            Route::get('preview/{student}','ProfileController@preview')->name('preview');
            Route::get('download/{student}','ProfileController@download')->name('download');
            Route::post('list', 'ProfileController@list')->name('list');
            Route::post('reset', 'ProfileController@reset')->name('reset');
            Route::delete('destroy', 'ProfileController@destroy')->name('destroy');
        });
        Route::resource('profile', 'ProfileController')->except(['destroy']);
        
        // Account
        Route::prefix('account')->name('account.')->group(function(){
            Route::get('me', 'AccountController@me')->name('me');
            Route::post('list', 'AccountController@list')->name('list');
            Route::post('reset', 'AccountController@reset')->name('reset');
            Route::delete('destroy', 'AccountController@destroy')->name('destroy');
            Route::put('update/{user}', 'AccountController@update')->name('update');
        });
        Route::resource('account', 'AccountController')->except(['update', 'destroy']);
    });

});

// guard Teacher
Route::middleware(['auth:teacher'])->group(function () {
    Route::namespace('Teacher')->prefix('facilitator')->name('teacher.')->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');

        // school
        Route::prefix('school')->name('school.')->group(function(){
            Route::post('list', 'SchoolController@list')->name('list');
            Route::post('reset', 'SchoolController@reset')->name('reset');
            Route::delete('destroy', 'SchoolController@destroy')->name('destroy');
        });
        Route::resource('school', 'schoolController')->except(['destroy']);

        // lesson
        Route::prefix('lesson')->name('lesson.')->group(function(){
            Route::post('list', 'LessonController@list')->name('list');
            Route::post('reset', 'LessonController@reset')->name('reset');
            Route::delete('destroy', 'LessonController@destroy')->name('destroy');
        });
        Route::resource('lesson', 'LessonController')->except(['destroy']);

        // student
        Route::prefix('student')->name('student.')->group(function(){
            Route::get('upload', 'StudentController@upload')->name('upload');
            Route::post('import', 'StudentController@import')->name('import');
            Route::post('export', 'StudentController@export')->name('export');
            Route::get('preview/{student}','StudentController@preview')->name('preview');
            Route::get('download/{student}','StudentController@download')->name('download');
            Route::post('list', 'StudentController@list')->name('list');
            Route::post('reset', 'StudentController@reset')->name('reset');
            Route::delete('destroy', 'StudentController@destroy')->name('destroy');
        });
        Route::resource('student', 'StudentController')->except(['destroy']);

        // new student
        Route::prefix('newStudent')->name('newStudent.')->group(function(){
            Route::get('/import', 'NewStudentController@import')->name('import');
            Route::post('list', 'NewStudentController@list')->name('list');
            Route::post('reset', 'NewStudentController@reset')->name('reset');
            Route::delete('destroy', 'NewStudentController@destroy')->name('destroy');
            Route::delete('approve', 'NewStudentController@approve')->name('approve');
            Route::delete('reject', 'NewStudentController@reject')->name('reject');
        });
        Route::resource('newStudent', 'NewStudentController')->except(['destroy']);

        // Update
        Route::prefix('update')->name('update.')->group(function () {
            Route::post('list', 'UpdateController@list')->name('list');
            Route::get('/', 'UpdateController@index')->name('index');
            Route::prefix('status')->name('status.')->group(function () {
                Route::get('/', 'UpdateController@status')->name('index');
            });
        });

        // Account
        Route::prefix('account')->name('account.')->group(function(){
            Route::get('me', 'AccountController@me')->name('me');
            Route::post('list', 'AccountController@list')->name('list');
            Route::post('reset', 'AccountController@reset')->name('reset');
            Route::delete('destroy', 'AccountController@destroy')->name('destroy');
            Route::put('update/{user}', 'AccountController@update')->name('update');
        });
        Route::resource('account', 'AccountController')->except(['update', 'destroy']);
    });

});

// guard Partner
Route::middleware(['auth:partner'])->group(function () {
    Route::namespace('Partner')->prefix('partner')->name('partner.')->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');
        
        // student
        Route::prefix('student')->name('student.')->group(function(){
            Route::get('preview/{student}','StudentController@preview')->name('preview');
            Route::get('download/{student}','StudentController@download')->name('download');
            Route::post('list', 'StudentController@list')->name('list');
            Route::post('reset', 'StudentController@reset')->name('reset');
            Route::delete('destroy', 'StudentController@destroy')->name('destroy');
        });
        Route::resource('student', 'StudentController')->except(['destroy']);

        // SDM request
        Route::prefix('request')->name('request.')->group(function(){
            Route::post('list', 'RequestController@list')->name('list');
            Route::delete('destroy', 'RequestController@destroy')->name('destroy');
        });
        Route::resource('request', 'RequestController')->except(['destroy']);
        
        // Account
        Route::prefix('account')->name('account.')->group(function(){
            Route::get('me', 'AccountController@me')->name('me');
            Route::post('list', 'AccountController@list')->name('list');
            Route::post('reset', 'AccountController@reset')->name('reset');
            Route::delete('destroy', 'AccountController@destroy')->name('destroy');
            Route::put('update/{user}', 'AccountController@update')->name('update');
        });
        Route::resource('account', 'AccountController')->except(['update', 'destroy']);
    });

});

// guard Admin
Route::middleware(['auth:admin'])->group(function () {
    Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::post('/', 'HomeController@index')->name('filterChart');

        // teacher
        Route::resource('teacher', 'TeacherController')->except(['destroy']);
        Route::prefix('teacher')->name('teacher.')->group(function(){
            Route::post('list', 'TeacherController@list')->name('list');
            Route::post('reset', 'TeacherController@reset')->name('reset');
            Route::delete('destroy', 'TeacherController@destroy')->name('destroy');
        });

        // school
        Route::prefix('school')->name('school.')->group(function(){
            Route::post('list', 'SchoolController@list')->name('list');
            Route::post('reset', 'SchoolController@reset')->name('reset');
            Route::delete('destroy', 'SchoolController@destroy')->name('destroy');
        });
        Route::resource('school', 'SchoolController')->except(['destroy']);

        // student
        Route::prefix('student')->name('student.')->group(function(){
            Route::get('import', 'StudentController@import')->name('import');
            Route::get('preview/{student}','StudentController@preview')->name('preview');
            Route::get('download/{student}','StudentController@download')->name('download');
            Route::post('list', 'StudentController@list')->name('list');
            Route::post('listLogSkill/{student}', 'StudentController@listLogSkill')->name('listLogSkill');
            Route::post('reset', 'StudentController@reset')->name('reset');
            Route::delete('destroy', 'StudentController@destroy')->name('destroy');
        });
        Route::resource('student', 'StudentController')->except(['destroy']);

        // partner
        Route::prefix('partner')->name('partner.')->group(function(){
            Route::post('list', 'PartnerController@list')->name('list');
            Route::post('reset', 'PartnerController@reset')->name('reset');
            Route::delete('destroy', 'PartnerController@destroy')->name('destroy');
        });
        Route::resource('partner', 'PartnerController')->except(['destroy']);

        // psychotest
        Route::prefix('psychotest-result')->name('psychotest-result.')->group(function(){
            Route::post('mbti_list', 'PsychologicalTestResultController@mbti_list')->name('mbti_list');
            Route::get('{psychologicalTest}/attempt/{psychologicalTestAttempt}/result', 'PsychologicalTestResultController@mbti_show')->name('mbti_show');

            Route::post('disc_list', 'PsychologicalTestResultController@disc_list')->name('disc_list');
            Route::get('{user}/result', 'PsychologicalTestResultController@disc_show')->name('disc_show');
        });
        Route::resource('psychotest-result', 'PsychologicalTestResultController')->except(['list', 'show']);

        // training
        Route::prefix('training')->name('training.')->group(function(){
            Route::post('list', 'TrainingController@list')->name('list');
            Route::post('reset', 'TrainingController@reset')->name('reset');
            Route::delete('destroy', 'TrainingController@destroy')->name('destroy');
        });
        Route::resource('training', 'TrainingController')->except(['destroy']);

        // Update
        Route::prefix('update')->name('update.')->group(function () {
            Route::post('list', 'UpdateController@list')->name('list');
            Route::get('/', 'UpdateController@index')->name('index');
            Route::prefix('status')->name('status.')->group(function () {
                Route::get('/', 'UpdateController@status')->name('index');
                Route::post('status/store', 'UpdateController@statusStore')->name('store');
            });
        });

        // Account
        Route::prefix('account')->name('account.')->group(function(){
            Route::get('me', 'AccountController@me')->name('me');
            Route::post('list', 'AccountController@list')->name('list');
            Route::post('reset', 'AccountController@reset')->name('reset');
            Route::delete('destroy', 'AccountController@destroy')->name('destroy');
            Route::put('update/{user}', 'AccountController@update')->name('update');
        });
        Route::resource('account', 'AccountController')->except(['update', 'destroy']);
    });
});