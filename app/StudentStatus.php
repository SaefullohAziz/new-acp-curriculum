<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Datakrama\Eloquid\Traits\Uuids;

class StudentStatus extends Pivot
{
	use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id', 'status_id'
    ];
    
	public function status() {
		return $this->hasOne('App\Status');
	}

	public function student() {
		return $this->hasOne('App\Student');
	}
}
