<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class JobDesk extends Model
{
    use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'slug', 'color', 'description'
    ];

    public function partnerJobRequirements() {
        return $this->hasMany('App\PartnerJobRequirement');
    }
}
