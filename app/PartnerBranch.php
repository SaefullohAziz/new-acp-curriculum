<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class PartnerBranch extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id', 'partner_id', 'name', 'address'
    ];

    public function partnerRequirement(){
    	return $this->hasMany('App\PartnerRequirement');
    }

    public function partner() {
        return $this->belongsTo('App\Partner');
    }

    public function city() {
        return $this->belongsTo('App\City');
    }
}
