<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Teacher extends Model
{
	use Uuids;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id', 'nip', 'name', 'gender', 'phone_number', 'email', 'date_of_birth', 'position', 'address'
    ];

    public function school() {
    	return $this->hasOne('App\School', 'id', 'school_id');
    }

    public function teacherTraining() {
    	return $this->hasMany('App\TeacherTraining');
    }

    public function trainingBatch() {
    	return $this->belongsToMany('App\TrainingBatch', 'teacher_trainings')->using('App\TeacherTraining')->withTimestamps()
    	->withPivot('created_at');
    }

    public function trainings() {
    	return $this->trainingBatch()->training();
    }

    public function user() {
    	return $this->belongsToMany('App\User', 'tracher_accounts')->using('App\TeacherAccount')->withTimestamps()
    	->withPivot('created_at');
    }

    public function pic() {
        return $this->hasOne('App\SchoolPic');
    }

    public function lessonClasses() {
        return $this->hasMany('App\LessonClass');
    }

    /**
     * Main query for listing
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public static function get(Request $request)
    {
        return DB::table('teachers')
            ->leftjoin('schools', 'teachers.school_id', '=', 'schools.id')
            ->leftJoin('school_pics', 'teachers.id', '=', 'school_pics.teacher_id')
            ->leftJoin('teacher_accounts', 'teachers.id', '=', 'teacher_accounts.teacher_id')
            ->leftJoin('users', 'teacher_accounts.user_id', '=', 'users.id')
            ->leftJoin('cities', 'schools.city_id', '=', 'cities.id')
            ->leftJoin('school_statuses', 'schools.id', '=', 'school_statuses.school_id')
            ->leftJoin('statuses', 'school_statuses.status_id', '=', 'statuses.id')
            ->when( ! empty($request->school), function ($query) use ($request) {
                $query->where('schools.id', $request->school);
            });
    }

    /**
     * Show student list for datatable
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public static function list(Request $request)
    {
        return self::get($request)->select('teachers.*', 'schools.name AS school', 'cities.name AS city', 'school_pics.teacher_id AS pic', 'users.username AS username');
    }

    /**
     * Get the avatar.
     *
     * @param  string  $value
     * @return string
     */
    public function getAvatarAttribute()
    {
        if ($this->attributes['photo'] == 'default.png') {
            return '/img/avatar/default.png';
        }
        return '/storage/teacher/photo/'.$this->attributes['photo'];
    }
}
