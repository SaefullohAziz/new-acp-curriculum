<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class LessonClass extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'generation_id', 'teacher_id', 'start_time', 'end_time', 'max_reported_at', 'max_absend_at'
    ];

    public function teacher() {
    	return $this->belongsTo('App\Teacher');
    }

    public function generation() {
    	return $this->belongsTo('App\Generation');
    }

    public function materies() {
        return $this->hasMany('App\LessonClassMatery');
    }

    public function lesson() {
        return $this->hasManyThrough('App\Lesson', 'App\LessonClassMatery', 'lesson_class_id', 'id', 'id', 'lesson_id');
    }

    public function studentLessonsClass() {
        return $this->hasMany('App\StudentLessonClass');
    }

    public function studentLessons() {
        return $this->hasManyThrough('App\StudentLesson', 'App\StudentLessonClass', 'lesson_class_id', 'student_lesson_class_id', 'id', 'id');
    }
}
