<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Student extends Model
{
	use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'school_origin', 'phone_number', 'study_status', 'registered_status', 'generation_id', 'place_of_birth', 'date_of_birth', 'gender', 'identition_number', 'city_id', 'address', 'weight', 'height'
    ];
    
    public function generation() {
    	return $this->belongsTo('App\Generation');
    }

    public function school() {
    	return $this->hasOneThrough('App\School', 'App\Generation', 'id', 'id', 'generation_id', 'school_id');
    }

    public function studentStatuses() {
        return $this->hasMany('App\StudentStatus');
    }

    public function statuses() {
        return $this->belongsToMany('App\Status', 'student_statuses')->using('App\StudentStatus')->withTimestamps()->withPivot('created_at');
    }

    public function status() {
    	return $this->statuses()->newest();
    }

    public function statusLatest() {
        return $this->hasOneThrough('App\Status', 'App\StudentStatus', 'student_id', 'id', 'id', 'status_id')->latest();
    }

    public function level() {
    	return $this->status()->level();
    }

    public function user() {
    	return $this->hasOneThrough('App\User', 'App\StudentAccount', 'student_id', 'id', 'id', 'user_id');
    }

    public function city() {
        return $this->belongsTo('App\City');
    }

    public function documents() {
    	return $this->hasMany('App\StudentDocument');
    }

    public function lessonAchievements() {
        return $this->hasMany('App\StudentLessonAchievement');
    }

    // public function achievements() {
    //     return $this->hasManyThrough('App\LessonAchievement', 'App\StudentLessonAchievement', 'student_id', 'id', 'id', 'lesson_achievement_id');
    // }

    public function lesson() {
        return $this->hasMany('App\StudentLesson');
    }
    
    public function alocatedStudent() {
        return $this->hasMany('App\AlocatedStudent');
    }

    // end of relation function

    // attribute function
    public function getProfilePercentageAttribute() {
        $column = collect($this->getAttributes())->except(['id', 'created_at', 'updated_at', 'pivot']);
        return substr(($column->filter(function ($item) { return $item; })->count() / $column->count())*100, 0 , 4 );
    }

}
