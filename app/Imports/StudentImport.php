<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentImport implements ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function model(array $data)
    {
        $data['phone_number'] = (int)$data['phone_number'];
        // cek unique phone number, id, and email
        $validate_student = Student::where('phone_number', $data['phone_number'])->orWhere('identition_number', $data['identition_number'])->first();
        $validate_user = User::where('email', $data['email'])->first();

        if (empty($validate_student) && empty($validate_user)) {
        	$status = Status::where('intention', 'student')->where('name', 'Selection')->first();
            $data['phone_number'] = (int)$data['phone_number'];
            $data_merge = [
                'name' => strtoupper($data['name']),
                'generation_id' => auth()->user()->teacher->school->generations()->latest()->first()->id,
                'registered_status' => 'approved',
                'city_id' => !empty($data['city']) ? City::where('name', 'LIKE', '%' . $data['city'] . '%')->first()->id : null,
                'date_of_birth' => !empty($data['date_of_birth']) ? date('Y-m-d', ($data['date_of_birth'] - 25569) * 86400) : null,
            ];
            $insert = array_merge($data, $data_merge);

            // insert to some table
            $student = '';
            $user = '';
            $student = new Student($insert);
            // dd($student->id);
            $student->studentStatuses()->Create(['status_id' => $status->id]);
            $user = User::create([
                'username' => $student->phone_number,
                'name' => $student->name,
                'email' => $data['email'],
                'password' => Hash::make('Indonesia2017!'),
            ]);
            $user->userRole()->firstOrCreate(['role_id' => Role::where('name', 'siswa')->first()->id]);
            $user->studentAccount()->firstOrCreate(['student_id' => $student->id]);
        }
    }
}
