<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Partner extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'phone_number', 'address', 'email', 'slug', 'color'
    ];
    
    public function userAccount() {
        return $this->hasOne('App\PartnerAccount');
    }

    public function user() {
        return $this->hasOneThrough('App\User', 'App\PartnerAccount', 'partner_id', 'id', 'id', 'user_id');
    }

    public function partnerBranches() {
        return $this->hasMany('App\PartnerBranch');
    }
}
