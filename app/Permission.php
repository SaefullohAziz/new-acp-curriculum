<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Permission extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'table', 'action'
    ];

	public function rolePermission() {
		return $this->hasOne('App\RolePermission');
	}

	public function roles() {
		return $this->belongsToMany('App\Role', 'role_permissions')->using('App\RolePermission')->withTimeStamps()->withPivot('created_at');
	}

	public function status_permission() {
		return $this->hasOne('App\StatusPermisson');
	}

	public function statuses() {
		return $this->belongsToMany('App\Status', 'status_permissions')->using('App\StatusPermisson')->withTimeStamps()->withPivot('created_at');
	}
}
