<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class StudentLesson extends Model
{
	use Uuids;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lesson_duration_id', 'student_lesson_class_id', 'student_id', 'teacher_id', 'score', 'photo'
    ];

    public function duration() {
        return $this->belongsTo('App\LessonDuration', 'lesson_duration_id');
    }

    public function cLass() {
        return $this->hasOneThrough('App\LessonCLass', 'App\StudentLessonClass', 'id', 'id', 'student_lesson_class_id', 'lesson_class_id');
    }

    public function lesson() {
        return $this->hasOneThrough('App\Lesson', 'App\LessonDuration', 'id', 'id', 'lesson_duration_id', 'lesson_id');
    }

    public function student() {
        return $this->belongSto('App\Student');
    }

    public function getNameAttribute() {
        return $this->lesson()->name;
    }
}
