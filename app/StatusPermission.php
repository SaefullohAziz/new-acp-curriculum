<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Datakrama\Eloquid\Traits\Uuids;

class StatusPermission extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status_id', 'permission_id'
    ];
    
    public function status() {
    	return $this->hasOne('App\Status');
    }

	public function permission() {
    	return $this->hasOne('App\Permission');
    }    
}
