<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class LessonType extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'description'
    ];

    public function lessonSkills() {
		return $this->hasMany('App\LessonSkill');
	}

	public function lessons() {
		return $this->hasMany('App\Lesson');
	}
}
