<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class PartnerJobRequirement extends Model
{
    use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'partner_requirement_id', 'job_desk_id'
    ];

    public function partnerRequirement(){
    	return $this->belongsTo('App\PartnerRequirement');
    }

    public function jobDesk(){
    	return $this->belongsTo('App\JobDesk');
    }
}
