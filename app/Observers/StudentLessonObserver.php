<?php

namespace App\Observers;

use App\StudentLesson;
use App\StudentLessonAchievement;

class StudentLessonObserver
{
    /**
     * Handle the student lesson "created" event.
     *
     * @param  \App\StudentLesson  $studentLesson
     * @return void
     */
    public function created(StudentLesson $studentLesson)
    {
        $action = 'Create';
        $achievements = $studentLesson->lesson->achievements;
        foreach ($achievements as $achievement) {
            $rule = $achievement->rule;
            if ( $rule->type == 'spesific_score' ) {
                $result = $this->compare($studentLesson->score, $rule);
                if ($result) {
                    StudentLessonAchievement::firstOrCreate([
                        'student_id' => auth()->user()->student->id,
                        'lesson_achievement_rule_id' => $rule->id
                    ]);
                }

            } elseif( $rule->type == 'flight_hours' ) {
                $studentFlightHours = StudentLesson::with('duration')->whereHas('duration', function($query) use ($studentLesson) {
                    $query->where('lesson_id', $studentLesson->lesson->id);
                })->get();
                if ( $rule->unit_score == $studentLesson->duration->duration_unit) {
                    $result = $this->compare($studentFlightHours->sum('duration.duration'), $rule);
                }

                if ($result) {
                    StudentLessonAchievement::firstOrCreate([
                        'student_id' => auth()->user()->student->id,
                        'lesson_achievement_rule_id' => $rule->id
                    ]);
                }
            }
        }

        saveLog($studentLesson, $action);
    }

    /**
     * Handle the student lesson "updated" event.
     *
     * @param  \App\StudentLesson  $studentLesson
     * @return void
     */
    public function updated(StudentLesson $studentLesson)
    {
        //
    }

    /**
     * Handle the student lesson "deleted" event.
     *
     * @param  \App\StudentLesson  $studentLesson
     * @return void
     */
    public function deleted(StudentLesson $studentLesson)
    {
        //
    }

    /**
     * Handle the student lesson "restored" event.
     *
     * @param  \App\StudentLesson  $studentLesson
     * @return void
     */
    public function restored(StudentLesson $studentLesson)
    {
        //
    }

    /**
     * Handle the student lesson "force deleted" event.
     *
     * @param  \App\StudentLesson  $studentLesson
     * @return void
     */
    public function forceDeleted(StudentLesson $studentLesson)
    {
        //
    }

    public function compare($data, $rule) {
        switch ($rule->operator) {
            case '>':
                if ($data > $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<':
                if ($data < $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<=':
                if ($data <= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '>=':
                if ($data >= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            default:
            return false;
                // session()->flash('alert-danger', 'Some achievement rule wrong!');
            break;
        }

        return false;
    }
}
