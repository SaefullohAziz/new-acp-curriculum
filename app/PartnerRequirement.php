<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class PartnerRequirement extends Model
{
    use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'partner_branch_id', 'quota', 'month', 'year', 'partner_branch_id'
    ];

    public function partnerBranch(){
    	return $this->belongsTo('App\PartnerBranch');
    }

    public function partnerDocRequirements(){
        return $this->hasMany('App\PartnerDocumentRequirement');
    }
    
    public function partnerJobRequirement() {
        return $this->hasMany('App\PartnerJobRequirement');
    }
}
