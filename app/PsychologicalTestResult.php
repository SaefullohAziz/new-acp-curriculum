<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class PsychologicalTestResult extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id', 'name', 'type', 'sub_type', 'result'
    ];
    
    public function students() {
    	return $this->belongsTo('App\Student');
    }
}
