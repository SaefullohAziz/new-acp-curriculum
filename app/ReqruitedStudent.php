<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class ReqruitedStudent extends Model
{
    use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'student_id', 'partner_id', 'partner_branches_id'
    ];
    
}
