<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Role extends Model
{
	use Uuids;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function rolePermission() {
    	return $this->hasOne('App\RolePermission');
    }

    public function permissions() {
    	return $this->hasManyThrough('App\Permission', 'App\RolePermission', 'role_id', 'id', 'id', 'permission_id');
    }

    public function users() {
    	return $this->hasManyThrough('App\User', 'App\UserRole', 'role_id', 'id', 'id', 'user_id');
    }
}
