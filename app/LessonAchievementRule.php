<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class LessonAchievementRule extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'lesson_achievement_id', 'type', 'operator', 'score', 'unit_score', 'implemented_at'
    ];

    public function achievement() {
        return $this->belongsTo('App\LessonAchievement');
    }
}
