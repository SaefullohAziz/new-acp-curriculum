<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class StudentDocument extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id', 'document_id', 'photo'
    ];
    
    public function student() {
    	$this->hasOne('App\Student');
    }
}
