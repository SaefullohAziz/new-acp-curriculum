<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RequiredDocument;
use App\StudentStatus;
use App\Student;
use App\Status;
use DataTables;

class NewStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => __('Student'),
            'columns' => [ __('Tanggal Submit'), __('Nama'), __('Nomor Telpon'), __('E-Mail'), __('Alamat'), __('Asal Sekolah'), __('Progres Dokumen') ]
        ];
        return view('facilitator.new_student.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $doc = RequiredDocument::count();
            $student = Student::with(['user', 'generation', 'statusLatest'])->withCount('documents')
                        ->whereHas('generation', function($query){
                            $query->where('school_id', Auth()->user()->teacher->school_id);
                        })
                        ->where('registered_status', 'waiting')->get();
            return DataTables::of($student)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->editColumn('created_at', function ($data) {
                    return (date('d-m-Y H:i:s', strtotime($data->created_at)));
                })
                ->addColumn('document_progress', function($data) use ($doc) {
                    $percentage = substr(($data->documents_count/$doc)*100, 0, 4);
                    return '<div class="progress" data-height="20" data-toggle="tooltip" title="" data-original-title="' . $percentage . '%" style="height: 20px;">
                                <div class="progress-bar bg-warning text-dark" data-width="' . $percentage . '" style="width: ' . $percentage . '%;">'. $percentage .'%</div>
                            </div>';
                })
                ->addColumn('email', function($data) {
                    return $data->user->email ;
                })
                ->addColumn('action', function ($data) {
                    return '<a class="btn btn-icon btn-sm btn-info" href="'.route('teacher.newStudent.show', $data->id).'" title="'.("See detail").'"><i class="fa fa-eye"></i></a> <a class="btn btn-icon btn-sm btn-warning" href="'.route('teacher.student.edit', $data->id).'" title="'.("Edit").'"><i class="fa fa-edit"></i></a>';
                })
                ->rawColumns(['DT_RowIndex', 'document_progress', 'email', 'action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $view = [
            'title' => __('Detail Student'),
            'breadcrumbs' => [
                route('teacher.new_student.index') => __('Peserta Baru'),
                null => __('Detail')
            ],
            'data' => $student
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\User  $user
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request)
    {
        $id = [];
        foreach ($request->selectedData as $data) {
            $id[] = $data;
        }
        Student::whereIn('id', $id)->update(['registered_status' => 'approved']);
        foreach ($id as $student_id) {
            StudentStatus::create(['student_id' => $student_id, 'status_id' => Status::where('intention', 'student')->where('name', 'Selection')->first()->id]);
        }
        return response()->json(['status' => true, 'message' => __($this->updatedMessage)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\User  $user
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request)
    {
        $id = [];
        foreach ($request->selectedData as $data) {
            $id[] = $data;
        }
        Student::whereIn('id', $id)->update(['registered_status' => 'rejected']);
        foreach ($id as $student_id) {
            StudentStatus::create(['student_id' => $student_id, 'status_id' => Status::where('name', 'Gugur')->first()->id]);
        }
        return response()->json(['status' => true, 'message' => __($this->updatedMessage)]);
    }
}
