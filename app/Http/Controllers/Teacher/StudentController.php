<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Imports\StudentImport;
use App\Exports\StudentsExport;
use Illuminate\Http\Request;
use App\RequiredDocument;
use App\StudentAccount;
use App\StudentStatus;
use App\UserROle;
use App\Province;
use App\Student;
use App\Lesson;
use DataTables;
use App\Status;
use App\User;
use App\Role;
use App\City;
use Excel;
use PDF;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => __('Student'),
            'columns' => [ __('Name'), __('Status'), __('Generation'), __('Phone Number'), __('Place of Birth'), __('Date of Birth'), __('Document Progress') ]
        ];
        return view('facilitator.student.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $doc = RequiredDocument::count();
            $student = Student::with(['user', 'generation'])->withCount('documents')
                        ->whereHas('generation', function($query){
                            $query->where('school_id', Auth()->user()->teacher()->first()->school_id);
                        })->get();
            return DataTables::of($student)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->addColumn('status', function($data) {
                    return $data->statuses()->latest('pivot_created_at')->count()
                                ? $data->statuses()->latest('pivot_created_at')->first()->name
                                : '-' ;
                })
                ->addColumn('generation', function($data) {
                    return $data->generation
                                ? '<a href="' . route('teacher.school.show', $data->school->id) . '" target="_blank" title="School detail">Angkatan '. $data->generation->number .' ('. $data->school->name .')</a>'
                                : '-' ;
                })
                ->addColumn('phone_number', function($data) {
                    return $data->phone_number
                                ? '(+62) '.$data->phone_number
                                : '-' ;
                })
                ->addColumn('place_of_birth', function($data) {
                    return $data->place_of_birth
                                ? $data->place_of_birth
                                : '-' ;
                })
                ->addColumn('document_progress', function($data) use ($doc) {
                    $percentage = substr(($data->documents_count/$doc)*100, 0, 4);
                    return '<div class="progress" data-height="20" data-toggle="tooltip" title="" data-original-title="' . $percentage . '%" style="height: 20px;">
                                <div class="progress-bar bg-warning text-dark" data-width="' . $percentage . '" style="width: ' . $percentage . '%;">'. $percentage .'%</div>
                            </div>';
                })
                ->addColumn('action', function ($data) {
                    return '<a class="btn btn-icon btn-sm btn-info" href="'.route('teacher.student.show', $data->id).'" title="'.__("See detail").'"><i class="fa fa-eye"></i></a> <a class="btn btn-icon btn-sm btn-warning" href="'.route('teacher.student.edit', $data->id).'" title="'.__("Edit").'"><i class="fa fa-edit"></i></a>';
                })
                ->rawColumns(['status', 'DT_RowIndex', 'generation', 'document_progress', 'achievement', 'action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => __('School Detail'),
            'provinces' => Province::pluck('name', 'id')->toArray(),
            'cities' => City::pluck('name', 'id')->toArray(),
            'documents' => RequiredDocument::all(),
        ];
        return view('facilitator.student.create', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upload()
    {
        $view = [
            'title' => __('Import Student'),
        ];

        return view('facilitator.student.import', $view);
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
    
        // menangkap file excel
        $file = $request->file('file');
    
        // membuat nama file unik
        $nama_file = $file->getClientOriginalName();
    
        // upload ke folder file_import di dalam folder public
        $file->move('file_import', $nama_file);
    
        // get data

        $datas = Excel::toArray(new StudentImport, public_path('/file_import/'.$nama_file))[0];

        // hitung baris
        $jumlah = count($datas);

        // pengecekan
        if ($jumlah > 0) {

            $i = 0;
            $lolos = 0;
            $gagal = 0;
            $data_merge = [];

            // pengulangan checking data
            foreach ($datas as $data) {
                $data['phone_number'] = (int)$data['phone_number'];
                // cek unique phone number, id, and email
                $validate_student = Student::where('phone_number', $data['phone_number'])->orWhere('identition_number', $data['identition_number'])->first();
                $validate_user = User::where('email', $data['email'])->first();

                if (!empty($validate_student)) {
                    $phone_number = Student::where('phone_number', $data['phone_number'])->first();
                    $identition_number = Student::where('identition_number', $data['identition_number'])->first();
                    if (!empty($phone_number)) {
                        $this->alert[] = $data['phone_number'];
                        $gagal++;
                    }
                    if (!empty($identition_number)) {
                        $this->alert[] = $data['identition_number'];
                        $gagal++;
                    } //end of switch
                }
                if (!empty($validate_user)) {
                    $this->alert[] = $data['email'];
                        $gagal++;
                }
                $i ++;
            } //end of foreach

            // cek kecacatan data untuk insert
            if ($gagal == 0) {
                $status = Status::where('intention', 'student')->where('name', 'Selection')->first();
                foreach ($datas as $data) {
                    $data['phone_number'] = (int)$data['phone_number'];
                    $data_merge = [
                        'name' => strtoupper($data['name']),
                        'generation_id' => auth()->user()->teacher->school->generations()->latest()->first()->id,
                        'registered_status' => 'approved',
                        'city_id' => !empty($data['city']) ? City::where('name', 'LIKE', '%' . $data['city'] . '%')->first()->id : null,
                        'date_of_birth' => !empty($data['date_of_birth']) ? date('Y-m-d', ($data['date_of_birth'] - 25569) * 86400) : null,
                    ];
                    $insert = array_merge($data, $data_merge);

                    // insert to some table
                    try {
                        $student = '';
                        $user = '';
                        $student = Student::create($insert);
                        // dd($student->id);
                        $student->studentStatuses()->Create(['status_id' => $status->id]);
                        $user = User::create([
                            'username' => $student->phone_number,
                            'name' => $student->name,
                            'email' => $data['email'],
                            'password' => Hash::make('Indonesia2017!'),
                        ]);
                        $user->userRole()->firstOrCreate(['role_id' => Role::where('name', 'siswa')->first()->id]);
                        $user->studentAccount()->firstOrCreate(['student_id' => $student->id]);
                    } catch (\Exception $e) {
                        if (!empty($student)) {
                            StudentAccount::where('student_id', $student->id)->delete();
                            StudentStatus::where('student_id', $student->id)->delete();
                            Student::where('id', $student->id)->delete();
                        }
                        if (!empty($user)) {
                            UserRole::where('user_id', $user->id)->delete();
                            User::where('id', $user->id)->forceDelete();
                        }
                        return back()->with('alert-danger', $e->getMessage());
                    }
                }
            }else
            {
                if ($this->alert != '') {
                    $data = implode(" , ",$this->alert);
                    return back()->with('alert-danger', $data.' is already exists. Please check again!');
                }
                else{
                    Excel::import(new StudentImport, public_path('/file_import/'.$nama_file));
                    return back()->with('alert-success', 'All datas has been imported!');
                }
            }
        }
        return back()->with('alert-success', 'All datas has been imported!');
    }

    /**
     * Export school listing as Excel
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function export(Request $request)
    {
        return (new StudentsExport($request))->download('student-'.date('d-m-Y-h-m-s').'.xlsx');
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview(Student $student)
    {
        return $this->pdfVariable($student, 'preview');
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download(Student $student)
    {
        return $this->pdfVariable($student, 'download');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $raport = $this->raport($student);

        $view = [
            'title' => __('Student Detail'),
            'provinces' => Province::pluck('name', 'id')->toArray(),
            'cities' => City::pluck('name', 'id')->toArray(),
            'documents' => RequiredDocument::all(),
            'data' => $student,
            'raport' => $raport
        ];
        return view('facilitator.student.show', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the PDF Variable page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdfVariable($student = NULL, $type = NULL)
    {
        $raport = $this->raport($student);
        $view = [
            'title' => $student->name,
            'data' => $student,
            'raport' => $raport
        ];

        $pdf = PDF::loadView('pdf.raport', $view);

        $pdf->setPaper('A4', 'potrait');
        if ($type == 'download') {
            return $pdf->download(str_replace(' ', '-', strtolower($student->name)).'-raport-'.date('d-m-Y-h-m-s').'.pdf');
        }
        elseif ($type == 'preview') {
            return $pdf->stream();
        }
    }

    /**
     * Display the Raport page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function raport(Student $student)
    {
        $students = Student::where('id', $student->id)->first();
        $lessons = Lesson::where('name', '!=', 'Melipat Kertas')->get();
        $raport = [];
        if ($students) {
            foreach ($lessons as $lesson) {
                $students_lesson = $students->lesson()->whereHas('duration', function($query) use($lesson){
                    $query->where('lesson_id', $lesson->id);
                });
                
                $score = $students_lesson->get()->sortBy('score')->pluck('score')->toArray();
                if($students_lesson->get()){
                    $flight_hour = 0;
                    $kali = 0;
                    $count_average = [];
                    foreach($students_lesson->get() as $studentLesson){
                        $achievement = $studentLesson->lesson->achievements()->whereHas('rules', function($query){
                            $query->where('implemented_at', '<', now())->orderBy('implemented_at', 'desc');
                        })->first();
                        $rule = $achievement->rule;
                        $result = $this->compare($studentLesson->score, $rule);
                        if ($result) {
                            $flight_hour += $studentLesson->score;
                            $kali++;
                        }
                    }
                }

                $average = $students_lesson->count() > 0  ? $flight_hour / $kali : 0;
                $total = ($average > 0 ? number_format($average, 2) : $average) . ' ' . $lesson->unit_score;
                $flight_hours = (count($score) > 0 ? $lesson->name == 'Money Ban' ? $flight_hour : $kali : 0) . ' ' . ($lesson->name == 'Money Ban' ? 'Stack' : 'Kali');

                $kkm = $lesson->achievements()->latest()->first()->rule()->first()->score . ' ' . $lesson->achievements()->latest()->first()->rule()->first()->unit_score;
                $raport[] = ['lesson' => $lesson->name,  'kkm' => $kkm, 'flight_hours' => $flight_hours, 'average' => $total];
            }
        }
        return $raport;
    }

    public function compare($data, $rule) {
        switch ($rule->operator) {
            case '>':
                if ($data > $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<':
                if ($data < $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<=':
                if ($data <= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '>=':
                if ($data >= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            default:
            return false;

        break;
        }

        return false;
    }
}
