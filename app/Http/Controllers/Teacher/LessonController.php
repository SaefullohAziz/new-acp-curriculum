<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LessonClass;
use DataTables;

class LessonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => __('Lesson'),
            'columns' => [ __('Created At'), __('Name'), __('Generation'), __('Facilitator'), __('Start'), __('End') ]
        ];
        return view('facilitator.lesson.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        $lesson = LessonClass::whereHas('teacher', function($query){
                    $query->where('school_id', Auth()->user()->teacher()->first()->school_id);
                  })->get();
        if ($request->ajax()) {
            return DataTables::of($lesson)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->editColumn('created_at', function ($data) {
                    return (date('d-m-Y H:i:s', strtotime($data->created_at)));
                })
                ->addColumn('generation', function ($data) {
                    return $data->generation
                            ? $data->generation->number
                            : '-';
                })
                ->addColumn('facilitator', function ($data) {
                    return $data->teacher
                            ? $data->teacher->name
                            : '-';
                })
                ->addColumn('action', function ($data) {
                    return '<a class="btn btn-icon btn-sm btn-warning" href="'.route('teacher.lesson.show', $data->id).'" title="'.__("See detail").'"><i class="fa fa-eye"></i> See</a>';
                })
                ->rawColumns(['status', 'DT_RowIndex', 'generation', 'facilitator', 'action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => __('Create Lesson'),
        ];
        return view('facilitator.lesson.create', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LessonClass $lesson)
    {
        $view = [
            'title' => __('Detail Lesson Class'),
            'data' => $lesson,
        ];
        return view('facilitator.lesson.show', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
