<?php

namespace App\Http\Controllers;

use App\SkillAchiviewmentRule;
use Illuminate\Http\Request;

class SkillAchiviementRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SkillAchiviewmentRule  $skillAchiviewmentRule
     * @return \Illuminate\Http\Response
     */
    public function show(SkillAchiviewmentRule $skillAchiviewmentRule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SkillAchiviewmentRule  $skillAchiviewmentRule
     * @return \Illuminate\Http\Response
     */
    public function edit(SkillAchiviewmentRule $skillAchiviewmentRule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SkillAchiviewmentRule  $skillAchiviewmentRule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SkillAchiviewmentRule $skillAchiviewmentRule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SkillAchiviewmentRule  $skillAchiviewmentRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(SkillAchiviewmentRule $skillAchiviewmentRule)
    {
        //
    }
}
