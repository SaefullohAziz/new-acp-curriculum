<?php

namespace App\Http\Controllers;

use App\Province;
use App\City;
use App\School;
use App\SchoolStatus;
use App\Teacher;
use App\Department;
use App\Http\Requests\SchoolRequest;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    private $table;
    private $isoCertificates;
    private $references;

    /**
     * Create a new class instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'schools';
        $this->isoCertificates = ['Sudah' => 'Sudah', 'Dalam Proses (persiapan dokumen / pembentukan team audit internal / pendampingan)' => 'Dalam Proses (persiapan dokumen / pembentukan team audit internal / pendampingan)', 'Belum' => 'Belum'];
        $this->references = ['Sekolah Peserta / Sekolah Binaan', 'Dealer', 'Internet (Facebook Page/Web)', 'Lain-Lain'];
        $this->gender = ['Laki - laki', 'Perempuan'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => __('Register School'),
            'provinces' => Province::orderBy('name', 'asc')->pluck('name', 'name')->toArray(),
            'cities' => City::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
            'departments' => array_merge(Department::orderBy('name', 'asc')->pluck('name')->toArray(), [__('Other')]),
            'isoCertificates' => $this->isoCertificates,
            'references' => $this->references,
            'gender' => $this->gender,
        ];
        return view('school.register', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request)
    {
        $request->merge([
            'department' => implode(', ', $request->department),
            'reference' => implode(', ', $request->reference),
        ]);
        $school = School::create($request->all());
        $school->document = $this->uploadDocument($school, $request);
        $school->save();
        $pic = Teacher::create([
            'school_id' => $school->id,
            'name' => $request->pic_name,
            'gender' => $request->gender,
            'date_of_birth' => $request->date_of_birth,
            'position' => $request->pic_position,
            'phone_number' => $request->pic_phone_number,
            'email' => $request->pic_email,
        ]);
        $school->pic()->attach($pic->id);
        $status = SchoolStatus::byName('Daftar')->first();
        $school->statuses()->attach($status->id, [
            'created_by' => 'school',
        ]);
        $this->uploadPhoto($school, $request);
        // event(new SchoolRegistered($school));
        return redirect(url()->previous())->with('alert-success', __('Thank you! Your registration has been successful. Please check your email for the next step.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        //
    }
}
