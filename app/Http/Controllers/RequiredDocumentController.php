<?php

namespace App\Http\Controllers;

use App\RequiredDocument;
use Illuminate\Http\Request;

class RequiredDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequiredDocument  $requiredDocument
     * @return \Illuminate\Http\Response
     */
    public function show(RequiredDocument $requiredDocument)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequiredDocument  $requiredDocument
     * @return \Illuminate\Http\Response
     */
    public function edit(RequiredDocument $requiredDocument)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequiredDocument  $requiredDocument
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequiredDocument $requiredDocument)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequiredDocument  $requiredDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequiredDocument $requiredDocument)
    {
        //
    }
}
