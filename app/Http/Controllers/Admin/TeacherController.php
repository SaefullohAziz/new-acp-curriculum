<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TeacherRequest;
use App\Http\Controllers\Controller;
use Spatie\Image\Manipulations;
use Illuminate\Http\Request;
use Spatie\Image\Image;
use App\Teacher;
use App\School;
use App\Level;
use DataTables;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // if ( ! auth()->guard('admin')->user()->can('access ' . $this->table)) {
        //     return redirect()->route('admin.home')->with('alert-danger', __($this->noPermission));
        // }
        $view = [
            'title' => __('Fasilitator'),
            'breadcrumbs' => [
                route('admin.teacher.index') => __('Fasilitator'),
                null => 'Data'
            ],
            'schools' => School::pluck('name', 'id')->toArray(),
            'levels' =>Level::whereHas('statuses', function ($query) {
                            $query->where('intention', 'school');
                        })->orderBy('order', 'asc')->pluck('name', 'id')->toArray(),
            'columns' => ['Name', 'School', 'Phone Number', 'E-Mail', 'Position']
        ];
        return view('admin.teacher.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $teachers = Teacher::list($request);
            return DataTables::of($teachers)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->editColumn('created_at', function ($data) {
                    return (date('d-m-Y H:i:s', strtotime($data->created_at)));
                })
                ->editColumn('username', function($data) {
                    return $data->username
                                ? $data->username
                                : '-' ;
                })
                ->editColumn('name', function($data) {
                    return $data->pic
                                ? '<p class="badge badge-primary">'.$data->name.' (Pic)</p>'
                                : $data->name ;
                })
                ->editColumn('school', function($data) {
                    return $data->school
                                ? '<a href="' . route('admin.school.show', $data->school_id) . '" target="_blank" title="School detail">'. $data->school .'</a>'
                                : '-' ;
                })
                // ->addColumn('action', function ($data) {
                //     return '<a class="btn btn-sm btn-success" href="'.route('admin.teacher.show', $data->id).'" title="'.__("See detail").'"><i class="fa fa-eye"></i> '.__("See").'</a> <a class="btn btn-sm btn-warning" href="'.route('admin.teacher.edit', $data->id).'" title="'.__("Edit").'"><i class="fa fa-edit"></i> '.__("Edit").'</a>';
                // })
                ->rawColumns(['DT_RowIndex', 'name', 'school'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if ( ! auth()->guard('admin')->user()->can('create ' . $this->table)) {
        //     return redirect()->route('admin.teacher.index')->with('alert-danger', __($this->noPermission));
        // }
        // if (auth()->guard('admin')->user()->cant('adminCreate', Teacher::class)) {
        //     return redirect()->route('admin.teacher.index')->with('alert-danger', __($this->unauthorizedMessage));
        // }
        $view = [
            'title' => __('Create Teacher'),
            'breadcrumbs' => [
                route('admin.teacher.index') => __('Teacher'),
                null => __('Create')
            ],
            'schools' => School::pluck('name', 'id')->toArray(),
        ];
        return view('admin.teacher.create', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request)
    {
        // if ( ! auth()->guard('admin')->user()->can('create ' . $this->table)) {
        //     return redirect()->route('admin.teacher.index')->with('alert-danger', __($this->noPermission));
        // }
        // if (auth()->guard('admin')->user()->cant('adminCreate', Teacher::class)) {
        //     return redirect()->route('admin.teacher.index')->with('alert-danger', __($this->unauthorizedMessage));
        // }
        $request->merge([
            'date_of_birth' => date('Y-m-d', strtotime($request->date_of_birth)),
            'phone_number' => '+62'.$request->phone_number,
        ]);
        $teacher = Teacher::create($request->except(['terms', 'submit']));
        $teacher->photo = $this->uploadPhoto($teacher, $request);
        $teacher->save();
        return redirect(url()->previous())->with('alert-success', __($this->createdMessage));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        // if ( ! auth()->guard('admin')->user()->can('read ' . $this->table)) {
        //     return redirect()->route('admin.teacher.index')->with('alert-danger', __($this->noPermission));
        // }
        $view = [
            'title' => __('Teacher Detail'),
            'breadcrumbs' => [
                route('admin.teacher.index') => __('Teacher'),
                null => __('Detail')
            ],
            'schools' => School::pluck('name', 'id')->toArray(),
            'data' => $teacher
        ];
        return view('admin.teacher.show', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        // if ( ! auth()->guard('admin')->user()->can('update ' . $this->table)) {
        //     return redirect()->route('admin.teacher.index')->with('alert-danger', __($this->noPermission));
        // }
        $view = [
            'title' => __('Edit Teacher'),
            'breadcrumbs' => [
                route('admin.teacher.index') => __('Teacher'),
                null => __('Edit')
            ],
            'schools' => School::pluck('name', 'id')->toArray(),
            'data' => $teacher
        ];
        return view('admin.teacher.edit', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherRequest $request, Teacher $teacher)
    {
        // if ( ! auth()->guard('admin')->user()->can('update ' . $this->table)) {
        //     return redirect()->route('admin.teacher.index')->with('alert-danger', __($this->noPermission));
        // }
        
        $request->merge([
            'date_of_birth' => date('Y-m-d', strtotime($request->date_of_birth)),
        ]);
        $teacher->fill($request->except(['terms', 'submit']));
        $teacher->photo = $this->uploadPhoto($teacher, $request, $teacher->photo);
        $teacher->save();
        return redirect(url()->previous())->with('alert-success', __($this->updatedMessage));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Upload photo
     * 
     * @param  \App\Teacher  $teacher
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $oldFile
     * @return string
     */
    public function uploadPhoto($teacher, Request $request, $oldFile = 'default.png')
    {
        if ($request->hasFile('photo')) {
            Image::load($request->photo)
                ->fit(Manipulations::FIT_CROP, 150, 150)
                ->optimize()
                ->save();
            $filename = 'photo_'.date('d_m_Y_H_i_s_').md5(uniqid(rand(), true)).'.'.$request->photo->extension();
            $path = $request->photo->storeAs('public/teacher/photo/'.$teacher->id, $filename);
            return $teacher->id.'/'.$filename;
        }
        return $oldFile;
    }
}
