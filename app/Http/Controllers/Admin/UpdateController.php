<?php

namespace App\Http\Controllers\Admin;

use App\Generation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Student;
use App\StudentStatus;
use App\Status;
use App\School;
use App\Level;
use App\RequiredDocument;

class UpdateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->statuses = Status::where('intention', 'student')->orderBy('name', 'ASC');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => __('Update Data'),
        ];

        return view('admin.update.index', $view);
    }

    /**
     * Show the student status page.
     *
     * @return \Illuminate\Http\Response
     */
    public function status()
    {
        $view = [
            'title' => __('Status Update'),
            'statuses' => $this->statuses->get(),
            'moveStatus' => $this->statuses->pluck('name', 'id')->toArray(),
            'schools' => School::whereHas('generations')->pluck('name', 'name')->toArray(),
            'columns' => ['Last Update', 'Name', 'Status', 'Generation', 'School', 'Phone Number', 'Place of Birth', 'Date of Birth', 'Document Progress'],
        ];

        return view('admin.update.status.index', $view);
    }

    /**
     * Show the list of student status
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $doc = RequiredDocument::count();
            $level = Level::where('name', 'Academy')->whereHas('statuses', function ($query) {
                            $query->where('intention', 'student');
                        })->pluck('id')->toArray();
            $statuses = $this->statuses;
            $students = Student::with(['user', 'generation', 'statuses'])
                ->when( ! empty($request->school), function ($query) use ($request) {
                    $query->whereHas('generation', function ($subQuery) use ($request) {
                        $subQuery->where('school_id', $request->school)
                        ->when(! empty($request->generation), function ($subInQuery) use ($request){
                            $subInQuery->where('id', $request->generation);
                        });
                    });
                })->when(! empty($request->level_siswa), function($query) use($request){
                    $query->whereHas('statuses', function($subQuery) use($request){
                        $subQuery->where('level_id', $request->level_siswa);
                    });
                })->when(! empty($request->status_siswa), function($query) use($request){
                    $query->whereHas('statuses', function($subQuery) use($request){
                        $subQuery->where('statuses.id', $request->status_siswa);
                    });
                })->withCount('documents')->get();
            return DataTables::of($students)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->addColumn('last_update', function($data) {
                    return $data->statuses()->count()
                    ? $data->statuses()->latest()->first()->created_at
                    : '-';
                })
                ->addColumn('status', function($data) {
                    return $data->statuses()->latest('pivot_created_at')->count()
                    ? $data->statuses()->latest('pivot_created_at')->first()->name
                    : '-';
                })
                ->addColumn('generation', function($data) {
                    return $data->generation
                                ? 'Angkatan '. $data->generation->number
                                : '-' ;
                })
                ->addColumn('school', function($data) {
                    return $data->school
                                ? $data->school->name
                                : '-' ;
                })
                ->addColumn('phone_number', function($data) {
                    return $data->phone_number
                                ? '(+62) '.$data->phone_number
                                : '-' ;
                })
                ->addColumn('place_of_birth', function($data) {
                    return $data->place_of_birth
                                ? $data->place_of_birth
                                : '-' ;
                })
                ->addColumn('document_progress', function($data) use ($doc) {
                    $percentage = substr(($data->documents_count/$doc)*100, 0, 4);
                    return '<div class="progress" data-height="20" data-toggle="tooltip" title="" data-original-title="' . $percentage . '%" style="height: 20px;">
                                <div class="progress-bar bg-warning text-dark" data-width="' . $percentage . '" style="width: ' . $percentage . '%;">'. $percentage .'%</div>
                            </div>';
                })                
                ->rawColumns(['DT_RowIndex', 'generation', 'document_progress'])
                ->make(true);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function statusStore(Request $request)
    {
        for ($i=0; $i < count($request->selectedData); $i++) { 
            $student = Student::find($request->selectedData[$i]);    
            $student->studentStatuses()->create([
                'status_id' => $request->statusData
            ]);
        }

        return response()->json(['status' => true, 'message' => __('Berhasil')]);
    }
}