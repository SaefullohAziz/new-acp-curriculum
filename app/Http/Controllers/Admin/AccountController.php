<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Spatie\Image\Manipulations;
use Illuminate\Http\Request;
use Spatie\Image\Image;
use App\User;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        $view = [
            'title' => __('Account Detail'),
            'breadcrumbs' => [
                route('admin.account.index') => __('Account'),
                null => __('Detail')
            ],
            'data' => $user
        ];
        if ($request->is('admin/account/me/*')) {
            $addonView = [
                'subtitle' => __('Hi') . ', ' . $user->name . '!',
                'description' => __('Change information about yourself on this page.'),
            ];
            $view = array_merge($view, $addonView);
        }
        return view('admin.account.show', $view);
    }

    /**
     * Display the current user's resource.
     * 
     * @return @show()
     */
    public function me(Request $request)
    {
        return $this->show($request, auth()->guard('admin')->user());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if ($request->filled('password')) {
            $request->merge(['password' => Hash::make($request->password)]);
        } else {
            $request->merge(['password' => $user->password]);
        }
        if ($request->hasFile('photo')) {
            $request->merge(['avatar' => $this->uploadPhoto($request)]);
        }
        $user->fill($request->all());
        $user->save();
        return redirect(url()->previous())->with('alert-success', __($this->updatedMessage));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Upload photo
     * 
     * @param  \App\Teacher  $user
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $oldFile
     * @return string
     */
    public function uploadPhoto(Request $request)
    {
        Image::load($request->photo)
            ->fit(Manipulations::FIT_CROP, 150, 150)
            ->optimize()
            ->save();
        $filename = 'photo_'.date('d_m_Y_H_i_s_').md5(uniqid(rand(), true)).'.'.$request->photo->extension();
        $path = $request->photo->storeAs('public/admin/photo/', $filename);
        return $filename;
    }
}
