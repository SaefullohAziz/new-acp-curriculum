<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StudentRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\RequiredDocument;
use App\Generation;
use App\Province;
use App\StudentLesson;
use App\Student;
use App\Status;
use App\School;
use DataTables;
use App\Level;
use App\Lesson;
use App\User;
use App\City;
use Form;
use PDF;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => __('Students'),
            'status' => Status::first(),
            'levels' => Level::whereHas('statuses', function ($query) {
                            $query->where('intention', 'school');
                        })->orderBy('order', 'asc')->pluck('name', 'id')->toArray(),
            'schools' => School::pluck('name', 'id')->toArray(),
            'generations' => Generation::pluck('number', 'id')->toArray(),
            'level_siswa' => Level::whereHas('statuses', function ($query) {
                            $query->where('intention', 'student');
                        })->orderBy('order', 'asc')->pluck('name', 'id')->toArray(),
            'status_siswa' => Status::where('intention', 'student')->pluck('name', 'id')->toArray(),
            'columns' => ['Name', 'Generation', 'Status', 'Phone Number', 'Place of Birth', 'Date of Birth', 'Document Progress']
        ];
        return view('admin.student.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $doc = RequiredDocument::count();
            $level = Level::where('name', 'Academy')->whereHas('statuses', function ($query) {
                            $query->where('intention', 'student');
                        })->pluck('id')->toArray();
            $statuses = Status::all()->map->only('name', 'id')->pluck('name', 'id')->toArray();
            $students = Student::with(['user', 'generation', 'statuses'])
            ->when( ! empty($request->school), function ($query) use ($request) {
                    $query->whereHas('generation', function ($subQuery) use ($request) {
                        $subQuery->where('school_id', $request->school)
                        ->when(! empty($request->generation), function ($subInQuery) use ($request){
                            $subInQuery->where('id', $request->generation);
                        });
                    });
                })->when(! empty($request->level_siswa), function($query) use($request){
                    $query->whereHas('statuses', function($subQuery) use($request){
                        $subQuery->where('level_id', $request->level_siswa);
                    });
                })->when(! empty($request->status_siswa), function($query) use($request){
                    $query->whereHas('statuses', function($subQuery) use($request){
                        $subQuery->where('statuses.id', $request->status_siswa);
                    });
                })->withCount('documents')->get();
            return DataTables::of($students)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->editColumn('created_at', function ($data) {
                    return (date('d-m-Y H:i:s', strtotime($data->created_at)));
                })
                ->addColumn('generation', function($data) {
                    return $data->generation
                                ? '<a href="' . route('admin.school.show', $data->school->id) . '" target="_blank" class="btn btn-link" title="School detail">Angkatan '. $data->generation->number .' ('. $data->school->name .')</a>'
                                : '-' ;
                })
                ->addColumn('status', function($data) {
                    return $data->statuses()->latest('pivot_created_at')->count()
                                ? $data->statuses()->latest('pivot_created_at')->first()->name
                                : '-' ;
                })
                ->addColumn('phone_number', function($data) {
                    return $data->phone_number
                                ? '(+62) '.$data->phone_number
                                : '-' ;
                })
                ->addColumn('place_of_birth', function($data) {
                    return $data->place_of_birth
                                ? $data->place_of_birth
                                : '-' ;
                })
                ->addColumn('document_progress', function($data) use ($doc) {
                    $percentage = substr(($data->documents_count/$doc)*100, 0, 4);
                    return '<div class="progress" data-height="20" data-toggle="tooltip" title="" data-original-title="' . $percentage . '%" style="height: 20px;">
                                <div class="progress-bar bg-warning text-dark" data-width="' . $percentage . '" style="width: ' . $percentage . '%;">'. $percentage .'%</div>
                            </div>';
                })
                // ->addColumn('achievement', function($data) {
                //     if ($data->achievements->count()) {
                //         $text = '';
                //         $achievements = $data->achievements()->get()->toArray();
                //         foreach ($achievements as $achievement) {
                //             $text .= $achievement['name'] . (!next( $achievements ) ? '.' : ', ');
                //         }
                //         $text = '<div id="tab-scroll"><p>' . $text . '</p></div>';
                //     } else {
                //         $text = '-';
                //     }
                //     return $text;
                // })
                ->addColumn('action', function ($data) {
                    return '<a class="btn btn-icon btn-sm btn-info" href="'.route('admin.student.show', $data->id).'" title="'.__("See detail").'"><i class="fa fa-eye"></i></a> <a class="btn btn-icon btn-sm btn-warning" href="'.route('admin.student.edit', $data->id).'" title="'.__("Edit").'"><i class="fa fa-edit"></i></a>';
                })
                ->rawColumns(['DT_RowIndex', 'generation', 'document_progress', 'action'])
                ->make(true);
        }
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => __('Create Student'),
            'breadcrumbs' => [
                route('admin.student.index') => __('Student'),
                null => __('Create')
            ],
            'back' => route('admin.student.index'),
            'provinces' => Province::orderBy('name', 'asc')->pluck('name', 'name')->toArray(),
            'cities' => City::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
            'documents' => RequiredDocument::orderBy('order', 'asc')->get(),
        ];
        return view('admin.student.create', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $users = User::create([
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $students = Student::create($request->all());

        $studentStatuses = StudentStatus::create([
            'student_id' => $students->id,
            'status_id' => Status::where('name', 'Registered')->first()->id,
        ]);

        $studentAccount = StudentAccount::create([
            'student_id' => $students->id,
            'user_id' => $users->id,
        ]);
        
        return redirect(url()->previous())->with('alert-success', __($this->createdMessage));
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        $view = [
            'title' => __('Import Student'),
        ];

        return view('admin.student.import', $view);
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview(Student $student)
    {
        return $this->pdfVariable($student, 'preview');
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download(Student $student)
    {
        return $this->pdfVariable($student, 'download');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {  
        $raport = $this->raport($student);

        $view = [
            'title' => __('Detail Student'),
            'back' => route('admin.partner.index'),
            'breadcrumbs' => [
                route('admin.partner.index') => __('Partner'),
                null => __('Edit')
            ],
            'provinces' => Province::pluck('name', 'id')->toArray(),
            'documents' => RequiredDocument::all(),
            'cities' => City::pluck('name', 'id')->toArray(),
            'data' => $student,
            'raport' => $raport
        ];

        return view('admin.student.show', $view);
    }
    
    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function listLogSkill(Request $request, Student $student)
    {
        $student_lesson = StudentLesson::where('student_id', $student->id)->orderBy('created_at', 'DESC')->whereHas('duration')->limit(5)->get();
        if ($request->ajax()) {
            $student_lesson = StudentLesson::where('student_id', $student->id)->orderBy('created_at', 'DESC')->whereHas('duration')->get();
            
            return DataTables::of($student_lesson)
            ->addIndexColumn()
            ->editColumn('created_at', function ($data) {
                return (date('d-m-Y H:i:s', strtotime($data->created_at)));
            })
            ->addColumn('name', function($data) {
                return $data->lesson->name
                            ? $data->lesson->name
                            : '-' ;
            })
            ->addColumn('kkm', function($data) {
                return $data->lesson->achievements()->first()->rule->score
                            ? $data->lesson->achievements()->first()->rule->score . ' ' . $data->lesson->unit_score
                            : '-' ;
            })
            ->addColumn('score', function($data) {
                return $data->score
                            ? $data->score . ' ' . $data->lesson->unit_score
                            : '-' ;
            })
            ->rawColumns(['name', 'kkm', 'score'])
            ->make(true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        if ($request->filled('status_id')) {
            $student->studentStatuses()->create(['status_id' => $request->status_id]);
        }
        if ($request->ajax()) {
            return response()->json(['status' => true, 'message' => __($this->savedSettingMessage)]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the PDF Variable page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdfVariable($student = NULL, $type = NULL)
    {
        $raport = $this->raport($student);
        $view = [
            'title' => $student->name,
            'data' => $student,
            'raport' => $raport
        ];

        $pdf = PDF::loadView('pdf.raport', $view);

        $pdf->setPaper('A4', 'potrait');
        if ($type == 'download') {
            return $pdf->download(str_replace(' ', '-', strtolower($student->name)).'-raport-'.date('d-m-Y-h-m-s').'.pdf');
        }
        elseif ($type == 'preview') {
            return $pdf->stream();
        }
    }
    
    /**
     * Display the Raport page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function raport(Student $student)
    {
        $students = Student::where('id', $student->id)->first();
        $lessons = Lesson::where('name', '!=', 'Melipat Kertas')->get();
        $raport = [];
        if ($students) {
            foreach ($lessons as $lesson) {
                $students_lesson = $students->lesson()->whereHas('duration', function($query) use($lesson){
                    $query->where('lesson_id', $lesson->id);
                });
                
                $score = $students_lesson->get()->sortBy('score')->pluck('score')->toArray();
                if($students_lesson->get()){
                    $flight_hour = 0;
                    $kali = 0;
                    $count_average = [];
                    foreach($students_lesson->get() as $studentLesson){
                        $achievement = $studentLesson->lesson->achievements()->whereHas('rules', function($query){
                            $query->where('implemented_at', '<', now())->orderBy('implemented_at', 'desc');
                        })->first();
                        $rule = $achievement->rule;
                        $result = $this->compare($studentLesson->score, $rule);
                        if ($result) {
                            $flight_hour += $studentLesson->score;
                            $kali++;
                        }
                    }
                }

                $average = $students_lesson->count() > 0  ? $flight_hour / $kali : 0;
                $total = ($average > 0 ? number_format($average, 2) : $average) . ' ' . $lesson->unit_score;
                $flight_hours = (count($score) > 0 ? $lesson->name == 'Money Ban' ? $flight_hour : $kali : 0) . ' ' . ($lesson->name == 'Money Ban' ? 'Stack' : 'Kali');

                $kkm = $lesson->achievements()->latest()->first()->rule()->first()->score . ' ' . $lesson->achievements()->latest()->first()->rule()->first()->unit_score;
                $raport[] = ['lesson' => $lesson->name,  'kkm' => $kkm, 'flight_hours' => $flight_hours, 'average' => $total];
            }
        }
        return $raport;
    }

    public function compare($data, $rule) {
        switch ($rule->operator) {
            case '>':
                if ($data > $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<':
                if ($data < $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<=':
                if ($data <= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '>=':
                if ($data >= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            default:
            return false;

        break;
        }

        return false;
    }
}
