<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PartnerRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\PartnerBranch;
use App\Partner;
use App\Province;
use DataTables;
use App\User;

class PartnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => __('Partner'),
            'partners' => Partner::pluck('name', 'id')->toArray(),
            'provinces' => Province::pluck('name', 'id')->toArray(),
            'columns' => ['Name', 'Phone Number', 'Address']
        ];
        return view('admin.partner.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $partners = PartnerBranch::when( ! empty($request->partners), function ($query) use ($request) {
                    $query->whereHas('partner', function ($subQuery) use ($request) {
                        $subQuery->whereIn('id', $request->partners);
                    });
                })->when( ! empty($request->provinces), function ($query) use ($request) {
                    $query->whereHas('city', function ($subQuery) use ($request) {
                        $subQuery->whereIn('province_id', $request->provinces)
                        ->when(! empty($request->regencies), function ($subInQuery) use ($request){
                            $subInQuery->where('id', $request->regencies);
                        });
                    });
                })->get();
            return DataTables::of($partners)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->editColumn('created_at', function ($data) {
                    return (date('d-m-Y H:i:s', strtotime($data->created_at)));
                })
                ->editColumn('phone_number', function($data) {
                    return $data->phone_number
                                ? $data->phone_number
                                : '-';
                })
                ->editColumn('email', function($data) {
                    return $data->email
                                ? $data->email
                                : '-';
                })
                ->editColumn('address', function($data) {
                    return $data->address
                                ? $data->address
                                : '-';
                })
                // ->addColumn('action', function ($data) {
                //     return '<a class="btn btn-icon btn-sm btn-info" href="'.route('admin.partner.show', $data->id).'" title="'.__("See detail").'"><i class="fa fa-eye"></i></a> <a class="btn btn-icon btn-sm btn-warning" href="'.route('admin.partner.edit', $data->id).'" title="'.__("Edit").'"><i class="fa fa-edit"></i></a>';
                // })
                ->rawColumns(['status', 'DT_RowIndex', 'phone_number', 'email', 'address'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => __('Create Partner'),
            'breadcrumbs' => [
                route('admin.partner.index') => __('Partner'),
                null => __('Create')
            ],
        ];
        return view('admin.partner.create', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PartnerRequest $request)
    {
        // $request->merge([
        //     'date_of_birth' => date('Y-m-d', strtotime($request->date_of_birth)),
        //     'phone_number' => '+62'.$request->phone_number,
        // ]);
        $request->merge(['password' => Hash::make($request->password)]);
        $partner = Partner::create($request->except(['username', 'password', 'submit']));
        $user = User::create($request->except(['address', 'submit']));
        $partner->userAccount()->create(['user_id' => $user->id]);
        return redirect(url()->previous())->with('alert-success', __($this->createdMessage));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        $view = [
            'title' => __('Partner'),
            'breadcrumbs' => [
                route('admin.partner.index') => __('Partner'),
                null => __('Edit')
            ],
            'data' => $partner
        ];
        return view('admin.partner.show', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        $view = [
            'title' => __('Edit Partner'),
            'breadcrumbs' => [
                route('admin.partner.index') => __('Partner'),
                null => __('Edit')
            ],
            'data' => $partner
        ];
        return view('admin.partner.edit', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PartnerRequest $request, Partner $partner)
    {
        $partner->fill($request->all());
        $partner->save();
        return redirect(url()->previous())->with('alert-success', __($this->updatedMessage));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
