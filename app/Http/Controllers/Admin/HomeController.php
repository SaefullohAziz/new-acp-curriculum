<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PartnerJobRequirement;
use Illuminate\Http\Request;
use App\PartnerRequirement;
use App\PartnerBranch;
use App\Province;
use App\Partner;
use App\JobDesk;
use App\Island;
use App\Lesson;
use App\City;
use App\School;
use App\Student;
use App\AlocatedStudent;
use App\LessonAchievement;

class HomeController extends Controller
{
    protected $studentAchiviementData;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        for ($i = 0; $i <= 5; $i++) 
        {
            $time = strtotime( date( 'Y-m-01' )." -$i months");
            $dates[] = [
                'bulan' => date("M", $time),
            ];
        }

        $reqJob = collect($this->reqJobDesk()['reqJob']);
        $vokasiReqJob = $this->reqJobDesk()['vokasi'];
        $reqPartner = $this->reqPartner();
        $reqJava = $this->reqJava($request);
        $total_req = PartnerRequirement::sum('quota');
        $reqOutOfJavas = $this->reqOutJavas($request);
        $filterReqSimJava = $this->filterReqSimJava($request);
        $filterReqSimOutJava = $this->filterReqSimOutJava($request);
        $filterPartnerReqJava = $this->filterPartnerReqJava($request);
        $filterPartnerReqOutOfJava = $this->filterPartnerReqOutOfJava($request);
        $studentAchiviementChartData = $this->studentAchiviementData;
        $school = School::whereHas('students', function($query){
                    $query->whereHas('lessonAchievements');
                });
        $view = [
            'header' => true,
            'title' => 'Home',
            'filterPartnerReqOutOfJavas' => $filterPartnerReqOutOfJava,
            'filterPartnerReqJavas' => $filterPartnerReqJava,
            'filterReqSimOutJavas' => $filterReqSimOutJava,
            'filterReqSimJavas' => $filterReqSimJava,
            'reqOutOfJavas' => $reqOutOfJavas,
            'total_req' => $total_req,
            'reqPartners' => $reqPartner,
            'months' => $this->months,
            'reqJavas' => $reqJava,
            'reqJobs' => $reqJob,
            'vokasiReqJobs' => $vokasiReqJob,
            'active_month' => date('F'),
            'month_tables' => $dates,
            'partner_branches' => Partner::get(),
            'active_lesson' => Lesson::whereHas('students')->first(),
            'lessons' => Lesson::select('id', 'name')->get(),
            'schools' => $school->get(),
            'students' => $school->count() > 0 ? $school->first()->students()->whereHas('lessonAchievements')->get() : '',
            'active_student' => Student::whereHas('lessonAchievements')->first(),
            'academySchool' => School::whereHas('statuses', function($query){
                                    $query->where('name', 'Academy');
                                })->pluck('name'),
            'studentAchiviementChartData' => $studentAchiviementChartData
        ];

        return view('admin.home', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Query get data for request job desk statistik.
     *
     * @param  get  $request
     * @return \Illuminate\Http\Request
     */
    public function reqJobDesk()
    {
        // $bulan = array_unique(PartnerRequirement::pluck('month')->toArray());

        $sort = ["CPC", "RPL", "CIT", "FLM"];
        $months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $jobDesks = JobDesk::get()->sortBy(function($model) use ($sort) {
            return array_search($model->slug, $sort);
        });
        $reqJob = [];
        foreach ($jobDesks as $jobDesk) {
            $no = 0;
            foreach ($months as $key) {
                $aa[$key] = $jobDesk->partnerJobRequirements()->with('partnerRequirement')->whereHas('partnerRequirement', function ($query) use($key) {
                        $query->where('month', 'like', '%'.$key);
                        $query->where('year', now()->year);
                    })->get()->sum('partnerRequirement.quota');
                $vok[$no++] = ALocatedStudent::where('month', 'like', '%'.$key)->where('year', now()->year)->count();
            }
            $reqJob += [$jobDesk->slug => [$aa['January'], $aa['February'], $aa['March'], $aa['April'], $aa['May'], $aa['June'], $aa['July'], $aa['August'], $aa['September'], $aa['October'], $aa['November'], $aa['December'], $jobDesk->color]];
        }
        // $vokasi =[$vok['January'], $vok['February'], $vok['March'], $vok['April'], $vok['May'], $vok['June'], $vok['July'], $vok['August'], $vok['September'], $vok['October'], $vok['November'], $vok['December']];
        $vokasi = '[';
        for ($i = 0; $i < date('n'); $i++)
        {
            $vokasi .= $vok[$i] . ',';
        }
        $vokasi .= ']';
        $data['reqJob'] = $reqJob;
        $data['vokasi'] = $vokasi;
        return $data;
    }

    /**
     * Query get data for request partner statistik.
     *
     * @param  get  $request
     * @return \Illuminate\Http\Request
     */
    public function reqPartner()
    {
        // $bulan = array_unique(PartnerRequirement::pluck('month')->toArray());
        
        $months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $partners = Partner::get();
        $reqPartner = [];
        foreach ($partners as $partner) {
            foreach ($months as $key) {
                $total_quota = 0;
                foreach ($partner->partnerBranches()->get() as $branch) {
                    $total_quota += $branch->partnerRequirement()->where('month', 'like', '%'.$key)->get()->sum('quota');
                }
                $aa[$key] = $total_quota;
            }
            $reqPartner += [$partner->slug => [$aa['January'], $aa['February'], $aa['March'], $aa['April'], $aa['May'], $aa['June'], $aa['July'], $aa['August'], $aa['September'], $aa['October'], $aa['November'], $aa['December'], $partner->color]];
        }
        return collect($reqPartner);
    }

    /**
     * Query get data for request in Java statistik.
     *
     * @param  get  $request
     * @return \Illuminate\Http\Request
     */
    public function reqJava(Request $request)
    {
        $jawa = Province::whereIn('abbreviation', ["Banten", "JKT", "Jabar", "Jateng", "Jatim", "DIY"])->get();
        foreach ($jawa as $key) {
            $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
            $partnerBranchs = PartnerBranch::whereIn('city_id', $pulau_jawa)->get();
            $total = 0;
            foreach ($partnerBranchs as $partnerBranch) {
                $total += $partnerBranch->partnerRequirement()
                    ->when(empty($request->bulan) , function ($query) use ($request) {
                        return $query->where('month', date('F'));
                    })->when(!empty($request->bulan) , function ($query) use ($request) {
                        return $query->where('month', $request->bulan);
                    })->get()->sum('quota');
            }
            $aa[$key->abbreviation] = $total;
            
        }
        $reqJava = [$aa['Banten'], $aa['JKT'], $aa['Jabar'], $aa['Jateng'], $aa['Jatim'], $aa['DIY']];
        return $reqJava;
    }

    /**
     * Query get data for request filter partner in Java statistik.
     *
     * @param  get  $request
     * @return \Illuminate\Http\Request
     */
    public function filterPartnerReqJava(Request $request)
    {
        $jawa = Province::whereIn('abbreviation', ["Banten", "JKT", "Jabar", "Jateng", "Jatim", "DIY"])->get();
        $partners = Partner::get();
        $reqJava = [];
        foreach ($partners as $partner) {
            foreach ($jawa as $key) {
                $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
                $partnerBranchs = PartnerBranch::where('partner_id', $partner->id)
                    ->whereIn('city_id', $pulau_jawa)->get();
                $total_quota = 0;
                foreach ($partnerBranchs as $branch) {
                    $total_quota += $branch->partnerRequirement()
                        ->when(empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', date('F'));
                        })->when(!empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', $request->bulan);
                        })->get()->sum('quota');
                }
                $aa[$key->abbreviation] = $total_quota;
            }
            $reqJava += [$partner->slug => [$aa['Banten'], $aa['JKT'], $aa['Jabar'], $aa['Jateng'], $aa['Jatim'], $aa['DIY'], $partner->color]];
        }
        return collect($reqJava);
    }

    /**
     * Query get data for request out of Java statistik.
     *
     * @param  get  $request
     * @return \Illuminate\Http\Request
     */
    public function reqOutJavas(Request $request)
    {
        $outOfJavas = Island::whereIn('name', ["Kalimantan", "Kepulauan Maluku", "Kepulauan Nusa Tenggara", "Papua", "Sulawesi", "Sumatera"])->get();
        foreach ($outOfJavas as $outOfJava) {
            $provinceOutJavas = Province::where('island_id', $outOfJava->id)->get();
            $total = 0;
            foreach ($provinceOutJavas as $key) {
                $cityOutJavas = City::where('province_id', $key->id)->pluck('id')->toarray();
                $partnerBranchs = PartnerBranch::whereIn('city_id', $cityOutJavas)->get();
                foreach ($partnerBranchs as $partnerBranch) {
                    $total += $partnerBranch->partnerRequirement()
                        ->when(empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', date('F'));
                        })->when(!empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', $request->bulan);
                        })->get()->sum('quota');
                }
            }
            $aa[$outOfJava->name] = $total;
            
        }
        $reqOutJawa = [$aa['Kalimantan'], $aa['Kepulauan Maluku'], $aa['Kepulauan Nusa Tenggara'], $aa['Papua'], $aa['Sulawesi'], $aa['Sumatera']];
        return $reqOutJawa;
    }

    /**
     * Query get data for request filter partner in Java statistik.
     *
     * @param  get  $request
     * @return \Illuminate\Http\Request
     */
    public function filterPartnerReqOutOfJava(Request $request)
    {
        $islands = Island::whereIn('name', ["Kalimantan", "Kepulauan Maluku", "Kepulauan Nusa Tenggara", "Papua", "Sulawesi", "Sumatera"])->get();
        $partners = Partner::get();
        $reqJava = [];
        foreach ($partners as $partner) {
            foreach ($islands as $island) {
                $provinceOutJavas = Province::where('island_id', $island->id)->get();
                $total_quota = 0;
                foreach ($provinceOutJavas as $key) {
                    $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
                    $partnerBranchs = PartnerBranch::where('partner_id', $partner->id)
                        ->whereIn('city_id', $pulau_jawa)->get();
                    foreach ($partnerBranchs as $branch) {
                        $total_quota += $branch->partnerRequirement()
                            ->when(empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', date('F'));
                            })->when(!empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', $request->bulan);
                            })->get()->sum('quota');
                    }
                }
                $aa[$island->name] = $total_quota;
            }
            $reqJava += [$partner->slug => [$aa['Kalimantan'], $aa['Kepulauan Maluku'], $aa['Kepulauan Nusa Tenggara'], $aa['Papua'], $aa['Sulawesi'], $aa['Sumatera'], $partner->color]];
        }
        return collect($reqJava);
    }

    /**
     * Query get data for request job desk statistik.
     *
     * @param  get  $request
     * @return \Illuminate\Http\Request
     */
    public function filterReqSimJava(Request $request)
    {
        $jawa = Province::whereIn('abbreviation', ["Banten", "JKT", "Jabar", "Jateng", "Jatim", "DIY"])->get();
        $docs = ["Sim", "Non-Sim"];
        $reqJava = [];
        foreach ($jawa as $key) {
            $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
            $partnerBranchs = PartnerBranch::whereIn('city_id', $pulau_jawa)->get();
            $sim = 0;
            $non_sim = 0;
            foreach ($partnerBranchs as $branch) {
                $sim += $branch->partnerRequirement()
                    ->when(empty($request->bulan) , function ($query) use ($request) {
                        return $query->where('month', date('F'));
                    })->when(!empty($request->bulan) , function ($query) use ($request) {
                        return $query->where('month', $request->bulan);
                    })->whereHas('partnerDocRequirements', function ($query) use($key) {
                        $query->where('id', '!=', null);
                    })->get()->sum('quota');
                $non_sim += $branch->partnerRequirement()
                    ->when(empty($request->bulan) , function ($query) use ($request) {
                        return $query->where('month', date('F'));
                    })->when(!empty($request->bulan) , function ($query) use ($request) {
                        return $query->where('month', $request->bulan);
                    })->get()->sum('quota');
            }
            $aa[$key->abbreviation] = $sim;
            $ab[$key->abbreviation] = $non_sim;
        }
        $reqJava += ['Sim' => [$aa['Banten'], $aa['JKT'], $aa['Jabar'], $aa['Jateng'], $aa['Jatim'], $aa['DIY'], 'rgba(103, 119, 239, 0.6)'], 
                     'Non Sim' => [$ab['Banten'], $ab['JKT'], $ab['Jabar'], $ab['Jateng'], $ab['Jatim'], $ab['DIY'], 'rgba(252, 84, 75, 0.6)']];
        return collect($reqJava);
    }

    /**
     * Query get data for request job desk statistik.
     *
     * @param  get  $request
     * @return \Illuminate\Http\Request
     */
    public function filterReqSimOutJava(Request $request)
    {
        $islands = Island::whereIn('name', ["Kalimantan", "Kepulauan Maluku", "Kepulauan Nusa Tenggara", "Papua", "Sulawesi", "Sumatera"])->get();
        $docs = ["Sim", "Non-Sim"];
        $reqJava = [];
        foreach ($islands as $island) {
                $provinceOutJavas = Province::where('island_id', $island->id)->get();
                $sim = 0;
                $non_sim = 0;
                foreach ($provinceOutJavas as $key) {
                    $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
                    $partnerBranchs = PartnerBranch::whereIn('city_id', $pulau_jawa)->get();
                    foreach ($partnerBranchs as $branch) {
                        $sim += $branch->partnerRequirement()
                            ->when(empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', date('F'));
                            })->when(!empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', $request->bulan);
                            })->whereHas('partnerDocRequirements', function ($query) use($key) {
                                $query->where('id', '!=', null);
                            })->get()->sum('quota');
                        $non_sim += $branch->partnerRequirement()
                            ->when(empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', date('F'));
                            })->when(!empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', $request->bulan);
                            })->get()->sum('quota');
                    }
                }
                $aa[$island->name] = $sim;
                $ab[$island->name] = $non_sim;
            }
        $reqJava += ['Sim' => [$aa['Kalimantan'], $aa['Kepulauan Maluku'], $aa['Kepulauan Nusa Tenggara'], $aa['Papua'], $aa['Sulawesi'], $aa['Sumatera'], 'rgba(103, 119, 239, 0.6)'], 
                     'Non Sim' => [$ab['Kalimantan'], $ab['Kepulauan Maluku'], $ab['Kepulauan Nusa Tenggara'], $ab['Papua'], $ab['Sulawesi'], $ab['Sumatera'], 'rgba(252, 84, 75, 0.6)']];
        return collect($reqJava);
    }
}
