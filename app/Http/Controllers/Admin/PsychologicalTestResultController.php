<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PsychologicalTestResultController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => 'Psychotest Result',
        ];

        return view('admin.psychotest-result.index', $view);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mbti_list()
    {
        $sumber = 'https://jobal.bcateachingfactory.com/api/mbti_result?api_token=xV1yUR0pdC5tLkq4OOir2IyMgQeGME7RwRl060jhfLTfFXxDgSkptbpPbo2d';
        $content = file_get_contents($sumber);
        $data = json_decode($content, true);

        return DataTables::of($data['result'])
        ->addIndexColumn()
        ->editColumn('created_at', function($data) {
            return (date('d-m-y h:m:s', strtotime($data['created_at'])));
        })
        ->addColumn('action', function($data) {
            return '<a class="btn btn-xs btn-primary" href="admin/psychotest-result/'.$data['psychological_test_id'].'/attempt/'.$data['id'].'/result" title="Download"><i class="fa fa-eye"></i></a>';
        })
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function disc_list()
    {
        $sumber = 'https://jobal.bcateachingfactory.com/api/disc_result?api_token=xV1yUR0pdC5tLkq4OOir2IyMgQeGME7RwRl060jhfLTfFXxDgSkptbpPbo2d';
        $content = file_get_contents($sumber);
        $data = json_decode($content, true);

        return DataTables::of($data['result'])
        ->addIndexColumn()
        ->editColumn('created_at', function($data) {
            return (date('d-m-y h:m:s', strtotime($data['created_at'])));
        })
        ->addColumn('action', function($data) {
            return '<a class="btn btn-xs btn-primary" href="admin/psychotest-result/'.$data['id'].'/result" title="Download"><i class="fa fa-eye"></i></a>';
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PsychologicalTestResult  $psychologicalTestResult
     * @return \Illuminate\Http\Response
     */
    public function mbti_show($psychologicalTest, $psychologicalTestAttempt)
    {
        $sumber = 'https://jobal.bcateachingfactory.com/api/mbti_result/'.$psychologicalTest.'/attempt/'.$psychologicalTestAttempt.'/result?api_token=xV1yUR0pdC5tLkq4OOir2IyMgQeGME7RwRl060jhfLTfFXxDgSkptbpPbo2d';
        $content = file_get_contents($sumber);
        $data = json_decode($content, true);
        if($data['status'] == true) {
            return view('psychotest-result.mbti_show', $data['data']);
        }
        return redirect(url()->previous())->with('alert-danger', __('Incomplete data!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PsychologicalTestResult  $psychologicalTestResult
     * @return \Illuminate\Http\Response
     */
    public function disc_show($user)
    {
        $sumber = 'https://jobal.bcateachingfactory.com/api/disc_result/'.$user.'/result?api_token=xV1yUR0pdC5tLkq4OOir2IyMgQeGME7RwRl060jhfLTfFXxDgSkptbpPbo2d';
        $konten = file_get_contents($sumber);
        $data = json_decode($konten, true);

        $resource = 'https://jobal.bcateachingfactory.com/api/disc_result?api_token=xV1yUR0pdC5tLkq4OOir2IyMgQeGME7RwRl060jhfLTfFXxDgSkptbpPbo2d';
        $content = file_get_contents($resource);
        $datas = json_decode($content, true);

        return view('admin.psychotest-result.disc_show', $data['view'], $datas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PsychologicalTestResult  $psychologicalTestResult
     * @return \Illuminate\Http\Response
     */
    public function edit(PsychologicalTestResult $psychologicalTestResult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PsychologicalTestResult  $psychologicalTestResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PsychologicalTestResult $psychologicalTestResult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PsychologicalTestResult  $psychologicalTestResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(PsychologicalTestResult $psychologicalTestResult)
    {
        //
    }
}
