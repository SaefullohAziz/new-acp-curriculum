<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolRequest;
use DataTables;
use App\Department;
use App\Province;
use App\School;
use App\Status;
use App\Level;
use App\City;
use Form;

class SchoolController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => __('Schools'),
            'status' => Status::first(),
            'levels' => Level::whereHas('statuses', function ($query) {
                            $query->where('intention', 'school');
                        })->orderBy('order', 'asc')->pluck('name', 'id')->toArray(),
            'provinces' => Province::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
            'columns' => ['Name', 'Status', 'Headmaster Name', 'Headmaster E-Mail', 'PIC Name', 'PIC E-Mail', 'Address'],
        ];
        return view('admin.school.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $statuses = Status::where('intention', 'school')->get()->map->only('name', 'id')->pluck('name', 'id')->toArray();
            $schools = School::when( ! empty($request->levels), function ($query) use ($request) {
                            $query->whereHas('statuses', function ($subQuery) use ($request) {
                                $subQuery->whereIn('level_id', $request->levels)
                                ->when(! empty($request->statuses), function ($subInQuery) use ($request){
                                    $subInQuery->where('statuses.id', $request->statuses);
                                });
                            });
                        })->when( ! empty($request->provinces), function ($query) use ($request) {
                            $query->whereHas('city', function ($subQuery) use ($request) {
                                $subQuery->whereIn('province_id', $request->provinces)
                                ->when(! empty($request->regencies), function ($subInQuery) use ($request){
                                    $subInQuery->where('id', $request->regencies);
                                });
                            });
                        })->get();

            return DataTables::of($schools)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->editColumn('created_at', function ($data) {
                    return (date('d-m-Y H:i:s', strtotime($data->created_at)));
                })
                ->addColumn('status', function ($data) use ($statuses) {
                    // static permission
                    if (! auth()->user()->should('student_statuses')) {
                        return $data->statuses()->count()
                                ? $data->statuses()->latest()->first()->name
                                : 'Registerred';
                    }
                    return Form::bsSelect('rounded-fill', null, 'status', $statuses, (
                        $data->statuses()->latest()->first() 
                            ? $data->statuses()->latest()->first()->id 
                            : null
                    ), null, ['data-id' => $data->id]);
                })
                ->addColumn('pic_name', function ($data) {
                    return $data->pics->first()
                                ? $data->pics()->first()->name
                                : '-';
                })
                ->addColumn('pic_email', function ($data) {
                    return $data->pics->first()
                                ? $data->pics()->first()->email
                                : '-';
                })
                // ->addColumn('action', function ($data) {
                //     return '<a class="btn btn-icon btn-sm btn-info" href="'.route('admin.teacher.show', $data->id).'" title="'.__("See detail").'"><i class="fa fa-eye"></i></a> <a class="btn btn-icon btn-sm btn-warning" href="'.route('admin.teacher.edit', $data->id).'" title="'.__("Edit").'"><i class="fa fa-edit"></i></a>';
                // })
                ->rawColumns(['DT_RowIndex', 'status', 'pic_name', 'pic_email'])
                ->make(true);
        }
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => 'Create school',
            'school_type' => ['negeri','Swasta'],
            'provinces' => Province::pluck('name', 'id')->toArray(),
            'cities' => City::pluck('name', 'id')->toArray(),
            'departments' => array_merge(Department::orderBy('name', 'asc')->pluck('name')->toArray(), [__('Other')]),
            'isoCertificates' => ['Sudah' => 'Sudah', 'Dalam Proses (persiapan dokumen / pembentukan team audit internal / pendampingan)' => 'Dalam Proses (persiapan dokumen / pembentukan team audit internal / pendampingan)', 'Belum' => 'Belum']
        ];

        return view('admin.school.create', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request)
    {
        // dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        $view = [
            'title' => __('Schools'),
            'data' => $school,
            'pics' => $school->schoolPic,
        ];
        return view('admin.school.show', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school)
    {
        if ($request->filled('status_id')) {
            $school->schoolStatuses()->create(['status_id' => $request->status_id]);
        }
        if ($request->ajax()) {
            return response()->json(['status' => true, 'message' => __($this->savedSettingMessage)]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
