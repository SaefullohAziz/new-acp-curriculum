<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Level;
use App\Lesson;
use App\School;
use App\Status;
use App\Island;
use App\Student;
use App\Partner;
use App\JobDesk;
use App\Province;
use App\Generation;
use App\SchoolStatus;
use App\PartnerBranch;
use App\AlocatedStudent;

class GetController extends Controller
{
    /**
     * Get City For step by step form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function city(Request $request)
    {
        /** THIS FUNCTION ARE
         * Use on : Admin school create, school register
         * Used by : 
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */

        if ($request->ajax()) {
            $data = City::when( ! empty($request->provinces), function ($query) use ($request) {
                $query->whereIn('name', $request->provinces);
            })->when( ! empty($request->province), function ($query) use ($request) {
                $query->getByProvinceName($request->province);
            })->pluck('name')->toArray();
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION array_key_exists(key, array)
     * Use on : AdminHome
     * Used by : S.Aziz, Ahmad
     * Create n modified By : Ahmad
     * 
     * 
     * Please edit when use or modify
    */
    public function adminChartSchool(Request $request)
    {
        // butuh perapihan OOP !!

        if ($request->ajax()) {
            // schoolStatuses
            $levels = Level::whereHas('statuses', function($query) {
                $query->where('intention', 'school');
            })->orderBy('order', 'desc');
            
            $school_id = [];
            foreach ($levels->get() as $level) {
                $countStatus = 0;
                foreach (Status::where(['level_id' => $level->id, 'intention' => 'school'])->orderBy('order', 'desc')->get() as $status) {
                    $schools = School::whereHas('schoolStatuses', function($query) use ($status) {
                        $query->where('status_id', $status->id);
                    })->whereNotIn('id', $school_id)->get();
                    foreach ($schools as $school) {
                        if (! in_array($school->id, $school_id)) {
                            $school_id[] = $school->id;
                            $countStatus++;
                        }
                    }
                }
                $data[] = $countStatus;
            };

            $array['schooLStatuses'] = [
                'labels' => $levels->pluck('name')->toArray(),
                'datasets' => [
                    [
                        'defaultFontSize' => '16',
                        'backgroundColor' => $levels->pluck('color')->toArray(),
                        'data' => $data
                    ],
                ]
            ];

            // Vokasi

                // job_desk
            $jobdesks = JobDesk::all();

            $data = [];
            foreach ($jobdesks as $jobdesk) {
                $data[] = AlocatedStudent::where('job_desk_id', $jobdesk->id)->count();
            }

            $array['totalVokasiJobDesk'] = [
                'labels' => $jobdesks->pluck('slug')->toArray(),
                'datasets' => [
                    [
                        'defaultFontSize' => '16',
                        'backgroundColor' => $jobdesks->pluck('color')->toArray(),
                        'data' => $data
                    ],
                ]
            ];
            
                // pkt
            $partners = Partner::all();
            $data = [];
            foreach ($partners as $partner) {
                $partner_brach_id = PartnerBranch::where('partner_id', $partner->id)->pluck('id');
                $data[] = AlocatedStudent::where('partner_id', $partner->id)
                ->orWhereIn('partner_branch_id', $partner_brach_id)->count();
            }
            
            $array['totalVokasiPkt'] = [
                'labels' => $partners->pluck('slug')->toArray(),
                'datasets' => [
                    [
                        'defaultFontSize' => '16',
                        'backgroundColor' => $partners->pluck('color')->toArray(),
                        'data' => $data
                    ],
                ]
            ];
            
            // studentStatuses
            $levels = Level::whereHas('statuses', function($query) {
                $query->where('intention', 'student');
            })->whereNotIn('name', ['Alumnus'])->orderBy('order', 'desc');

            $data = [];
            $student_id = [];
            foreach ($levels->get() as $level) {
                $countStatus = 0;
                foreach (Status::where(['level_id' => $level->id, 'intention' => 'Student'])->orderBy('order', 'desc')->get() as $status) {
                    $students = Student::whereHas('studentStatuses', function($query) use ($status) {
                        $query->where('status_id', $status->id);
                    })->whereNotIn('id', $student_id)->get();
                    
                    if ($students->count()) {
                        foreach ($students as $student) {
                            if (! in_array($student->id, $student_id)) {
                                $student_id[] = $student->id;
                                $countStatus++;
                            }
                        }
                    }
                    $status->name == 'Intensif Class' ? $data[] = $countStatus : '';
                }
                $level->name == 'Registration' ? $countStatus += 500 : '';
                $level->name == 'Seleksi' ? $countStatus += 450 : '';
                $data[] = $countStatus;
            };
            $array['studentStatuses'] = [
                'labels' => ['Daftar', 'Seleksi', 'Akademi', 'Kelas Intensif'],
                'datasets' => [
                    [
                        'defaultFontSize' => '16',
                        'backgroundColor' => $levels->pluck('color')->toArray(),
                        'data' => array_reverse($data)
                    ],
                ]
            ];

            // SchoolAlumniStatus
            $student = Student::select(['study_status'])->get();
            $data = [];
            $data [] = $student->where('study_status', 'Lulus SMK/SMA')->count();
            $data [] = $student->where('study_status', '!=', 'Lulus SMK/SMA')->count();
            $array['aktifAlumni'] = [
                'labels' => ['Siswa Aktif', 'Siswa Alumni Sekolah'],
                'datasets' => [
                    [
                        'defaultFontSize' => '16',
                        'backgroundColor' => ['rgba(58, 186, 244, 0.6)', 'rgba(103, 119, 239, 0.6)'],
                        'data' => array_reverse($data)
                    ],
                ]
            ];

            // Student study status alocated
            $student = Student::select(['study_status'])->whereHas('alocatedStudent')->get();
            $data = [];
            $data [] = $student->where('study_status', 'Lulus SMK/SMA')->count();
            $data [] = $student->where('study_status', '!=', 'Lulus SMK/SMA')->count();
            $array['countStatusVokasi'] = collect($data)->sum();
            $array['totalStatusVokasi'] = [
                'labels' => ['Siswa Aktif', 'Siswa Alumni Sekolah'],
                'datasets' => [
                    [
                        'defaultFontSize' => '16',
                        'backgroundColor' => ['rgba(58, 186, 244, 0.6)', 'rgba(103, 119, 239, 0.6)'],
                        'data' => array_reverse($data)
                    ],
                ]
            ];

            // TalentPool n studentFinal
            $thead = ['total'];
            $tfootReady = ['total' => 0];
            $tfootAlocated = ['total' => 0];
            // get 6 months before and now
            for ($i = 0; $i <= 5; $i++) 
            {
                $time = strtotime( date( 'Y-m-01' )." -$i months");
                $dates[] = [
                    'M' => date("M", $time),
                    'm' => date("m", $time),
                    'Y' => date("Y", $time),
                ];
                $thead[] = date("M", $time);
                $tfootReady[date('M', $time)] = 0;
                $tfootAlocated[date('M', $time)] = 0;
                if ($i == 5) {
                    $thead[] = 'Nama Sekolah';
                    $tfootReady['nama_sekolah'] = 'Total';
                    $tfootAlocated['nama_sekolah'] = 'Total';
                }
            }
            $schools = School::whereHas('students', function($query) {
                $query->whereHas('lessonAchievements');
            })->get();
    
            $dates = array_reverse($dates);
            $schoolData = [];
            // get student by achievement per months
            foreach ($schools as $school) {
                $ready = [];
                $alocated = [];
                foreach ($dates as $date) {
                    $students =$school->students()->whereHas('lessonAchievements', function ($query) use ($date) {
                        $query->whereMonth('student_lesson_achievements.created_at', $date['m']);
                        $query->whereYear('student_lesson_achievements.created_at', $date['Y']);
                    });
                    $readyNow = $students->count() - $students->whereHas('alocatedStudent')->count();
                    $alocatedNow = $students->whereHas('alocatedStudent')->count();
                    $ready[$date['m']] = $readyNow;
                    $tfootReady[$date['M']] += $readyNow;
                    $alocated[$date['m']] = $alocatedNow;
                    $tfootAlocated[$date['M']] += $alocatedNow;
                    if ( ! next($dates)) {
                        $sumReady = array_sum($ready);
                        $sumAlocated = array_sum($alocated);
                        $ready['total'] = $sumReady;
                        $alocated['total'] = $sumAlocated;
                        $tfootReady['total'] += $sumReady;
                        $tfootAlocated['total'] += $sumAlocated;
                    }
                }
                $schoolData[$school->name] = [
                    'ready' => $ready,
                    'alocated' => $alocated
                ];
            }

            $array['TalentPool'] = [
                'thead' => array_reverse($thead),
                'tbody' => $schoolData,
                'tfoot' => [
                    'ready' => array_reverse($tfootReady),
                    'alocated' => array_reverse($tfootAlocated),
                ],
            ];

            //permintaan peserta vokasi
            $sort = ["CPC", "RPL", "CIT", "FLM"];
            $months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            $jobDesks = JobDesk::get()->sortBy(function($model) use ($sort) {
                return array_search($model->slug, $sort);
            });
            $reqJob = [];
            $count_permintaan = 0;
            for ($i = 0; $i < 12; $i++)
            {
                $count_month[$i] = 0;
            }
            foreach ($jobDesks as $jobDesk) {
                $getData = [];
                $no = 0;
                foreach ($months as $key) {
                    $getData[] = $jobDesk->partnerJobRequirements()->with('partnerRequirement')->whereHas('partnerRequirement', function ($query) use($key, $request) {
                            $query->when(!empty($request->partner_id), function($subQuery) use($request){
                                $subQuery->whereHas('partnerBranch', function($subInQuery) use($request){
                                        $subInQuery->where('partner_id', $request->partner_id);
                                });
                            });
                            $query->where('month', 'like', '%'.$key);
                            $query->where('year', now()->year);
                        })->get()->sum('partnerRequirement.quota');
                    $vok[$no++] = ALocatedStudent::when(!empty($request->partner_id), function($query) use($request){
                            $query->whereHas('partnerBranch', function($subQuery) use($request){
                                    $subQuery->where('partner_id', $request->partner_id);
                            });
                    })->where('month', 'like', '%'.$key)->where('year', now()->year)->count();
                }
                for ($i = 0; $i < 12; $i++)
                {
                    $count_month[$i] += $getData[$i];
                }
                $datasetVokasi[] = [
                        'label' => $jobDesk->slug,
                        'data'=> $getData,
                        'backgroundColor'=> $jobDesk->color,
                    ];
                $count_permintaan += collect($getData)->sum();
            }
            for ($i = 0; $i < date('n'); $i++)
            {
                $vokasi[] = $vok[$i];
            }
            $datasetVokasi = array_reverse($datasetVokasi);
            $datasetVokasi[] = [
                        'data' => $vokasi,
                        'type' => 'line',
                        'label' => 'Pemenuhan',
                        'fill' => false,
                        'backgroundColor' => "#fff",
                        'borderColor' => "#3f3f3f",
                        'borderWidth' => 2,
                    ];
            $array['countPermintaan'] = $count_permintaan;
            $array['countPemenuhan'] = collect($vokasi)->sum();
            $array['reqVokasi'] = [
                'labels' => [['Jan', $count_month[0]], ['Feb', $count_month[1]], ['Mar', $count_month[2]], ['Apr', $count_month[3]], ['May', $count_month[4]], ['Jun', $count_month[5]], ['Jul', $count_month[6]], ['Aug', $count_month[7]], ['Sep', $count_month[8]], ['Oct', $count_month[9]], ['Nov', $count_month[10]], ['Dec', $count_month[11]]],
                'datasets' => array_reverse($datasetVokasi),
            ];


            return response()->json(['status' => true, 'data' => $array]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin school list
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function schoolStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = Status::where('intention', 'school')->whereHas('level', function ($query) use ($request) {
                $query->when( ! empty($request->levels), function ($subQuery) use ($request) {
                    $subQuery->whereIn('id', $request->levels);
                });
            })->orderBy('order', 'asc')->pluck('name', 'id')->toArray();
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

     /** THIS FUNCTION ARE
         * Use on : Admin school list, Admin partner list
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function regency(Request $request)
    {
        if ($request->ajax()) {
            $data = City::when( ! empty($request->provinces), function ($query) use ($request) {
                $query->whereIn('province_id', $request->provinces);
            })->pluck('name', 'id')->toArray();
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin teacher list, Admin student list
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function school(Request $request)
    {
        if ($request->ajax()) {
            $data = School::with(['schoolStatuses.statuses.level'])->when($request->filled('level'), function ($query) use ($request) {
                $query->byLevel($request->level);
            })->whereHas('statusLast', function ($status) use ($request) {
                $status->when($request->filled('status'), function ($query) {
                    $query->where('statuses.id', $request->status);
                });
            })->pluck('name', 'id')->toArray();
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin teacher list, Admin student list
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function academy(Request $request)
    {
        if ($request->ajax()) {
            $data = School::whereHas('statuses', function ($query) {
                $query->whereHas('level', function($subQuery){
                    $subQuery->where('name', 'academy');
                });
            })->when($request->filled('province'), function ($query) use ($request) {
                $query->whereHas('city', function($subQuery) use($request){
                    $subQuery->where('province_id', $request->province); 
                });
            })->pluck('name', 'id')->toArray();
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin school list
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function generation(Request $request)
    {
        if ($request->ajax()) {
            $data = Generation::when( ! empty($request->school), function ($query) use ($request) {
                $query->where('school_id', $request->school);
            })->pluck('number', 'id')->toArray();
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Student pasport input
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function unitScore(Request $request)
    {
        if ($request->ajax()) {
            $data = Lesson::where('id', $request->lesson)->first()->unit_score;
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Home Admin
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function studentAchievement(Request $request)
    {
        if ($request->ajax()) {
            $student = Student::when(!empty($request->student_id), function($query) use($request){
                    $query->where('id', $request->student_id);
                })->when(empty($request->student_id), function($query) use($request){
                    $query->whereHas('lessonAchievements');
                })->first();
            $lessons = Lesson::get();
            $data = [];
            if ($student) {
                foreach ($lessons as $lesson) {
                    $student_lesson = $student->lesson()->whereHas('duration', function($query) use($lesson){
                        $query->where('lesson_id', $lesson->id);
                    });
                    $average = $student_lesson->count() > 0  ? $student_lesson->get()->sum('score') / $student_lesson->count() : 0;
                    $total = ($average > 0 ? number_format($average, 2) : $average) . ' ' . $lesson->unit_score;
                    $jam = $student_lesson->get()->sum('duration.duration') . ' ' . $lesson->durations()->latest()->first()->duration_unit;
                    $data[] = ['lesson' => $lesson->name, 'jam_terbang' => $jam, 'achievement' => $total];
                }
            }
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Home Admin
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function schoolAchievement(Request $request)
    {
        if ($request->ajax()) { 
            $lesson = Lesson::when(!empty($request->lesson_id), function($query) use($request){
                    $query->where('id', $request->lesson_id);
                })->when(empty($request->lesson_id), function($query) use($request){
                    $query->whereHas('students');
                })->first();
            $schools = School::whereHas('students', function($query){
                $query->whereHas('lessonAchievements');
            })->get();
            $data = [];
            if ($schools) {
                foreach ($schools as $school) {
                    $fligth_hour = 0; $loop = 0; $total = 0;
                    $students = $school->students()->whereHas('lessonAchievements')->get();
                    foreach ($students as $student) {
                        $student_lesson = $student->lesson()->whereHas('duration', function($query) use($lesson){
                            $query->where('lesson_id', $lesson->id);
                        });
                        foreach ($student_lesson->get() as $studentLesson) {
                            $total += str_replace(',', '.', $studentLesson->score);
                        }
                        $fligth_hour += $student_lesson->get()->sum('duration.duration');
                        $loop += $student_lesson->count();
                    }
                    $unit_loop = $loop . ' Kali';
                    $averages = $loop > 0 ? $total / $loop : false;
                    $average = ($averages ? number_format($averages, 2) : $averages) . ' ' . $lesson->unit_score;
                    $fligth_hour = $fligth_hour > 60 ? number_format(($fligth_hour/60), 2) . ' Jam' : $fligth_hour . ' ' . $lesson->durations()->latest()->first()->duration_unit;
                    $data[] = ['school' => $school->name, 'fligth_hour' => $fligth_hour, 'average' => $average, 'loop' => $unit_loop];
                }
            }
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Student Skill list
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function student(Request $request)
    {
        if ($request->ajax()) {
            $data = Student::when(!empty($request->school_id), function ($query) use ($request) {
                $query->whereHas('school', function($subQuery) use($request){
                    $subQuery->where('schools.id', $request->school_id);
                });
            })->whereHas('lessonAchievements')->pluck('name', 'id')->toArray();
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    public function studentStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = Status::where('intention', 'student')->whereHas('level', function ($query) use ($request) {
                $query->when( ! empty($request->level_siswa), function ($subQuery) use ($request) {
                    $subQuery->where('id', $request->level_siswa);
                });
            })->orderBy('order', 'asc')->pluck('name', 'id')->toArray();
            return response()->json(['status' => true, 'result' => $data]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin home
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function chartReqJava(Request $request)
    {
        if ($request->ajax()) {
            // totalDalamJawa
            $jawa = Province::whereIn('abbreviation', ["Jabar", "JKT", "Banten", "Jateng", "DIY", "Jatim"])->get();
            $totalJawa = [];
            foreach ($jawa as $key) {
                $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
                $partnerBranchs = PartnerBranch::whereIn('city_id', $pulau_jawa)->get();
                $total = 0;
                foreach ($partnerBranchs as $partnerBranch) {
                    $total += $partnerBranch->partnerRequirement()
                        ->when(empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', date('F'));
                        })->when(!empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', $request->bulan);
                        })->where('year', date('Y'))->get()->sum('quota');
                }
                $totalJawa[] = $total;    
            }
            $array['countTotalJawa'] = collect($totalJawa)->sum();
            $array['totalDalamJawa'] = [
                'labels' => ["Jabar", "JKT", "Banten", "Jateng", "DIY", "Jatim"],
                'datasets' => [
                    [
                        'label' => 'Total Permintaan Dalam Jawa (' . collect($totalJawa)->sum() . ')' ,
                        'data'=> array_reverse($totalJawa),
                        'backgroundColor'=> 'rgba(103, 119, 239, 0.6)',
                    ],
                ]
            ];

            // pktDalamJawa
            $jawa = Province::whereIn('abbreviation', ["Jabar", "JKT", "Banten", "Jateng", "DIY", "Jatim"])->get();
            $partners = Partner::get();
            $reqJava = [];
            foreach ($partners as $partner) {
                foreach ($jawa as $key) {
                    $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
                    $partnerBranchs = PartnerBranch::where('partner_id', $partner->id)
                        ->whereIn('city_id', $pulau_jawa)->get();
                    $total_quota = 0;
                    foreach ($partnerBranchs as $branch) {
                        $total_quota += $branch->partnerRequirement()
                            ->when(empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', date('F'));
                            })->when(!empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', $request->bulan);
                            })->where('year', date('Y'))->get()->sum('quota');
                    }
                    $reqJava[] = $total_quota;
                }
                $datasets[] = [
                        'label' => $partner->slug,
                        'data'=> array_reverse($reqJava),
                        'backgroundColor'=> $partner->color,
                    ];
            }
            $array['pktDalamJawa'] = [
                'labels' => ["Jabar", "JKT", "Banten", "Jateng", "DIY", "Jatim"],
                'datasets' => $datasets,
            ];

            //simDalamJawa
            $jawa = Province::whereIn('abbreviation', ["Jabar", "JKT", "Banten", "Jateng", "DIY", "Jatim"])->get();
            $simJawa = [];
            $non_simJawa = [];
            foreach ($jawa as $key) {
                $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
                $partnerBranchs = PartnerBranch::whereIn('city_id', $pulau_jawa)->get();
                $sim = 0;
                $non_sim = 0;
                foreach ($partnerBranchs as $branch) {
                    $sim += $branch->partnerRequirement()
                        ->when(empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', date('F'));
                        })->when(!empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', $request->bulan);
                        })->whereHas('partnerDocRequirements', function ($query) use($key) {
                            $query->where('id', '!=', null);
                        })->where('year', date('Y'))->get()->sum('quota');
                    $non_sim += $branch->partnerRequirement()
                        ->when(empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', date('F'));
                        })->when(!empty($request->bulan) , function ($query) use ($request) {
                            return $query->where('month', $request->bulan);
                        })->where('year', date('Y'))->get()->sum('quota');
                }
                $simJawa[] = $sim;
                $non_simJawa[] = $non_sim;
            }

            $array['simDalamJawa'] = [
                'labels' => ["Jabar", "JKT", "Banten", "Jateng", "DIY", "Jatim"],
                'datasets' => [
                    [
                        'label' => 'Sim',
                        'data'=> array_reverse($simJawa),
                        'backgroundColor'=> 'rgba(103, 119, 239, 0.6)',
                    ],
                    [
                        'label' => 'Non-Sim',
                        'data'=> array_reverse($non_simJawa),
                        'backgroundColor'=> 'rgba(252, 84, 75, 0.6)',
                    ],
                ]
            ];

            return response()->json(['status' => true, 'data' => $array]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin home Permintaan Luar Jawa
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function chartReqOutJava(Request $request)
    {
        if ($request->ajax()) {
            //totalLuarJawa
            $outOfJavas = Island::whereIn('name', ["Kalimantan", "Kepulauan Maluku", "Kepulauan Nusa Tenggara", "Papua", "Sulawesi", "Sumatera"])->get();
            $totalLuar = [];
            foreach ($outOfJavas as $outOfJava) {
                $provinceOutJavas = Province::where('island_id', $outOfJava->id)->get();
                $total = 0;
                foreach ($provinceOutJavas as $key) {
                    $cityOutJavas = City::where('province_id', $key->id)->pluck('id')->toarray();
                    $partnerBranchs = PartnerBranch::whereIn('city_id', $cityOutJavas)->get();
                    foreach ($partnerBranchs as $partnerBranch) {
                        $total += $partnerBranch->partnerRequirement()
                            ->when(empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', date('F'));
                            })->when(!empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', $request->bulan);
                            })->where('year', date('Y'))->get()->sum('quota');
                    }
                }
                $totalLuar[] = $total; 
            }
            $array['countTotalLuar'] = collect($totalLuar)->sum();
            $array['totalLuarJawa'] = [
                'labels' => ["Sulawesi", "Nusa Tenggara", "Papua", "Maluku", "Sumatera", "Kalimantan"],
                'datasets' => [
                    [
                        'label' => 'Total Permintaan Luar Jawa (' . collect($totalLuar)->sum() . ')',
                        'data'=> array_reverse($totalLuar),
                        'backgroundColor'=> 'rgba(103, 119, 239, 0.6)',
                    ],
                ]
            ];

            //pktLuarJawa
            $islands = Island::whereIn('name', ["Kalimantan", "Kepulauan Maluku", "Kepulauan Nusa Tenggara", "Papua", "Sulawesi", "Sumatera"])->get();
            $partners = Partner::get();
            $reqOutJava = [];
            foreach ($partners as $partner) {
                foreach ($islands as $island) {
                    $provinceOutJavas = Province::where('island_id', $island->id)->get();
                    $total_quota = 0;
                    foreach ($provinceOutJavas as $key) {
                        $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
                        $partnerBranchs = PartnerBranch::where('partner_id', $partner->id)
                            ->whereIn('city_id', $pulau_jawa)->get();
                        foreach ($partnerBranchs as $branch) {
                            $total_quota += $branch->partnerRequirement()
                                ->when(empty($request->bulan) , function ($query) use ($request) {
                                    return $query->where('month', date('F'));
                                })->when(!empty($request->bulan) , function ($query) use ($request) {
                                    return $query->where('month', $request->bulan);
                                })->where('year', date('Y'))->get()->sum('quota');
                        }
                    }
                    $reqOutJava[] = $total_quota;
                }
                $datasetsLuar[] = [
                        'label' => $partner->slug,
                        'data'=> array_reverse($reqOutJava),
                        'backgroundColor'=> $partner->color,
                    ];
            }

            $array['pktLuarJawa'] = [
                'labels' => ["Sulawesi", "Nusa Tenggara", "Papua", "Maluku", "Sumatera", "Kalimantan"],
                'datasets' => $datasetsLuar,
            ];

            $islands = Island::whereIn('name', ["Kalimantan", "Kepulauan Maluku", "Kepulauan Nusa Tenggara", "Papua", "Sulawesi", "Sumatera"])->get();
            $simLuarJawa = [];
            $non_simLuarJawa = [];
            foreach ($islands as $island) {
                $provinceOutJavas = Province::where('island_id', $island->id)->get();
                $sim = 0;
                $non_sim = 0;
                foreach ($provinceOutJavas as $key) {
                    $pulau_jawa = City::where('province_id', $key->id)->pluck('id')->toarray();
                    $partnerBranchs = PartnerBranch::whereIn('city_id', $pulau_jawa)->get();
                    foreach ($partnerBranchs as $branch) {
                        $sim += $branch->partnerRequirement()
                            ->when(empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', date('F'));
                            })->when(!empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', $request->bulan);
                            })->whereHas('partnerDocRequirements', function ($query) use($key) {
                                $query->where('id', '!=', null);
                            })->get()->sum('quota');
                        $non_sim += $branch->partnerRequirement()
                            ->when(empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', date('F'));
                            })->when(!empty($request->bulan) , function ($query) use ($request) {
                                return $query->where('month', $request->bulan);
                            })->where('year', date('Y'))->get()->sum('quota');
                    }
                }
                $simLuarJawa[] = $sim;
                $non_simLuarJawa[] = $non_sim;
            }

            $array['simLuarJawa'] = [
                'labels' => ["Sulawesi", "Nusa Tenggara", "Papua", "Maluku", "Sumatera", "Kalimantan"],
                'datasets' => [
                    [
                        'label' => 'Sim',
                        'data'=> array_reverse($simLuarJawa),
                        'backgroundColor'=> 'rgba(103, 119, 239, 0.6)',
                    ],
                    [
                        'label' => 'Non-Sim',
                        'data'=> array_reverse($non_simLuarJawa),
                        'backgroundColor'=> 'rgba(252, 84, 75, 0.6)',
                    ],
                ]
            ];
            return response()->json(['status' => true, 'data' => $array]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin home table academy siswa
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function schoolAcademy(Request $request)
    {
        if ($request->ajax()) {
            //find school that student
            $schools = School::whereHas('students')->get();
            $data = [];
            //looping data $schools
            foreach ($schools as $school) {
                $academy = 0;
                //looping data student per school
                foreach ($school->students()->get() as $student) {
                    //find student whose academy status
                    $check = $student->statusLatest()->where('name', 'Academy')->first();
                    //if $check not empty
                    if ($check) {
                        $academy++;
                    }
                }
                //override $data
                $data[] = [
                    'nama' => $school->name, 
                    'count' => $academy
                ];
            }
            //add $array for tbody
            $array['tbody'] = $data;
            //add $array for tfoot
            $array['tfoot'] = collect($data)->sum('count');

            return response()->json(['status' => true, 'data' => $array]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin home siswa intensif class
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function schoolClassIntensif(Request $request)
    {
        if ($request->ajax()) {
            //find school that student
            $schools = School::whereHas('students')->get();
            $data = [];
            //looping data $schools
            foreach ($schools as $school) {
                $classIntensif = 0;
                //looping data student per school
                foreach ($school->students()->get() as $student) {
                    //find student whose intensif class status
                    $check = $student->statusLatest()->where('name', 'Intensif Class')->first();
                    //if $check not empty
                    if ($check) {
                        $classIntensif++;
                    }
                }
                //override $data
                $data[] = [
                    'nama' => $school->name, 
                    'count' => $classIntensif
                ];
            }
            //add $array for tbody
            $array['tbody'] = $data;
            //add $array for tfoot
            $array['tfoot'] = collect($data)->sum('count');

            return response()->json(['status' => true, 'data' => $array]);
        }
    }

    /** THIS FUNCTION ARE
         * Use on : Admin home table talent pool
         * Used by : ahmad
         * Create n modified By : 
         * 
         * 
         * Please edit when use or modify
        */
    public function talentPool(Request $request)
    {
        if ($request->ajax()) {
            // get 6 months before and now
            for ($i = 0; $i <= 5; $i++) 
            {
                $time = strtotime( date( 'Y-m-01' )." -$i months");
                $dates[] = [
                    'M' => date("M", $time),
                    'm' => date("m", $time),
                    'Y' => date("Y", $time),
                ];
                $count_ready[$i] = 0;
                $count_alocated[$i] = 0;
            }
            $schools = School::whereHas('students', function($query) {
                $query->whereHas('alocatedStudent')
                  ->orWhereHas('statuses', function($subQuery){
                      $subQuery->where('name', 'Complete Achievement');
                  });
            })->get();

            $dates = $dates;
            // get student by achievement per months
            $dihasilkan = [];
            $belum_terserap = [];
            foreach ($schools as $school) {
                $ready = [];
                $alocated = [];
                $no = 0;
                foreach ($dates as $date) {
                    $next = $no++;
                    $ready[$next] = $school->students()->whereHas('statuses', function ($query) use ($date) {
                        $query->where('name', 'Complete Achievement');
                        $query->whereMonth('student_statuses.created_at', $date['m']);
                        $query->whereYear('student_statuses.created_at', $date['Y']);
                    })->count();
                    $count_ready[$next] += $ready[$next];

                    $alocated[$next] = $school->students()->whereHas('alocatedStudent', function ($query) use ($date) {
                        $query->whereMonth('alocated_students.created_at', $date['m']);
                        $query->whereYear('alocated_students.created_at', $date['Y']);
                    })->count();
                    $count_alocated[$next] += $alocated[$next];
                }

                $dihasilkan[] = [$school->name, $alocated[0], $alocated[1], $alocated[2], $alocated[3], $alocated[4], $alocated[5], collect($alocated)->sum()];
                $belum_terserap[] = [$school->name, $ready[0], $ready[1], $ready[2], $ready[3], $ready[4], $ready[5], collect($ready)->sum()];
            }
            $count_alocated[] = collect($count_alocated)->sum();
            $count_ready[] = collect($count_ready)->sum();

            //add $array for tbody
            $array['alocated'] = ['tbody' => $dihasilkan, 'tfoot' => $count_alocated];
            $array['ready'] = ['tbody' => $belum_terserap, 'tfoot' => $count_ready];

            return response()->json(['status' => true, 'data' => $array]);
        }
    }
}
