<?php

namespace App\Http\Controllers\Student;

use App\Http\Requests\StudentLessonRequest;
use App\Http\Controllers\Controller;
use Spatie\Image\Manipulations;
use Illuminate\Http\Request;
use Spatie\Image\Image;
use App\StudentLessonClass;
use App\StudentLesson;
use App\LessonClass;
use App\Lesson;

class StudentLessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $lessons = Lesson::where('name', '!=', 'Melipat kertas')->get();
        $averageAchievement = [];
        foreach ($lessons as $lesson) {
            $student = auth()->user()->student;
            // declare count = 0
            $count = 0;
            // get student lesson
            $student_lesson = $student->lesson()->whereHas('duration', function($query) use($lesson){
                $query->where('lesson_id', $lesson->id);
            });

            // get student score
            $student_score = collect($student_lesson->get())->sortBy('score')->pluck('score')->toArray();
            // replace , to .
            foreach ($student_lesson->get() as $student) {
                $count += str_replace(',', '.', $student->score);
            }
            // get total value
            $total = $student_lesson->count()  ? $count / $student_lesson->count() : false;
            // get average value
            $total = $total ? number_format($total, 2) : 0;
            // get unit score
            $achievement = $total . ' ' . $lesson->unit_score;

            // get highest
            $highest = end($student_score);
            // get percentage
            $persen = $highest ? ($total/str_replace(',', '.', $highest))*100 : false;
            // 
            $percentage = $persen ? number_format($persen, 0) : 0;
            $averageAchievement[] = ['lesson' => $lesson->name, 'percentage' => $percentage, 'achievement' => $achievement];
        }

        if (session('class_id')) {
            session()->forget('class_id');
        }
        // for max absend validation
        $studentLessonClassId = StudentLessonClass::where('student_id', auth()->user()->student->id)->pluck('lesson_class_id')->toArray();
        $lessonClass = LessonClass::withCount([
            'studentLessons' => function ($query) {
                $query->where('student_lessons.student_id', auth()->user()->student->id);
            },
            'studentLessonsClass' => function ($query) {
                $query->where('student_lesson_classes.student_id', auth()->user()->student->id);
            }
        ])->whereHas('generation', function($query){
            $query->where('id', auth()->user()->student()->first()->generation->id);
        })
        ->orderBy('start_time', 'desc')->get();
        
        $view = [
            'title' => 'Passport',
            'averageAchievement' => $averageAchievement,
            'classes' => $lessonClass->filter(function($item) use($studentLessonClassId) {
                return ($item->student_lessons_count > 0 ||
                $item->student_lessons_class_count > 0 ||
                ( ($item->max_absend_at && $item->max_absend_at > now()) || ! $item->max_absend_at && $item->end_time > now() ));
            }),
        ];

        return view('student.passport.class', $view);
    }

    /**
     * Get a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {   
        if ($request->ajax()) {
            $studentLesson = StudentLesson::with('lesson')
            ->where('student_id', auth()->user()->student->id)
            ->when( ! $request->student_lesson_class_id, function($query) {
                $query->where('student_lesson_class_id', null);
            })
            ->when($request->student_lesson_class_id, function($query) use($request) {
                $query->where('student_lesson_class_id', $request->student_lesson_class_id);
            })
            ->orderBy('created_at', 'desc')->get()->toArray();
            return response()->json(['status' => true, 'data' => $studentLesson]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function preCreate(Request $request)
    {
        // validate if student has absend before
        $studentClass = StudentLessonClass::where([
            'lesson_class_id' => $request->class,
            'student_id' => auth()->user()->student->id
        ]);
        if ($studentClass->count()) {
            Session([
                'student_lesson_class_id' => $studentClass->first()->id,
                'class_id' => $request->class
            ]);
            return redirect()->route('student.studentLesson.create');
        }

        // validate absend time
        $lessonClass = LessonClass::find($request->class);
        if ($lessonClass->max_absend_at && now() > $lessonClass->max_absend_at) {
            return redirect()->route('student.studentLesson.index')->with('alert-danger', 'Sorry! time for absence is up.');
        } elseif (now() > $lessonClass->end_time) {
            return redirect()->route('student.studentLesson.index')->with('alert-danger', 'Sorry! time for absence is up.');
        }

        // if absend time still valid
        $studentClass = StudentLessonClass::Create([
            'student_id' => auth()->user()->student->id,
            'lesson_class_id' => $request->class
        ]);
        Session([
            'student_lesson_class_id' => $studentClass->id,
            'class_id' => $request->class
        ]);
        return redirect()->route('student.studentLesson.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => 'Input Passport',
            'lessons' => Lesson::get(),
        ];

        if (session('class_id')) {
            $class = LessonClass::find(session('class_id'));
            if ($class->lesson()->count() > 0) {
                $additional_view = [
                    'lessons' => $class->lesson()->get()
                ];
                $view = array_merge($view, $additional_view);
            }
            
            
            $additional_view = [
                'class' => $class,
                'studentLessons' => StudentLesson::where(['student_lesson_class_id' => session('student_lesson_class_id'), 'student_id' => auth()->user()->student->id])->orderBy('created_at', 'desc')->get(),
            ];
            $view = array_merge($view, $additional_view);
            
            return view('student.passport.create', $view);
        }

        return redirect()->route('student.studentLesson.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentLessonRequest $request)
    {
        // max reported at validation
        if (session('class_id')) {
            $lessonClass = LessonClass::find(session('class_id'));
            if ($lessonClass->max_reported_at && now() > $lessonClass->max_reported_at) {
                if ($request->ajax()) {
                    return response()->json(['status' => false, 'error' => 'Sorry! The class was end or max report duration is up. Contact your fasilitator!']);
                }
                return redirect()->route('student.studentLesson.create')->with('alert-danger', 'Sorry! The class was end or max report duration is up. Contact your fasilitator!');
            } elseif ((!$lessonClass->max_reported_at) && now() > $lessonClass->end_time) {
                if ($request->ajax()) {
                    return response()->json(['status' => false, 'error' => 'Sorry! The class was end or max report duration was end. Contact your fasilitator!']);
                }
                return redirect()->route('student.studentLesson.create')->with('alert-danger', 'Sorry! The class was end or max report duration was end. Contact your fasilitator!');
            }
        }

        $request->merge([
            'lesson_duration_id' => Lesson::find($request->lesson_duration_id)->getDurationId(),
            'student_id' => auth()->user()->student->id
        ]);

        $studentLesson = StudentLesson::create($request->except('photo'));

        if ($request->hasFile('photo')) {
            $studentLesson->photo = $this->uploadPhoto($studentLesson, $request);
            $studentLesson->save();
        }

        if ($request->ajax()) {
            return response()->json(['status' => true]);
        }

        return redirect()->previous()->with(['alert-succes' => $this->createdMessage]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentLesson $studentLesson)
    {
        $request->merge([
            'lesson_duration_id' => Lesson::find($request->lesson_duration_id)->getDurationId(),
            'student_id' => auth()->user()->student->id
        ]);
        $studentLesson->fill($request->all());
        $studentLesson->save();

        if ($request->ajax()) {
            return response()->json(['status' => true]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       StudentLesson::find($request->id)->delete();

       return response()->json(['status' => true]);
    }

    /**
     * Upload photo
     * 
     * @param  \App\Student Lesson  $studentLesson
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $oldFile
     * @return string
     */
    public function uploadPhoto($studentLesson, Request $request)
    {
        $filename = 'photo_'.date('d_m_Y_H_i_s_').md5(uniqid(rand(), true)).'.'.$request->photo->extension();
        $path = $request->photo->storeAs('public/student/studentLesson/', $filename);
        return $filename;
    }
}
