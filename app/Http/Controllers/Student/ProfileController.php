<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RequiredDocument;
use App\Student;
use App\Lesson;
use App\User;
use PDF;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(auth()->user()->id);
        $raport = $this->raport();

        $view = [
            'title' => __('Profile'),
            'documents' => RequiredDocument::orderBy('order', 'ASC')->get(),
            'data' => $user->student,
            'raport' => $raport
        ];
        return view('student.profile.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the preview raport page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview()
    {
        $student = auth()->user()->student->id;
        return $this->pdfVariable($student, 'preview');
    }

    /**
     * Display the download raport page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        $student = auth()->user()->student->id;
        return $this->pdfVariable($student, 'download');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        $request->merge([
            'date_of_birth' => date('Y-m-d', strtotime($request->date_of_birth)),
        ]);
        $student->fill($request->all());
        $student->save();
        return redirect(url()->previous())->with('alert-success', __($this->updatedMessage));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the PDF Variable page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdfVariable($student = null, $type = NULL)
    {
        $raport = $this->raport();
        $student = auth()->user()->student;

        $view = [
            'title' => $student->name,
            'data' => $student,
            'raport' => $raport
        ];

        $pdf = PDF::loadView('pdf.raport', $view);

        $pdf->setPaper('A4', 'potrait');
        if ($type == 'download') {
            return $pdf->download(str_replace(' ', '-', strtolower($student->name)).'-raport-'.date('d-m-Y-h-m-s').'.pdf');
        }
        elseif ($type == 'preview') {
            return $pdf->stream();
        }
    }
    
    /**
     * Display the Raport page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function raport()
    {
        $students = Student::where('id', auth()->user()->student->id)->first();
        $lessons = Lesson::where('name', '!=', 'Melipat Kertas')->get();
        $raport = [];
        if ($students) {
            foreach ($lessons as $lesson) {
                $students_lesson = $students->lesson()->whereHas('duration', function($query) use($lesson){
                    $query->where('lesson_id', $lesson->id);
                });
                
                $score = $students_lesson->get()->sortBy('score')->pluck('score')->toArray();
                if($students_lesson->get()){
                    $flight_hour = 0;
                    $kali = 0;
                    $count_average = [];
                    foreach($students_lesson->get() as $studentLesson){
                        $achievement = $studentLesson->lesson->achievements()->whereHas('rules', function($query){
                            $query->where('implemented_at', '<', now())->orderBy('implemented_at', 'desc');
                        })->first();
                        $rule = $achievement->rule;
                        $result = $this->compare($studentLesson->score, $rule);
                        if ($result) {
                            $flight_hour += $studentLesson->score;
                            $kali++;
                        }
                    }
                }

                $average = $students_lesson->count() > 0  ? $flight_hour / $kali : 0;
                $total = ($average > 0 ? number_format($average, 2) : $average) . ' ' . $lesson->unit_score;
                $flight_hours = (count($score) > 0 ? $lesson->name == 'Money Ban' ? $flight_hour : $kali : 0) . ' ' . ($lesson->name == 'Money Ban' ? 'Stack' : 'Kali');

                $kkm = $lesson->achievements()->latest()->first() ? $lesson->achievements()->latest()->first()->rule()->first()->score . ' ' . $lesson->achievements()->latest()->first()->rule()->first()->unit_score : 0;
                $raport[] = ['lesson' => $lesson->name,  'kkm' => $kkm, 'flight_hours' => $flight_hours, 'average' => $total];
            }
        }
        return $raport;
    }

    public function compare($data, $rule) {
        switch ($rule->operator) {
            case '>':
                if ($data > $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<':
                if ($data < $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<=':
                if ($data <= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '>=':
                if ($data >= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            default:
            return false;

        break;
        }

        return false;
    }
}