<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Spatie\Image\Manipulations;
use Illuminate\Http\Request;
use App\RequiredDocument;
use App\StudentDocument;
use Spatie\Image\Image;
use App\Generation;
use App\Province;
use App\Student;
use App\School;
use App\City;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:student');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doc = RequiredDocument::count();
        $view = [
            'title' => __('Home'),
            'provinces' => Province::pluck('name', 'id')->toArray(),
            'cities' => City::pluck('name', 'id')->toArray(),
            'academy' => School::whereHas('statuses', function ($query) {
                                    $query->whereHas('level', function($subQuery){
                                        $subQuery->where('name', 'academy');
                                    });
                                })->pluck('name', 'id')->toArray(),
            'profile' => collect(auth()->user()->student()->first()->toArray())->except(['id', 'created_at', 'updated_at', 'pivot']),
            'profilePercentage' => auth()->user()->student()->first()->ProfilePercentage,
            'document' => $doc,
            'student' => auth()->user()->student()->first(),
            'documentPercentage' => substr((auth()->user()->student()->first()->documents_count/$doc)*100, 0, 4),
        ];

        return view('student.home', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $documents = RequiredDocument::get();
        foreach ($documents as $document) {
            $this->validate($request, [
                $document->name => 'mimes:jpeg,png,jpg',
            ]);
            if($request->hasFile($document->name)){
                StudentDocument::firstOrCreate([
                    'student_id' => auth()->user()->student()->first()->id,
                    'document_id' => $document->id,
                    'photo' => $this->uploadPhoto($document, $request),
                ]);
            }
        }

        $request->merge([
            'date_of_birth' => date('Y-m-d', strtotime($request->date_of_birth)),
            'generation_id' => !empty($request->academy) ? 
                Generation::where('school_id', $request->academy)->latest()->first()->id
                : null
        ]);
        $student = Student::find(auth()->user()->student()->first()->id);
        $student->update($request->except(['identition_number']));
        $student->update(['identition_number' => $request->identition]);
        return redirect()->route('student.home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Upload photo
     * 
     * @param  \App\Teacher  $teacher
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $oldFile
     * @return string
     */
    public function uploadPhoto($modal, Request $request)
    {
        $filename = 'doc_'.date('d_m_Y_H_i_s_').md5(uniqid(rand(), true)).'.'.$request{$modal->name}->extension();
        $path = $request{$modal->name}->storeAs('public/student/document/'.$modal->name, $filename);
        return $modal->name.'/'.$filename;
    }
}
