<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $createdMessage, $updatedMessage, $deletedMessage, $noPermission, $unauthorizedMessage, $restoredMessage, $savedSettingMessage, $months;

    public function __construct()
    {
        $this->createdMessage = __('Data successfully created.');
        $this->updatedMessage = __('Data successfully updated.');
        $this->deletedMessage = __('Data successfully deleted.');
        $this->noPermission = __('You have no related permission.');
        $this->unauthorizedMessage = __('This action is unauthorized.');
        $this->restoredMessage = __('Data successfully restored.');
        $this->savedSettingMessage = __('Settings saved successfully.');

        $this->months = [
            "January" => "January", "February" => "February", "March" => "March", 
            "April" => "April", "May" => "May", "June" => "June", "July" => "July", 
            "August" => "August", "September" => "September", "October" => "October", 
            "November" => "November", "December" => "December"
        ];
    }
}
