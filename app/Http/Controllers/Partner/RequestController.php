<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PartnerRequirement;
use App\Partner;
use App\JobDesk;

class RequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = auth()->user()->partner->partnerBranches;
        $branch = $branches->first();

        $years = array_merge(PartnerRequirement::where([
           'partner_branch_id' => '5b8209b4-60d9-458d-ba0b-689eb51e918a'
        ])->orderBy('year', 'desc')->pluck('year', 'year')->toArray());
        if (! count($years) ) {
            $years = [date('Y')];
        }

        $view = [
            'title' => __('Permintaan SDM'),
            'breadcrumbs' => [
                route('partner.request.index') => __('Permintaan SDM')
            ],
            'branches' => $branches,
            'branch' => $branch,
            'years' => $years,
            'jobdesks' => JobDesk::pluck('name')->toJson(),
        ];

        return view('partner.request.index', $view);
    }

    /**
     * Get a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request) {
        if ($request->ajax()) {
            foreach (JobDesk::all() as $jobdesk) {
                $datas[$jobdesk->id] = [$jobdesk->name => $jobdesk->name];
                foreach ($this->months as $month) {
                    $result = PartnerRequirement::where([
                        'partner_branch_id' => $request->partner_branch_id,
                        'month' => $month,
                        'year' => $request->year,
                    ])->whereHas('partnerJobRequirement', function($query) use ($jobdesk) {
                        $query->where('job_desk_id', $jobdesk->id);
                    });
                    $datas[$jobdesk->id][$month] = $result->count() ? $result->first()->quota : 0;
                }
            }
            return response()->json(['status' => true, 'datas' => $datas], 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $PartnerRequirement = PartnerRequirement::where([
            'partner_branch_id' => $request->partner_branch_id,
            'month' => $request->month,
            'year' => $request->year,
        ])->whereHas('partnerJobRequirement', function($query) use ($request) {
            $query->where('job_desk_id', $request->job_desk_id);
        });
        if ($PartnerRequirement->count()) {
            $PartnerRequirement->update($request->except(['_token', 'job_desk_id']));
            $PartnerRequirement = $PartnerRequirement->first();
            // save log
            saveLog($PartnerRequirement, 'Update');
        } else {
            $PartnerRequirement = PartnerRequirement::create($request->all());
            $PartnerRequirement->partnerJobRequirement()->create(['job_desk_id' => $request->job_desk_id]);
            // save Log
            saveLog($PartnerRequirement);
        }

        return response()->json(['status' => true, 'data' => $PartnerRequirement], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
