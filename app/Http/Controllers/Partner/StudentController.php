<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\PartnerBranch;
use Illuminate\Http\Request;
use App\RequiredDocument;
use App\Province;
use App\Student;
use App\Lesson;
use App\City;
use DataTables;
use Auth;
use PDF;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => __('Student'),
            'columns' => [ __('No'), __('Name'), __('Academy'), __('Phone Number'), __('Place of Birth'), __('Date of Birth'), __('Document Progress') ]
        ];

        return view('partner.student.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $doc = RequiredDocument::count();
            $students = Student::whereHas('alocatedStudent')->get();
            return DataTables::of($students)
                ->addIndexColumn()
                ->addColumn('school', function($data) {
                    return $data->school->name;
                })
                ->addColumn('phone_number', function($data) {
                    return $data->phone_number
                                ? '(+62) '.$data->phone_number
                                : '-' ;
                })
                ->addColumn('place_of_birth', function($data) {
                    return $data->place_of_birth
                                ? $data->place_of_birth
                                : '-' ;
                })
                ->addColumn('document_progress', function($data) use ($doc) {
                    $percentage = substr(($data->documents_count/$doc)*100, 0, 4);
                    return '<div class="progress" data-height="20" data-toggle="tooltip" title="" data-original-title="' . $percentage . '%" style="height: 20px;">
                                <div class="progress-bar bg-warning text-dark" data-width="' . $percentage . '" style="width: ' . $percentage . '%;">'. $percentage .'%</div>
                         
                                </div>';
                })
                ->addColumn('action', function ($data) {
                    return '<a class="btn btn-icon btn-sm btn-info" href="'.route('partner.student.show', $data->id).'" title="'.__("See detail").'"><i class="fa fa-eye"></i></a> <a class="btn btn-icon btn-sm btn-warning" href="'.route('partner.student.edit', $data->id).'" title="'.__("Edit").'"><i class="fa fa-edit"></i></a>';
                })
                ->rawColumns(['DT_RowIndex', 'document_progress', 'action'])
                ->make(true);
        }
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => 'Permintaan SDM',
            'branch' => PartnerBranch::where('partner_id', auth::user()->partner->id)->pluck('name', 'id')->toArray(),
        ];

        return view('partner.student.create', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview(Student $student)
    {
        return $this->pdfVariable($student, 'preview');
    }

    /**
     * Display the import page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download(Student $student)
    {
        return $this->pdfVariable($student, 'download');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $raport = $this->raport($student);

        $view = [
            'title' => __('Student Detail'),
            'back' => route('partner.student.index'),
            'breadcrumbs' => [
                route('partner.student.index') => __('Student'),
                null => __('Detail')
            ],
            'provinces' => Province::pluck('name', 'id')->toArray(),
            'documents' => RequiredDocument::all(),
            'cities' => City::pluck('name', 'id')->toArray(),
            'data' => $student,
            'raport' => $raport
        ];
        return view('partner.student.show', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the PDF Variable page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdfVariable($student = NULL, $type = NULL)
    {
        $raport = $this->raport($student);
        $view = [
            'title' => $student->name,
            'data' => $student,
            'raport' => $raport
        ];

        $pdf = PDF::loadView('pdf.raport', $view);

        $pdf->setPaper('A4', 'potrait');
        if ($type == 'download') {
            return $pdf->download(str_replace(' ', '-', strtolower($student->name)).'-raport-'.date('d-m-Y-h-m-s').'.pdf');
        }
        elseif ($type == 'preview') {
            return $pdf->stream();
        }
    }
    
    /**
     * Display the Raport page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function raport(Student $student)
    {
        $students = Student::where('id', $student->id)->first();
        $lessons = Lesson::where('name', '!=', 'Melipat Kertas')->get();
        $raport = [];
        if ($students) {
            foreach ($lessons as $lesson) {
                $students_lesson = $students->lesson()->whereHas('duration', function($query) use($lesson){
                    $query->where('lesson_id', $lesson->id);
                });
                
                $score = $students_lesson->get()->sortBy('score')->pluck('score')->toArray();
                if($students_lesson->get()){
                    $flight_hour = 0;
                    $kali = 0;
                    $count_average = [];
                    foreach($students_lesson->get() as $studentLesson){
                        $achievement = $studentLesson->lesson->achievements()->whereHas('rules', function($query){
                            $query->where('implemented_at', '<', now())->orderBy('implemented_at', 'desc');
                        })->first();
                        $rule = $achievement->rule;
                        $result = $this->compare($studentLesson->score, $rule);
                        if ($result) {
                            $flight_hour += $studentLesson->score;
                            $kali++;
                        }
                    }
                }

                $average = $students_lesson->count() > 0  ? $flight_hour / $kali : 0;
                $total = ($average > 0 ? number_format($average, 2) : $average) . ' ' . $lesson->unit_score;
                $flight_hours = (count($score) > 0 ? $lesson->name == 'Money Ban' ? $flight_hour : $kali : 0) . ' ' . ($lesson->name == 'Money Ban' ? 'Stack' : 'Kali');

                $kkm = $lesson->achievements()->latest()->first()->rule()->first()->score . ' ' . $lesson->achievements()->latest()->first()->rule()->first()->unit_score;
                $raport[] = ['lesson' => $lesson->name,  'kkm' => $kkm, 'flight_hours' => $flight_hours, 'average' => $total];
            }
        }
        return $raport;
    }

    public function compare($data, $rule) {
        switch ($rule->operator) {
            case '>':
                if ($data > $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<':
                if ($data < $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<=':
                if ($data <= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '>=':
                if ($data >= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            default:
            return false;

        break;
        }

        return false;
    }
}
