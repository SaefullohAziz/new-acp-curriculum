<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

        /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.ends_with' => 'Harap isi dengan akun Gmail',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'school_id' => [
                'required',
            ],
            'name' => [
                'required'
            ],
            'gender' => [
                'required'
            ],
            'phone_number' => [
                'required'
            ],
            'email' => [
                'required',
                'email',
                'unique:teachers,email'
            ],
            'date_of_birth' => [
                'required'
            ],
            'position' => [
                'required'
            ],
        ];
        if ($this->isMethod('post')) {
            $user = $this->route('teacher');
            $addonRules = [
                'email' => [
                    'required',
                    'email',
                    'ends_with:@gmail.com',
                    Rule::unique('teachers')->ignore($user),
                ],
                'school_id' => [
                    'required',
                ],
                'name' => [
                    'required'
                ],
                'gender' => [
                    'required'
                ],
                'phone_number' => [
                    'required'
                ],
                'date_of_birth' => [
                    'required'
                ],
                'position' => [
                    'required'
                ],
                'terms' => [
                    'required'
                ],
            ];
            $rules = array_merge($rules, $addonRules);
        }
        if ($this->isMethod('put')) {
            $user = $this->route('teacher');
            $addonRules = [
                'email' => [
                    'required',
                    'email',
                    'ends_with:@gmail.com',
                    Rule::unique('teachers')->ignore($user),
                ],
                'name' => [
                    'required'
                ],
                'gender' => [
                    'required'
                ],
                'phone_number' => [
                    'required'
                ],
                'date_of_birth' => [
                    'required'
                ],
                'position' => [
                    'required'
                ],
                'terms' => [
                    'required'
                ],
            ];
            $rules = array_merge($rules, $addonRules);
        }
        return $rules;
    }
}
