<?php

namespace App\Http\Requests;

use App\RequiredDocument;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required'],
            'username' => ['required', 'unique:users,username'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required','confirmed', 'min:8'],
        ];

        if ($this->isMethod('put')) {
            $addonRules = [
                'nisn' => [
                    'required', 
                    'digits:10',
                    Rule::unique('students')->ignore($student),
                ],
                'email' => [
                    'required', 
                    'email', 
                    Rule::unique('students')->ignore($student),
                ],
                'phone_number' => [
                    'required', 
                    'numeric', 
                    'digits_between:8,11', 
                    Rule::unique('students')->ignore($student),
                ],
            ];
            $rules = array_merge($rules, $addonRules);
        }
        return $rules;
    }
}