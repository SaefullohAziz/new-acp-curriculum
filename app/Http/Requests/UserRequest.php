<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'username' => [
                'required',
                'unique:users,username',
                'unique:users,username'
            ],
            'name' => [
                'required'
            ],
            'email' => [
                'required',
                'email',
                'unique:users,email'
            ]
        ];
        if ($this->isMethod('post')) {
            $addonRules = [
                'password' => [
                    'required'
                ],
                'password_confirmation' => [
                    'required',
                    'same:password'
                ],
            ];
            $rules = array_merge($rules, $addonRules);
        }
        if ($this->isMethod('put')) {
            $user = $this->route('user');

            $addonRules = [
                'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->ignore($user),
                ],
                'username' => [
                    'required',
                    Rule::unique('users')->ignore($user),
                ],
            ];
            $rules = array_merge($rules, $addonRules);
        }
        return $rules;
    }
}
