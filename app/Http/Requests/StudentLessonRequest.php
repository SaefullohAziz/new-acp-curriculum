<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\LessonClass;

class StudentLessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'score' => ['required'],
            'lesson_duration_id' => ['required']
        ];

        if (session('class_id')) {
            $class = LessonClass::find(session('class_id'));
            if ($class->materies->count() > 0)
            $additional_rule = [
                'student_lesson_class_id' => [
                    Rule::in($class->lesson->pluck('id')->toArray())
                ],
            ];
        }

        return [
            'score' => ['required'],
            'lesson_duration_id' => ['required']
        ];
    }
}
