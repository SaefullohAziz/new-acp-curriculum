<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'student':
                if (Auth::guard('student')->check()) {
                    return redirect()->route('student.home');
                }
                break;
            
            case 'partner':
                if (Auth::guard('partner')->check()) {
                    return redirect()->route('partner.home');
                }
                break;

            case 'teacher':
                if (Auth::guard('teacher')->check()) {
                    return redirect()->route('teacher.home');
                }
                break;

            case 'admin':
                if (Auth::guard('admin')->check()) {
                    return redirect()->route('admin.home');
                }
                break;

            default:
                if (Auth::guard()->check()) {
                    return redirect()->route('home');
                }
                break;
        }

        return $next($request);
    }
}
