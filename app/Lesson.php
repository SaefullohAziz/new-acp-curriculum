<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Lesson extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'lesson_type_id', 'status_id', 'name', 'unit_score', 'detail'
    ];

    public function lessonType()
    {
        return $this->belongsTo('App\LessonType');
    }

    public function achievements()
    {
        return $this->hasMany('App\LessonAchievement');
    }

    public function status() 
    {
        return $this->belongsTo('App\Status');
    }

    public function durations()
    {
        return $this->hasMany('App\LessonDuration');
    }

    public function students() {
        return $this->hasManyThrough('App\StudentLesson', 'App\LessonDuration', 'lesson_id', 'lesson_duration_id');
    }

    public function getDurationId()
    {
        return $this->durations()->count()
            ? $this->durations()->where('implemented_at', '<', now())
                ->orderBy('implemented_at', 'desc')->first()->id 
            : null;
    }
}
