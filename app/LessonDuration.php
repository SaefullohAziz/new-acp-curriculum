<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class LessonDuration extends Model
{
    use Uuids;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'lesson_id', 'type', 'duration', 'duration_unit', 'implemented_at'
    ];

    public function lesson()
    {
        return $this->belongsTo('App\Lesson');
    }
}
