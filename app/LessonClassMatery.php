<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class LessonClassMatery extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'lesson_class_id', 'lesson_id'
    ];

    public function lessonClass() {
        return $this->belongsTo('App\LessonClass');
    }

    public function lesson() {
        return $this->belongsTo('App\Lesson');
    }
}
