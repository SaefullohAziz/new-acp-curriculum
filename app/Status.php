<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Status extends Model
{
	use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'level_id', 'intention'
    ];
    
    public function students() {
    	return $this->belongToMany('App\Student', 'students')->using('App\StudentStatus')->withTimestamps()->withPivot('created_at');
    }

    public function permissions() {
    	return $this->belongToMany('App\Permission', 'status_permissions')->using('App\StatusPermission')
    			->withTimestamps()->withPivot('created_at');
    }

    public function schoolStatuses() {
        return $this->hasMany('App\SchoolStatus');
    }

    public function schools() {
    	return $this->hasManyThrough('App\School', 'App\SchoolStatus', 'status_id', 'id', 'id', 'school_id');
    }

    public function level() {
    	return $this->belongsTo('App\Level');
    }
    
    // /**
    //  * Get the avatar.
    //  *
    //  * @param  string  $value
    //  * @return string
    //  */
    // public function getNameAttribute() 
    // {
    //     return $this->attributes['name'] . ' (' . $this->level->slug . ')';
    // }

    public function getLevelNameAttrbute() {
        return $this->level->name;
    }
}
