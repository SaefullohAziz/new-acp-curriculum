<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class AlocatedStudent extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'partner_id', 'student_id', 'job_desk_id', 'partner_branch_id', 'month', 'year'
    ];

    public function student() {
        return $this->hasOne('App\Student');
    }

    public function PartnerBranch() {
        return $this->belongsTo('App\PartnerBranch');
    }
}
