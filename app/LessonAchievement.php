<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class LessonAchievement extends Model
{
	use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'lesson_id',
    ];

    public function lesson()
    {
        return $this->belongsTo('App\Lesson');
    }

    public function rules() {
        return $this->hasMany('App\LessonAchievementRule');
    }

    public function rule() {
        return $this->hasOne('App\LessonAchievementRule')
            ->where('implemented_at', '<', now())
            ->orderBy('implemented_at', 'desc');
    }
}
