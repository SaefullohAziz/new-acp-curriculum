<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Training extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug'
    ];

    public function batches() {
    	return $this->hasMany('App\TrainingBatch');
    }

    public function teacherTranings() {
    	return $this->belongsToMany('App\TeacherTraining', 'training_batches')->using('App\TrainingBatch');
    }

    public function teachers() {
    	return $this->teacherTraining()->teachers();
    }
}
