<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Datakrama\Eloquid\Traits\Uuids;

class TeacherTraining extends Pivot
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'teacher_id', 'training_batch_id'
    ];
    
    public function teachera() {
    	return $this->hasMany('App\Teacher');
    }

    public function trainingBatch() {
    	return $this->hasOne('App\trainingBatch');
    }
}
