<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Island extends Model
{
    use Uuids;
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get the province for the island.
     */
    public function provinces()
    {
        return $this->hasMany('App\Province');
    }
    
}
