<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class School extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id', 'type', 'name', 'address', 'since', 'school_phone_number', 'school_email', 'school_web', 'total_student', 'department', 'iso_certificate', 'headmaster_name', 'headmaster_phone_number', 'headmaster_email', 'proposal', 'reference', 'document', 'notif', 'code', 'zip_code'
    ];

	public function generations() {
		return $this->hasMany('App\Generation');
	}

	public function teachers() {
		return $this->hasMany('App\Teacher');
	}

	public function students() {
		return $this->hasManyThrough('App\Student', 'App\Generation', 'school_id', 'generation_id');
	}

	public function schoolStatuses() {
		return $this->hasMany('App\SchoolStatus');
	}

	public function statuses() {
		return $this->belongsToMany('App\Status', 'school_statuses')->using('App\SchoolStatus')->withTimestamps()->withPivot('created_at');
	}

	public function status() {
		return $this->statuses()->latest();
	}

	/**
     * The status that belong to the school.
     */
    public function statusLast()
    {
        return $this->statuses()->whereRaw('NOT EXISTS (SELECT 1 FROM school_statuses t2 WHERE t2.school_id = school_statuses.school_id AND t2.created_at > school_statuses.created_at)');
    }

	public function pics() {
		return $this->hasManyThrough('App\Teacher', 'App\SchoolPic', 'school_id', 'id', 'id', 'teacher_id');
	}

	public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function schoolPic()
    {
        return $this->hasMany('App\SchoolPic');
    }

    /**
     * Scope a query to only include specific school of given level.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByLevel($query, $level)
    {
        return $query->whereHas('statusLast', function ($subQuery) use ($level) {
            $subQuery->where('statuses.level_id', $level);
        });
    }
}
