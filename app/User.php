<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Datakrama\Eloquid\Traits\Uuids;

class User extends Authenticatable
{
    use Uuids, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'avatar', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {
        return $this->hasOneThrough('App\Role', 'App\UserRole', 'user_id', 'id', 'id', 'role_id');
    }

    public function userRole() {
        return $this->hasOne('App\UserRole');
    }

    public function studentAccount() {
        return $this->hasOne('App\StudentAccount');
    }

    public function teacherAccount() {
        return $this->hasOne('App\TeacherAccount');
    }

    public function partnerAccount() {
        return $this->hasOne('App\PartnerAccount');
    }

    public function student() {
        return $this->hasOneThrough('App\Student', 'App\StudentAccount', 'user_id', 'id', 'id', 'student_id');
    }

    public function teacher() {
        return $this->hasOneThrough('App\Teacher', 'App\TeacherAccount', 'user_id', 'id', 'id', 'teacher_id');
    }

    public function partner() {
        return $this->hasOneThrough('App\Partner', 'App\PartnerAccount', 'user_id', 'id', 'id', 'partner_id');
    }

    /**
     * Main query for listing
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public static function get(Request $request)
    {
        return Self::all();
    }

    /**
     * Show user list for datatable
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public static function list(Request $request)
    {
        return self::get($request);
    }

    /**
     * Get the user's avatar.
     *
     * @param  string  $value
     * @return string
     */
    public function getAvatarAttribute()
    {
        if ($this->attributes['avatar'] == 'default.png' || $this->attributes['avatar'] == null) {
            return '/img/avatar/default.png';
        }else{
            if (auth()->guard('admin')->check()) {
                return '/storage/admin/photo/'.$this->attributes['avatar'];
            }elseif(auth()->guard('teacher')->check()){
                return '/storage/teacher/photo/'.$this->attributes['avatar'];
            }elseif(auth()->guard('partner')->check()){
                return '/storage/partner/photo/'.$this->attributes['avatar'];
            }elseif(auth()->guard('student')->check()){
                return '/storage/student/photo/'.$this->attributes['avatar'];
            }else{
                return '/storage/student/photo/'.$this->attributes['avatar'];
            }
        }
    }

    /** 
     * Get the permission result
     * 
     * @param string $table, $action
     * @return bool $result
     */
    public function should($table, $action = 'create') {
        return $this->role->permissions()->where([
            'table' => 'student_statuses',
            'action' => $action
        ])->count();
    }
}
