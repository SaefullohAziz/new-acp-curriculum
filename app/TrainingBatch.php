<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class TrainingBatch extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'training_id', 'number'
    ];
    
    public function training() {
    	return $this->hasOne('App\Training', 'id', 'training_id');
    }

    public function teachers() {
    	return $this->belongsToMany('App\Teachers', 'teacher_trainings')->using('App\TeacherTraining')->withTimestamps()->withPivot('crated_at');
    }
}
