<?php 

use Illuminate\Http\Request;
use App\ApplicationLog;

if ( ! function_exists('saveLog')) {
    /**
     * Save activity log
     *
     * @param
     * @return
     */
    function saveLog($data, $action = 'Create')
    {
        return ApplicationLog::Create([
            'user_id' => auth()->id(),
            'data_id' => $data->id,
            'action' => $action,
            'table_name' => $data->getTable(),
            'relation_name' => rtrim($data->getTable(), 's'),
            'old_data' => $action == 'Update' || $action == 'Delete' ? json_encode($data->getOriginal) : null,
            'new_data' => $action == 'Create' || $action == 'Update' ? json_encode($data->toArray()) : null,
        ]);
    }
}