<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class StudentLessonAchievement extends Model
{
    use Uuids;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id', 'lesson_achievement_rule_id'
    ];

    public function student() {
    	return $this->belongsTo('App\Student');
    }

    // public function studentLesson() {
    //     $this->belongsTo('App\Student');
    // }

    public function lessonAchievementRule() {
    	return $this->belongsTo('App\LessonAchievementRule');
    }
}
