<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class RequiredDocument extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'order', 'point'
    ];
    
    public function studentDocument() {
    	return $this->hasMany('App\StudentDocument');
    }
}
