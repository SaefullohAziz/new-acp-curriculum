<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class City extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'code', 'province_id', 'umr'
    ];

    public function schools() {
    	return $this->hasMany('App\School');
    }

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function scopeGetByProvinceName($query, $name)
    {
        return $query->whereHas('province', function ($subQuery) use ($name) {
            $subQuery->where('name', $name);
        });
    }
}
