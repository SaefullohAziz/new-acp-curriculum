<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class PartnerNearestSchool extends Model
{
    use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'partner_id', 'school_id', 'distance'
    ];
    
}
