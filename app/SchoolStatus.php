<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Datakrama\Eloquid\Traits\Uuids;

class SchoolStatus extends Pivot
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id', 'status_id'
    ];

	public function schools() {
		return $this->belongsTo('App\School');
	}

	public function statuses() {
		return $this->belongsTo('App\Status');
	}
}
