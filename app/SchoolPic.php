<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class SchoolPic extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id', 'teacher_id'
    ];

    public function teacher() {
    	return $this->belongsTo('App\Teacher');
    }
}
