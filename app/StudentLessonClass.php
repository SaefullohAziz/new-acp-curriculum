<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class StudentLessonClass extends Model
{
    use Uuids;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id', 'lesson_class_id'
    ];

    public function student() {
        return $this->belongsTo('App\Student');
    }

    public function class() {
        return $this->belongsTo('App\LessonClass');
    }
}
