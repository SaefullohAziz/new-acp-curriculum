<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class ApplicationLog extends Model
{
	use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id', 'data_id', 'action', 'table_name', 'relation_name', 'old_data', 'new_data'
    ];

    /**
     * The data that will loaded.
     *
     * @var array
     */
    protected $with = [
       'city', 'generation', 'table_name', 'relation_name', 'old_data', 'new_data'
    ];
    
    public function city() {
    	return $this->hasOne('App\City', 'id', 'data_id');
    }

    public function generation() {
    	return $this->hasOne('App\Generation', 'id', 'data_id');
    }

    public function lesson() {
    	return $this->hasOne('App\Lesson', 'id', 'data_id');
    }

    public function lessonSkill() {
    	return $this->hasOne('App\LessonSkill', 'id', 'data_id');
    }

    public function level() {
    	return $this->hasOne('App\Level', 'id', 'data_id');
    }

    public function partner() {
    	return $this->hasOne('App\Partner', 'id', 'data_id');
    }

    public function partnerAccount() {
    	return $this->hasOne('App\PartnerAccount', 'id', 'data_id');
    }

    public function permission() {
    	return $this->hasOne('App\Permission', 'id', 'data_id');
    }

    public function province() {
    	return $this->hasOne('App\Province', 'id', 'data_id');
    }

    public function psychologicalTestResult() {
    	return $this->hasOne('App\PsychologicalTestResult', 'id', 'data_id');
    }

    public function requiredDocument() {
    	return $this->hasOne('App\RequiredDocument', 'id', 'data_id');
    }

    public function role() {
    	return $this->hasOne('App\Role', 'id', 'data_id');
    }

    public function rolePermission() {
    	return $this->hasOne('App\RolePermission', 'id', 'data_id');
    }

    public function school() {
    	return $this->hasOne('App\School', 'id', 'data_id');
    }

    public function schoolStatus() {
    	return $this->hasOne('App\SchoolStatus', 'id', 'data_id');
    }

    public function Skill() {
    	return $this->hasOne('App\Skill', 'id', 'data_id');
    }

    public function skillAchiviementRule() {
    	return $this->hasOne('App\SkillActiviementRule', 'id', 'data_id');
    }

    public function status() {
    	return $this->hasOne('App\Status', 'id', 'data_id');
    }

    public function studentAccount() {
    	return $this->hasOne('App\StudentAccount', 'id', 'data_id');
    }

    public function studentDocument() {
    	return $this->hasOne('App\Province', 'id', 'data_id');
    }

    public function studentLesson() {
    	return $this->hasOne('App\StudentLesson', 'id', 'data_id');
    }

    public function studentStatus() {
    	return $this->hasOne('App\StudentStatus', 'id', 'data_id');
    }

    public function teacher() {
    	return $this->hasOne('App\Teacher', 'id', 'data_id');
    }

    public function teacherAccount() {
    	return $this->hasOne('App\teacherAccount', 'id', 'data_id');
    }

    public function teacherTraining() {
    	return $this->hasOne('App\teacherTraining', 'id', 'data_id');
    }

    public function training() {
    	return $this->hasOne('App\Training', 'id', 'data_id');
    }

    public function trainingBatch() {
    	return $this->hasOne('App\TrainingBatch', 'id', 'data_id');
    }

    public function user() {
    	return $this->hasOne('App\user', 'id', 'data_id');
    }

    public function userRole() {
    	return $this->hasOne('App\UserRole', 'id', 'data_id');
    }
}
