<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Generation extends Model
{
	use Uuids;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'school_id', 'school_year', 'number'
    ];

    public function school() {
    	return $this->belongsTo('App\School');
    }

    public function students() {
    	return $this->hasMany('App\Student');
    }

    public function lessonClasses() {
        return $this->hasMany('App\LessonClass');
    }

    public function getNameAttribute() 
    {
        return 'Angkatan (' . $this->attributes['number'] . ')';
    }
}
