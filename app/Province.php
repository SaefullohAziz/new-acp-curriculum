<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datakrama\Eloquid\Traits\Uuids;

class Province extends Model
{
	use Uuids;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'island_id', 'name', 'abbreviation'
    ];
    
    public function cities()
    {
        return $this->hasMany('App\City');
    }

    /**
     * Get the island that owns the province.
     */
    public function island()
    {
        return $this->belongsTo('App\Island');
    }
}
