<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Student;
use App\User;
use App\Role;

class StudentAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$students = Student::get();
    	$no = 0;
        foreach ($students as $student) {
            $explode = explode(' ', $student->name);
            $email = collect($explode)->count();
            $user = User::firstOrCreate([
                'username' => $student->phone_number,
                'name' => $student->name,
                'email' => $explode[$email-1] . '_' . $no++ . '@example.com',
                'password' => Hash::make('!Indonesia!'),
            ]);
            $user->userRole()->firstOrCreate(['role_id' => Role::where('name', 'admin')->first()->id]);
            $user->studentAccount()->firstOrCreate(['student_id' => $student->id]);
        }
    }
}
