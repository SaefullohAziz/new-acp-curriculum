<?php

use Illuminate\Database\Seeder;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'name' => 'Abacus Cash Solution',
                'slug' => 'ACS',
                'branch' => [
                    [
                        'name' => 'Abacus Cash Solution Bali',
                        'address' => 'Jl. Wayan Gebyag No. 2, Kerobokan, Kuta Utara, Denpasar'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Bandung',
                        'address' => 'Jl Terusan Jakarta No 57 Antapani Bandung'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Ciamis',
                        'name' => 'Jl. Kawali No. 204 Baregbeg-Ciamis'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Kediri',
                        'address' => 'Jl. Raya Kediri Kertosono Km 5 No.36 Rt 02 Rw 03 Dsn. Susuhan Kel. Gampeng Kec. Gampeng Rejo Kab. Kediri'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Makassar',
                        'address' => 'Jl. Sultan Alauddin No. 105 B, Mannuruki/Tamalate, Makassar'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Malang',
                        'address' => 'Jl Dr Wahidin No 26 Lawang, Malang'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Medan',
                        'address' => 'Jl.Beringin Komplek Wartawan No 6,Kelurahan Pulo Brayan Darat Ii, Medan'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Rawamangun',
                        'address' => 'Jl. Cipinang Baru Utara No28, Jakarta Timur'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Solo',
                        'address' => 'Jl.Setiabudi No.11. Gilingan Banjarsari, Surakarta'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Sukabumi',
                        'address' => 'Jl. Sriwidari No 57 Sukabumi'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Surabaya',
                        'address' => 'Jl. Anjasmoro No. 14 Surabaya'
                    ],
                    [
                        'name' => 'Abacus Cash Solution Yogyakarta',
                        'address' => 'Jl.Glagahsari No.103 Rt.024/26 Warungboto Tegal Catak Umbulharjo Yogyakarta'
                    ],
                    [
                        'name' => 'Abacus Cash Soultion Sidoarjo',
                        'address' => 'Jl Bawean No 18, Kelurahan Wadungasih Kecamatan  Buduran, Sidoarjo'
                    ],
                    [
                        'name' => 'Abacus Cash Soultion Subang',
                        'address' => '-'
                    ],
                ]
            ],
            [
                'name' => 'Abacus Dana Pensiuntama',
                'slug' => 'ADP',
                'branch' => [
                    [
                        'name' => 'Abacus Dana Pensiuntama',
                        'address' => 'Jl Suci No 14. Kel. Susukan, Kec Ciracas Jakarta Timur'
                    ],
                    [
                        'name' => 'Abacus Dana Pensiuntama Alam Sutera',
                        'address' '-'
                    ],
                ]
            ],
            [
                'name' => 'Advantage SCM',
                'slug' => 'ACSM',
                'branch' => [
                        [
                            'name' => 'Advantage SCM Balikpapan',
                            'address' => 'Jl. Markoni Atas Rt.03 No.26 Kel.Damai Kec. Balikpapan Selatan'
                        ], 
                        [
                            'name' => 'Advantage SCM Banjarmasin',
                            'address' => 'Jl. Simpang Gusti Iv No 85 Kayu Tangi Banjarmasin Utara'
                        ], 
                        [
                            'name' => 'Advantage SCM Batam',
                            'address' => 'Perumahan Rosedale Blok E No 88, Batam'
                        ], 
                        [
                            'name' => 'Advantage SCM Jakarta',
                            'address' => 'Jl Cideng Barat 48 - 49 Jakarta Pusat'
                        ], 
                        [
                            'name' => 'Advantage SCM Jambi',
                            'address' => 'Jl.Paruhutan Lubis,Lorong Pancasila No.08 Jambi'
                        ], 
                        [
                            'name' => 'Advantage SCM Jember',
                            'address' => 'Jl. Mastrip Ix, No 54, Jember'
                        ], 
                        [
                            'name' => 'Advantage SCM Karawang',
                            'address' => 'Jl.Karang No.23/24 Kelurahan Nagasari Kec.Karawang Barat'
                        ], 
                        [
                            'name' => 'Advantage SCM Lampung',
                            'address' => 'Jl.Cipto Mangunkusomo No.69,Lk.Ii/Rt.034,Kel.Kupang Teba,Kec.Teluk Betung Utara, Bandar Lampung'
                        ], 
                        [
                            'name' => 'Advantage SCM Manado',
                            'address' => 'Jl. 17 Agustus Lingkungan V.  Manado'
                        ], 
                        [
                            'name' => 'Advantage SCM Mataram',
                            'address' => 'Jl. Gendang Beleq No 10 Karang Sukun Mataram'
                        ], 
                        [
                            'name' => 'Advantage SCM Medan',
                            'address' => 'Jl. Setia Budi Komplek Tasbi Blok Hh No.62 Medan'
                        ], 
                        [
                            'name' => 'Advantage SCM Padang',
                            'address' => 'Jl. Ujung Gurun Komp Pondok Mungil No C5. Kel Ujung Gurun. Kec Padang Barat'
                        ], 
                        [
                            'name' => 'Advantage SCM Palembang',
                            'address' => 'Jl. Kapten Anwar Arsyad 71A Palembang'
                        ], 
                        [
                            'name' => 'Advantage SCM Pekanbaru',
                            'address' => 'Jl.Garuda Sakti No.19 Kel. Labuh Baru Timur Kec. Payung Sekaki Pekanbaru (Riau)'
                        ], 
                        [
                            'name' => 'Advantage SCM Petukangan',
                            'address' => 'Jl.Masjid Darul Fallah No 5, Jakarta Selatan'
                        ], 
                        [
                            'name' => 'Advantage SCM Pontianak',
                            'address' => 'Jl. Natuna No. 74 Akcaya Pontianak Selatan'
                        ], 
                        [
                            'name' => 'Advantage SCM Purwokerto',
                            'address' => 'Jl. Martadiredja Ii No.51A Purwokerto'
                        ], 
                        [
                            'name' => 'Advantage SCM Rawamangun',
                            'address' => 'Cipinang Baru Utara No 24 Rt 011/002, Jakarta Timur'
                        ], 
                        [
                            'name' => 'Advantage SCM Samarinda',
                            'address' => 'Jl.Juanda 1B No.36A Kel.Air Hitam Kec.Samarinda Ulu Kaltim.'
                        ], 
                        [
                            'name' => 'Advantage SCM Serang',
                            'address' => 'Link. Sumur Putat No 8 Rt 01 Rw 02, Serang'
                        ], 
                        [
                            'name' => 'Advantage SCM Singkawang',
                            'address' => 'Kel. Pasiran Kec. Singkawang Barat – Kalimantan Barat'
                        ], 
                        [
                            'name' => 'Advantage SCM Surabaya',
                            'address' => 'Jl. Tropodo 1 No 281, Sidoarjo'
                        ], 
                        [
                            'name' => 'Advantage SCM Tegal',
                            'address' => 'Jl. Sultan Hasanudin Rt03/04 Kelurahan Kalinyamat Wetan - Tegal Selatan'
                        ], 
                ],
            ],
            [
                'name' => 'Andalan Arthalestari',
                'slug' => 'ANDALAN',
                'branch' => [
                    [
                        'name' => 'Andalan Arthalestari Bali',
                        'address' => 'Jl.Tukad Citarum No.2Bb, Denpasar'
                    ], 
                    [
                        'name' => 'Andalan Arthalestari Bandung',
                        'address' => 'Jl. Karasak Lama No.8, Bandung'
                    ], 
                    [
                        'name' => 'Andalan Arthalestari Jakarta',
                        'address' => 'Jl. Kemayoran Ketapang No.18, Jakarta Pusat'
                    ], 
                    [
                        'name' => 'Andalan Arthalestari Makassar ',
                        'address' => 'Jl. Ap Pettarani Blok E27 No.5 Makassar'
                    ], 
                    [
                        'name' => 'Andalan Arthalestari Palembang',
                        'address' => 'Jl. Sersan Sani No.988 Kemuning, Palembang'
                    ], 
                    [
                        'name' => 'Andalan Arthalestari Semarang',
                        'address' => 'Jl. Brigjend Sudiarto No. 568A  Semarang'
                    ], 
                    [
                        'name' => 'Andalan Arthalestari Surabaya',
                        'address' => 'Jl. Semolowaru No 48 Surabaya'
                    ], 
                ]
            ],
            [
                'name' => 'Armorindo Artha',
                'slug' => 'ARMORINDO',
                'branch' => [
                    [
                        'name' => 'Armorindo Artha Jakarta',
                        'address' => 'Jl. Palmerah Utara Raya No. 32A , Palmerah, Jakarta'
                    ], 
                    [
                        'name' => 'Armorindo Artha Rawamangun',
                        'address' => 'Jl. Balai Pustaka Raya No. 5, Rawamangun,  Jakarta Timur'
                    ], 
                    [
                        'name' => 'Armorindo Artha Tangerang',
                        'address' => 'Jl. Bumi Mas Raya No. A6-A7, Cikokol, Tangerang'
                    ], 
                ]
            ],
            [
                'name' => 'Tunas Artha Gardatama',
                'slug' => 'TAG',
                'branch' => [
                    [
                        'name' => 'Tunas Artha Gardatama Bandung',
                        'address' => 'Jl. Cibodas Raya Kampung Parakansaat Rt. 01 / Rw. 023 No. A46 Bandung'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Bekasi',
                        'address' => 'Jl.Cipete Raya Nomor 29, Rt 003/ Rw 005 Kelurahan Mustika Sari, Kecamatan Mustika Jaya, Bekasi'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Bintaro',
                        'address' => 'Jl. Rc Veteran No.100. Bintaro Jak Sel'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Cirebon',
                        'address' => 'Jl. Pegambiran No 76 Kertasemboja Cirebon'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Jakarta',
                        'address' => 'Jl. Lenteng Agung Raya No. 7 Jagakarsa Jakarta Selatan'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Jember',
                        'address' => 'Jl. Danau Toba No. 71, Kelurahan Tegal Gede, Kecamatan Sumbersari, Jember,  68126.'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Kediri',
                        'address' => 'Jl. Raya Maron Rt 003 / Rw 008, Ds Maron Kec Banyakan Kab Kediri'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Madiun',
                        'address' => 'Jl. Kantil No 23, Rt 01/ Rw 01, Kelurahan, Munggut, Kecamatan Wungu, Kabupaten Madiun, Jawa Timur, 63181.'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Malang',
                        'address' => 'Jl. Mawar 2 No 2 Asrikaton Pakis Malang'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Pangkal Pinang',
                        'address' => 'Jl. Sungai Selan, Gg. Sriwijaya  No. 241  Kec. Rangkui Pangkal Pinang'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Senopati',
                        'address' => 'Jl.Purnawarman No.4 Senopati Keb.Baru, Jakarta Selatan'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Tangerang',
                        'address' => 'Jl. Delta Raya Perum No 291-292 Karawaci Tangerang'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Tanjung Pinang',
                        'address' => 'Jl. Sulaiman Abdullah No.33 Tanjung Pinang, Kepri'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Ambon',
                        'address' => '-'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Ternate',
                        'address' => '-'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Denpasar',
                        'address' => '-'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Yogyakarta',
                        'address' => '-'
                    ], 
                    [
                        'name' => 'Tunas Artha Gardatama Solo',
                        'address' => '-'
                    ], 
                ]
            ],
        ])->each(function ($item, $key) {
            PartnerBranch::updateOrCreate(collect($item)->except('statuses')->toArray())
                ->statuses()->createMany($item['statuses']);
        });
    }
}
