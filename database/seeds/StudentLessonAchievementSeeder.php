<?php

use Illuminate\Database\Seeder;
use App\StudentLesson;
use App\StudentLessonAchievement;

class StudentLessonAchievementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $studentLessons = StudentLesson::whereHas('student', function($query){
          $query->whereHas('alocatedStudent')
          ->orWhereHas('statuses', function($query){
              $query->where('name', 'Complete Achievement');
          });
        })->get();
     foreach ($studentLessons as $studentLesson) {
         $achievements = $studentLesson->lesson->achievements;
         foreach ($achievements as $achievement) {
             $rule = $achievement->rule;
             // if ( $rule->type == 'spesific_score' ) {
              // if ($studentLesson->student->whereHas('lessonAchievement')->count == 0 && $rule->whereHas('studentLessonAchievement') == 0 ) {
                  // $result = $this->compare($studentLesson->score, $rule);
                  // if ($result) {
                      StudentLessonAchievement::firstOrCreate([
                          'student_id' => $studentLesson->student_id,
                          'lesson_achievement_rule_id' => $rule->id
                      ]);
                  // }
              // }

             // } elseif( $rule->type == 'flight_hours' ) {
             //     $studentFlightHours = StudentLesson::with('duration')->whereHas('duration', function($query) use ($studentLesson) {
             //         $query->where('lesson_id', $studentLesson->lesson->id);
             //     })->get();
             //  // if ($studentLesson->student->whereHas('lessonAchievement')->count == 0 && $rule->whereHas('studentLessonAchievement') == 0) {
             //      if ( $rule->unit_score == $studentLesson->duration->duration_unit) {
             //          $result = $this->compare($studentFlightHours->sum('duration.duration'), $rule);
             //      }

             //      if ($result) {
             //          StudentLessonAchievement::firstOrCreate([
             //              'student_id' => $studentLesson->student_id,
             //              'lesson_achievement_rule_id' => $rule->id
             //          ]);
             //      }
             //  // }
             // }
         }
     }
    }

    public function compare($data, $rule) {
        switch ($rule->operator) {
            case '>':
                if ($data > $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<':
                if ($data < $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '<=':
                if ($data <= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            case '>=':
                if ($data >= $rule->score) {
                    return true;                    
                }
                return false;
            break;
            
            default:
            return false;
                // session()->flash('alert-danger', 'Some achievement rule wrong!');
            break;
        }

        return false;
    }
}