<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
			[
	            'username' => 'Superadmin',
	            'name' => 'Supepadmin',
	            'email' => 'superadmin@system.com',
				'password' => Hash::make('superadmin'),
	        ],
			[
	            'username' => 'admin',
	            'name' => 'Admin',
	            'email' => 'admin@system.com',
				'password' => Hash::make('sysAdmin'),
	        ],
	    	[
				'username' => 'ahmad',
	            'name' => 'Ahmad',
	            'email' => 'ahmad@system.com',
				'password' => Hash::make('sysAdmin'),
	        ],
	        [
				'username' => 'azriel',
	            'name' => 'Azriel Fatur',
	            'email' => 'azriel@system.com',
				'password' => Hash::make('sysAdmin'),
	        ],
	    	[
				'username' => 'saeful',
	            'name' => 'Saeful',
	            'email' => 'saeful@system.com',
				'password' => Hash::make('sysAdmin'),
	        ],
	    	[
				'username' => 'bca',
	            'name' => 'bca',
	            'email' => 'bca@system.com',
				'password' => Hash::make('IndonesiaJaya'),
	        ],
	    	[
				'username' => 'esi',
	            'name' => 'esi',
	            'email' => 'esi@system.com',
				'password' => Hash::make('IndonesiaJaya'),
	        ],
		];

		$user_role = [
			'Superadmin' => 'superadmin',
			'admin' => 'admin',
			'ahmad' => 'admin',
			'azriel' => 'admin',
			'saeful' => 'admin',
			'bca' => 'bca',
			'esi' => 'esi',
		];

		foreach ($data as $insert) {
			$user = User::firstOrCreate($insert);
			$user->userRole()->create(['role_id' => Role::where('name', $user_role[$insert['username']])->first()->id]);
		}
    }
}
