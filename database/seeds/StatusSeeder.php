<?php

use Illuminate\Database\Seeder;
use App\Level;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'name' => 'Registration',
                'slug' => 'A',
                'order' => '01',
                'statuses' => [
                    [
                        'name' => 'Registration',
                        'intention' => 'school',
                        'order' => '01',
                    ],
                    [
                        'name' => 'Document',
                        'intention' => 'school',
                        'order' => '02',
                    ],
                    [
                        'name' => 'Visitation',
                        'intention' => 'school',
                        'order' => '03',
                    ],
                    [
                        'name' => 'Interview',
                        'intention' => 'school',
                        'order' => '04',
                    ],
                    [
                        'name' => 'Potential School',
                        'intention' => 'school',
                        'order' => '05',
                    ],
                    [
                        'name' => 'Registration',
                        'intention' => 'Student',
                        'order' => '01',
                    ],
                    [
                        'name' => 'Accepted',
                        'intention' => 'Student',
                        'order' => '02',
                    ],
                ]
            ],
            [
                'name' => 'Pre Academy',
                'slug' => 'B',
                'order' => '02',
                'statuses' => [
                    [
                        'name' => 'Candidates',
                        'intention' => 'school',
                        'order' => '06',
                    ], 
                    [
                        'name' => 'Training Of Fasilitator',
                        'intention' => 'school',
                        'order' => '07',
                    ],
                    [
                        'name' => 'Fasilities',
                        'intention' => 'school',
                        'order' => '08',
                    ],
                    [
                        'name' => 'Socialization',
                        'intention' => 'school',
                        'order' => '09',
                    ],
                    [
                        'name' => 'Selection',
                        'intention' => 'school',
                        'order' => '10',
                    ],
                ]
            ],
            [
                'name' => 'Academy',
                'slug' => 'C',
                'order' => '03',
                'statuses' => [
                    [
                        'name' => 'Academy',
                        'intention' => 'school',
                        'order' => '11',
                    ],
                ]
            ],
            [
                'name' => 'Seleksi',
                'slug' => 'B',
                'order' => '04',
                'statuses' => [
                    [
                        'name' => 'Seleksi',
                        'intention' => 'student',
                        'order' => '03', 
                    ],
                ]
            ],
            [
                'name' => 'Academy',
                'slug' => 'C',
                'order' => '05',
                'statuses' => [
                    [
                        'name' => 'Academy',
                        'intention' => 'student',
                        'order' => '04',
                    ],
                    [
                        'name' => 'Class',
                        'intention' => 'student',
                        'order' => '05',
                    ],
                    [
                        'name' => 'Ujikom',
                        'intention' => 'student',
                        'order' => '06',
                    ],
                    [
                        'name' => 'Intensif Class',
                        'intention' => 'student',
                        'order' => '07',
                    ],
                    [
                        'name' => 'Alocated',
                        'intention' => 'student',
                        'order' => '08',
                    ],
                ]
            ],
            [
                'name' => 'Alumnus',
                'slug' => 'C',
                'order' => '06',
                'statuses' => [
                    [
                        'name' => 'Alumnus',
                        'intention' => 'student',
                        'order' => '09',
                    ],
                    [
                        'name' => 'Reqcruit',
                        'intention' => 'student',
                        'order' => '10',
                    ],
                ]
            ],
        ])->each(function ($item, $key) {
            Level::updateOrCreate(collect($item)->except('statuses')->toArray())
                ->statuses()->createMany($item['statuses']);
        });
    }
}
