<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::all();
        $tables = collect([
            'alocated_students' => ['superadmin', 'admin'], 
            'cities' => ['superadmin'], 
            'departments' => ['superadmin', 'admin'], 
            'generations' => ['superadmin', 'admin'], 
            'islands' => ['superadmin', 'admin'], 
            'job_desks' => ['superadmin', 'admin'],
            'lessons' => ['superadmin', 'admin'], 
            'lessons_achievements' => ['superadmin', 'admin', 'bca'], 
            'lesson_achievements_rules' => ['superadmin', 'admin', 'bca'], 
            'lesson_classes' => ['superadmin', 'admin', 'bca'], 
            'lesson_class_materies' => ['superadmin'],
            'lesson_durations' => ['superadmin', 'admin', 'bca'], 
            'lesson_types' => ['superadmin', 'admin'], 
            'levels' => ['superadmin'], 
            'partners' => ['superadmin', 'admin'], 
            'partners_accounts' => ['superadmin', 'admin'], 
            'partner_branches' => ['superadmin'],
            'partner_document_requirements' => ['superadmin', 'admin'], 
            'partner_job_requirements' => ['superadmin', 'admin'], 
            'partner_nearest_schools' => ['superadmin', 'admin'], 
            'partner_requirements' => ['superadmin'],
            'permissions' => ['superadmin', 'admin'], 
            'provinces' => ['superadmin'], 
            'psychological_test_result' => ['superadmin', 'admin'], 
            'reqruited_students' => ['superadmin', 'admin'], 
            'required_documents' => ['superadmin', 'admin'], 
            'roles' => ['superadmin'],
            'role_permissions' => ['superadmin'], 
            'schools' => ['superadmin', 'admin'], 
            'school_pics' => ['superadmin', 'admin'], 
            'school_statuses' => ['superadmin', 'admin'], 
            'statuses' => ['superadmin', 'admin'], 
            'status_permission' => ['superadmin'], 
            'students' => ['superadmin'],
            'student_accounts' => ['superadmin', 'admin'], 
            'student_documents' => ['superadmin', 'admin'], 
            'student_lessons' => ['superadmin', 'admin'], 
            'student_lesson_achievements' => ['superadmin', 'admin'], 
            'student_lesson_classes' => ['superadmin'],
            'student_classes' => ['superadmin', 'admin'], 
            'student_statuses' => ['superadmin', 'admin'], 
            'teachers' => ['superadmin', 'admin'], 
            'teacher_accounts' => ['superadmin', 'admin'], 
            'teacher_trainings' => ['superadmin', 'admin'], 
            'trainings' => ['superadmin', 'admin'], 
            'training_batches' => ['superadmin', 'admin'], 
            'users' => ['superadmin', 'admin']
        ]);

        $actions = collect([
            'create', 'update', 'delete'
        ]);
        $role->each( function($role) use ($tables, $actions) {
            $roleTables = $tables->filter( function($val, $key) use ($role) {
                return in_array($role->name, $val);
            })->each( function($val, $tableName) use ($actions, $role) {
                $actions->each( function ($action) use ($tableName, $role) {
                    $permission = Permission::where([
                        'table' => $tableName,
                        'action' => $action
                    ]);
                    if ($permission->count()) {
                        $permission = $permission->first();
                        $role->rolePermission()->updateOrCreate(['permission_id' => $permission->id]);
                    }
                });
            });
        });

    }
}
