<?php

use Illuminate\Database\Seeder;
use App\RequiredDocument;

class RequiredDocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'name' => 'personal_photo',
                'order' => '01',
            ],
            [
                'name' => 'identition_number',
                'order' => '02',
            ],
            [
                'name' => 'family_card',
                'order' => '03',
            ],
            [
                'name' => 'npwp',
                'order' => '04',
            ],
            [
                'name' => 'health_card',
                'order' => '05',
            ],
            [
                'name' => 'bca_account_book',
                'order' => '06',
            ],
            [
                'name' => 'skbn',
                'order' => '07',
            ],
            [
                'name' => 'skck',
                'order' => '08',
            ],
            [
                'name' => 'diploma',
                'order' => '09',
            ],
            [
                'name' => 'sim_c',
                'order' => '10',
            ],
            [
                'name' => 'sim_a',
                'order' => '11',
            ],
            [
                'name' => 'curriculum_vitae',
                'order' => '12',
            ],
            [
                'name' => 'application_letter',
                'order' => '13',
            ],
            [
                'name' => 'statement_intership_program',
                'order' => '14',
            ],
        ];

        foreach ($datas as $record) {
            RequiredDocument::firstOrCreate($record);
        }
    }
}
