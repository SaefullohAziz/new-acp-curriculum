<?php

use Illuminate\Database\Seeder;

class DummyDatasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schools = factory(App\School::class, 20)
                                            ->create()
                                            ->each(function ($school) {
                                                $school->teachers()->createMany(factory(App\Teacher::class, 3)->make()->toArray());

                                                $school->generations()->createMany(factory(App\Generation::class, 3)->make()->toArray());
                                                
                                                foreach ($school->generations as $class) {
                                                    $class->students()->createMany(factory(App\Student::class, 5)->make()->toArray());
                                                }
                                            });	
    }
}
