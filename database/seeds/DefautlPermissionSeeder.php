<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\Status;

class DefautlPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$table = [
    		'application_log',
    		'users', 'student_account', 'partner_account', 'teacher_account',
    		'student', 'generation', 'schools', 'teachers', 'cities', 'provinces',
    		'student_documents', 'required_documents', 'student_psycological_test_results',
    		'statuses', 'student_statuses', 'school_statuses', 'levels',
    		'permission', 'status_permissions',
    		'roles', 'role_permissions', 'user_roles',
    		'trainings', 'training_batches', 'teacher_trainings',
    		'lessons', 'student_lessons', 'lesson_types', 'lesson_achiviements', 'lesson_achiviement_rules',
    		'lesson_class', 'student_lesson_classes', 'student_lesson_achiviements'
    	];

        $permission = [
        	'Create', 'Read', 'Update', 'Delete'
        ];

        $role = [
        	'Superadmin', 'admin', 'partner', 'teacher', 'student'
        ];

        $Role_permission = [
        	'Superadmin' => [
        		'application_log', 'users', 'student_account', 'partner_account', 'teacher_account',
	    		'student', 'generation', 'schools', 'teachers', 'cities', 'provinces',
	    		'student_documents', 'required_documents', 'student_psycological_test_results',
	    		'statuses', 'student_statuses', 'school_statuses', 'levels',
	    		'permission', 'status_permissions',
	    		'roles', 'role_permissions', 'user_roles',
	    		'trainings', 'training_batches', 'teacher_trainings',
	    		'lessons', 'student_lessons', 'lesson_types', 'lesson_achiviements', 'lesson_achiviement_rules',
	    		'lesson_class', 'student_lesson_classes', 'student_lesson_achiviements'	
        	]
        ];
    }
}
