<?php

use Illuminate\Database\Seeder;
use App\Partner;
use App\User;
use App\Role;

class PartnerAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partners = Partner::get();
    	$no = 0;
        foreach ($partners as $partner) {
            $explode = explode(' ', $partner->name);
            $email = collect($explode)->count();
            $user = User::firstOrCreate([
                'username' => $partner->phone_number,
                'name' => $partner->name,
                'email' => $explode[$email-1] . '_' . $no++ . '@example.com',
                'password' => Hash::make('IndonesiaJaya'),
            ]);
            $user->userRole()->firstOrCreate(['role_id' => Role::where('name', 'partner')->first()->id]);
            $user->partnerAccount()->firstOrCreate(['partner_id' => $partner->id]);
        }
    }
}
