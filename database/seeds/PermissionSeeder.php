<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = [
            'alocated_students', 'cities', 'departments', 'generations', 'islands', 'job_desks',
            'lessons', 'lessons_achievements', 'lesson_achievements_rules', 'lesson_classes', 'lesson_class_materies',
            'lesson_durations', 'lesson_types', 'levels', 'partners', 'partners_accounts', 'partner_branches',
            'partner_document_requirements', 'partner_job_requirements', 'partner_nearest_schools', 'partner_requirements',
            'permissions', 'provinces', 'psychological_test_result', 'reqruited_students', 'required_documents', 'roles',
            'role_permissions', 'schools', 'school_pics', 'school_statuses', 'statuses', 'status_permission', 'students',
            'student_statuses', 'student_accounts', 'student_documents', 'student_lessons', 'student_lesson_achievements',
            'student_lesson_classes', 'student_classes', 'teachers', 'teacher_accounts', 'teacher_trainings', 'trainings', 'training_batches',
            'users'
        ];

        $actions = [
            'create', 'update', 'delete'
        ];

        foreach ($tables as $table) {
            foreach ($actions as $action) {
                Permission::updateOrCreate([
                    'table' => $table,
                    'action' => $action
                ]);
            }
        }
    }
}
