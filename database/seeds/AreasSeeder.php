<?php

use Illuminate\Database\Seeder;
use App\Island;
use App\Province;
use App\Cities;

class AreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
        	[
        		'island' => [
        			'name' => 'Jawa',
	        		'provinces' => [
	        			[
	        				'name' => 'Banten',
	        				'abbreviation' => 'Banten',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Pandeglang',
		        					'code' => 'PDG',
		        					'umr' => 2758909,
	        					],
	        					[
		        					'name' => 'Kabupaten Lebak',
		        					'code' => 'RKB',
		        					'umr' => 2710654,
	        					],
	        					[
		        					'name' => 'Kabupaten Tangerang',
		        					'code' => 'TGR',
		        					'umr' => 4119029,
	        					],
	        					[
		        					'name' => 'Kabupaten Serang',
		        					'code' => 'SRG',
		        					'umr' => 4152887,
	        					],
	        					[
		        					'name' => 'Kota Tangerang',
		        					'code' => 'TNG',
		        					'umr' => 4119029,
	        					],
	        					[
		        					'name' => 'Kota Cilegon',
		        					'code' => 'CLG',
		        					'umr' => 4246081,
	        					],
	        					[
		        					'name' => 'Kota Serang',
		        					'code' => 'SRG',
		        					'umr' => 3773940,
	        					],
	        					[
		        					'name' => 'Kota Tangerang Selatan',
		        					'code' => 'CPT',
		        					'umr' => 4168268,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'DKI Jakarta',
	        				'abbreviation' => 'JKT',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Kepulauan Seribu',
		        					'code' => 'KSU',
		        					'umr' => 4276349,
	        					],
	        					[
		        					'name' => 'Kota Jakarta Selatan',
		        					'code' => 'KYB',
		        					'umr' => 4276349,
	        					],
	        					[
		        					'name' => 'Kota Jakarta Timur',
		        					'code' => 'CKG',
		        					'umr' => 4276349,
	        					],
	        					[
		        					'name' => 'Kota Jakarta Pusat',
		        					'code' => 'TNA',
		        					'umr' => 4276349,
	        					],
	        					[
		        					'name' => 'Kota Jakarta Barat',
		        					'code' => 'GGP',
		        					'umr' => 4276349,
	        					],
	        					[
		        					'name' => 'Kota Jakarta Utara',
		        					'code' => 'TJP',
		        					'umr' => 4276349,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'Jawa Barat',
	        				'abbreviation' => 'Jabar',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bogor',
		        					'code' => 'CBI',
		        					'umr' => 4083670,
	        					],
	        					[
		        					'name' => 'Kabupaten Sukabumi',
		        					'code' => 'SBM',
		        					'umr' => 3028531,
	        					],
	        					[
		        					'name' => 'Kabupaten Cianjur',
		        					'code' => 'CJR',
		        					'umr' => 2534798,
	        					],
	        					[
		        					'name' => 'Kabupaten Bandung',
		        					'code' => 'SOR',
		        					'umr' => 3139275,
	        					],
	        					[
		        					'name' => 'Kabupaten Garut',
		        					'code' => 'GRT',
		        					'umr' => 1961085,
	        					],
	        					[
		        					'name' => 'Kabupaten Tasikmalaya',
		        					'code' => 'SPA',
		        					'umr' => 2251787,
	        					],
	        					[
		        					'name' => 'Kabupaten Ciamis',
		        					'code' => 'CMS',
		        					'umr' => 1880654,
	        					],
	        					[
		        					'name' => 'Kabupaten Kuningan',
		        					'code' => 'KNG',
		        					'umr' => 1882642,
	        					],
	        					[
		        					'name' => 'Kabupaten Cirebon',
		        					'code' => 'SBR',
		        					'umr' => 2196416,
	        					],
	        					[
		        					'name' => 'Kabupaten Majalengka',
		        					'code' => 'MJL',
		        					'umr' => 1944166,
	        					],
	        					[
		        					'name' => 'Kabupaten Sumedang',
		        					'code' => 'SMD',
		        					'umr' => 3139275,
	        					],
	        					[
		        					'name' => 'Kabupaten Indramayu',
		        					'code' => 'IDM',
		        					'umr' => 2297931,
	        					],
	        					[
		        					'name' => 'Kabupaten Subang',
		        					'code' => 'SNG',
		        					'umr' => 2965468,
	        					],
	        					[
		        					'name' => 'Kabupaten Purwakarta',
		        					'code' => 'PWK',
		        					'umr' => 4039067,
	        					],
	        					[
		        					'name' => 'Kabupaten Karawang',
		        					'code' => 'KWG',
		        					'umr' => 4594324,
	        					],
	        					[
		        					'name' => 'Kabupaten Bekasi',
		        					'code' => 'CKR',
		        					'umr' => 4498961,
	        					],
	        					[
		        					'name' => 'Kabupaten Bandung Barat',
		        					'code' => 'NPH',
		        					'umr' => 3145427,
	        					],
	        					[
		        					'name' => 'Kabupaten Pangandaran',
		        					'code' => 'PRI',
		        					'umr' => 1860591,
	        					],
	        					[
		        					'name' => 'Kota Bogor',
		        					'code' => 'BGR',
		        					'umr' => 4169806,
	        					],
	        					[
		        					'name' => 'Kota Sukabumi',
		        					'code' => 'SKB',
		        					'umr' => 2530182,
	        					],
	        					[
		        					'name' => 'Kota Bandung',
		        					'code' => 'BDG',
		        					'umr' => 3623778,
	        					],
	        					[
		        					'name' => 'Kota Cirebon',
		        					'code' => 'CBN',
		        					'umr' => 2219487,
	        					],
	        					[
		        					'name' => 'Kota Bekasi',
		        					'code' => 'BKS',
		        					'umr' => 4589708,
	        					],
	        					[
		        					'name' => 'Kota Depok',
		        					'code' => 'DPK',
		        					'umr' => 4202105,
	        					],
	        					[
		        					'name' => 'Kota Cimahi',
		        					'code' => 'CMH',
		        					'umr' => 3139274,
	        					],
	        					[
		        					'name' => 'Kota Tasikmalaya',
		        					'code' => 'TSM',
		        					'umr' => 2264093,
	        					],
	        					[
		        					'name' => 'Kota Banjar',
		        					'code' => 'BJR',
		        					'umr' => 1831884,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'Jawa Tengah',
	        				'abbreviation' => 'Jateng',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Cilacap',
		        					'code' => 'CLP',
		        					'umr' => 2158327,
	        					],
	        					[
		        					'name' => 'Kabupaten Banyumas',
		        					'code' => 'PWT',
		        					'umr' => 1900000,
	        					],
	        					[
		        					'name' => 'Kabupaten Purbalingga',
		        					'code' => 'PBG',
		        					'umr' => 1940800,
	        					],
	        					[
		        					'name' => 'Kabupaten Banjarnegara',
		        					'code' => 'BNR',
		        					'umr' => 1748000,
	        					],
	        					[
		        					'name' => 'Kabupaten Kebumen',
		        					'code' => 'KBM',
		        					'umr' => 1835000,
	        					],
	        					[
		        					'name' => 'Kabupaten Purworejo',
		        					'code' => 'PWR',
		        					'umr' => 1845000,
	        					],
	        					[
		        					'name' => 'Kabupaten Wonosobo',
		        					'code' => 'WSB',
		        					'umr' => 1859000,
	        					],
	        					[
		        					'name' => 'Kabupaten Magelang',
		        					'code' => 'MKD',
		        					'umr' => 2042200,
	        					],
	        					[
		        					'name' => 'Kabupaten Boyolali',
		        					'code' => 'BYL',
		        					'umr' => 1942500,
	        					],
	        					[
		        					'name' => 'Kabupaten Klaten',
		        					'code' => 'KLN',
		        					'umr' => 1947821,
	        					],
	        					[
		        					'name' => 'Kabupaten Sukoharjo',
		        					'code' => 'SKH',
		        					'umr' => 1938000,
	        					],
	        					[
		        					'name' => 'Kabupaten Wonogiri',
		        					'code' => 'WNG',
		        					'umr' => 1797000,
	        					],
	        					[
		        					'name' => 'Kabupaten Karanganyar',
		        					'code' => 'KRG',
		        					'umr' => 1989000,
	        					],
	        					[
		        					'name' => 'Kabupaten Sragen',
		        					'code' => 'SGN',
		        					'umr' => 1815914,
	        					],
	        					[
		        					'name' => 'Kabupaten Grobogan',
		        					'code' => 'PWD',
		        					'umr' => 1830000,
	        					],
	        					[
		        					'name' => 'Kabupaten Blora',
		        					'code' => 'BLA',
		        					'umr' => 1834000,
	        					],
								[
		        					'name' => 'Kabupaten Rembang',
		        					'code' => 'RBG',
		        					'umr' => 1802000,
	        					],
								[
		        					'name' => 'Kabupaten Pati',
		        					'code' => 'PTI',
		        					'umr' => 1891000,
	        					],
								[
		        					'name' => 'Kabupaten Kudus',
		        					'code' => 'KDS',
		        					'umr' => 2218451,
	        					],
								[
		        					'name' => 'Kabupaten Jepara',
		        					'code' => 'JPA',
		        					'umr' => 2040000,
	        					],
								[
		        					'name' => 'Kabupaten Demak',
		        					'code' => 'DMK',
		        					'umr' => 2432000,
	        					],
								[
		        					'name' => 'Kabupaten Semarang',
		        					'code' => 'UNR',
		        					'umr' => 2715000,
	        					],
								[
		        					'name' => 'Kabupaten Temanggung',
		        					'code' => 'TMG',
		        					'umr' => 1825200,
	        					],
								[
		        					'name' => 'Kabupaten Kendal',
		        					'code' => 'KDL',
		        					'umr' => 2261775,
	        					],
								[
		        					'name' => 'Kabupaten Batang',
		        					'code' => 'BTG',
		        					'umr' => 2061700,
	        					],
								[
		        					'name' => 'Kabupaten Pekalongan',
		        					'code' => 'KJN',
		        					'umr' => 2018161,
	        					],
								[
		        					'name' => 'Kabupaten Pemalang',
		        					'code' => 'PML',
		        					'umr' => 1865000,
	        					],
								[
		        					'name' => 'Kabupaten Tegal',
		        					'code' => 'SLW',
		        					'umr' => 1896000,
	        					],
								[
		        					'name' => 'Kabupaten Brebes',
		        					'code' => 'BBS',
		        					'umr' => 1807614,
	        					],
								[
		        					'name' => 'Kota Magelang',
		        					'code' => 'MGG',
		        					'umr' => 1913321,
	        					],
								[
		        					'name' => 'Kota Surakarta',
		        					'code' => 'SKT',
		        					'umr' => 1956200,
	        					],
								[
		        					'name' => 'Kota Salatiga',
		        					'code' => 'SLT',
		        					'umr' => 2034915,
	        					],
								[
		        					'name' => 'Kota Semarang',
		        					'code' => 'SMG',
		        					'umr' => 2219487,
	        					],
								[
		        					'name' => 'Kota Pekalongan',
		        					'code' => 'PKL',
		        					'umr' => 2072000,
	        					],
								[
		        					'name' => 'Kota Tegal',
		        					'code' => 'TGL',
		        					'umr' => 1925000,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'Jawa Timur',
	        				'abbreviation' => 'Jatim',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Pacitan',
		        					'code' => 'PCT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ponorogo',
		        					'code' => 'PNG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Trenggalek',
		        					'code' => 'TRK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tulungagung',
		        					'code' => 'TLG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Blitar',
		        					'code' => 'KNR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kediri',
		        					'code' => 'KDR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Malang',
		        					'code' => 'KPN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lumajang',
		        					'code' => 'LMJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Jember',
		        					'code' => 'JMR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Banyuwangi',
		        					'code' => 'BYW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bondowoso',
		        					'code' => 'BDW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Situbondo',
		        					'code' => 'SIT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Probolinggo',
		        					'code' => 'KRS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pasuruan',
		        					'code' => 'PSR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sidoarjo',
		        					'code' => 'SDA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mojokerto',
		        					'code' => 'MJK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Jombang',
		        					'code' => 'JBG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nganjuk',
		        					'code' => 'NJK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Madiun',
		        					'code' => 'MJY',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Magetan',
		        					'code' => 'MGT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ngawi',
		        					'code' => 'NGW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bojonegoro',
		        					'code' => 'BJN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tuban',
		        					'code' => 'TBN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lamongan',
		        					'code' => 'LMG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Gresik',
		        					'code' => 'GSK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bangkalan',
		        					'code' => 'BKL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sampang',
		        					'code' => 'SPG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pamekasan',
		        					'code' => 'PMK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sumenep',
		        					'code' => 'SMP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Kediri',
		        					'code' => 'KDR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Blitar',
		        					'code' => 'BLT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Malang',
		        					'code' => 'MLG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Probolinggo',
		        					'code' => 'PBL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Pasuruan',
		        					'code' => 'PSN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Mojokerto',
		        					'code' => 'MJK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Madiun',
		        					'code' => 'MAD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Surabaya',
		        					'code' => 'SBY',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Batu',
		        					'code' => 'BTU',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'DI Yogyakarta',
	        				'abbreviation' => 'DIY',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Kulon Progo',
		        					'code' => 'WAT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bantul',
		        					'code' => 'BTL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Gunung Kidul',
		        					'code' => 'WNO',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sleman',
		        					'code' => 'SMN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Yogyakarta',
		        					'code' => 'YYK',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        		]
        		],
        	],
        	[
        		'island' => [
        			'name' => 'Kalimantan',
	        		'provinces' => [
	        			[
	        				'name' => 'Kalimantan Barat',
	        				'abbreviation' => 'Kalbar',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Sambas',
		        					'code' => 'SBS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bengkayang',
		        					'code' => 'BEK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Landak',
		        					'code' => 'NBA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mempawah',
		        					'code' => 'MPW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sanggau',
		        					'code' => 'SAG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ketapang',
		        					'code' => 'KTP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sintang',
		        					'code' => 'STG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kapuas Hulu',
		        					'code' => 'PTS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sekadau',
		        					'code' => 'SED',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Melawi',
		        					'code' => 'NGP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kayong Utara',
		        					'code' => 'SKD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kubu Raya',
		        					'code' => 'SRY',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Pontianak',
		        					'code' => 'PTK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Singkawang',
		        					'code' => 'SKW',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'Kalimantan Selatan',
	        				'abbreviation' => 'Kalsel',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Tanah Laut',
		        					'code' => 'PLI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kota Baru',
		        					'code' => 'KBR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Banjar',
		        					'code' => 'MTP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Barito Kuala',
		        					'code' => 'MRH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tapin',
		        					'code' => 'RTA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Hulu Sungai Selatan',
		        					'code' => 'KGN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Hulu Sungai Tengah',
		        					'code' => 'BRB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Hulu Sungai Utara',
		        					'code' => 'AMT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tabalong',
		        					'code' => 'TJG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tanah Bumbu',
		        					'code' => 'BLN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Balangan',
		        					'code' => 'PRN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Banjarmasin',
		        					'code' => 'BJM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Banjar Baru',
		        					'code' => 'BJB',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'Kalimantan Tengah',
	        				'abbreviation' => 'Kalteng',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Kotawaringin Barat',
		        					'code' => 'PBU',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kotawaringin Timur',
		        					'code' => 'SPT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kapuas',
		        					'code' => 'KLK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Barito Selatan',
		        					'code' => 'BNT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Barito Utara',
		        					'code' => 'MTW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sukamara',
		        					'code' => 'SKR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lamandau',
		        					'code' => 'NGB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Seruyan',
		        					'code' => 'KLP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Katingan',
		        					'code' => 'KSN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pulang Pisau',
		        					'code' => 'PPS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Gunung Mas',
		        					'code' => 'KKN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Barito Timur',
		        					'code' => 'TML',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Murung Raya',
		        					'code' => 'PRC',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Palangka Raya',
		        					'code' => 'PLK',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'Kalimantan Timur',
	        				'abbreviation' => 'Kaltim',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Paser',
		        					'code' => 'TGT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kutai Barat',
		        					'code' => 'SDW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kutai Kartanegara',
		        					'code' => 'TRG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kutai Timur',
		        					'code' => 'SGT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Berau',
		        					'code' => 'TNR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Penajam Paser Utara',
		        					'code' => 'PNJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mahakam Hulu',
		        					'code' => 'UBL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Balikpapan',
		        					'code' => 'BPP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Samarinda',
		        					'code' => 'SMR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Bontang',
		        					'code' => 'BON',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'Kalimantan Utara',
	        				'abbreviation' => 'Kalut',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Malinau',
		        					'code' => 'MLN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bulungan',
		        					'code' => 'TJS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tana Tidung',
		        					'code' => 'TDP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nunukan',
		        					'code' => 'NNK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Tarakan',
		        					'code' => 'TAR',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        		]
        		],
        	],
        	[
        		'island' => [
        			'name' => 'Kepulauan Maluku',
	        		'provinces' => [
	        			[
	        				'name' => 'Maluku',
	        				'abbreviation' => 'Maluku',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Maluku Tenggara Barat',
		        					'code' => 'SML',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Maluku Tenggara',
		        					'code' => 'TUL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Maluku Tengah',
		        					'code' => 'MSH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Buru',
		        					'code' => 'NLA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepulauan Aru',
		        					'code' => 'DOB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Seram Bagian Barat',
		        					'code' => 'DRH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Seram Bagian Timur',
		        					'code' => 'DTH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Maluku Barat Daya',
		        					'code' => 'TKR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Buru Selatan',
		        					'code' => 'NMR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Ambon',
		        					'code' => 'AMB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Tual',
		        					'code' => 'TUL',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        			[
	        				'name' => 'Maluku Utara',
	        				'abbreviation' => 'Maluku Utara',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Halmahera Barat',
		        					'code' => 'JLL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Halmahera Tengah',
		        					'code' => 'WED',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepulauan Sula',
		        					'code' => 'SNN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Halmahera Selatan',
		        					'code' => 'LBA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Halmahera Utara',
		        					'code' => 'TOB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Halmahera Timur',
		        					'code' => 'MAB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pulau Morotai',
		        					'code' => 'MTS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pulau Taliabu',
		        					'code' => 'BBG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Ternate',
		        					'code' => 'TTE',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Tidore Kepulauan',
		        					'code' => 'TDR',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        		]
        		],
        	],
        	[
        		'island' => [
        			'name' => 'Kepulauan Nusa Tenggara',
	        		'provinces' => [
	        			[
	        				'name' => 'Bali',
	        				'abbreviation' => 'Bali',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Badung',
		        					'code' => 'MGW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bangli',
		        					'code' => 'BLI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Buleleng',
		        					'code' => 'SGR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Gianyar',
		        					'code' => 'GIN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Jembrana',
		        					'code' => 'NGA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Karang Asem',
		        					'code' => 'KRA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Klungkung',
		        					'code' => 'SRP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tabanan',
		        					'code' => 'TAB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Denpasar',
		        					'code' => 'DPR',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Nusa Tenggara Barat',
	        				'abbreviation' => 'NTB',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bima',
		        					'code' => 'WHO',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Dompu',
		        					'code' => 'DPU',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lombok Barat',
		        					'code' => 'GRG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lombok Tengah',
		        					'code' => 'PYA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lombok Timur',
		        					'code' => 'SEL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lombok Utara',
		        					'code' => 'TJN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sumbawa',
		        					'code' => 'SBW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sumbawa Barat',
		        					'code' => 'TLW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Bima',
		        					'code' => 'BIM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Mataram',
		        					'code' => 'MTR',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Nusa Tenggara Timur',
	        				'abbreviation' => 'NTT',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Alor',
		        					'code' => 'KLB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Belu',
		        					'code' => 'ATB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ende',
		        					'code' => 'END',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Flores Timur',
		        					'code' => 'LRT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kupang',
		        					'code' => 'KPG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lembata',
		        					'code' => 'LWL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Malaka',
		        					'code' => 'BTN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Manggarai',
		        					'code' => 'RTG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Manggarai Barat',
		        					'code' => 'LBJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Manggarai Timur',
		        					'code' => 'BRG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nagekeo',
		        					'code' => 'MBY',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ngada',
		        					'code' => 'BJW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Rote Ndao',
		        					'code' => 'BAA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sabu Raijua',
		        					'code' => 'SBB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sikka',
		        					'code' => 'MME',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sumba Barat',
		        					'code' => 'WKB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sumba Barat Daya',
		        					'code' => 'TAM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sumba Tengah',
		        					'code' => 'WBL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sumba Timur',
		        					'code' => 'WGP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Timor Tengah Selatan',
		        					'code' => 'SOE',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Timor Tengah Utara',
		        					'code' => 'KFM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Kupang',
		        					'code' => 'KPG',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        		]
        		],
        	],
        	[
        		'island' => [
        			'name' => 'Papua',
	        		'provinces' => [
	        			[
	        				'name' => 'Papua',
	        				'abbreviation' => 'Papua',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Asmat',
		        					'code' => 'AGT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Biak Numfor',
		        					'code' => 'BIK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Boven Digoel',
		        					'code' => 'TMR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Deiyai',
		        					'code' => 'TIG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Dogiyai',
		        					'code' => 'KGM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Intan Jaya',
		        					'code' => 'SGP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Jayapura',
		        					'code' => 'JAP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Jayawijaya',
		        					'code' => 'WAM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Keerom',
		        					'code' => 'WRS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepulauan Yapen',
		        					'code' => 'SRU',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lanny Jaya',
		        					'code' => 'TOM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mamberamo Raya',
		        					'code' => 'BRM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mamberamo Tengah',
		        					'code' => 'KBK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mappi',
		        					'code' => 'KEP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Merauke',
		        					'code' => 'MRK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mimika',
		        					'code' => 'TIM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nabire',
		        					'code' => 'NAB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nduga',
		        					'code' => 'KYM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Paniai',
		        					'code' => 'ERT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pegunungan Bintang',
		        					'code' => 'OSB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Puncak',
		        					'code' => 'ILG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Puncak Jaya',
		        					'code' => 'MUL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sarmi',
		        					'code' => 'SMI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Supiori',
		        					'code' => 'SRW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tolikara',
		        					'code' => 'KBG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Waropen',
		        					'code' => 'BTW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Yahukimo',
		        					'code' => 'SMH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Yalimo',
		        					'code' => 'ELL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Jayapura',
		        					'code' => 'JAP',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Papua Barat',
	        				'abbreviation' => 'Papua Barat',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Fakfak',
		        					'code' => 'FFK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kaimana',
		        					'code' => 'KMN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Manokwari',
		        					'code' => 'MNK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Manokwari Selatan',
		        					'code' => 'RSK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Maybrat',
		        					'code' => 'AFT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pegunungan Arfak',
		        					'code' => 'ANG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Raja Ampat',
		        					'code' => 'WAS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sorong',
		        					'code' => 'AMS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sorong Selatan',
		        					'code' => 'TMB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tambrauw',
		        					'code' => 'FEF',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Teluk Bintuni',
		        					'code' => 'BTI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Teluk Wondama',
		        					'code' => 'RAS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Sorong',
		        					'code' => 'SON',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        		]
        		],
        	],
        	[
        		'island' => [
        			'name' => 'Sulawesi',
	        		'provinces' => [
	        			[
	        				'name' => 'Gorontalo',
	        				'abbreviation' => 'Gorontalo',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Boalemo',
		        					'code' => 'TMT',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kabupaten Bone Bolango',
		        					'code' => 'SWW',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kabupaten Gorontalo',
		        					'code' => 'GTO',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kabupaten Gorontalo Utara',
		        					'code' => 'KWD',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kabupaten Pohuwato',
		        					'code' => 'MAR',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kota Gorontalo',
		        					'code' => 'GTO',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Sulawesi Barat',
	        				'abbreviation' => 'Sulbar',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Majene',
		        					'code' => 'MJN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mamasa',
		        					'code' => 'MMS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mamuju',
		        					'code' => 'MAM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mamuju Tengah',
		        					'code' => 'TBD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mamuju Utara',
		        					'code' => 'PKY',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Polewali Mandar',
		        					'code' => 'PLW',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Sulawesi Selatan',
	        				'abbreviation' => 'Sulsel',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bantaeng',
		        					'code' => 'BAN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Barru',
		        					'code' => 'BAR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bone',
		        					'code' => 'WTP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bulukumba',
		        					'code' => 'BLK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Enrekang',
		        					'code' => 'ENR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Gowa',
		        					'code' => 'SGM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Jeneponto',
		        					'code' => 'JNP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepulauan Selayar',
		        					'code' => 'BEN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Luwu',
		        					'code' => 'PLP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Luwu Timur',
		        					'code' => 'MLL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Luwu Utara',
		        					'code' => 'MSB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Maros',
		        					'code' => 'MRS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pangkajene Dan Kepulauan',
		        					'code' => 'PKJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pinrang',
		        					'code' => 'PIN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sidenreng Rappang',
		        					'code' => 'SDR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sinjai',
		        					'code' => 'SNJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Soppeng',
		        					'code' => 'WNS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Takalar',
		        					'code' => 'TKA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tana Toraja',
		        					'code' => 'MAK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Toraja Utara',
		        					'code' => 'RTP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Wajo',
		        					'code' => 'SKG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Makassar',
		        					'code' => 'MKS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Palopo',
		        					'code' => 'PLP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Parepare',
		        					'code' => 'PRE',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Sulawesi Tengah',
	        				'abbreviation' => 'Sulteng',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Banggai',
		        					'code' => 'LWK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Banggai Kepulauan',
		        					'code' => 'SKN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Banggai Laut',
		        					'code' => 'BGI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Buol',
		        					'code' => 'BUL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Donggala',
		        					'code' => 'DGL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Morowali',
		        					'code' => 'BGK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Morowali Utara',
		        					'code' => 'KLD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Parigi Moutong',
		        					'code' => 'PRG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Poso',
		        					'code' => 'PSO',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sigi',
		        					'code' => 'SGB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tojo Una-Una',
		        					'code' => 'APN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Toli-Toli',
		        					'code' => 'TLI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Palu',
		        					'code' => 'PAL',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Sulawesi Tenggara',
	        				'abbreviation' => 'Sultra',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bombana',
		        					'code' => 'RMB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Buton',
		        					'code' => 'PSW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Buton Selatan',
		        					'code' => 'BGA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Buton Tengah',
		        					'code' => 'LBK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Buton Utara',
		        					'code' => 'BNG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kolaka',
		        					'code' => 'KKA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kolaka Timur',
		        					'code' => 'TWT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kolaka Utara',
		        					'code' => 'LSS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Konawe',
		        					'code' => 'UNH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Konawe Kepulauan',
		        					'code' => 'LGR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Konawe Selatan',
		        					'code' => 'ADL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Konawe Utara',
		        					'code' => 'WGD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Muna',
		        					'code' => 'RAH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Muna Barat',
		        					'code' => 'LWR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Wakatobi',
		        					'code' => 'WGW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Baubau',
		        					'code' => 'BAU',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Kendari',
		        					'code' => 'KDI',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Sulawesi Utara',
	        				'abbreviation' => 'Sulut',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bolaang Mongondow',
		        					'code' => 'LLK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bolaang Mongondow Selatan',
		        					'code' => 'BLU',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bolaang Mongondow Timur',
		        					'code' => 'TTY',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bolaang Mongondow Utara',
		        					'code' => 'BRK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepulauan Sangihe',
		        					'code' => 'THN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepulauan Talaud',
		        					'code' => 'MGN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Minahasa',
		        					'code' => 'TNN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Minahasa Selatan',
		        					'code' => 'AMR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Minahasa Tenggara',
		        					'code' => 'RTN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Minahasa Utara',
		        					'code' => 'ARM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Siau Tagulandang Biaro',
		        					'code' => 'ODS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Bitung',
		        					'code' => 'BIT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Kotamobagu',
		        					'code' => 'KTG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Manado',
		        					'code' => 'MND',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Tomohon',
		        					'code' => 'TMH',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        		]
        		],
        	],
        	[
        		'island' => [
        			'name' => 'Sumatera',
	        		'provinces' => [
	        			[
	        				'name' => 'Aceh',
	        				'abbreviation' => 'Aceh',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Aceh Barat',
		        					'code' => 'MBO',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Barat Daya',
		        					'code' => 'BPD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Besar',
		        					'code' => 'JTH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Jaya',
		        					'code' => 'CAG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Selatan',
		        					'code' => 'TTN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Singkil',
		        					'code' => 'SKL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Tamiang',
		        					'code' => 'KRB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Tengah',
		        					'code' => 'TKN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Tenggara',
		        					'code' => 'KTN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Timur',
		        					'code' => 'LGS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Aceh Utara',
		        					'code' => 'LSK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bener Meriah',
		        					'code' => 'STR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bireuen',
		        					'code' => 'BIR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Gayo Lues',
		        					'code' => 'BKJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nagan Raya',
		        					'code' => 'SKM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pidie',
		        					'code' => 'SGI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pidie Jaya',
		        					'code' => 'MRN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Simeulue',
		        					'code' => 'SNB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Banda Aceh',
		        					'code' => 'BNA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Langsa',
		        					'code' => 'LGS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Lhokseumawe',
		        					'code' => 'LSM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Sabang',
		        					'code' => 'SAB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Subulussalam',
		        					'code' => 'SUS',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Bengkulu',
	        				'abbreviation' => 'Bengkulu',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bengkulu Selatan',
		        					'code' => 'MNA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bengkulu Tengah',
		        					'code' => 'KRT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bengkulu Utara',
		        					'code' => 'AGM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kaur',
		        					'code' => 'BHN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepahiang',
		        					'code' => 'KPH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lebong',
		        					'code' => 'TUB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mukomuko',
		        					'code' => 'MKM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Rejang Lebong',
		        					'code' => 'CRP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Seluma',
		        					'code' => 'TAS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Bengkulu',
		        					'code' => 'BGL',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Jambi',
	        				'abbreviation' => 'Jambi',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Batang Hari',
		        					'code' => 'MBN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bungo',
		        					'code' => 'MRB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kerinci',
		        					'code' => 'SPN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Merangin',
		        					'code' => 'BKO',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Muaro Jambi',
		        					'code' => 'SNT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sarolangun',
		        					'code' => 'SRL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tanjung Jabung Barat',
		        					'code' => 'KLT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tanjung Jabung Timur',
		        					'code' => 'MSK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tebo',
		        					'code' => 'MRT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Jambi',
		        					'code' => 'JMB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Sungai Penuh',
		        					'code' => 'SPN',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Kepulauan Bangka Belitung',
	        				'abbreviation' => 'B. Belitung',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bangka',
		        					'code' => 'SGL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bangka Barat',
		        					'code' => 'MTK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bangka Selatan',
		        					'code' => 'TBL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Bangka Tengah',
		        					'code' => 'KBA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Belitung',
		        					'code' => 'TDN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Belitung Timur',
		        					'code' => 'MGR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Pangkal Pinang',
		        					'code' => 'PGP',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Kepulauan Riau',
	        				'abbreviation' => 'Kp. Riau',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bintan',
		        					'code' => 'BSB',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kabupaten Karimun',
		        					'code' => 'TBK',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kabupaten Kepulauan Anambas',
		        					'code' => 'TRP',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kabupaten Lingga',
		        					'code' => 'DKL',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kabupaten Natuna',
		        					'code' => 'RAN',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kota Batam',
		        					'code' => 'BTM',
		        					'umr' => null,
	        					],
	        					[
		        					'name' => 'Kota Tanjung Pinang',
		        					'code' => 'TPG',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Lampung',
	        				'abbreviation' => 'Lampung',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Lampung Barat',
		        					'code' => 'LIW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lampung Selatan',
		        					'code' => 'KLA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lampung Tengah',
		        					'code' => 'GNS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lampung Timur',
		        					'code' => 'SDN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lampung Utara',
		        					'code' => 'KTB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mesuji',
		        					'code' => 'MSJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pesawaran',
		        					'code' => 'GDT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pesisir Barat',
		        					'code' => 'KRU',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pringsewu',
		        					'code' => 'PRW',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tanggamus',
		        					'code' => 'KOT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tulang Bawang Barat',
		        					'code' => 'TWG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tulangbawang',
		        					'code' => 'MGL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Way Kanan',
		        					'code' => 'BBU',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Bandar Lampung',
		        					'code' => 'BDL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Metro',
		        					'code' => 'MET',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Riau',
	        				'abbreviation' => 'Riau',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Bengkalis',
		        					'code' => 'BLS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Indragiri Hilir',
		        					'code' => 'TBH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Indragiri Hulu',
		        					'code' => 'RGT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kampar',
		        					'code' => 'BKN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepulauan Meranti',
		        					'code' => 'TTG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kuantan Singingi',
		        					'code' => 'TLK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pelalawan',
		        					'code' => 'PKK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Rokan Hilir',
		        					'code' => 'UJT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Rokan Hulu',
		        					'code' => 'PRP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Siak',
		        					'code' => 'SAK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Dumai',
		        					'code' => 'DUM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Pekanbaru',
		        					'code' => 'PBR',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Sumatera Barat',
	        				'abbreviation' => 'Sumbar',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Agam',
		        					'code' => 'LBB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Dharmasraya',
		        					'code' => 'PLJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Kepulauan Mentawai',
		        					'code' => 'TPT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lima Puluh Kota',
		        					'code' => 'SRK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Padang Pariaman',
		        					'code' => 'NPM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pasaman',
		        					'code' => 'LBS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pasaman Barat',
		        					'code' => 'SPE',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pesisir Selatan',
		        					'code' => 'PNN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Sijunjung',
		        					'code' => 'MRJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Solok',
		        					'code' => 'ARS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Solok Selatan',
		        					'code' => 'PDA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tanah Datar',
		        					'code' => 'BSK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Bukittinggi',
		        					'code' => 'BKT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Padang',
		        					'code' => 'PAD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Padang Panjang',
		        					'code' => 'PDP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Pariaman',
		        					'code' => 'PMN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Payakumbuh',
		        					'code' => 'PYH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Sawah Lunto',
		        					'code' => 'SWL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Solok',
		        					'code' => 'SLK',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Sumatera Selatan',
	        				'abbreviation' => 'Sumsel',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Banyu Asin',
		        					'code' => 'PKB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Empat Lawang',
		        					'code' => 'TBG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Lahat',
		        					'code' => 'LHT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Muara Enim',
		        					'code' => 'MRE',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Musi Banyuasin',
		        					'code' => 'SKY',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Musi Rawas',
		        					'code' => 'MBL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Musi Rawas Utara',
		        					'code' => 'RUP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ogan Ilir',
		        					'code' => 'IDL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ogan Komering Ilir',
		        					'code' => 'KAG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ogan Komering Ulu',
		        					'code' => 'BTA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ogan Komering Ulu Selatan',
		        					'code' => 'MRD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Ogan Komering Ulu Timur',
		        					'code' => 'MPR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Penukal Abab Lematang Ilir',
		        					'code' => 'TBI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Lubuklinggau',
		        					'code' => 'LLG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Pagar Alam',
		        					'code' => 'PGA',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Palembang',
		        					'code' => 'PLG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Prabumulih',
		        					'code' => 'PBM',
		        					'umr' => null,
	        					],
	        				],
	        			],
						[
	        				'name' => 'Sumatera Utara',
	        				'abbreviation' => 'Sumut',
	        				'regencies' => [
	        					[
		        					'name' => 'Kabupaten Asahan',
		        					'code' => 'KIS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Batu Bara',
		        					'code' => 'LMP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Dairi',
		        					'code' => 'SDK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Deli Serdang',
		        					'code' => 'LBP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Humbang Hasundutan',
		        					'code' => 'DLS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Karo',
		        					'code' => 'KBJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Labuhan Batu',
		        					'code' => 'RAP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Labuhan Batu Selatan',
		        					'code' => 'KPI',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Labuhan Batu Utara',
		        					'code' => 'AKK',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Langkat',
		        					'code' => 'STB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Mandailing Natal',
		        					'code' => 'PYB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nias',
		        					'code' => 'GST',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nias Barat',
		        					'code' => 'LHM',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nias Selatan',
		        					'code' => 'TLD',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Nias Utara',
		        					'code' => 'LTU',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Padang Lawas',
		        					'code' => 'SBH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Padang Lawas Utara',
		        					'code' => 'GNT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Pakpak Bharat',
		        					'code' => 'SAL',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Samosir',
		        					'code' => 'PRR',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Serdang Bedagai',
		        					'code' => 'SRH',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Simalungun',
		        					'code' => 'PMS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tapanuli Selatan',
		        					'code' => 'PSP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tapanuli Tengah',
		        					'code' => 'SBG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Tapanuli Utara',
		        					'code' => 'TRT',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kabupaten Toba Samosir',
		        					'code' => 'BLG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Binjai',
		        					'code' => 'BNJ',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Gunungsitoli',
		        					'code' => 'GST',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Medan',
		        					'code' => 'MDN',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Padangsidimpuan',
		        					'code' => 'PSP',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Pematang Siantar',
		        					'code' => 'PMS',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Sibolga',
		        					'code' => 'SBG',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Tanjung Balai',
		        					'code' => 'TJB',
		        					'umr' => null,
	        					],
								[
		        					'name' => 'Kota Tebing Tinggi',
		        					'code' => 'TBT',
		        					'umr' => null,
	        					],
	        				],
	        			],
	        		]
        		],
        	],
        ];

        foreach ($areas as $area) {
        	$island = Island::updateOrCreate(['name' => $area['island']['name']]);
        	foreach ($area['island']['provinces'] as $provinces) {
        		$province = $island->provinces()->updateOrCreate([
        			'name' => $provinces['name'],
        			'abbreviation' => $provinces['abbreviation']
        		]);
        		foreach ($provinces['regencies'] as $regency) { 
        			$province->cities()->updateOrCreate([
        				'name' => $regency['name'],
        				'code' => $regency['code'],
        				'umr' => $regency['umr'] ? $regency['umr'] : null,
        			]);
        		}
        	}
        }
    }
}
