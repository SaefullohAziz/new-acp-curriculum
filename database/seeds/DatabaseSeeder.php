<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StatusSeeder::class);
        $this->call(LessonSeeder::class);
        $this->call(RequiredDocumentSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RolePermissionSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AreasSeeder::class);
        $this->call(DepartmentSeeder::class);
        // $this->call(DummyDatasSeeder::class);
    }
}
