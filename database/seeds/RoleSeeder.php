<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = collect(['superadmin', 'admin', 'esi', 'bca', 'siswa', 'partner', 'guru']);

        $roles->each(function ($role) {
            Role::firstOrCreate(['name' => $role]);
        });
    }
}
