<?php

use Illuminate\Database\Seeder;
use App\Lesson;
use App\Status;
use App\LessonType;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return collect([
            [
                'name' => 'Basic skills',
                'lessons' => [
                    [
                        'name' => 'Money Ban',
                        'unit_score' => 'Stack',
                        'status_id' => 'Class',
                        'achievements' => [
                            [
                                'name' => 'Minimum Money Ban',
                                'rules' => [
                                    'type' => 'spesific_score',
                                    'operator' => '>',
                                    'score' => '100',
                                    'unit_score' => 'Stack',
                                    'implemented_at' => now()
                                ]
                            ],
                            [
                                'name' => 'Minimum Money Ban Flight Hours',
                                'rules' => [
                                    'type' => 'flight_hours',
                                    'operator' => '>',
                                    'score' => '90',
                                    'unit_score' => 'Minutes',
                                    'implemented_at' => now()
                                ]
                            ],
                        ],
                        'durations' => [
                            'duration' => '30',
                            'duration_unit' => 'Minutes',
                            'implemented_at' => now()
                        ]
                    ],
                    [
                        'name' => 'Packing Plastik',
                        'status_id' => 'Class',
                        'unit_score' => 'Minutes',
                        'achievements' => [
                            [
                                'name' => 'Minimum Packing Plastik',
                                'rules' => [
                                    'type' => 'spesific_score',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Minutes',
                                    'implemented_at' => now()
                                ]
                            ],
                            [
                                'name' => 'Minimum Packing Plastic Flight Hours',
                                'rules' => [
                                    'type' => 'flight_hours',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Times',
                                    'implemented_at' => now()
                                ]
                            ],
                        ],
                        'durations' => [
                            'duration' => '10',
                            'duration_unit' => 'Broots',
                            'implemented_at' => now()
                        ]
                    ],
                    [
                        'name' => 'Packing Canvas Bag',
                        'status_id' => 'Class',
                        'unit_score' => 'Minutes',
                        'achievements' => [
                            [
                                'name' => 'Minimum Packing Canvas Bag',
                                'rules' => [
                                    'type' => 'spesific_score',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Minutes',
                                    'implemented_at' => now()
                                ]
                            ],
                            [
                                'name' => 'Minimum Packing Canvas Bag Flight Hours',
                                'rules' => [
                                    'type' => 'flight_hours',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Times',
                                    'implemented_at' => now()
                                ]
                            ],
                        ],
                        'durations' => [
                            'duration' => '5',
                            'duration_unit' => 'Cassetes',
                            'implemented_at' => now()
                        ]
                    ],
                    [
                        'name' => 'Replenish ATM',
                        'status_id' => 'Class',
                        'unit_score' => 'Minutes',
                        'achievements' => [
                            [
                                'name' => 'Minimum Replenish ATM',
                                'rules' => [
                                    'type' => 'spesific_score',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Minutes',
                                    'implemented_at' => now()
                                ]
                            ],
                            [
                                'name' => 'Minimum Replenish ATM Flight Hours',
                                'rules' => [
                                    'type' => 'flight_hours',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Times',
                                    'implemented_at' => now()
                                ]
                            ],
                        ],
                        'durations' => [
                            'duration' => '5',
                            'duration_unit' => 'Cassetes',
                            'implemented_at' => now()
                        ]
                    ],
                    [
                        'name' => 'Fill Money Cassetes',
                        'status_id' => 'Class',
                        'unit_score' => 'Minutes',
                        'achievements' => [
                            [
                                'name' => 'Minimum Fill Money Cassetes',
                                'rules' => [
                                    'type' => 'spesific_score',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Minutes',
                                    'implemented_at' => now()
                                ]
                            ],
                            [
                                'name' => 'Minimum Fill Money Cassetes Flight Hours',
                                'rules' => [
                                    'type' => 'flight_hours',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Times',
                                    'implemented_at' => now()
                                ]
                            ],
                        ],
                        'durations' => [
                            'duration' => '4',
                            'duration_unit' => 'Cassetes',
                            'implemented_at' => now()
                        ]
                    ],
                    [
                        'name' => 'Unpacking Money In Cassetes',
                        'status_id' => 'Class',
                        'unit_score' => 'Minutes',
                        'achievements' => [
                            [
                                'name' => 'Minimum Unpacking Money In Cassetes',
                                'rules' => [
                                    'type' => 'spesific_score',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Minutes',
                                    'implemented_at' => now()
                                ]
                            ],
                            [
                                'name' => 'Minimum Unpacking Money In Cassetes Flight Hours',
                                'rules' => [
                                    'type' => 'flight_hours',
                                    'operator' => '>',
                                    'score' => '3',
                                    'unit_score' => 'Times',
                                    'implemented_at' => now()
                                ]
                            ],
                        ],
                        'durations' => [
                            'duration' => '5',
                            'duration_unit' => 'Cassetes',
                            'implemented_at' => now()
                        ]
                    ],
                ]
            ],
            [
                'name' => 'Endurances',
                'lessons' => [
                    [
                        'name' => 'Melipat kertas',
                        'status_id' => 'Class',
                        'unit_score' => 'folds',
                        'achievements' => [
                            [
                                'name' => 'Minimum melipat kertas',
                                'rules' => [
                                    'type' => 'spesific_score',
                                    'operator' => '>',
                                    'score' => '1000',
                                    'unit_score' => 'folds',
                                    'implemented_at' => now()
                                ]
                            ],
                        ],
                        'durations' => [
                            'duration' => '5',
                            'duration_unit' => 'Minutes',
                            'implemented_at' => now()
                        ]
                    ]
                ],
            ],
            [
                'name' => 'ATM'
            ],
        ])->each( function($item) {
            $item = collect($item);
            $type = LessonType::updateOrCreate($item->except(['lessons'])->toArray());
            if ($item->has('lessons')) {
                $lessons = collect($item->except(['name'])->toArray()['lessons']);
                $lessons->each( function($lesson) use ($type) {
                    $data = collect($lesson);
                    $data['status_id'] = Status::where('name', $data['status_id'])->first()->id;
                    $lesson = $type->lessons()->updateOrCreate($data->except(['achievements', 'durations'])->toArray());

                    foreach ($data['achievements'] as $achievement) {
                        $achievements = $lesson->achievements()
                            ->updateOrCreate(collect($achievement)->only(['name'])->toArray());
                        $achievements->rule()->updateOrCreate(collect($achievement)->only(['rules'])->toArray()['rules']);
                    }

                    $lesson->durations()->updateOrCreate($data['durations']);
                });
            }
        });
    }
}
