<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Generation;
use Faker\Generator as Faker;

$factory->define(Generation::class, function (Faker $faker) {
    return [
        'school_year' => '2018/2019',
        'number' => '1'
    ];
});
