<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Province;
use App\Student;
use App\City;

$factory->define(Student::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    $study_status = $faker->randomElement(['sudah alumni', 'sedang belajar']);
    $regency = City::pluck('name')->toArray();
    return [
        'name' => $faker->name($gender),
        'study_status' => $study_status,
        'place_of_birth' => $faker->randomElement($regency),
        'date_of_birth' => $faker->date('Y-m-d', '2004-01-01'),
        'identition_number' => $faker->unique()->numberBetween(100000000, 99999999999),
        'address' => $faker->address,
        'height' => $faker->numberBetween(150, 200),
        'weight' => $faker->numberBetween(50, 100),
    ];
});
