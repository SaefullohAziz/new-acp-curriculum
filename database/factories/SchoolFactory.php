<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Province;
use App\City;
use App\School;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(School::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    $study_status = $faker->randomElement(['sudah alumni', 'sedang belajar']);
    $province = $faker->randomElement(Province::pluck('name')->toArray());
    $cities = City::join('provinces', 'cities.province_id', '=', 'provinces.id')->where('provinces.name', $province)->pluck('cities.id')->toArray();
    $regencies = City::join('provinces', 'cities.province_id', '=', 'provinces.id')->where('provinces.name', $province)->pluck('cities.name')->toArray();
    $city = $faker->randomElement($cities);
    $regency = $faker->randomElement($regencies);
    return [
        'city_id' => $city, 
        'type' => $faker->randomElement(['Negeri', 'Swasta']),
        'name' => 'SMK Negeri ' . $faker->numberBetween(1, 99) . ' ' . $regency,
        'address' => $faker->address,
        'since' => $faker->year('1998'), 
        // 'study_status' => $study_status, 
        'school_phone_number' => $faker->unique()->phoneNumber,
        'school_email' => $faker->unique()->safeEmail, 
        'school_web' => 'http://' . $faker->domainName, 
        'total_student' => $faker->numberBetween(900, 1500), 
        'department' => $faker->randomElement(['RPL', 'TKJ', 'MM']), 
        'iso_certificate' => $faker->randomElement(['Sudah', 'Belum']), 
        'headmaster_name' => $faker->unique()->name($gender), 
        'headmaster_phone_number' => $faker->unique()->phoneNumber, 
        'headmaster_email' => $faker->unique()->safeEmail, 
        'proposal' => $faker->randomElement(['Sudah', 'Belum']),
        'reference' => $faker->randomElement(['Sekolah Peserta / Sekolah Binaan', 'Internet (Facebook Page/Web)']), 
        // 'dealer_name' => null, 
        // 'dealer_phone_number' => null, 
        // 'dealer_email' => null,
        'document' => null,
        'notif' => null,
        'code' => strtoupper(Str::random(6)),
        'zip_code' => $faker->randomElement(['40112', '40113', '40114', '40115', '40116', '40117', '40121', '40121', '40122', '40123', '40124', '40124', '40125', '40125', '40131', '40132', '40132', '40133']),
    ];
});

