<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Teacher;
use Faker\Generator as Faker;

$factory->define(Teacher::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    $genders = ['male' => 'Laki-Laki', 'female' => 'Perempuan'];
    return [
        'name' => $faker->name($gender),
        'gender' => $genders[$gender],
        'phone_number' => $faker->unique()->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'date_of_birth' => $faker->date('Y-m-d', '2004-01-01'),
        'position' => 'Guru Umum',
        'address' => $faker->address,
    ];
        // 'date_of_birth' => $faker->dateTimeBetween('1970-01-01', '2000-12-30')
    				// 		->format('d/m/Y'),
});
