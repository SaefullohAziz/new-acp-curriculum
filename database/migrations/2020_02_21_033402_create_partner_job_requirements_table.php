<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerJobRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_job_requirements', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('partner_requirement_id');
            $table->foreign('partner_requirement_id')->references('id')->on('partner_requirements')->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('job_desk_id');
            $table->foreign('job_desk_id')->references('id')->on('job_desks')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('quota')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_job_requirements');
    }
}
