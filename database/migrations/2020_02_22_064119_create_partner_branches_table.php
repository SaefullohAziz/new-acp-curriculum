<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_branches', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('city_id');
            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('partner_id');
            $table->foreign('partner_id')->references('id')->on('partners')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partner_branches', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropForeign('partner_branches_partner_id_foreign');
            $table->dropIndex('partner_branches_partner_id_foreign');
            $table->dropForeign('partner_branches_city_id_foreign');
            $table->dropIndex('partner_branches_city_id_foreign');
        });
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('partner_branches');
    }
}
