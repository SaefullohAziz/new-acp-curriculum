<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRelationPartnerIdInPartnerRequirements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partner_requirements', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropForeign('partner_requirements_partner_id_foreign');
            $table->dropIndex('partner_requirements_partner_id_foreign');
            $table->renameColumn('partner_id', 'partner_branch_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partner_requirements', function (Blueprint $table) {
            $table->renameColumn('partner_branch_id', 'partner_id');
            $table->foreign('partner_id')->references('id')->on('partners')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
