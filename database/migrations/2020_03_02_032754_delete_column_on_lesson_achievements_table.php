<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteColumnOnLessonAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Lesson Achievement
        Schema::table('lesson_achievements', function (Blueprint $table) {
            $table->dropColumn('rule');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Lesson Achievement
        Schema::table('lesson_achievements', function (Blueprint $table) {
            $table->char('rule', 36)->after('lesson_id');
        });
    }
}
