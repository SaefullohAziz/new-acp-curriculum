<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Level
        Schema::table('levels', function (Blueprint $table) {
            $table->string('order', 2)->nullable()->after('name');
        });

        // Statuses
        Schema::table('statuses', function (Blueprint $table) {
            $table->string('order', 2)->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Level
        Schema::table('levels', function (Blueprint $table) {
            $table->dropColumn('order');
        });

        // Statuses
        Schema::table('statuses', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
