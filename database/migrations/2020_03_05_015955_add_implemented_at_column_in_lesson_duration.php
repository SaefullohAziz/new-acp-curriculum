<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImplementedAtColumnInLessonDuration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_durations', function (Blueprint $table) {
            $table->timestamp('implemented_at')->nullable()->after('duration_time');
            $table->renameColumn('duration_time', 'duration_unit');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_durations', function (Blueprint $table) {
            $table->dropColumn('implemented_at');
            $table->renameColumn('duration_unit', 'duration_time');
        });
    }
}
