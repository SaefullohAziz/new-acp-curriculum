<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnAlocatedStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Alocated Student
        Schema::table('alocated_students', function (Blueprint $table) {
            $table->uuid('partner_branch_id')->after('partner_id');
            $table->foreign('partner_branch_id')->references('id')->on('partner_branches')->onUpdate('cascade')->onDelete('cascade');
            $table->dropColumn('quartal');
            $table->string('month')->nullable()->after('job_desk_id');
            $table->string('year')->nullable()->after('month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Alocated Student
        Schema::table('alocated_students', function (Blueprint $table) {
            $table->dropColumn('year');
            $table->dropColumn('month');
            $table->integer('quartal');
            $table->dropForeign('alocated_students_partner_branch_id_foreign');
            $table->dropIndex('alocated_students_partner_branch_id_foreign');
            $table->dropColumn('partner_branch_id');
        });
    }
}
