<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRelationOnStudentLessonAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_lesson_achievements', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropForeign('student_lesson_achievements_lesson_achievement_id_foreign');
            $table->dropIndex('student_lesson_achievements_lesson_achievement_id_foreign');
            $table->renameColumn('lesson_achievement_id', 'lesson_achievement_rule_id');
            $table->foreign('lesson_achievement_rule_id')->references('id')->on('lesson_achievement_rules')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_lesson_achievements', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropForeign('student_lesson_achievements_lesson_achievement_rule_id_foreign');
            $table->dropIndex('student_lesson_achievements_lesson_achievement_rule_id_foreign');
            $table->renameColumn('lesson_achievement_rule_id', 'lesson_achievement_id');
            $table->foreign('lesson_achievement_id')->references('id')->on('lesson_achievements')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
