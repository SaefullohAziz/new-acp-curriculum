<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSomeStrukturColumnOnTeachers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->string('gender')->nullable()->change();
            $table->string('phone_number')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->date('date_of_birth')->nullable()->change();
            $table->string('position')->nullable()->change();
            $table->string('phone_number')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->string('gender')->nullable(false)->change();
            $table->string('phone_number')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
            $table->date('date_of_birth')->nullable(false)->change();
            $table->string('position')->nullable(false)->change();
            $table->string('phone_number')->nullable(false)->change();
        });
    }
}
