<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDescriptionOnLessonClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Lesson Class
        Schema::table('lesson_classes', function (Blueprint $table) {
            $table->text('description')->nullable()->after('max_reported_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Lesson Class
        Schema::table('lesson_classes', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
