<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddApprovedByColumnOnPartnerRequirement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partner_requirements', function (Blueprint $table) {
            $table->uuid('approved_by')->after('year')->index();
            $table->foreign('approved_by')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partner_requirements', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropIndex('partner_requirements_approved_by_index');
            $table->dropForeign('partner_requirements_approved_by_index');
            Schema::disableForeignKeyConstraints();
            $table->dropColumn('approved_by');
        });
    }
}
