<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnLessonAchievementRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Lesson Achievement Rule
        Schema::table('lesson_achievement_rules', function (Blueprint $table) {
            $table->timestamp('implemented_at')->nullable()->after('unit_score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Lesson Achievement Rule
        Schema::table('lesson_achievement_rules', function (Blueprint $table) {
            $table->dropColumn('implemented_at');
        });
    }
}
