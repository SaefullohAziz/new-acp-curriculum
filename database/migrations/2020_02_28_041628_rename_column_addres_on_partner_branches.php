<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnAddresOnPartnerBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Partner Branch
        Schema::table('partner_branches', function (Blueprint $table) {
            $table->renameColumn('addres', 'address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Partner Branch
        Schema::table('partner_branches', function (Blueprint $table) {
            $table->renameColumn('address', 'addres');
        });
    }
}
