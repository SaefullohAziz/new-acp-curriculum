<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnLessonClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_classes', function (Blueprint $table) {
            $table->timestamp('max_absend_at')->nullable()->after('end_time');
            $table->renameColumn('max_date_reported', 'max_reported_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_classes', function (Blueprint $table) {
            $table->dropColumn('max_absend_at');
            $table->renameColumn('max_reported_at', 'max_date_reported');
        });
    }
}
