<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnColorOnSomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Level
        Schema::table('levels', function (Blueprint $table) {
            $table->string('color')->nullable()->after('slug');
        });

        // Partner
        Schema::table('partners', function (Blueprint $table) {
            $table->string('color')->nullable()->after('slug');
        });

        // JobDesk
        Schema::table('job_desks', function (Blueprint $table) {
            $table->string('color')->nullable()->after('slug');
        });

        // Partner Requirement
        Schema::table('partner_requirements', function (Blueprint $table) {
            $table->foreign('partner_branch_id')->references('id')->on('partner_branches')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Partner Requirement
        Schema::table('partner_requirements', function (Blueprint $table) {
            $table->dropForeign('partner_requirements_partner_branch_id_foreign');
            $table->dropIndex('partner_requirements_partner_branch_id_foreign');
        });

        // JobDesk
        Schema::table('job_desks', function (Blueprint $table) {
            $table->dropColumn('color');
        });

        // Partner
        Schema::table('partners', function (Blueprint $table) {
            $table->dropColumn('color');
        });

        // Level
        Schema::table('levels', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
