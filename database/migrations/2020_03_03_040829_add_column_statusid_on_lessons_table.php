<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnStatusidOnLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Lesson
        Schema::table('lessons', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->uuid('status_id')->after('id');
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Lesson
        Schema::table('lessons', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropIndex('lessons_status_id_foreign');
            $table->dropForeign('lessons_status_id_foreign');
            Schema::disableForeignKeyConstraints();
            $table->dropColumn('status_id');
        });
    }
}