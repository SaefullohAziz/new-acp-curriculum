<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInStudentLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_lessons', function (Blueprint $table) {
            $table->uuid('student_id')->after('id');
            $table->foreign('student_id')->references('id')->on('students')->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('teacher_id')->nullable()->after('student_id');
            $table->foreign('teacher_id')->references('id')->on('teachers')->onUpdate('cascade')->onDelete('cascade');
            $table->string('score')->after('student_lesson_class_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_lessons', function (Blueprint $table) {
            $table->dropColumn('score');
            $table->dropForeign('student_lessons_teacher_id_foreign');
            $table->dropIndex('student_lessons_teacher_id_foreign');
            $table->dropColumn('teacher_id');
            $table->dropForeign('student_lessons_student_id_foreign');
            $table->dropIndex('student_lessons_student_id_foreign');
            $table->dropColumn('student_id');
        });
    }
}
