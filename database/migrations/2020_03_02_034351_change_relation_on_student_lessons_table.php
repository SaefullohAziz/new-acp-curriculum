<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRelationOnStudentLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_lessons', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropForeign('student_lessons_lesson_id_foreign');
            $table->dropIndex('student_lessons_lesson_id_foreign');
            $table->renameColumn('lesson_id', 'lesson_duration_id');
            $table->foreign('lesson_duration_id')->references('id')->on('lesson_durations')->onUpdate('cascade')->onDelete('cascade');
            $table->string('photo')->nullable()->after('score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_lessons', function (Blueprint $table) {
            $table->dropColumn('photo');
            Schema::enableForeignKeyConstraints();
            $table->dropForeign('student_lessons_lesson_duration_id_foreign');
            $table->dropIndex('student_lessons_lesson_duration_id_foreign');
            $table->renameColumn('lesson_duration_id', 'lesson_id');
            $table->foreign('lesson_id')->references('id')->on('lessons')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
