<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnNameInLessonAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_achievements', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropForeign('lesson_achievements_lesson_achievement_rule_id_foreign');
            $table->dropIndex('lesson_achievements_lesson_achievement_rule_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_achievements', function (Blueprint $table) {
            // Schema::disableForeignKeyConstraints();
            $table->foreign('lesson_achievement_rule_id')->references('id')->on('lesson_achievement_rules')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
