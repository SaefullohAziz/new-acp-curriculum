<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonClassMateriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_class_materies', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('lesson_class_id');
            $table->foreign('lesson_class_id')->references('id')->on('lesson_classes')->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('lesson_id');
            $table->foreign('lesson_id')->references('id')->on('lessons')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_class_materies');
    }
}
