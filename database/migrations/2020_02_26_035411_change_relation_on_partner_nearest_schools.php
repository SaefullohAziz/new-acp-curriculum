<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRelationOnPartnerNearestSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partner_nearest_schools', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();
            $table->dropForeign('partner_nearest_schools_partner_id_foreign');
            $table->dropIndex('partner_nearest_schools_partner_id_foreign');
            $table->renameColumn('partner_id', 'partner_branch_id');
            $table->foreign('partner_branch_id')->references('id')->on('partner_branches')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
