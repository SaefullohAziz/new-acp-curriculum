<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteColumnOnPartnerJobRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Partner Job Requirement
        Schema::table('partner_job_requirements', function (Blueprint $table) {
            $table->dropColumn('quota');
        });

        // School
        Schema::table('schools', function (Blueprint $table) {
            $table->string('headmaster_phone_number')->nullable()->change();
            $table->string('school_phone_number')->nullable()->change();
        });

        // Status
        Schema::table('statuses', function (Blueprint $table) {
            $table->string('intention')->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Status
        Schema::table('statuses', function (Blueprint $table) {
            $table->dropColumn('intention');
        });

        // School
        Schema::table('schools', function (Blueprint $table) {
            $table->string('headmaster_phone_number')->nullable(false)->change();
            $table->string('school_phone_number')->nullable(false)->change();
        });

        // Partner Job Requirement
        Schema::table('partner_job_requirements', function (Blueprint $table) {
            $table->integer('quota')->nullable();
        });
    }
}
