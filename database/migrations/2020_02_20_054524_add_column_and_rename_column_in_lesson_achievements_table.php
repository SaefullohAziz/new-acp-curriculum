<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAndRenameColumnInLessonAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_achievements', function (Blueprint $table) {
            $table->uuid('lesson_id')->after('name');
            $table->foreign('lesson_id')->references('id')->on('lessons')->onUpdate('cascade')->onDelete('cascade');
            $table->renameColumn('lesson_achievement_rule_id', 'rule');
        });

        Schema::drop('lesson_achievement_rules');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_achievements', function (Blueprint $table) {
            $table->renameColumn('rule', 'lesson_achievement_rule_id');
            $table->dropForeign('lesson_achievements_lesson_id_foreign');
            $table->dropIndex('lesson_achievements_lesson_id_foreign');
            $table->dropColumn('lesson_id');
        });
    }
}
