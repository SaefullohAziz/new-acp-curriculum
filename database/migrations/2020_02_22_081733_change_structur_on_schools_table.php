<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeStructurOnSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // School
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn('dealer_name');
            $table->dropColumn('dealer_phone_number');
            $table->dropColumn('dealer_email');
            $table->string('school_phone_number')->nullable()->change();
            $table->string('school_email')->nullable()->change();
            $table->integer('total_student')->nullable()->change();
            $table->text('department')->nullable()->change();
            $table->string('iso_certificate')->nullable()->change();
            $table->string('headmaster_name')->nullable()->change();
            $table->string('headmaster_email')->nullable()->change();
            $table->string('proposal')->nullable()->change();
            $table->string('reference')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // School
        Schema::table('schools', function (Blueprint $table) {
            $table->string('reference')->nullable(false)->change();
            $table->string('headmaster_email')->nullable(false)->change();
            $table->string('headmaster_name')->nullable(false)->change();
            $table->string('iso_certificate')->nullable(false)->change();
            $table->string('proposal')->nullable(false)->change();
            $table->text('department')->nullable(false)->change();
            $table->integer('total_student')->nullable(false)->change();
            $table->string('school_email')->nullable(false)->change();
            $table->string('school_phone_number')->nullable(false)->change();
            $table->string('dealer_email')->nullable();
            $table->string('dealer_phone_number')->nullable();
            $table->string('dealer_name')->nullable();
        });
    }
}
