<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Partner
        Schema::table('partners', function (Blueprint $table) {
            $table->string('slug')->nullable()->after('name');
        });

        // Job Desk
        Schema::table('job_desks', function (Blueprint $table) {
            $table->string('slug')->nullable()->after('name');
        });

        // Partner Branch
        Schema::table('partner_branches', function (Blueprint $table) {
            $table->string('addres')->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Partner Branch
        Schema::table('partner_branches', function (Blueprint $table) {
            $table->dropColumn('addres');
        });

        // Job Desk
        Schema::table('job_desks', function (Blueprint $table) {
            $table->dropColumn('slug');
        });

        // Partner
        Schema::table('partners', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
