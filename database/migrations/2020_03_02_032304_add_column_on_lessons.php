<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnLessons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Lesson
        Schema::table('lessons', function (Blueprint $table) {
            $table->text('detail')->nullable()->after('unit_score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Lesson
        Schema::table('lessons', function (Blueprint $table) {
            $table->dropColumn('detail');
        });
    }
}
