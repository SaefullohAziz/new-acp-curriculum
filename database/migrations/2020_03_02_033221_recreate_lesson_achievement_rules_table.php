<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecreateLessonAchievementRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_achievement_rules', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('lesson_achievement_id');
            $table->foreign('lesson_achievement_id')->references('id')->on('lesson_achievements')->onUpdate('cascade')->onDelete('cascade');
            $table->string('type')->nullable();
            $table->char('operator', 2)->nullable();
            $table->string('score')->nullable();
            $table->string('unit_score')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_achievement_rules');
    }
}
