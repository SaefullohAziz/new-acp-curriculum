<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSomeColumnOnSomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Student
        Schema::table('students', function (Blueprint $table) {
            $table->string('phone_number')->nullable()->after('name');
            $table->string('study_status')->nullable()->after('phone_number');
        });

        // Province
        Schema::table('provinces', function (Blueprint $table) {
            $table->uuid('island_id')->after('id');
            $table->foreign('island_id')->references('id')->on('islands')->onUpdate('cascade')->onDelete('cascade');
        });

        // City
        Schema::table('cities', function (Blueprint $table) {
            $table->bigInteger('umr')->nullable()->after('code');
        });

        // Partner
        Schema::table('partners', function (Blueprint $table) {
            $table->string('phone_number')->nullable()->after('name');
        });

        // Alocated Stduent
        Schema::table('alocated_students', function (Blueprint $table) {
            $table->uuid('job_desk_id')->after('student_id');
            $table->foreign('job_desk_id')->references('id')->on('job_desks')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('quartal')->after('job_desk_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Alocated Student
        Schema::table('alocated_students', function (Blueprint $table) {
            $table->dropForeign('alocated_students_job_desk_id_foreign');
            $table->dropIndex('alocated_students_job_desk_id_foreign');
            $table->dropColumn('job_desk_id');
        });

        // Partner
        Schema::table('partners', function (Blueprint $table) {
            $table->dropColumn('phone_number');
        });

        // City
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('umr');
        });

        // Province
        Schema::table('provinces', function (Blueprint $table) {
            $table->dropForeign('provinces_island_id_foreign');
            $table->dropIndex('provinces_island_id_foreign');
            $table->dropColumn('island_id');
        });

        // Student
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('study_status');
        });
    }
}
