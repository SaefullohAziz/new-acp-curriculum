<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('city_id');
            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('cascade')->onDelete('cascade');
            $table->string('type');
            $table->string('name');
            $table->text('address');
            $table->string('since');
            $table->string('school_phone_number');
            $table->string('school_email');
            $table->string('school_web');
            $table->integer('total_student');
            $table->text('department');
            $table->string('iso_certificate');
            $table->string('headmaster_name');
            $table->string('headmaster_phone_number');
            $table->string('headmaster_email');
            $table->string('proposal');
            $table->string('reference');
            $table->string('dealer_name')->nullable();
            $table->string('dealer_phone_number')->nullable();
            $table->string('dealer_email')->nullable();
            $table->string('document')->nullable();
            $table->string('notif')->nullable();
            $table->string('code')->nullable();
            $table->string('zip_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
