<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerDocumentRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_document_requirements', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('partner_requirement_id');
            $table->foreign('partner_requirement_id')->references('id')->on('partner_requirements')->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('required_document_id');
            $table->foreign('required_document_id')->references('id')->on('required_documents')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_document_requirements');
    }
}
