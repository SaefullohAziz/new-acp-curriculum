<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('home') }}">{{ config('app.shortname', 'LV') }}</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">{{ __('Dashboard') }}</li>
      <li class="{{ (request()->is('facilitator/home')?'active':'') }}">
        <a class="nav-link" href="{{ route('teacher.home') }}">
          <i class="fa fa-home"></i> <span>{{ __('Home') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Menu') }}</li>
      <li class="{{ (request()->is('facilitator/school/*') || request()->is('facilitator/school')?'active':'') }}">
        <a class="nav-link" href="{{ route('teacher.school.show', Auth()->user()->teacher()->first()->school->id) }}">
          <i class="fa fa-university"></i> <span>{{ __('School') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('facilitator/lesson/*') || request()->is('facilitator/lesson')?'active':'') }}">
        <a class="nav-link" href="{{ route('teacher.lesson.index') }}">
          <i class="fa fa-chalkboard-teacher"></i> <span>{{ __('Lesson') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('facilitator/student/*') || request()->is('facilitator/student')?'active':'') }}">
        <a class="nav-link" href="{{ route('teacher.student.index') }}">
          <i class="fa fa-user-graduate"></i> <span>{{ __('Student') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Update') }}</li>
      <li class="{{ (request()->is('facilitator/newStudent/*') || request()->is('facilitator/newStudent')?'active':'') }}">
        <a class="nav-link" href="{{ route('teacher.newStudent.index') }}">
          <i class="fa fa-user"></i> <span>{{ __('Peserta Baru') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('teacher/update/*') || request()->is('teacher/update')?'active':'') }}">
        <a class="nav-link" href="{{ route('teacher.update.index') }}">
          <i class="fas fa-clipboard-list"></i> <span>{{ __('Update Data') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Logout') }}</li>
      <li>
        <a class="nav-link text-danger" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i> <span>{{ __('Logout') }}</span>
        </a>
      </li>
    </ul>
  </aside>
</div>