<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
  <form class="form-inline mr-auto">
    <ul class="navbar-nav mr-3">
      <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
    </ul>
  </form>
  @if(auth()->guard('teacher')->check())
    @php $new_student = App\Student::where('registered_status', 'waiting')
        ->whereHas('generation', function($query){
            $query->where('school_id', auth()->user()->teacher->school_id);
        })->get(); 
    @endphp
    <ul class="navbar-nav navbar-right">
      <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg {{ $new_student ? 'beep' : '' }}" aria-expanded="false"><i class="far fa-bell"></i></a>
        <div class="dropdown-menu dropdown-list dropdown-menu-right">
          <div class="dropdown-header">Notifications</div>
          <div class="dropdown-list-content dropdown-list-icons" tabindex="3" style="overflow: hidden; outline: none;">
              @if($new_student)
                @foreach($new_student as $student)
                  @php
                    $one = new \DateTime($student->created_at);
                    $two = new \DateTime(date('Y-m-d H:i:s'));
                    $diff = $one->diff($two);
                    if($diff->format('%h') != 0){
                      $get = $diff->format('%h') . ' Hour Ago';
                    }else{
                      $get = $diff->format('%i') . ' Min Ago';
                    }
                  @endphp
                  <a href="{{ route('teacher.newStudent.index') }}" class="dropdown-item dropdown-item-unread">
                    <div class="dropdown-item-icon bg-primary text-white">
                      <i class="fas fa-bell"></i>
                    </div>
                    <div class="dropdown-item-desc">
                    <b>{{ $student->name }}</b> mengajukan untuk masuk ke akademi
                      <div class="time text-primary">{{ $get }}</div>
                    </div>
                  </a>
                @endforeach
          </div>
          <div class="dropdown-footer text-center">
            <a href="#">View All <i class="fas fa-chevron-right"></i></a>
          </div>
              <!-- <div id="ascrail2002" class="nicescroll-rails nicescroll-rails-vr" style="width: 9px; z-index: 1000; cursor: default; position: absolute; top: 58px; left: 341px; height: 150px; opacity: 0.3; display: none;"><div class="nicescroll-cursors" style="position: relative; top: 0px; float: right; width: 7px; height: 306px; background-color: rgb(66, 66, 66); border: 1px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 5px;"></div></div><div id="ascrail2002-hr" class="nicescroll-rails nicescroll-rails-hr" style="height: 9px; z-index: 1000; top: 399px; left: 0px; position: absolute; cursor: default; display: none; width: 341px; opacity: 0.3;"><div class="nicescroll-cursors" style="position: absolute; top: 0px; height: 7px; width: 350px; background-color: rgb(66, 66, 66); border: 1px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 5px; left: 0px;"></div></div></div> -->
          </div>
      </li>
      <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
        <img alt="image" src="{{ asset(auth()->user()->avatar ? : 'img/avatar/avatar.png') }}" class="rounded-circle mr-1">
        <div class="d-sm-none d-lg-inline-block">{{ __('Hi') }}, {{ auth()->user()->name }}</div></a>
        <div class="dropdown-menu dropdown-menu-right">
          <a href="{{ route('teacher.account.me') }}" class="dropdown-item has-icon">
            <i class="far fa-user"></i> {{ __('Profile') }}
          </a>
          <!-- <a href="#" class="dropdown-item has-icon">
            <i class="fas fa-cog"></i> {{ __('Setting') }}
          </a> -->
          <!-- <div class="dropdown-divider"></div> -->
          <a class="dropdown-item has-icon text-danger" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
    </ul>
    @endif
  @endif
</nav>