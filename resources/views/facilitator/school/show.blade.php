@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-12">

        @if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
        @endif
        
        <div class="card card-hero">
            <div class="card-header p-4">
                <div class="card-icon">
                    <i class="fas fa-university"></i>
                </div>
                <h4>{{ $data->status->first()->name }}</h4>
                <div class="card-description">{{ $data->status->first()->name }}</div>
            </div>
            <div class="card-body p-0">
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-body">
                <legend>{{ __('School Data') }}</legend>
                <div class="row mb-3">
                    {{ Form::bsSelect('col-lg-6', __('Type'), 'type', ['Negeri' => 'Negeri', 'Swasta' => 'Swasta'], $data->type, __('Select'), ['placeholder' => __('Select'), 'disabled' => '']) }}
                    
                    {{ Form::bsText('col-lg-6', __('Name'), 'name', $data->name, __('Name'), ['disabled' => '']) }}
                    
                    {{ Form::bsTextarea('col-lg-6', __('Address'), 'address', $data->address, __('Address'), ['disabled' => '']) }}

                    {{ Form::bsSelect('col-lg-6', __('Province'), 'province', $data->city->province, $data->city->province, __('Select'), ['placeholder' => __('Select'), 'disabled' => '']) }}

                    {{ Form::bsSelect('col-lg-6', __('Regency'), 'regency', $data->city, $data->city, __('Select'), ['placeholder' => __('Select'), 'disabled' => '']) }}

                    {{ Form::bsText('col-lg-6', __('Since'), 'since', $data->since, __('Since'), ['maxlength' => '4', 'disabled' => '']) }}

                    {{ Form::bsPhoneNumber('col-lg-6', __('School Phone Number'), 'school_phone_number', $data->school_phone_number, __('School Phone Number'), ['maxlength' => '13', 'disabled' => '']) }}

                    {{ Form::bsEmail('col-lg-6', __('School E-Mail'), 'school_email', $data->school_email, __('School E-Mail'), ['disabled' => '']) }}

                    {{ Form::bsText('col-lg-6', __('School Website (URL)'), 'school_web', $data->school_web, __('School Website (URL)'), ['disabled' => '']) }}

                    {{ Form::bsText('col-lg-6', __('Total Student'), 'total_student', $data->total_student, __('Total Student'), ['disabled' => '']) }}
                </div>
                <legend>{{ __('Headmaster Data') }}</legend>
                <div class="row">
                    {{ Form::bsText('col-lg-6', __('Headmaster Name'), 'headmaster_name', $data->headmaster_name, __('Headmaster Name'), ['disabled' => ''], ['Complete with an academic degree and or degree of expertise.']) }}
                    
                    {{ Form::bsPhoneNumber('col-lg-6', __('Headmaster Phone Number'), 'headmaster_phone_number', $data->headmaster_phone_number, __('Headmaster Phone Number'), ['maxlength' => '13', 'disabled' => '']) }}
                    
                    {{ Form::bsEmail('col-lg-6', __('Headmaster E-Mail'), 'headmaster_email', $data->headmaster_email, __('Headmaster E-Mail'), ['disabled' => '']) }}
                </div>

            </div>
        </div>
        
        <div class="card card-primary">
            <div class="card-body">
                <legend>{{ __('Student Data') }}</legend>
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col" class="text-capitalize">Name</th>
                            <th scope="col" class="text-capitalize">Generation</th>
                            <th scope="col" class="text-capitalize">Student Status</th>
                            <th scope="col" class="text-capitalize">Email</th>
                            <th scope="col" class="text-capitalize">Gender	</th>
                        </tr>
                        <tr>
                            <td scope="row">1</td>
                            <td>Ramdan Adriansah</td>
                            <td>Generation 1</td>
                            <td>Alumni</td>
                            <td>ramdan.adriansyah@gmail.com</td>
                            <td>Laki-Laki</td>
                        </tr>
                        <tr>
                            <td scope="row">2</td>
                            <td>Khabil Putra Pratama</td>
                            <td>Generation 1</td>
                            <td>Siswa Aktif (XI)</td>
                            <td>khabil.putra@gmail.com</td>
                            <td>Laki-Laki</td>
                        </tr>
                        <tr>
                            <td scope="row">3</td>
                            <td>Agus Hermawan</td>
                            <td>Generation 1</td>
                            <td>Alumni</td>
                            <td>agus.hermawan@gmail.com</td>
                            <td>Laki-Laki</td>
                        </tr>
                        <tr>
                            <td scope="row">4</td>
                            <td>Riza Ismansyah</td>
                            <td>Generation 1</td>
                            <td>Alumni</td>
                            <td>rizaismansyah@gmail.com</td>
                            <td>Laki-Laki</td>
                        </tr>
                        <tr>
                            <td scope="row">5</td>
                            <td>Ardian Alam Nur Cahyo</td>
                            <td>Generation 1</td>
                            <td>Siswa Aktif (XI)</td>
                            <td>alam.nur@gmail.com</td>
                            <td>Laki-Laki</td>
                        </tr>
                        <tr>
                            <td scope="row">6</td>
                            <td>Ahmad rizal muttadho</td>
                            <td>Generation 1</td>
                            <td>Siswa Aktif (XI)</td>
                            <td>a.rizahmuttadho@gmail.com</td>
                            <td>Laki-Laki</td>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    
    </div>
</div>

@endsection