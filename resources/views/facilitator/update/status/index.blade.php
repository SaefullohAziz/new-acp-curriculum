@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">
        <div id="table1" class="card card-primary">
			<div class="card-header">
                <button class="btn btn-icon btn-secondary" title="{{ __('Filter') }}" data-toggle="modal" data-target="#filterModal"><i class="fa fa-filter"></i></button>
				<button class="btn btn-icon btn-secondary" onclick="reloadTable()" title="{{ __('Refresh') }}"><i class="fa fa-sync"></i></i></button>
				<a class="btn btn-icon collapsed btn-secondary" title=" {{ __('Setting Table') }}" data-toggle="collapse" href="#showHide" role="button" aria-expanded="false" aria-controls="showHide"><i class="fa fa-cog"></i></a>
            </div>
                <div class="collapse ml-4 mb-3" id="showHide">
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <div class="section-title" style="margin: 0px 0px 10px 0px">{{ __('Show/Hide Column') }}</div>
                            <div class="row">
								@php $no = 1; @endphp
								@foreach($columns as $column)
								<div class="custom-control custom-checkbox ml-3">
									<input type="checkbox" class="custom-control-input" id="column{{ str_replace(' ', '', $column) }}" checked>
									<label class="custom-control-label showHide" data-column="{{ $no++ }}" for="column{{ str_replace(' ', '', $column) }}">{{ $column }}</label>
								</div>
								@endforeach
                            </div>
                        </div>
                    </div>
                </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped" id="table4data">
                        <thead>
                            <tr>
                                <th><div class="checkbox icheck"><label><input type="checkbox" name="selectData"></label></div></th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Generation') }}</th>
                                <th>{{ __('Phone Number') }}</th>
                                <th>{{ __('Place of Birth') }}</th>
                                <th>{{ __('Date of Birth') }}</th>
                                <th>{{ __('Document Progress') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="card-footer bg-whitesmoke">
				<button class="btn btn-primary btn-sm" name="moveStatus" title="{{ __('Pindah Status') }}">{{ __('Pindah Status') }}</button>
			</div>
        </div>

        <div id="table2" class="card card-primary mt-5 d-none">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped" id="table4data2">
                        <thead>
                            <tr>
                                <th><div class="checkbox icheck"><label><input type="checkbox" name="selectData2"></label></div></th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Dari Status') }}</th>
                                <th>{{ __('Ke Status') }}</th>
                                <th>{{ __('Generation') }}</th>
                                <th>{{ __('School') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    {{ Form::bsSelect(null, __('Status'), 'status', $moveStatus, old('status'), __('Select'), ['placeholder' => __('Select')]) }}
                </div>
                <div class="col-lg-4">
                    <label for="proses">Proses</label>
                    <div class="btn btn-primary btn-large btn-block btn-save">Proses</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    let table;
    let status;
    function search(data, column) {
        table.columns(column).search(data).draw();
    }
	$(document).ready(function() {
		table = $('#table4data').DataTable({
			processing: true,
			serverSide: true,
			"ajax": {
				"url": "{{ route('teacher.update.list') }}",
				"type": "POST",
				"data": function (d) {
		          d._token = "{{ csrf_token() }}";
				  d.student = $('select[name="student"]').val();
				  d.school = $('select[name="school"]').val();
				  d.generation = $('select[name="generation"]').val();
				  d.level_siswa = $('select[name="level_siswa"]').val();
				  d.status_siswa = $('select[name="status_siswa"]').val();
		        }
			},
			columns: [
				{ data: 'DT_RowIndex', name: 'DT_RowIndex', 'searchable': false },
				{ data: 'name', name: 'name' },
				{ data: 'status', name: 'status' },
				{ data: 'generation', name: 'generation'},
				{ data: 'phone_number', name: 'phone_number' },
				{ data: 'place_of_birth', name: 'place_of_birth' },
				{ data: 'date_of_birth', name: 'date_of_birth' },
				{ data: 'document_progress', name: 'document_progress' },
			],
			"order": [[ 1, 'desc' ]],
			"columnDefs": [
			{   
          		"targets": [ 0, -1 ], //last column
          		"orderable": false, //set not orderable
      		},
      		],
      		"drawCallback": function(settings) {
      			$('input[name="selectData"], input[name="selectedData[]"]').iCheck({
      				checkboxClass: 'icheckbox_flat-green',
      				radioClass: 'iradio_flat-green',
      				increaseArea: '20%' /* optional */
      			});
                
                $('input[name="selectedData[]"]').on('ifChecked', function(event) {
                    if (status !== $(this).closest('tr').find('td:nth-child(3)').text()) {
                        status = $(this).closest('tr').find('td:nth-child(3)').text();
                        search($(this).closest('tr').find('td:nth-child(3)').text(), 2);
                    }
                });

      			$('[name="selectData"]').on('ifChecked', function(event){
      				$('[name="selectedData[]"]').iCheck('check');
      			}).on('ifUnchecked', function(event){
      				$('[name="selectedData[]"]').iCheck('uncheck');
      			});
                  
      			$('input[name="selectData2"], input[name="selectedData2[]"]').iCheck({
      				checkboxClass: 'icheckbox_flat-green',
      				radioClass: 'iradio_flat-green',
      				increaseArea: '20%' /* optional */
      			});
      			$('[name="selectData2"]').on('ifChecked', function(event){
      				$('[name="selectedData2[]"]').iCheck('check');
      			}).on('ifUnchecked', function(event){
      				$('[name="selectedData2[]"]').iCheck('uncheck');
      			});

				if($("#tab-scroll").length) {
				    $("#tab-scroll").css({
				        height: 25,
				        width: 250,
				    }).niceScroll();
				}
      		},
  		});
        $('[name="moveStatus"]').click(function(event) {
            event.preventDefault();
            let selected = $('#table4data [name="selectedData[]"]:checked');
            if (selected.length > 0) {
                $('#table2').removeClass('d-none');
                selected.closest('tr').each(function(){
                    var cells = $('td', this);
                    $('#table4data2').append('<tr><td>' + cells.eq(0).html() + '</td><td>' + cells.eq(1).text() + '</td><td>' + cells.eq(2).text() + '</td><td>-</td><td>' + cells.eq(3).text() + '</td><td>' + cells.eq(4).text() + '</td></tr>');
                });
            } else {
                swal("{{ __('Please select a data..') }}", "", "warning");
            }
        });
          
        $('label.showHide').on('click', function(e){
            // Get the column API object
            var column = table.column( $(this).attr('data-column') );
    
            // Toggle the visibility
            column.visible( ! column.visible() );
        });
	});
    
    function reloadTable() {
        table.ajax.reload(null,false); //reload datatable ajax
        $('[name="selectData"]').iCheck('uncheck');
    }
</script>
@endsection