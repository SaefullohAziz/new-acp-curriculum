@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-lg-8">
    <div class="card profile-widget">
      <div class="profile-widget-header">                     
        <img alt="image" src="{{ asset('img/avatar/avatar.png') }}" class="rounded-circle profile-widget-picture">
        <div class="profile-widget-items">
          <div class="profile-widget-item">
            <div class="profile-widget-item-value">Nama</div>
            <div class="profile-widget-item-label">{{ $data->name }}</div>
          </div>
          <div class="profile-widget-item">
            <div class="profile-widget-item-value">Akademi</div>
            <div class="profile-widget-item-label">{{ $data->school->name }}</div>
          </div>
          <div class="profile-widget-item">
            <div class="profile-widget-item-value">Status</div>
            <div class="profile-widget-item-label">{{ $data->study_status ? $data->study_status : '-' }}</div>
          </div>
        </div>
      </div>
      <div class="profile-widget-description">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
          <a class="nav-link active show" id="profil-tab" data-toggle="tab" href="#profil" role="tab" aria-controls="profil" aria-selected="true">Profil</a>
          </li>
          <li class="nav-item">
          <a class="nav-link" id="dokumen-tab" data-toggle="tab" href="#dokumen" role="tab" aria-controls="dokumen" aria-selected="false">Dokumen</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade active show" id="profil" role="tabpanel" aria-labelledby="profil-tab">
            <div class="row">
              {{ Form::bsText('col-lg-6', ('Nama'), 'name', $data->name, $data->name, ['disabled' => '']) }}
              
              {{ Form::bsText('col-lg-6', ('Akademi'), 'academy', $data->school->name, $data->school->name, ['disabled' => '']) }}
              
              {{ Form::bsText('col-lg-6', ('Tempat Lahir'), 'place_of_birth', $data->place_of_birth, $data->place_of_birth, ['disabled' => '']) }}  
              
              {{ Form::bsText('col-lg-6', ('Tanggal Lahir'), 'date_of_birth', $data->date_of_birth, $data->date_of_birth, ['disabled' => '']) }}  
              
              {{ Form::bsText('col-lg-6', ('Jenis Kelamin'), 'gender', $data->gender, $data->gender, ['disabled' => '']) }}  
              
              {{ Form::bsText('col-lg-6', ('Nomor Telpon'), 'phone_number', '(+62) '.$data->phone_number, ('Nomor Telpon'), ['disabled' => '']) }}
              
              {{ Form::bsTextAppend('col-lg-6', ('Berat Badan'), 'weight', $data->weight, $data->weight, 'Kg', ['disabled' => '']) }}  
              
              {{ Form::bsTextAppend('col-lg-6', __('Tinggi Badan'), 'height', $data->height, $data->height, 'Cm', ['disabled' => '']) }}  
            </div>
          </div>
          <div class="tab-pane fade" id="dokumen" role="tabpanel" aria-labelledby="dokumen-tab">
            <div class="row">
              <div class="col-lg-6">
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-warning mb-1 float-right">Incomplete</div>
                    <h6 class="media-title">Kartu Identitas/KTP/Kartu Pelajar</h6>
                    <div class="text-small text-muted">Tidak ada data</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
                    <h6 class="media-title">Surat Keterangan Domisili</h6>
                    <div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-warning mb-1 float-right">Incomplete</div>
                    <h6 class="media-title">Nomor Pokok Wajib Pajak (NPWP)</h6>
                    <div class="text-small text-muted">Tidak ada data</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
                    <h6 class="media-title">Kartu BPJS/KIS/KJS (Asuransi Kesehatan)</h6>
                    <div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
                    <h6 class="media-title">Foto Buku / No Rekening BCA</h6>
                    <div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-warning mb-1 float-right">Incomplete</div>
                    <h6 class="media-title">Surat Keterangan Bebas Narkotika (SKBN)</h6>
                    <div class="text-small text-muted">Tidak ada data</div>
                  </div>
                </li>
              </div>
              <div class="col-lg-6">
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
                    <h6 class="media-title">Surat Keterangan Kelakuan Baik (SKCK)</h6>
                    <div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
                    <h6 class="media-title">Ijazah (SMK/SMA/Sederajat bagi yang sudah lulus)</h6>
                    <div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
                    <h6 class="media-title">SIM C</h6>
                    <div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-warning mb-1 float-right">Incomplete</div>
                    <h6 class="media-title">SIM A</h6>
                    <div class="text-small text-muted">Tidak ada data</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
                    <h6 class="media-title">Curriculum Vitae (CV)</h6>
                    <div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
                  </div>
                </li>
                <li class="media mt-3 mb-3">
                  <div class="media-body">
                    <div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
                    <h6 class="media-title">Surat Lamaran</h6>
                    <div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
                  </div>
                </li>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection