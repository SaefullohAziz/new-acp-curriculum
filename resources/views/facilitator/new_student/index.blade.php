@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-primary">
			<div class="card-header">
				<button class="btn btn-icon btn-secondary" title="{{ __('Filter') }}" data-toggle="modal" data-target="#filterModal"><i class="fa fa-filter"></i></button>
				<button class="btn btn-icon btn-secondary" onclick="reloadTable()" title="{{ __('Refresh') }}"><i class="fa fa-sync"></i></i></button>
				<a class="btn btn-icon collapsed btn-secondary" title=" {{ __('Setting Table') }}" data-toggle="collapse" href="#showHide" role="button" aria-expanded="false" aria-controls="showHide"><i class="fa fa-cog"></i></a>
			</div>
				<div class="collapse ml-4 mb-3" id="showHide">
					<div class="row">
						<div class="col-lg-12 mb-3">
							<div class="section-title" style="margin: 0px 0px 10px 0px">{{ __('Show/Hide Column') }}</div>
							<div class="row">
								@php $no = 1; @endphp
								@foreach($columns as $column)
								<div class="custom-control custom-checkbox ml-3">
									<input type="checkbox" class="custom-control-input" id="column{{ str_replace(' ', '', $column) }}" checked>
									<label class="custom-control-label showHide" data-column="{{ $no++ }}" for="column{{ str_replace(' ', '', $column) }}">{{ $column }}</label>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-sm table-striped" id="table4data">
						<thead>
							<tr>
								<th>
									<div class="checkbox icheck"><label><input type="checkbox" name="selectData"></label></div>
								</th>
								<th>{{ __('Created At') }}</th>
								<th>{{ __('Nama') }}</th>
								<th>{{ __('Nomor Telepon') }}</th>
								<th>{{ __('E-mail') }}</th>
								<th>{{ __('Alamat') }}</th>
								<th>{{ __('Asal Sekolah') }}</th>
								<th>{{ __('Progres Dokumen') }}</th>
								<th>{{ __('Action') }}</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-footer bg-whitesmoke">
				<button class="btn btn-success btn-sm" name="approve" title="{{ __('Approve') }}">{{ __('Accept') }}</button>
				<button class="btn btn-warning btn-sm" name="reject" title="{{ __('Reject') }}">{{ __('Reject') }}</button>
				<button class="btn btn-danger btn-sm" name="deleteData" title="{{ __('Delete') }}">{{ __('Delete') }}</button>
			</div>
		</div>

	</div>
</div>
@endsection

@section('script')
<script>
	var table;
	$(document).ready(function() {
		table = $('#table4data').DataTable({
			processing: true,
			serverSide: true,
			"ajax": {
				"url": "{{ route('teacher.newStudent.list') }}",
				"type": "POST",
				"data": function (d) {
		          d._token = "{{ csrf_token() }}";
		        }
			},
			columns: [
				{ data: 'DT_RowIndex', name: 'DT_RowIndex', 'searchable': false },
				{ data: 'created_at', name: 'created_at' },
				{ data: 'name', name: 'name' },
				{ data: 'phone_number', name: 'phone_number' },
				{ data: 'email', name: 'email' },
				{ data: 'address', name: 'address' },
				{ data: 'school_origin', name: 'school_origin' },
				{ data: 'document_progress', name: 'document_progress' },
				{ data: 'action', name: 'action' }
			],
			"order": [[ 1, 'desc' ]],
			"columnDefs": [
			{   
          		"targets": [ 0, -1 ], //last column
          		"orderable": false, //set not orderable
      		},
      		],
      		"drawCallback": function(settings) {
      			$('input[name="selectData"], input[name="selectedData[]"]').iCheck({
      				checkboxClass: 'icheckbox_flat-green',
      				radioClass: 'iradio_flat-green',
      				increaseArea: '20%' /* optional */
      			});
      			$('[name="selectData"]').on('ifChecked', function(event){
      				$('[name="selectedData[]"]').iCheck('check');
      			}).on('ifUnchecked', function(event){
      				$('[name="selectedData[]"]').iCheck('uncheck');
      			});
				// update student status
				$('select[name="status"]').change(function () {
					let id = $(this).data('id');
					$.ajax({
						url : "{{ route('teacher.student.index') }}/" + id,
						type: "POST",
						dataType: "JSON",
						data: {
								'_token' : '{{ csrf_token() }}',
								'_method' : 'PUT',
								'status_id' : $(this).val(),
							  },
						success: function(data)
						{
							swal("{{ __('Success!') }}", "", "success");
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
							swal("{{ __('Failed!') }}", "", "warning");
						}
					});
				});
				if($("#tab-scroll").length) {
				    $("#tab-scroll").css({
				        height: 25,
				        width: 250,
				    }).niceScroll();
				}
      		},
  		});

		$('[name="deleteData"]').click(function(event) {
			if ($('[name="selectedData[]"]:checked').length > 0) {
				event.preventDefault();
				var selectedData = $('[name="selectedData[]"]:checked').map(function(){
					return $(this).val();
				}).get();
				swal({
			      	title: '{{ __("Are you sure want to delete this data?") }}',
			      	text: '',
			      	icon: 'warning',
			      	buttons: ['{{ __("Cancel") }}', true],
			      	dangerMode: true,
			    })
			    .then((willDelete) => {
			      	if (willDelete) {
			      		$.ajax({
							url : "{{ route('teacher.student.destroy') }}",
							type: "DELETE",
							dataType: "JSON",
							data: {"selectedData" : selectedData, "_token" : "{{ csrf_token() }}"},
							success: function(data)
							{
								reloadTable();
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								if (JSON.parse(jqXHR.responseText).status) {
									swal("{{ __('Failed!') }}", "{{ __("Data cannot be deleted.") }}", "warning");
								} else {
									swal(JSON.parse(jqXHR.responseText).message, "", "error");
								}
							}
						});
			      	}
    			});
			} else {
				swal("{{ __('Please select a data..') }}", "", "warning");
			}
		});

		$('[name="approve"]').click(function(event) {
			if ($('[name="selectedData[]"]:checked').length > 0) {
				event.preventDefault();
				var selectedData = $('[name="selectedData[]"]:checked').map(function(){
					return $(this).val();
				}).get();
				swal({
			      	title: '{{ __("Are you sure want to approve this student?") }}',
			      	text: '',
			      	icon: 'warning',
			      	buttons: ['{{ __("Cancel") }}', true],
			      	dangerMode: true,
			    })
			    .then((willReset) => {
			      	if (willReset) {
			      		$.ajax({
							url : "{{ route('teacher.newStudent.approve') }}",
							type: "DELETE",
							dataType: "JSON",
							data: {"_token" : "{{ csrf_token() }}", "selectedData" : selectedData},
							success: function(data)
							{
								swal("{{ __('Success!') }}", '{{ __("Approved succeed.") }}', "success");
								reloadTable();
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								if (JSON.parse(jqXHR.responseText).status) {
									swal("{{ __('Failed!') }}", '{{ __("Data cannot be approved.") }}', "warning");
								} else {
									swal(JSON.parse(jqXHR.responseText).message, "", "error");
								}
							}
						});
			      	}
    			});
			} else {
				swal("{{ __('Please select a data..') }}", "", "warning");
			}
		});

		$('[name="reject"]').click(function(event) {
			if ($('[name="selectedData[]"]:checked').length > 0) {
				event.preventDefault();
				var selectedData = $('[name="selectedData[]"]:checked').map(function(){
					return $(this).val();
				}).get();
				swal({
			      	title: '{{ __("Are you sure want to reject this student?") }}',
			      	text: '',
			      	icon: 'warning',
			      	buttons: ['{{ __("Cancel") }}', true],
			      	dangerMode: true,
			    })
			    .then((willReset) => {
			      	if (willReset) {
			      		$.ajax({
							url : "{{ route('teacher.newStudent.reject') }}",
							type: "DELETE",
							dataType: "JSON",
							data: {"_token" : "{{ csrf_token() }}", "selectedData" : selectedData},
							success: function(data)
							{
								swal("{{ __('Success!') }}", '{{ __("Rejected succeed.") }}', "success");
								reloadTable();
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								if (JSON.parse(jqXHR.responseText).status) {
									swal("{{ __('Failed!') }}", '{{ __("Data cannot be rejected.") }}', "warning");
								} else {
									swal(JSON.parse(jqXHR.responseText).message, "", "error");
								}
							}
						});
			      	}
    			});
			} else {
				swal("{{ __('Please select a data..') }}", "", "warning");
			}
		});
				
		$('label.showHide').on('click', function(e){
            // Get the column API object
            var column = table.column( $(this).attr('data-column') );
    
            // Toggle the visibility
            column.visible( ! column.visible() );
        });
	});

	function reloadTable() {
	    table.ajax.reload(null,false); //reload datatable ajax
	    $('[name="selectData"]').iCheck('uncheck');
	}

	function filter() {
		reloadTable();
		$('#filterModal').modal('hide');
	}

	
</script>
@endsection