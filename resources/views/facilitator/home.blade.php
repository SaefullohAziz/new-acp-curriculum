@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-4">
        <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
                <i class="fab fa-elementor"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header"><h4>Total Generasi</h4></div>
                <div class="card-body">2</div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
                <i class="fas fa-user-graduate"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header"><h4>Total Siswa</h4></div>
                <div class="card-body">32</div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
                <i class="fas fa-briefcase"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header"><h4>Total Magang</h4></div>
                <div class="card-body">12</div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header"><h4>Data Peserta Magang</h4></div>
            <canvas id="totalVokasiPkt" style="height:300px; width:100%"></canvas>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header"><h4>Pencapaian Rata-rata</h4>
                <div class="card-header-action">
                    <div class="dropdown show">
                        <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" aria-expanded="true">Filter Siswa</a>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-132px, 26px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <a href="#" class="dropdown-item has-icon"> Dian Ardianto</a>
                            <a href="#" class="dropdown-item has-icon"> Fadli Nur Ramdani</a>
                            <a href="#" class="dropdown-item has-icon"> Irpan Alip Wibisana</a>
                            <a href="#" class="dropdown-item has-icon"> A'la Firdaus</a>
                            <a href="#" class="dropdown-item has-icon"> Ari Maulana</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body" id="tab-scroll" tabindex="2" style="height: 315px; overflow: hidden; outline: none;">
                <div class="row">
                    <div class="col-lg-6 mb-4">

                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">45 <small>/ </small> 90 Menit</div>
                            <div class="font-weight-bold mb-1">Band Uang</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="56%" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%;">50%</div>
                            </div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">125 Stack</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">300 Stack</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>

                    </div>
                    <div class="col-lg-6 mb-4">

                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">3 <small>/ </small>10 Kali</div>
                            <div class="font-weight-bold mb-1">Isi Kaset</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">30%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:49 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>

                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Bongkar Kaset</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:42 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">03:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Pengepakan Plastik</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">01:52 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 20 Kali</div>
                            <div class="font-weight-bold mb-1">Pengepakan Kanvas</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                            <div class="progress-bar" role="progressbar" data-width="20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">20%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:15 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:30 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Mengisi Kembali</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                            <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">00:52 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">01:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection()

@section('script')
<script>
if($("#tab-scroll").length) {
    $("#tab-scroll").css({
        height: 300
    }).niceScroll();
}

// Showing Total Vocation PKT Data in Polar Area Chart
let totalVokasiPktCtx = $('#totalVokasiPkt');
let totalVokasiPkt = new Chart(totalVokasiPktCtx, {
    type: 'polarArea',
    data: {
        labels: ['ACS','ADP','ASCM','ANDALAN','ARMORINDO','TAG'], // ,'PKT 7','PKT 8'
        datasets: [
            {
                defaultFontSize: '16',
                backgroundColor: ["rgba(252, 84, 75, 0.6)","rgba(255, 164, 38, 0.6)","rgba(253, 247, 76, 0.6)","rgba(99, 237, 122, 0.6)","rgba(58, 186, 244, 0.6)","rgba(103, 119, 239, 0.6)"],
                borderColor: ["rgb(255, 255, 255)","rgb(255, 255, 255)","rgb(255, 255, 255)","rgb(255, 255, 255)","rgb(255, 255, 255)","rgb(255, 255, 255)"],
                data: [2, 0, 2, 2, 3, 3] // , 21, 16
            }
        ],
    },
    options: {
        responsive: true,
        tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					var allData = data.datasets[tooltipItem.datasetIndex].data;
					var tooltipLabel = data.labels[tooltipItem.index];
					var tooltipData = allData[tooltipItem.index];
					var total = 0;
					for (var i in allData) {
						total += allData[i];
					}
					var tooltipPercentage = Math.round((tooltipData / total) * 100);
					return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
				}
			}
		}
    },
});

</script>
@endsection()