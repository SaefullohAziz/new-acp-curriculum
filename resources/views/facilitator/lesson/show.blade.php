@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-primary">
			<div class="card-body">
				<legend>Lesson Class</legend>
				<div class="row">
					{{ Form::bsText('col-sm-6', __('Name'), 'name', $data->name, __('Name'), ['disabled' => '']) }}

					{{ Form::bsSelect('col-sm-6', __('Generation'), 'generation', ['1' => '1', '2' => '2'], old('generation'), __('Select'), ['placeholder' => __('Select'), 'disabled' => '']) }}

					{{ Form::bsText('col-sm-6', __('Start Time'), 'start_time',  $data->start_time, __('Start Time'), ['disabled' => '']) }}
					
					{{ Form::bsText('col-sm-6', __('End Time'), 'end_time',  $data->end_time, __('End Time'), ['disabled' => '']) }}

					{{ Form::bsText('col-sm-6', __('Max Absend'), 'max_absend',  $data->max_absend_at, __('Max Absend'), ['disabled' => '']) }}
					
					{{ Form::bsText('col-sm-6', __('Max Reported'), 'max_reported', $data->max_reported_at, __('Max Reported'), ['disabled' => '']) }}

					{{ Form::bsTextarea('col-sm-12', __('Description'), 'description', $data->description, __('Description'), ['disabled' => '']) }}
				</div>
			</div>
		</div>
		<div class="card card-primary">
			<div class="card-body">
				<legend>Lesson Skill</legend>
				<div id="accordion" class="mt-3">
					<div class="accordion">
						<div class="accordion-header" role="button" data-toggle="collapse" data-target="#panel-body-1" aria-expanded="true"><h4>Juniarto Anugrah Pratama</h4></div>
						<div class="accordion-body collapse show" id="panel-body-1" data-parent="#accordion">
							<table class="table table-sm mt-3">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col" class="text-capitalize">Created At</th>
										<th scope="col" class="text-capitalize">Lesson Skill</th>
										<th scope="col" class="text-capitalize">Result</th>
										<th scope="col" class="text-capitalize">Photo</th>
										<th scope="col" class="text-capitalize">Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td scope="row">1</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>210 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning photo">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">2</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>221 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>227 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-2020 10:12:21</td>
										<td>Packing Canvas Bag</td>
										<td>0,63 Minutes</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-	2020 10:12:21</td>
										<td>Packing Canvas Bag</td>
										<td>0,65 Minutes</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="accordion">
						<div class="accordion-header" role="button" data-toggle="collapse" data-target="#panel-body-2"><h4>Khabil Putra Pratama</h4></div>
						<div class="accordion-body collapse" id="panel-body-2" data-parent="#accordion">
							<table class="table table-sm mt-3">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col" class="text-capitalize">Created At</th>
										<th scope="col" class="text-capitalize">Lesson Skill</th>
										<th scope="col" class="text-capitalize">Result</th>
										<th scope="col" class="text-capitalize">Photo</th>
										<th scope="col" class="text-capitalize">Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td scope="row">1</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>210 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">2</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>221 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>227 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-2020 10:12:21</td>
										<td>Packing Canvas Bag</td>
										<td>0,63 Minutes</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-	2020 10:12:21</td>
										<td>Packing Canvas Bag</td>
										<td>0,65 Minutes</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="accordion">
						<div class="accordion-header" role="button" data-toggle="collapse" data-target="#panel-body-3"><h4>Maulana Akhbar</h4></div>
						<div class="accordion-body collapse" id="panel-body-3" data-parent="#accordion">
							<table class="table table-sm mt-3">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col" class="text-capitalize">Created At</th>
										<th scope="col" class="text-capitalize">Lesson Skill</th>
										<th scope="col" class="text-capitalize">Result</th>
										<th scope="col" class="text-capitalize">Photo</th>
										<th scope="col" class="text-capitalize">Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td scope="row">1</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>210 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">2</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>221 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-2020 10:12:21</td>
										<td>Money Band</td>
										<td>227 Stack</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-2020 10:12:21</td>
										<td>Packing Canvas Bag</td>
										<td>0,63 Minutes</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
									<tr>
										<td scope="row">3</td>
										<td>07-01-	2020 10:12:21</td>
										<td>Packing Canvas Bag</td>
										<td>0,65 Minutes</td>
										<td><a href="javascript:void(0)"><span class="badge badge-warning">Photo</span></a></td>
										<td><a href="javascript:void(0)"><span class="badge badge-success">Approve</span></a><a href="javascript:void(0)"><span class="badge badge-danger">Decline</span></a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
	$('[name="start_time"]').daterangepicker({
		locale: {format: 'YYYY-MM-DD HH:mm:ss'},
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
	});

	$('[name="end_time"]').daterangepicker({
		locale: {format: 'YYYY-MM-DD HH:mm:ss'},
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
	});

	$('[name="max_absend"]').daterangepicker({
		locale: {format: 'YYYY-MM-DD HH:mm:ss'},
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
	});

	$('[name="max_reported"]').daterangepicker({
		locale: {format: 'YYYY-MM-DD HH:mm:ss'},
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
	});

	$('.photo').on('click', function(){
		$('#exampleModal').modal('show');
	});

</script>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Preview Photo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="{{ asset('storage/student/studentLesson/photo_06_03_2020_07_29_35_120df7be6092b686e8d86f4c2d9ba6ad.png') }}" width="100%">
      </div>
    </div>
  </div>
</div>
@endsection