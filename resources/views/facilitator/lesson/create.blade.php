@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-primary">

			{{ Form::open(['route' => 'teacher.lesson.store']) }}
				<div class="card-body">
					<div class="row">
						<fieldset>
                            <div class="row">
							{{ Form::bsText('col-sm-6', __('Name'), 'name', old('name'), __('Name'), ['required' => '']) }}

							{{ Form::bsSelect('col-sm-6', __('Generation'), 'generation', ['1' => '1', '2' => '2'], old('generation'), __('Select'), ['placeholder' => __('Select')]) }}

							{{ Form::bsText('col-sm-6', __('Start Time'), 'start_time', old('start_time'), __('Start Time'), ['required' => '']) }}
							
							{{ Form::bsText('col-sm-6', __('End Time'), 'end_time', old('end_time'), __('End Time'), ['required' => '']) }}

							{{ Form::bsText('col-sm-6', __('Max Absend'), 'max_absend', old('max_absend'), __('Max Absend'), ['required' => '']) }}
							
							{{ Form::bsText('col-sm-6', __('Max Reported'), 'max_reported', old('max_reported'), __('Max Reported'), ['required' => '']) }}

							{{ Form::bsTextarea('col-sm-12', __('Description (optional)'), 'description', old('description'), __('Description')) }}
                            </div>
						</fieldset>
					</div>
				</div>
				<div class="card-footer bg-whitesmoke text-center">
					{{ Form::submit(__('Save'), ['class' => 'btn btn-primary']) }}
					{{ link_to(route('teacher.lesson.index'),__('Cancel'), ['class' => 'btn btn-danger']) }}
				</div>
			{{ Form::close() }}

		</div>
	</div>
</div>
@endsection
@section('script')
<script>
	$('[name="start_time"]').daterangepicker({
		locale: {format: 'YYYY-MM-DD HH:mm:ss'},
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
	});

	$('[name="end_time"]').daterangepicker({
		locale: {format: 'YYYY-MM-DD HH:mm:ss'},
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
	});

	$('[name="max_absend"]').daterangepicker({
		locale: {format: 'YYYY-MM-DD HH:mm:ss'},
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
	});

	$('[name="max_reported"]').daterangepicker({
		locale: {format: 'YYYY-MM-DD HH:mm:ss'},
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
	});
</script>
@endsection