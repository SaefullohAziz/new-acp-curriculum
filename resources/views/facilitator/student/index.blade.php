@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-primary">
			<div class="card-header">
				<a href="{{ route('teacher.student.create') }}" class="btn btn-icon btn-success" title="{{ __('Create') }}"><i class="fa fa-plus"></i></a>
				<button class="btn btn-icon btn-secondary" title="{{ __('Filter') }}" data-toggle="modal" data-target="#filterModal"><i class="fa fa-filter"></i></button>
				<button class="btn btn-icon btn-secondary" onclick="reloadTable()" title="{{ __('Refresh') }}"><i class="fa fa-sync"></i></i></button>
				<a class="btn btn-icon collapsed btn-secondary" title=" {{ __('Setting Table') }}" data-toggle="collapse" href="#showHide" role="button" aria-expanded="false" aria-controls="showHide"><i class="fa fa-cog"></i></a>
            	<a href="{{ route('teacher.student.upload') }}" class="btn btn-icon btn-secondary" title="{{ __('Refresh') }}"><i class="fa fa-upload"></i> Import</a>
			</div>
				<div class="collapse ml-4 mb-3" id="showHide">
					<div class="row">
						<div class="col-lg-12 mb-3">
							<div class="section-title" style="margin: 0px 0px 10px 0px">{{ __('Show/Hide Column') }}</div>
							<div class="row">
								@php $no = 1; @endphp
								@foreach($columns as $column)
								<div class="custom-control custom-checkbox ml-3">
									<input type="checkbox" class="custom-control-input" id="column{{ str_replace(' ', '', $column) }}" checked>
									<label class="custom-control-label showHide" data-column="{{ $no++ }}" for="column{{ str_replace(' ', '', $column) }}">{{ $column }}</label>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-sm table-striped" id="table4data">
						<thead>
							<tr>
								<th>
									<div class="checkbox icheck"><label><input type="checkbox" name="selectData"></label></div>
								</th>
								<th>{{ __('Name') }}</th>
								<th>{{ __('Status') }}</th>
								<th>{{ __('Generation') }}</th>
								<th>{{ __('Phone Number') }}</th>
								<th>{{ __('Place of Birth') }}</th>
								<th>{{ __('Date of Birth') }}</th>
								<th>{{ __('Document Progress') }}</th>
								<th>{{ __('Action') }}</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-footer bg-whitesmoke">
				<button class="btn btn-danger btn-sm" name="deleteData" title="{{ __('Delete') }}">{{ __('Delete') }}</button>
			</div>
		</div>

	</div>
</div>
@endsection

@section('script')
<script>
	var table;
	$(document).ready(function() {
		table = $('#table4data').DataTable({
			processing: true,
			serverSide: true,
			"ajax": {
				"url": "{{ route('teacher.student.list') }}",
				"type": "POST",
				"data": function (d) {
		          d._token = "{{ csrf_token() }}";
				  d.student = $('select[name="student"]').val();
		        }
			},
			columns: [
				{ data: 'DT_RowIndex', name: 'DT_RowIndex', 'searchable': false },
				{ data: 'name', name: 'name' },
				{ data: 'status', name: 'status' },
				{ data: 'generation', name: 'generation' },
				{ data: 'phone_number', name: 'phone_number' },
				{ data: 'place_of_birth', name: 'place_of_birth' },
				{ data: 'date_of_birth', name: 'date_of_birth' },
				{ data: 'document_progress', name: 'document_progress' },
				{ data: 'action', name: 'action' }
			],
			"order": [[ 1, 'desc' ]],
			"columnDefs": [
			{   
          		"targets": [ 0, -1 ], //last column
          		"orderable": false, //set not orderable
      		},
      		],
      		"drawCallback": function(settings) {
      			$('input[name="selectData"], input[name="selectedData[]"]').iCheck({
      				checkboxClass: 'icheckbox_flat-green',
      				radioClass: 'iradio_flat-green',
      				increaseArea: '20%' /* optional */
      			});
      			$('[name="selectData"]').on('ifChecked', function(event){
      				$('[name="selectedData[]"]').iCheck('check');
      			}).on('ifUnchecked', function(event){
      				$('[name="selectedData[]"]').iCheck('uncheck');
      			});
				// update student status
				$('select[name="status"]').change(function () {
					let id = $(this).data('id');
					$.ajax({
						url : "{{ route('teacher.student.index') }}/" + id,
						type: "POST",
						dataType: "JSON",
						data: {
								'_token' : '{{ csrf_token() }}',
								'_method' : 'PUT',
								'status_id' : $(this).val(),
							  },
						success: function(data)
						{
							swal("{{ __('Success!') }}", "", "success");
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
							swal("{{ __('Failed!') }}", "", "warning");
						}
					});
				});
				if($("#tab-scroll").length) {
				    $("#tab-scroll").css({
				        height: 25,
				        width: 250,
				    }).niceScroll();
				}
      		},
  		});

		$('[name="deleteData"]').click(function(event) {
			if ($('[name="selectedData[]"]:checked').length > 0) {
				event.preventDefault();
				var selectedData = $('[name="selectedData[]"]:checked').map(function(){
					return $(this).val();
				}).get();
				swal({
			      	title: '{{ __("Are you sure want to delete this data?") }}',
			      	text: '',
			      	icon: 'warning',
			      	buttons: ['{{ __("Cancel") }}', true],
			      	dangerMode: true,
			    })
			    .then((willDelete) => {
			      	if (willDelete) {
			      		$.ajax({
							url : "{{ route('teacher.student.destroy') }}",
							type: "DELETE",
							dataType: "JSON",
							data: {"selectedData" : selectedData, "_token" : "{{ csrf_token() }}"},
							success: function(data)
							{
								reloadTable();
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								if (JSON.parse(jqXHR.responseText).status) {
									swal("{{ __('Failed!') }}", "{{ __("Data cannot be deleted.") }}", "warning");
								} else {
									swal(JSON.parse(jqXHR.responseText).message, "", "error");
								}
							}
						});
			      	}
    			});
			} else {
				swal("{{ __('Please select a data..') }}", "", "warning");
			}
		});
		
		$('label.showHide').on('click', function(e){
            // Get the column API object
            var column = table.column( $(this).attr('data-column') );
    
            // Toggle the visibility
            column.visible( ! column.visible() );
        });
	});

	function reloadTable() {
	    table.ajax.reload(null,false); //reload datatable ajax
	    $('[name="selectData"]').iCheck('uncheck');
	}

	function filter() {
		reloadTable();
		$('#filterModal').modal('hide');
	}

	
</script>

<!-- Modal -->
<div class="modal fade" id="filterModal" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="filterModallLabel">{{ __('Filter') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{{ Form::open(['route' => 'teacher.student.export', 'files' => true]) }}
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
						</div>
					</div>
				</div>
				<div class="modal-footer bg-whitesmoke d-flex justify-content-center">
					{{ Form::submit(__('Export'), ['class' => 'btn btn-primary']) }}
					{{ Form::button(__('Filter'), ['class' => 'btn btn-primary', 'onclick' => 'filter()']) }}
					{{ Form::button(__('Cancel'), ['class' => 'btn btn-secondary', ' data-dismiss' => 'modal']) }}
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endsection