@extends('layouts.main')
@section('style')
<style>
.dropzone {
    cursor: pointer;
    color: #999;
    font-size: 25px;
    width: 100%;
    height: auto;
    padding: 50px 0px !important;
    display: inline-block;
}

.uploads span {
    color: #3c8dbc;
}

.upload input[type="file"] {
    display: none;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="alert alert-primary alert-has-icon">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <div class="alert-title">Petunjuk</div>Gunakan template file excel yang sudah kami sediakan dibawah.
            </div>
        </div>

        @if (session('alert-success'))
            <div class="alert alert-success alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-success') }}
                </div>
            </div>
        @endif

        @if (session('alert-danger'))
            <div class="alert alert-danger alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-danger') }}
                </div>
            </div>
        @endif

        <div class="card card-primary">
            <div class="card-header">
                <h4>Import File</h4>
                <div class="card-header-action"><a href="{{ asset('template/Import Siswa.xlsx') }}" class="btn btn-primary">Download Template</a></div>
            </div>
            <div class="card-body">
                <form action="{{ route('teacher.student.import') }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="form-group upload">
                        <label class="dropzone">
                            <i class="fa fa-cloud-upload"></i>
                            <div class="dz-default dz-message"><span>Browse file to upload</span></div>
                            <input type="file" id="fileinput" name="file"></input>
                            <span id="selected_filename">No file selected</span>
                        </label>
                    </div>
                    <button class="btn btn-primary btn-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
  $('#fileinput').change(function() {
    $('#selected_filename').text($('#fileinput')[0].files[0].name);
  });
</script>
@endsection
