@include('layouts.header')

<div id="app">
  <div class="main-wrapper main-wrapper-1">
      @if( empty($header))
        <div class="navbar-bg"></div>
      @endif
      @if(auth()->guard('admin')->check())
        @include('admin.layouts.navbar')
        @include('admin.layouts.sidebar')
      @elseif(auth()->guard('student')->check())
        @include('student.layouts.navbar')
        @include('student.layouts.sidebar')
      @elseif(auth()->guard('partner')->check())
        @include('partner.layouts.navbar')
        @include('partner.layouts.sidebar')
      @elseif(auth()->guard('teacher')->check())
        @include('facilitator.layouts.navbar')
        @include('facilitator.layouts.sidebar')
      @else
        @include('layouts.sidebar')
      @endauth
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          @if( empty($header))
          <div class="section-header">
            @if ( ! empty($back))
              <div class="section-header-back">
                <a href="{{ $back }}" class="btn btn-icon" title="{{ __('Back') }}"><i class="fas fa-arrow-left"></i></a>
              </div>
            @endif
            <h1>{{ $title }}</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="{{ route('home') }}">{{ __('Dashboard') }}</a></div>
              @if ( ! empty($breadcrumbs))
                @foreach ($breadcrumbs as $url => $name)
                  @if ($loop->last)
                    <div class="breadcrumb-item">{{ $name }}</div>
                    @break
                  @endif
                  <div class="breadcrumb-item"><a href="{{ $url }}">{{ $name }}</a></div>
                @endforeach
              @endif
            </div>
          </div>
          @endif

        <div class="section-body">
          @isset($subtitle)
            <h2 class="section-title">
              {{ $subtitle }}
            </h2>
          @endisset
          @isset($description)
            <p class="section-lead">
              {{ $description }}
            </p>
          @endisset
          @yield('content')
        </div>
      </section>
    </div>
    <footer class="main-footer">
      <div class="footer-left">
        {{ __('Copyright') }} &copy; 2019
      </div>
      <div class="footer-right">

      </div>
    </footer>
  </div>
</div>

@include('layouts.footer')