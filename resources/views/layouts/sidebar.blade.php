<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('home') }}">{{ config('app.shortname', 'LV') }}</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">{{ __('Dashboard') }}</li>
      <li class="{{ (request()->is('home')?'active':'') }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="fa fa-home"></i> <span>{{ __('Home') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Menu') }}</li>
      <li class="{{ (request()->is('school/*') || request()->is('school')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.school.index') }}">
          <i class="fa fa-university"></i> <span>{{ __('School') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('teacher/*') || request()->is('teacher')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.teacher.index') }}">
          <i class="fa fa-chalkboard-teacher"></i> <span>{{ __('Fasilitator') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('psychotest-result/*') || request()->is('psychotest-result')?'active':'') }}">
        <a class="nav-link" href="{{ route('psychotest-result.index') }}">
          <i class="fa fa-brain"></i> <span>{{ __('Psychotest Result') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Account') }}</li>
      <li class="{{ (request()->is('user/*') || request()->is('user')?'active':'') }}">
        <a class="nav-link" href="{{ route('user.index') }}">
          <i class="fa fa-users"></i> <span>{{ __('Users') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Logout') }}</li>
      <li>
        <a class="nav-link text-danger" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i> <span>{{ __('Logout') }}</span>
        </a>
      </li>
    </ul>
  </aside>
</div>