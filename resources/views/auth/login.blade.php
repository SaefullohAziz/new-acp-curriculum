@include('layouts.header')

<div id="app">
  <section class="section">
    <div class="container mt-4">
      <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
          <div class="login-brand">
            <img src="{{ asset('img/logo/bca-cma.png') }}" alt="Logo BCA CMA" width="200px">
          </div>

          <div class="card card-primary">
            <div class="card-header"><h4>{{ __('Login') }}</h4></div>

            <div class="card-body">
              {{ Form::open(['route' => 'login.submit']) }}

              {{ Form::bsPhoneNumber(null, __('Phone Number'), 'email', old('username'), __('Phone Number'), ['maxlength' => '13', 'required' => '']) }}

                  {{ Form::bsPassword('password', 'Password:', 'password', 'Password', ['required' => '', 'autocomplete' => 'on', 'id' => 'password'], ['Show Password'], 'true') }}
                <div class="form-group">
                    {{ Form::submit(__('Login'), ['name' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                </div>

              {{ Form::close() }}

                <div class="mt-5 text-muted text-center">{{ __('Belum Memiliki Akun?') }} <a href="{{ route('register') }}">{{ __('Daftar Sekarang') }}</a></div>

            </div>
          </div>
          <div class="simple-footer">
            Copyright &copy; {{ config('app.name', 'Laravel') }}
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@include('layouts.footer')
<script>
$(document).ready(function(){

    $('.icon').hover(function () {
       $('#password').attr('type', 'text'); 
       $('.icon').attr('class', 'fa fa-eye icon');
    }, function () {
       $('#password').attr('type', 'password'); 
       $('.icon').attr('class', 'fa fa-eye-slash icon');
    });

    // $('.icon').click(function () {
    //   if($('.fa-eye-slash').length != 0){
    //     $('#password').attr('type', 'text');
    //     $('.icon').removeClass('fa-eye-slash');
    //     $('.icon').addClass('fa-eye'); 
    //   }else{
    //     $('#password').attr('type', 'password'); 
    //     $('.icon').removeClass('fa-eye');
    //     $('.icon').addClass('fa-eye-slash');
    //   };
    // });

});
</script>