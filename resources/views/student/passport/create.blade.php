@extends('layouts.main')

@section('style')
<style>
  .card .form-group:hover .table-links {
    opacity: 1;
  }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-8">
        @if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif
        <!-- BAGIAN INFO KELAS -->
        @if (session('class_id'))
            <div class="alert alert-primary alert-has-icon">
                <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                <div class="alert-body">
                    <div class="alert-title">{{ $class->name }}</div>{{ $class->description }}
                    <div class="mt-3">{{ $class->teacher->name }}<div class="bullet"></div>{{ $class->teacher->position }}<div class="bullet"></div> 09:00 - 10:00 (4 Maret 2020)</div>
                </div>
            </div>
        @endif
        <!-- PENUTUP BAGIAN INFO KELAS -->

        <!-- max reported_at validation -->
        @if( ! (
                session('class_id') &&
                (
                    $class->max_reported_at 
                        ? now() < $class->max_reported_at
                        : now() < $class->end_time
                )
            )
        )
        <div class="alert alert-danger alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>&times;</span>
                </button>
                {{ __('This max reported class time has been up!') }}
            </div>
        </div>
        <!-- BAGIAN INPUT SKILL -->
        <div class="card skill d-none">
        @else
        <div class="card skill">
        @endif
            <div class="card-header"><h4>Input Skill</h4></div>
            <div class="card-body">
                <!-- FORM -->
                <form id="addPassport" enctype="multipart/form-data">
                    @csrf
                    @if (session('class_id'))
                        <input type="hidden" name="student_lesson_class_id" value="{{ session('student_lesson_class_id') }}">
                    @endif
                    <input type="hidden" name="_method" disabled>
                    <input type="hidden" name="id" disabled>
                    <div class="form-group" id="input-form">
                        <div class="input-group">
                        <select class="custom-select" name="lesson_duration_id" id="select-0">
                            <option value="">{{ __('Select Skill') }}</option>
                            @foreach($lessons as $lesson)
                                <option value="{{ $lesson->id }}">{{ $lesson->name }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-primary add-more" type="submit"><i class="fas fa-plus"></i> Tambah</button>
                        </div>
                        </div>

                        <div class="input-group mt-1">
                            <input type="text" name="score" class="form-control" placeholder="Masukan jumlah">
                            <div class="input-group-append">
                                <div class="input-group-text" id="unitScore">Undefined</div>
                            </div>
                        </div>
                        <div class="table-links">
                            <a href="javascript:void(0)" class='d-none edit'>
                                <i class="fa fa-edit"></i>
                                {{ __('Edit') }}
                            </a>
                                <div class="bullet d-none"></div>
                            <a href="javascript:void(0)" class='d-none delete'>
                                <i class="fa fa-trash"></i>
                                {{ __('Hapus') }}
                            </a>
                                <div class="bullet d-none"></div>
                            <a href="javascript:void(0)" class="action-upload"><i class="fas fa-upload"></i> Show Upload</a>
                        </div>
                        <div class="upload-photo-input mt-2 d-none">
                            <div class="image-preview" id="image-preview" style="width:100%;">
                                <label for="image-upload" id="image-label">Choose File</label>
                                <input type="file" name="photo" class="image-upload">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <!-- PENAMPUNG CLONE FORM -->
                <div id="form-input">
                        <span class="text-center">{{ __("There's no data") }}</span>
                </div>
                <a href="javascript:void(0)" class="btn btn-light btn-more">Show More</a>
            </div>
            <!-- END PENAMPUNG CLONE FORM -->
            <div class="card-footer bg-whitesmoke">
                <a href="{{ route('student.studentLesson.index') }}" class="btn btn-primary btn-block">Done</a>
            </div>
            <!-- PENUTUP FORM -->
        </div>
        <!-- PENUTUP INPUT SKILL -->
    </div>
    <div class="col-lg-4">
        <div class="card">
            <!-- PENCAPAIAN RATA RATA -->
            <div class="card-header"><h4>Pencapaian Rata-rata</h4></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 mb-4">

                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">5 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Band Uang</div>
                            <div class="p   rogress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="56%" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%;">50%</div>
                            </div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">125 Stack <small>/ </small> 90 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">300 Stack <small>/ </small> 90 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>

                    </div>
                    <div class="col-lg-6 mb-4">

                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">3 <small>/ </small>10 Kali</div>
                            <div class="font-weight-bold mb-1">Isi Kaset</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">30%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:49 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>

                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Bongkar Kaset</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:42 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">03:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Pengepakan Plastik</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">01:52 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 20 Kali</div>
                            <div class="font-weight-bold mb-1">Pengepakan Kanvas</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                            <div class="progress-bar" role="progressbar" data-width="20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">20%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:15 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:30 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Mengisi Kembali</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                            <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">00:52 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">01:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PENUTUP PENCAPAIAN RATA RATA -->
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
// function
function loadData() {
    $.ajax({
        type: "post",
        url: "{{ route('student.studentLesson.list') }}",
        data: {
            '_token' : '{{ csrf_token() }}',
            'student_lesson_class_id' : '{{ session("student_lesson_class_id") ?? "" }}',
        },
        success: function (data) {
            data.data.length 
                ? $('#form-input').html('') : '';


            $.each(data.data, function (index, item) {
                let clonned = $('#addPassport').clone().appendTo('#form-input');
                
                // change form id
                clonned.attr('id', 'updateData');

                // disable all input
                clonned.find('select').val(item.lesson.id).prop('disabled', true);
                clonned.find('input[type="text"]').val(item.score).prop('disabled', true);
                clonned.find('input[type="file"]').prop('disabled', true);
                if (item.photo) {
                    clonned.find('.image-preview').css('background-image', 'url({{ asset("storage/student/studentLesson") }}/' + item.photo);
                }

                // make input id
                clonned.find('input[name="id"]').prop('disabled', false).prop('readonly', true).val(item.id);
                clonned.find('input[name="_method"]').prop('disabled', false).prop('readonly', true).val('PUT');

                // change button text
                clonned.find('.add-more').addClass('btn-success upload-success').removeClass('add-more btn-primary').html('<i class="fas fa-check"></i> Success');

                // show all table link
                clonned.find('.table-links .d-none').removeClass('d-none');

                // hide uploaded photo
                clonned.find('.upload-photo-input').addClass('d-none');
                // change unit score
                getUnitScore(clonned.find('select'));

                return true;
            });

            // active preview
            $('body').find('input[name="photo"]').change(function(){
                filePreview($(this), this);
            });

            // show photo upload action
            $('body').find('.action-upload').click(function(e){
                e.preventDefault();


                showUploadInput($(this).closest('.form-group'));
                // auto click upload
                $(this).closest('.form-group').find('input[type="file"]').prop('disable', false).click();
            });

            // edit data
            $('body').find('.edit').click(function (e) {
                e.preventDefault();

                editData($(this));
            })
            
            // save edited data
            $('body').find('#updateData').on('submit', function(e) {
                e.preventDefault();

                if (validateData($(this).find('.form-group'))) {
                    let fd = new FormData(this);
                    updateData(fd, $(this).find('input[name="id"]').val());
                }
            });

            // delete data
            $('body').find('.delete').on('click', function(e) {
                e.preventDefault();

                swal({
                    title: 'Are you sure?',
                    text: 'Once deleted, you will not be able to recover this file!',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        let id = $(this).closest('#updateData').find('input[name="id"]').val();
                        $.ajax({
                            type: "post",
                            url: "{{ route('student.studentLesson.store') }}/destroy",
                            data : {
                                '_method' : 'DELETE',
                                '_token' : '{{ csrf_token() }}',
                                'id' : id
                            },
                            success: function (response) {
                                loadData();
                                swal('Poof! Your file has been deleted!', {
                                    icon: 'success',
                                });
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                swal("{{ __('Failed!') }}", errorThrown, "warning");
                                status = false;
                            }
                        });
                    } else {
                    swal('Your imaginary file is safe!');
                }
                });
            });            
        } // end of success callback
    }); // end of ajax
    // enalbe add button
    $('#input-form').find('.add-more').prop('disabled', false).html('<i class="fas fa-plus"></i> Tambah');
    return true;
}; // end of function

// preview uploaded file function
function filePreview(element, input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            element.closest('.image-preview').css('background-image', 'url('+e.target.result+')');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

// show upload input
function showUploadInput(element) {
    if (element.find('.upload-photo-input').is(':visible')) {
        // hide upload input
        element.find('.upload-photo-input').addClass('d-none');
        $(this).html('<i class="fas fa-upload"></i> Show Upload');
    } else {
        // show upload input
        element.find('.upload-photo-input').removeClass('d-none');
        $(this).html('<i class="fas fa-upload"></i> Hide Upload');
    }
}

// add data
function addData(formData) {
    let status = true;
    $('.image-upload').on('change', function () {
        let file = this.files[0];
        fd.append('photo', file );
    });
    $.ajax({
        type: "POST",
        url: "{{ route('student.studentLesson.store') }}",
        data: formData,
        cache: false,
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        success: function(data)
        {
            if ( ! data.status) {
                swal("{{ __('Failed!') }}", data.error, "warning");
                status = false;
            } else {
                loadData();
                status = true;
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal("{{ __('Failed!') }}", errorThrown, "warning");
            status = false;
        }
    });
    return status;
} //end of function 

// edit data
function editData (element) {
    let item = element.closest('#updateData');

    // disable enabled all input
    if (item.find('.upload-photo-input').is(':visible')) {
        loadData();
    } else {
        item.find('.upload-success').addClass('btn-primary submit-update').removeClass('btn-success upload-success').html('<i class="fas fa-save"></i> Save');
        item.find('select, input').prop('disabled', false);
        element.html('<i class="fa fa-times"></i> Cancel');

    }
    showUploadInput(item.find('.form-group'));
}

// update Data
function updateData(formData, id) {
    $('.image-upload').on('change', function () {
        let file = this.files[0];
        fd.append('photo', file );
    });
    $.ajax({
        type: "POST",
        url: "{{ route('student.studentLesson.store') }}/" + id,
        data: formData,
        cache: false,
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        success: function(data)
        {
            if ( ! data.status) {
                swal("{{ __('Failed!') }}", data.error, "warning");
                status = false;
            } else {
                loadData();
                status = true;
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal("{{ __('Failed!') }}", errorThrown, "warning");
            status = false;
        }
    });
    return status;
}

// validate data function
function validateData(element) {
    if (element.find('[name="lesson_duration_id"]').val() == '' || ! element.find('[name="lesson_duration_id"]').val()) {
        swal({
            title: 'Please Select The Skill!',
            icon: "error",
            button: 'Back',
        });
        return false;
    }

    if( element.find("[name='score']").val() == '' || ! element.find("[name='score']").val() ) {
        swal({
            title: 'Mohon isi score terlebih dahulu!',
            icon: "error",
            button: 'Kembali',
        });
        return false;
    }

    return true;
} //end of function

// function get unitscore
function getUnitScore(element) {
    if (element.val() != '') {
        $.ajax({
            url : "{{ route('get.unitScore') }}",
            type: "POST",
            dataType: "JSON",
            cache: false,
            data: {'_token' : '{{ csrf_token() }}', 'lesson' : element.val()},
            success: function(data)
            {
                element.closest('.form-group').find('#unitScore').text(data.result);
                return true;
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                element.closest('.form-group').find('#unitScore').text("Undefined");
                return false;
            }
        });
    }
    return false;
}


// page fully loaded
$(document).ready(function() {
    // load all data
    loadData();

    // Add data
    $("#addPassport").on("submit", function(e){ // If button with class "add-more" clicked
        e.preventDefault();

        if (validateData($(this).find('.form-group'))) {
            $(this).find('.add-more').prop('disabled', true).html('<i class="fas fa-sync"></i> Loading..');
            let fd = new FormData(this);
            if (addData(fd)) {
                // blank n focus score input
                $(this).find('input[name="score"]').val('').focus();
                // hide upload input
                $(this).find('input[name="photo"]').val('');

                // hide upload input
                if ($(this).find('.upload-photo-input').is(':visible')) {
                    $(this).find('.upload-photo-input').addClass('d-none');
                }

            }
        }
        
    });

    // get unitsoce on select change
    $(".custom-select").change(function() {
        getUnitScore($(this));
    });

});

</script>
@endsection