@extends('layouts.main')

@section('style')
<style>
  .card .form-group:hover .table-links {
    opacity: 1;
  }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-8">
        @if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif
        <div class="">
            <!-- <div class="card-header"><h4>Kelas Aktif</h4></div> -->
            @foreach($classes as $class)
                <form action="{{ route('student.studentLesson.preCreate') }}" method="post">
                {{ csrf_field() }}
                    <article class="article article-style-c">
                        <div class="article-details">
                            <div class="article-title"><a href=""><h5>{{ $class->name }}</h5></a></div>
                            <div class="article-category"><a href="#">{{ $class->teacher ? $class->teacher->name : '' }}</a> <div class="bullet"></div> <a href="#">{{ $class->teacher ? $class->teacher->position : '' }}</a></div>
                            <p>{{ $class->description }}</p>
                            <input type="hidden" name="class" value="{{ $class->id }}">
                            <input type="submit" name="submit" value="Pilih kelas" class="btn btn-primary pilihClass">
                                <!-- <a href="" class="pilihClass">Pilih Kelas <i class="fas fa-chevron-right"></i></a> -->
                        </div>
                    </article>
                </form>
            @endforeach
        </div>
    </div>
    
    <div class="col-lg-4">
        <div class="card">
            <!-- PENCAPAIAN RATA RATA -->
            <div class="card-header"><h4>Pencapaian Rata-rata</h4></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 mb-4">

                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">5 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Band Uang</div>
                            <div class="p   rogress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="56%" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%;">50%</div>
                            </div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">125 Stack <small>/ </small> 90 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">300 Stack <small>/ </small> 90 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>

                    </div>
                    <div class="col-lg-6 mb-4">

                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">3 <small>/ </small>10 Kali</div>
                            <div class="font-weight-bold mb-1">Isi Kaset</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">30%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:49 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>

                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Bongkar Kaset</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:42 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">03:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Pengepakan Plastik</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">01:52 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 20 Kali</div>
                            <div class="font-weight-bold mb-1">Pengepakan Kanvas</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                            <div class="progress-bar" role="progressbar" data-width="20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">20%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:15 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:30 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Mengisi Kembali</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                            <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">00:52 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">01:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PENUTUP PENCAPAIAN RATA RATA -->
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function() {
     $(".add-more").click(function(){ // If button with class "add-more" clicked
        let score = $("[name='score[]']").val(); // Get score input
        if(score.length < 1) {
            swal({
                title: "Mohon isi Form terlebih dahulu!",
                icon: "error",
                button: "Kembali",
            });
            return false
        }
        let clonned = $("#input-form").clone().prependTo("#form-input"); // Clonning form
        clonned.find("[name='skill[]']").val($(this).parents('.form-group').find("[name='skill[]']").val()); // Auto selected skill if clonning form
        clonned.closest('.form-group').find("[name='skill[]']").prop('disabled', true); // Disabled skill field
        clonned.closest('.form-group').find("[name='score[]']").prop('disabled', true); // Disabled score field
        clonned.closest('.form-group').find(".upload-photo").addClass('d-none'); // // Hide upload photo on add form
        clonned.closest('.form-group').find(".table-links").removeClass('d-none'); // Hide table links on hover
        $(this).parents('.form-group').find("[name='score[]']").val(''); // Empty the input skill
        $("#form-input .check-more").removeClass("d-none"); // Display checklist button
        $("#form-input .add-more").remove(); // Remove add button
    });

    $(".remove-more").click(function(){ // If button with class "remove-more" clicked
        $(this).parents("#input-form").remove(); // Deleted the cloned form
    });

    $(".custom-select").change(function(){ // If one option is selected
        if($(this).val() == "band_uang"){ // Detect if band_uang selected
            $(this).parents('.form-group').find(".input-group-text").text("Stack (30 Mnt)"); // Show Stack (30 Mnt) if band_uang selected
        } else {
            $(this).parents('.form-group').find(".input-group-text").text("Minutes"); // Show Minutes else band_uang selected
        }
    });
    
    $(document.body).on('click', '.action-upload' ,function(){
        $(this).closest('.form-group').find(".upload-photo").removeClass('d-none');
        $(this).addClass('action-hide').removeClass('.action-upload');
        $(this).text("Hide Upload");
    });

    $(document.body).on('click', '.action-hide' ,function(){
        $(this).closest('.form-group').find(".upload-photo").addClass('d-none');
        $(this).addClass('action-upload').removeClass('action-hide');
        $(this).text("Show Upload");
    });
});

$.uploadPreview({
  input_field: "#image-upload",   // Default: .image-upload
  preview_box: "#image-preview",  // Default: .image-preview
  label_field: "#image-label",    // Default: .image-label
  label_default: "Choose File",   // Default: Choose File
  label_selected: "Change File",  // Default: Change File
  no_label: false,                // Default: false
  success_callback: null          // Default: null
});
</script>
@endsection