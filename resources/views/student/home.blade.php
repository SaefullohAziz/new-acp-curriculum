@extends('layouts.main')

@section('style')
<style>
    .card.card-hero .card-header .card-icon {
        margin: -80px;
    }

    .card.card-hero .card-header h5 {
        margin-top: 20px;
        font-size: 24px;
    }
    .circle-icon {
        font-size: 26px;
        width: 60px;
        height: 60px;
        border-radius: 50%;
        text-align: center;
        line-height: 60px;
        vertical-align: middle;
    }
    
    .color-info {
        background-color: #D7F1FD;
        color: #3ABAF4;
    }

    .color-warning {
        background-color: #FFEDD3;
        color: #FFA426;
    }

    .color-danger {
        background-color: #FFDDDB;
        color: #FC544B;
    }

</style>
@endsection


@section('content')
@if(count(collect(auth()->user()->student->generation)) == 0)
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-primary alert-has-icon">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <div class="alert-title">Selamat Bergabung!</div>Silahkan pilih akademi yang terdekat dengan tempat tinggal kamu!
            </div>
        </div>
        <div class="card card-primary">
            {{ Form::open(['route' => 'student.home.store', 'files' => true]) }}
                <div class="card-body">
                    <div class="academy d-block">
                        <div class="row selectAcademy justify-content-center">
                                <input type="hidden" name="registered_status" value="waiting">
                                {{ Form::bsSelect('col-lg-5', __('Provinsi'), 'province', $provinces, old('province'), __('Select'), ['placeholder' => __('Select'), '' => '']) }}
                                
                                {{ Form::bsSelect('col-lg-5', __('Akademi Tujuan'), 'academy', $academy, old('academy'), __('Select'), ['placeholder' => __('Select'), '' => '']) }}
                                <div class="col-lg-2">
                                    <label><br></label>
                                    <a href="javascript:void(0)" class="btn btn-primary btn-submit btn-block">Next <i class="fa fa-angle-double-right"></i></a>
                                </div>
                        </div>
                    </div>
                    <div class="profile d-none">
                        <div class="row selectAcademy justify-content-center">
                            <legend>{{ __('Personal Data') }}</legend>
                            {{ Form::bsText('col-lg-4', __('No. Identitas').' (Nik / Nisn / Ktp)', 'identition', $student->identition_number, ('Identition Number'), ['' => '']) }}
                    
                            {{ Form::bsSelect('col-lg-4', __('Jenis Kelamin'), 'gender', ['Laki-Laki' => 'Laki-Laki', 'Perempuan' => 'Perempuan'], $student->gender, ('Select'), ['placeholder' => __('Select')]) }}
                            
                            {{ Form::bsText('col-lg-4', __('Tempat Lahir'), 'place_of_birth', $student->place_of_birth, ('Place of Birth')) }}
                            
                            {{ Form::bsText('col-lg-4', __('Tanggal Lahir'), 'date_of_birth', $student->date_of_birth, ('Date of Birth'), ['id' => 'dateofbirth']) }}

                            {{ Form::bsTextAppend('col-lg-4', __('Berat'), 'weight', $student->weight, ('Weight'), 'Kg', ['' => '']) }}

                            {{ Form::bsTextAppend('col-lg-4', __('Tinggi'), 'height', $student->height, ('Height'), 'Cm', ['' => '']) }}

                            {{ Form::bsSelect('col-lg-4', __('Provinsi'), 'provinces', $provinces, $student->province, ('Select'), ['placeholder' => __('Select')]) }}
                
                            {{ Form::bsSelect('col-lg-4', __('Kota'), 'city_id', $cities, $student->city_id, ('Select'), ['placeholder' => __('Select')]) }}

                            {{ Form::bsText('col-lg-4', __('Asal Sekolah'), 'school_origin', $student->school_origin, ('Asal Sekolah')) }}

                            {{ Form::bsTextarea('col-lg-4', __('Alamat'), 'address', $student->address, ('Address')) }}

                            {{ Form::bsSelect('col-lg-4', __('Status Pendidikan'), 'study_status', ['alumni' => 'alumni'], $student->study_status, ('Select'), ['placeholder' => __('Select')]) }}

                            <div class="col-lg-2">
                                <label><br></label>
                                <a href="javascript:void(0)" class="btn btn-danger profil-back btn-block"><i class="fa fa-angle-double-left"></i> Previous</a>
                            </div>
                            <div class="col-lg-2">
                                <label><br></label>
                                <a href="javascript:void(0)" class="btn btn-primary profil-next btn-block">Next <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="document d-none">
                        <div class="row selectAcademy justify-content-center">
                            <legend>{{ __('Document Data') }}</legend>
                            {{ Form::bsBrowseFile('col-lg-4 file', 'Personal Photo', 'personal_photo', old('personal_photo'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'No. Identitas (Nik / Nisn / Ktp)', 'identition_number', old('identition_number'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Kartu Keluarga', 'family_card', old('family_card'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Npwp', 'npwp', old('npwp'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Kartu Kesehatan', 'health_card', old('health_card'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Bca Account Book', 'bca_account_book', old('bca_account_book'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Skbn', 'skbn', old('skbn'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Skck', 'skck', old('skck'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Diploma', 'diploma', old('diploma'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Sim C', 'sim_c', old('sim_c'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Sim A', 'sim_a', old('sim_a'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Curriculum Vitae', 'curriculum_vitae', old('curriculum_vitae'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Surat Lamaran', 'surat_lamaran', old('surat_lamaran'), ['' => '']) }}

                            {{ Form::bsBrowseFile('col-lg-4 file', 'Statement Intership Program*', 'statement_intership_program', old('statement_intership_program'), ['' => '']) }}

                            <div class="col-lg-2">
                                <label><br></label>
                                <a href="javascript:void(0)" class="btn btn-danger btn-block" id="back"><i class="fa fa-angle-double-left"></i> Previous</a>
                            </div>
                            <div class="col-lg-2">
                                <label><br></label>
                                {{ Form::submit(__('Save'), ['class' => 'btn btn-primary btn-block']) }}
                            </div>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@elseif (auth()->user()->student->registered_status = 'waiting' && count(collect(auth()->user()->student->generation)) != 0)
    <div class="row justify-content-center">
        <div class="col-lg-4">    
            <img src="{{ asset('img/svg/progress.svg') }}" width="100%">
        </div>
        <div class="col-lg-12 mt-2 text-center">
            <h2>Permohonanmu Berhasil!</h2>
            <p>Pengajuan untuk bergabung di {{ auth()->user()->student->generation->school->name }} Bogor sedang di proses.</p>
        </div>
    </div>
@else
<div class="row">
    @if (session('alert-success'))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>&times;</span>
                </button>
                {{ session('alert-success') }}
            </div>
        </div>
    @endif

    @if (session('alert-danger'))
        <div class="alert alert-danger alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>&times;</span>
                </button>
                {{ session('alert-danger') }}
            </div>
        </div>
    @endif

    <div class="col-lg-6">
        <div class="card shadow-sm card-hero">
            <div class="card-header">
                <div class="card-icon">
                    <i class="fas fa-star"></i>
                </div>
                <div class="text-center">
                    <i class="fas fa-fire circle-icon bg-primary"></i>
                    <h5>Congratulations, {{ Auth::user()->name }}</h5>
                    <div class="card-description">You are in <b style="color: #000000;">{{ auth()->user()->student()->first()->statuses()->first() ? auth()->user()->student()->first()->statuses()->first()->name : 'Registration' }}</b> status</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card shadow-sm">
            <div class="card-body">
                <i class="fas fa-user circle-icon color-info"></i>
                <h4 class="mt-4">{{ $profilePercentage }}% <small>of {{ $profile->count() }}</small></h4>
                <h6>Kelengkapan Profil</h6>
                <div class="progress mb-4">
                    <div class="progress-bar bg-info" role="progressbar" data-width="{{$profilePercentage}}%" aria-valuenow="{{$profilePercentage}}%" aria-valuemin="0%" aria-valuemax="100%"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card shadow-sm">
            <div class="card-body">
                <i class="fas fa-file circle-icon color-warning"></i>
                <h4 class="mt-4">{{$documentPercentage}}% <small>of {{$document}}</small></h4>
                <h6>Kelengkapan Dokumen</h6>
                <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" data-width="{{$documentPercentage}}%" aria-valuenow="{{$documentPercentage}}%" aria-valuemin="0%" aria-valuemax="100%"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-lg-3">
        <div class="card shadow-sm">
            <div class="card-body">
                <i class="fas fa-brain circle-icon color-danger"></i>
                <h4 class="mt-4">50% <small>of 2</small></h4>
                <h6>Completeness of Psychotest</h6>
                <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" data-width="50%" aria-valuenow="50%" aria-valuemin="0%" aria-valuemax="100%"></div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header"><h4>{{ __('Pencapaian Rata-rata') }}</h4></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 mb-4">

                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">5 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Band Uang</div>
                            <div class="p   rogress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="56%" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%;">50%</div>
                            </div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">125 Stack <small>/ </small> 90 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">300 Stack <small>/ </small> 90 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>

                    </div>
                    <div class="col-lg-6 mb-4">

                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">3 <small>/ </small>10 Kali</div>
                            <div class="font-weight-bold mb-1">Isi Kaset</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">30%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:49 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>

                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Bongkar Kaset</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:42 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">03:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Pengepakan Plastik</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                                <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">01:52 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 20 Kali</div>
                            <div class="font-weight-bold mb-1">Pengepakan Kanvas</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                            <div class="progress-bar" role="progressbar" data-width="20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">20%</div>
                            </div>
                        </div>

                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:15 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">02:30 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="wrap mt-2">
                            <div class="float-right font-weight-bold text-muted">4 <small>/ </small> 10 Kali</div>
                            <div class="font-weight-bold mb-1">Mengisi Kembali</div>
                            <div class="progress" data-height="15" style="height: 15px;">
                            <div class="progress-bar" role="progressbar" data-width="40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">40%</div>
                            </div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">00:52 Menit</div>
                            <div class="font-weight-bold">Rata - rata</div>
                        </div>
                        <div class="wrap mt-3">                            
                            <div class="float-right font-weight-bold text-muted">01:00 Menit</div>
                            <div class="font-weight-bold">KKM</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('select[name="province"]').change(function() {
            let prov = $(this).find("option:selected").text();
            $('select[name="academy"]').html('<option value="">Select</option>');
            if ($(this).val() != '') {
                $.ajax({
                    url : "{{ route('get.academy') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: {'_token' : '{{ csrf_token() }}', 'province' : $(this).val()},
                    success: function(data)
                    {
                        if (!$.trim(data.result)){   
                            alert("Maaf akademi di provinsi " + prov + " saat ini belum tersedia");
                        }
                        else{   
                            $.each(data.result, function(key, value) {
                                $('select[name="academy"]').append('<option value="'+key+'">'+value+'</option>');
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log('dds');
                        $('select[name="academy"]').html('<option value="">Select</option>');
                    }
                });
            }
        });

        $('select[name="provinces"]').change(function() {
            $('select[name="city_id"]').html('<option value="">Select</option>');
            if ($(this).val() != '') {
                $.ajax({
                    url : "{{ route('get.city') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: {'_token' : '{{ csrf_token() }}', 'province' : $(this).find("option:selected").text()},
                    success: function(data)
                    {
                        $.each(data.result, function(key, value) {
                            $('select[name="city_id"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $('select[name="city_id"]').html('<option value="">Select</option>');
                    }
                });
            }
        });
    });

    $('.btn-submit').on('click', function() {
       $('.academy').removeClass('d-block');
       $('.academy').addClass('d-none');
       $('.profile').removeClass('d-none');
       $('.profile').addClass('d-block');
    });

    $('.profil-next').on('click', function() {
       $('.document').addClass('d-block');
       $('.document').removeClass('d-none');
       $('.profile').addClass('d-none');
       $('.profile').removeClass('d-block');
    });

    $('.profil-back').on('click', function() {
       $('.academy').addClass('d-block');
       $('.academy').removeClass('d-none');
       $('.profile').addClass('d-none');
       $('.profile').removeClass('d-block');
    });

    $('#back').on('click', function() {
       $('.profile').addClass('d-block');
       $('.profile').removeClass('d-none');
       $('.document').addClass('d-none');
       $('.document').removeClass('d-block');
    });
</script>
@endsection