@include('layouts.header')

<div id="app">
  <section class="section">
    <div class="container mt-5">
      <div class="row">
        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
          <div class="login-brand">
              {{ config('app.name', 'Laravel') }}
          </div>
        </div>
      </div>

        <div class="col-12 col-lg-10 offset-lg-1">
          <div class="nav nav-pills wizard-steps" id="pills-tab" role="tablist">
            <li class="wizard-step nav-link active" id="pills-school-tab" data-toggle="pill" href="#pills-school" role="tab" aria-controls="pills-school" aria-selected="true">
              <div class="wizard-step-icon"><i class="fas fa-school"></i></div>
              <div class="wizard-step-label">School Data</div>
            </li>
            <li class="wizard-step nav-link" id="pills-headmaster-tab" data-toggle="pill" href="#pills-headmaster" role="tab" aria-controls="pills-headmaster" aria-selected="false">
              <div class="wizard-step-icon"><i class="fas fa-user"></i></div>
              <div class="wizard-step-label">Headmaster Data</div>
            </li>
            <li class="wizard-step nav-link" id="pills-pic-tab" data-toggle="pill" href="#pills-pic" role="tab" aria-controls="pills-pic" aria-selected="false">
              <div class="wizard-step-icon"><i class="fas fa-info-circle"></i></div>
              <div class="wizard-step-label">PIC Data</div>
            </li>
            <li class="wizard-step nav-link" id="pills-other-tab" data-toggle="pill" href="#pills-other" role="tab" aria-controls="pills-other" aria-selected="false">
              <div class="wizard-step-icon"><i class="fab fa-ethereum"></i></div>
              <div class="wizard-step-label">Other Data</div>
            </li>
          </div>
        </div>

        @if (session('alert-success'))
          <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
              <button class="close" data-dismiss="alert">
                <span>&times;</span>
              </button>
              {{ session('alert-success') }}
            </div>
          </div>
        @endif

        @if (session('alert-danger'))
          <div class="alert alert-danger alert-dismissible show fade">
            <div class="alert-body">
              <button class="close" data-dismiss="alert">
                <span>&times;</span>
              </button>
              {{ session('alert-danger') }}
            </div>
          </div>
        @endif

        <div class="tab-content wizard-content" id="pills-tabContent">
          <div class="tab-pane wizard-pane fade show active" id="pills-school" role="tabpanel" aria-labelledby="pills-school-tab">
            <div class="col-12 col-lg-10 offset-lg-1">
              <div class="card card-primary">
                <div class="card-body">
                  {{ Form::open(['route' => 'school.store', 'files' => true]) }}
                    <div class="row">
                        <fieldset class="col-lg-6">
                        <div class="form-group">
                          <label for="input-group">{{ __('Name') }}</LAbel>
                          <div class="input-group">
                            <select class="custom-select col-sm-3 col-lg-2" id="school" name="school">
                              <option value="SMK">SMK</option>
                              <option value="SMA">SMA</option>
                              <option value="MA">MA</option>
                            </select>
                            <select class="custom-select col-sm-4 col-lg-3" id="school-type" name="type">
                              <option value="Swasta">Swasta</option>
                              <option value="Negeri">Negeri</option>
                            </select>
                            <input type="text" class="form-control" name="name" placeholder="{{ __('School') . ' ' . __('Name') }}...">
                          </div>
                        </div>

                        {{ Form::bsTextarea(null, __('Address'), 'address', old('address'), __('Address'), ['required' => '']) }}
    
                        {{ Form::bsSelect(null, __('Province'), 'province', $provinces, old('province'), __('Select'), ['placeholder' => __('Select'), 'required' => '']) }}
    
                        {{ Form::bsSelect(null, __('City'), 'city_id', $cities, old('city_id'), __('Select'), ['placeholder' => __('Select'), 'required' => '']) }}
    
                        {{ Form::bsText(null, __('Since'), 'since', old('since'), __('Since'), ['maxlength' => '4', 'required' => '']) }}
                        
                        {{ Form::bsPhoneNumber(null, __('School Phone Number'), 'school_phone_number', old('school_phone_number'), __('School Phone Number'), ['maxlength' => '13', 'required' => '']) }}
                        
                        {{ Form::bsEmail(null, __('School E-Mail'), 'school_email', old('school_email'), __('School E-Mail'), ['required' => '']) }}
                      </fieldset>

                      <fieldset class="col-lg-6">   
                        {{ Form::bsText(null, __('School Website (URL)'), 'school_web', old('school_web'), __('example : http://sekolahsaya.com'), ['required' => '']) }}
    
                        {{ Form::bsText(null, __('Total Student'), 'total_student', old('total_student'), __('Total Student'), ['required' => '']) }}
    
                        {{ Form::bsCheckboxList(null, __('Department'), 'department[]', $departments) }}
    
                        {{ Form::bsSelect(null, __('ISO Certificate'), 'iso_certificate', $isoCertificates, old('iso_certificate'), __('Select'), ['placeholder' => __('Select'), 'required' => '']) }}
                      </fieldset>
                      <fieldset class="col-lg-12 text-right">
                        <button type="button" type="button" class="btn btn-icon icon-right btn-primary next-headmaster">Next <i class="fas fa-arrow-right"></i></a>
                      </fieldset>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-headmaster" role="tabpanel" aria-labelledby="pills-headmaster-tab">
            <div class="col-12 col-lg-10 offset-lg-1">
              <div class="card card-primary">
                <div class="card-body">
                  <div class="row">
                    <fieldset class="col-lg-6">
                      {{ Form::bsText(null, __('Headmaster Name'), 'headmaster_name', old('headmaster_name'), __('Headmaster Name'), ['required' => ''], [__('Complete with an academic degree and or degree of expertise.')]) }}
                      
                      {{ Form::bsPhoneNumber(null, __('Headmaster Phone Number'), 'headmaster_phone_number', old('headmaster_phone_number'), __('Headmaster Phone Number'), ['maxlength' => '13', 'required' => '']) }}
                    </fieldset>
                    <fieldset class="col-lg-6">
                      {{ Form::bsEmail(null, __('Headmaster E-Mail'), 'headmaster_email', old('headmaster_email'), __('Headmaster E-Mail'), ['required' => '']) }}
                    </fieldset>
                    <fieldset class="col-lg-12 text-right">
                        <button type="button" class="btn btn-icon icon-right btn-secondary prev-school"><i class="fas fa-arrow-left"></i> Previous</button>
                        <button type="button" class="btn btn-icon icon-right btn-primary next-pic">Next <i class="fas fa-arrow-right"></i></button>
                    </fieldset>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-pic" role="tabpanel" aria-labelledby="pills-pic-tab">
            <div class="col-12 col-lg-10 offset-lg-1">
              <div class="card card-primary">
                <div class="card-body">
                  <div class="row">
                    <fieldset class="col-lg-6">
                      {{ Form::bsText(null, __('PIC Name'), 'pic_name', old('pic_name'), __('PIC Name'), ['required' => ''], [__('Complete with an academic degree and or degree of expertise.')]) }}
                      
                      {{ Form::bsSelect(null, __('Gender'), 'pic_gender', $gender, old('pic_gender'), __('Select'), ['placeholder' => __('Select'), 'required' => '']) }}
                      
                      {{ Form::bsText(null, __('Date of Birth'), 'date_of_birth', old('date_of_birth'), __('DD-MM-YYYY'), ['required' => '']) }}
                    </fieldset>
                    <fieldset class="col-lg-6">
                      {{ Form::bsPhoneNumber(null, __('PIC Phone Number'), 'pic_phone_number', old('pic_phone_number'), __('PIC Phone Number'), ['maxlength' => '13', 'required' => '']) }}
                      
                      {{ Form::bsEmail(null, __('PIC E-Mail'), 'pic_email', old('pic_email'), __('PIC E-Mail'), ['required' => '']) }}
                      
                      {{ Form::bsText(null, __('PIC Position'), 'pic_position', old('pic_position'), __('PIC Position'), ['required' => '']) }}
                    </fieldset>
                    <fieldset class="col-lg-12 text-right">
                        <button type="button" class="btn btn-icon icon-right btn-secondary prev-headmaster"><i class="fas fa-arrow-left"></i> Previous</button>
                        <button type="button" class="btn btn-icon icon-right btn-primary next-other">Next <i class="fas fa-arrow-right"></i></button>
                    </fieldset>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-other" role="tabpanel" aria-labelledby="pills-other-tab">
            <div class="col-12 col-lg-10 offset-lg-1">
              <div class="card card-primary">
                <div class="card-body">
                  <div class="row">
                    <fieldset class="col-lg-6">
                      {{ Form::bsCheckboxList(null, __('Reference'), 'reference[]', $references) }}
                      
                      {{ Form::bsInlineRadio(null, 'Apakah Kepala Sekolah telah mempelajari proposal BCAP?', 'proposal', ['Sudah' => 'Sudah', 'Belum' => 'Belum'], old('proposal'), ['required' => '']) }}
                    </fieldset>
                    <fieldset class="col-lg-6">
                      {{ Form::bsFile(null, __('Requirement Document'), 'document', old('document'), [], [__('File must have extension *.ZIP/*.RAR with size 5 MB or less.')]) }}
                      
                      {{ Form::bsFile(null, __('School Photo'), 'photos[]', old('photos[]'), ['multiple' => ''], [__("Don't compress photos on RAR/ZIP. Because, some photos can be uploaded at once. Maximum size is 5 MB per file.")]) }}
                    </fieldset>
                    <fieldset class="col-lg-12 text-right">
                        <button type="button" class="btn btn-icon icon-right btn-secondary prev-pic"><i class="fas fa-arrow-left"></i> Previous</button>
                    {{ Form::submit(__('Register'), ['class' => 'btn btn-icon icon-right btn-primary']) }}
                    </fieldset>
                  </div>
                  {{ Form::close() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
          <div class="simple-footer">
            Copyright &copy; {{ config('app.name', 'Laravel') }} &middot; 2016 - {{ date('Y') }}
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


@section('script')
<script>
	$(document).ready(function () {
		$('select[name="province"]').change(function() {
			$('select[name="city_id"]').html('<option value="">Select</option>');
			if ($(this).val() != '') {
				$.ajax({
					url : "{{ route('get.city') }}",
					type: "POST",
					dataType: "JSON",
					cache: false,
					data: {'_token' : '{{ csrf_token() }}', 'province' : $(this).val()},
					success: function(data)
					{
            $('select[name="city_id"]')
						$.each(data.result, function(key, value) {
							$('select[name="city_id"]').append('<option value="'+value+'">'+value+'</option>');
						});
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						$('select[name="city_id"]').html('<option value="">Select</option>');
					}
				});
			}
		});
    $('[name="date_of_birth"]').daterangepicker({
      locale: {format: 'DD-MM-YYYY'},
      singleDatePicker: true,
    });

	});

  $('.next-headmaster').click(function(){
    $('#pills-tab li[href="#pills-headmaster"]').tab('show');         
  });

  $('.prev-school').click(function(){
    $('#pills-tab li[href="#pills-school"]').tab('show');
  });
  $('.next-pic').click(function(){
    $('#pills-tab li[href="#pills-pic"]').tab('show');
  });
  $('.prev-headmaster').click(function(){
    $('#pills-tab li[href="#pills-headmaster"]').tab('show');
  });
  $('.prev-pic').click(function(){
    $('#pills-tab li[href="#pills-pic"]').tab('show');
  });
  $('.next-other').click(function(){
    $('#pills-tab li[href="#pills-other"]').tab('show');
  });
</script>
@endsection

@include('layouts.footer')