@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header"><h4>{{ __('Data Permintaan per Bulan') }}</h4></div>
            <div class="card-body">
              <canvas id="statistikPerbulan" style="height:300px; width:100%"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header"><h4>{{ __('Data Permintaan per Job') }}</h4>
                <div class="card-header-action">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" aria-expanded="false">Maret</a>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(68px, 26px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <a href="#" class="dropdown-item has-icon"> Januari</a>
                            <a href="#" class="dropdown-item has-icon"> Februari</a>
                            <a href="#" class="dropdown-item has-icon active"> Maret</a>
                            <a href="#" class="dropdown-item has-icon"> April</a>
                            <a href="#" class="dropdown-item has-icon"> Mei</a>
                            <a href="#" class="dropdown-item has-icon"> Juni</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
              <canvas id="totalVokasiPkt" style="height:300px; width:100%"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
// Showing Total Vocation PKT Data in Polar Area Chart
let totalVokasiPktCtx = $('#totalVokasiPkt');
let totalVokasiPkt = new Chart(totalVokasiPktCtx, {
    type: 'polarArea',
    data: {
        labels: ['CPC','RPL','FLM','CIT'], // ,'PKT 7','PKT 8'
        datasets: [
            {
                defaultFontSize: '16',
                backgroundColor: ["rgba(252, 84, 75, 0.6)","rgba(255, 164, 38, 0.6)","rgba(253, 247, 76, 0.6)","rgba(99, 237, 122, 0.6)","rgba(58, 186, 244, 0.6)","rgba(103, 119, 239, 0.6)"],
                borderColor: ["rgb(255, 255, 255)","rgb(255, 255, 255)","rgb(255, 255, 255)","rgb(255, 255, 255)","rgb(255, 255, 255)","rgb(255, 255, 255)"],
                data: [4, 1, 3, 2]
            }
        ],
    },
    options: {
        responsive: true,
        tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					var allData = data.datasets[tooltipItem.datasetIndex].data;
					var tooltipLabel = data.labels[tooltipItem.index];
					var tooltipData = allData[tooltipItem.index];
					var total = 0;
					for (var i in allData) {
						total += allData[i];
					}
					var tooltipPercentage = Math.round((tooltipData / total) * 100);
					return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
				}
			}
		}
    },
});

let statistikPerbulanCtx = $('#statistikPerbulan');
let statistikPerbulan = new Chart(statistikPerbulanCtx, {
  type: 'line',
  data: {
    labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli"],
    datasets: [{
      label: 'Statistics',
      data: [12, 6, 10, 8, 16, 3, 4],
      borderWidth: 4,
      borderColor: '#6777ef',
      backgroundColor: 'transparent',
      pointBackgroundColor: '#fff',
      pointBorderColor: '#6777ef',
      pointRadius: 4
    }]
  },
  options: {
    legend: {
      display: false
    },
    scales: {
      yAxes: [{
        gridLines: {
          display: false,
          drawBorder: false,
        },
        ticks: {
          stepSize: 10
        }
      }],
      xAxes: [{
        gridLines: {
          color: '#fbfbfb',
          lineWidth: 2
        }
      }]
    },
  }
});
</script>
@endsection