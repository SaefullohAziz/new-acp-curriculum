@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-lg-8">
		<div class="card profile-widget">
			<div class="profile-widget-header">                     
				<img alt="image" src="{{ asset('img/avatar/avatar.png') }}" class="rounded-circle profile-widget-picture">
				<div class="profile-widget-items">
					<div class="profile-widget-item">
						<div class="profile-widget-item-value">Nama</div>
						<div class="profile-widget-item-label">{{ $data->name ? $data->name : '-' }}</div>
					</div>
					<div class="profile-widget-item">
						<div class="profile-widget-item-value">Akademi</div>
						<div class="profile-widget-item-label">{{ $data->school->name ? $data->school->name : '-' }}</div>
					</div>
					<div class="profile-widget-item">
						<div class="profile-widget-item-value">Status</div>
						<div class="profile-widget-item-label">{{ $data->study_status ? $data->study_status : '-' }}</div>
					</div>
				</div>
			</div>
			<div class="profile-widget-description">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
					<a class="nav-link active show" id="profil-tab" data-toggle="tab" href="#profil" role="tab" aria-controls="profil" aria-selected="true">{{ __('Profil') }}</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" id="dokumen-tab" data-toggle="tab" href="#dokumen" role="tab" aria-controls="dokumen" aria-selected="false">{{ __('Dokumen') }}</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" id="psikotes-tab" data-toggle="tab" href="#psikotes" role="tab" aria-controls="psikotes" aria-selected="false">{{ __('Psikotes') }}</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" id="raport-tab" data-toggle="tab" href="#raport" role="tab" aria-controls="raport" aria-selected="false">{{ __('Raport') }}</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" id="penilaian-tab" data-toggle="tab" href="#penilaian" role="tab" aria-controls="penilaian" aria-selected="false">{{ __('Penilaian') }}</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade active show" id="profil" role="tabpanel" aria-labelledby="profil-tab">
						<div class="row">
							{{ Form::bsText('col-lg-6', __('Nama'), 'name', $data->name, $data->name, ['disabled' => '']) }}
							
							{{ Form::bsText('col-lg-6', __('Akademi'), 'academy', $data->school->name, $data->school->name, ['disabled' => '']) }}
							
							{{ Form::bsText('col-lg-6', __('Tempat Lahir'), 'place_of_birth', $data->place_of_birth, $data->place_of_birth, ['disabled' => '']) }}	
							
							{{ Form::bsText('col-lg-6', __('Tanggal Lahir'), 'date_of_birth', $data->date_of_birth ? $data->date_of_birth : '-', $data->date_of_birth, ['disabled' => '']) }}	
							
							{{ Form::bsText('col-lg-6', __('Jenis Kelamin'), 'gender', $data->gender ? $data->gender : '-', $data->gender, ['disabled' => '']) }}	
							
							{{ Form::bsText('col-lg-6', __('Nomor Telpon'), 'phone_number', $data->phone_number ? '(+62) '.$data->phone_number : '-', __('Nomor Telpon'), ['disabled' => '']) }}
							
							{{ Form::bsTextAppend('col-lg-6', __('Berat Badan'), 'weight', $data->weight ? $data->weight : '-', $data->weight, 'Kg', ['disabled' => '']) }}	
							
							{{ Form::bsTextAppend('col-lg-6', __('Tinggi Badan'), 'height', $data->height ? $data->height : '-', $data->height, 'Cm', ['disabled' => '']) }}	
						</div>
					</div>
					<div class="tab-pane fade" id="dokumen" role="tabpanel" aria-labelledby="dokumen-tab">
						<div class="row">
							<div class="col-lg-6">
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-warning mb-1 float-right">Incomplete</div>
										<h6 class="media-title">Kartu Identitas/KTP/Kartu Pelajar</h6>
										<div class="text-small text-muted">Tidak ada data</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
										<h6 class="media-title">Surat Keterangan Domisili</h6>
										<div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-warning mb-1 float-right">Incomplete</div>
										<h6 class="media-title">Nomor Pokok Wajib Pajak (NPWP)</h6>
										<div class="text-small text-muted">Tidak ada data</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
										<h6 class="media-title">Kartu BPJS/KIS/KJS (Asuransi Kesehatan)</h6>
										<div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
										<h6 class="media-title">Foto Buku / No Rekening BCA</h6>
										<div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-warning mb-1 float-right">Incomplete</div>
										<h6 class="media-title">Surat Keterangan Bebas Narkotika (SKBN)</h6>
										<div class="text-small text-muted">Tidak ada data</div>
									</div>
								</li>
							</div>
							<div class="col-lg-6">
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
										<h6 class="media-title">Surat Keterangan Kelakuan Baik (SKCK)</h6>
										<div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
										<h6 class="media-title">Ijazah (SMK/SMA/Sederajat bagi yang sudah lulus)</h6>
										<div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
										<h6 class="media-title">SIM C</h6>
										<div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-warning mb-1 float-right">Incomplete</div>
										<h6 class="media-title">SIM A</h6>
										<div class="text-small text-muted">Tidak ada data</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
										<h6 class="media-title">Curriculum Vitae (CV)</h6>
										<div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
									</div>
								</li>
								<li class="media mt-3 mb-3">
									<div class="media-body">
										<div class="badge badge-pill badge-success mb-1 float-right">Complete</div>
										<h6 class="media-title">Surat Lamaran</h6>
										<div class="text-small text-muted">Terupload <div class="bullet"></div> 1 Minggu yang lalu</div>
									</div>
								</li>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="psikotes" role="tabpanel" aria-labelledby="psikotes-tab">
						<div class="row">
							<h2 class="section-title">Hasil DISC</h2>
							<canvas id="graph-3" width="100%" height="50px"></canvas>

							<h2 class="section-title">Hasil MBTI</h2>
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col" colspan="4" class="text-center">DIMENSION</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>INTROVERT (I)</td>
											<td>33%</td>
											<td>67%</td>
											<td>(E) EKSTROVERT</td>
										</tr>
										<tr>
											<td>2</td>
											<td>SENSING (S)</td>
											<td>27%</td>
											<td>73%</td>
											<td>(N) INTUITION</td>
										</tr>
										<tr>
											<td>3</td>
											<td>THINKING (T)</td>
											<td>60%</td>
											<td>40%</td>
											<td>(F) FEELING</td>
										</tr>
										<tr>
											<td>4</td>
											<td>JUDGING (J)</td>
											<td>40%</td>
											<td>60%</td>
											<td>(P) PERCEIVING</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>				
					</div>
					<div class="tab-pane fade" id="raport" role="tabpanel" aria-labelledby="raport-tab">
						<h4 class="text-center mt-3">{{ __('Raport Siswa') }}</h4>
						<table class="table table-sm">
							<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">{{ __('Skill') }}</th>
								<th scope="col">{{ __('KKM') }}</th>
								<th scope="col">{{ __('Nilai') }}</th>
								<th scope="col">{{ __('Nilai Rata-rata') }}</th>
							</tr>
							</thead>
							<tbody>
								@php $no = 1; @endphp
								@foreach($raport as $key)
									<tr>
										<th scope="row">{{ $no++ }}</th>
										<td>{{ $key['lesson'] }}</td>
										<td>{{ $key['kkm'] }}</td>
										<td>{{ $key['flight_hours'] }}</td>
										<td>{{ $key['average'] }} <small></small></td>
									</tr>
								@endforeach
							</tbody>
						</table>
						<div class="text-center">
							<a href="{{ route('partner.student.preview', $data->id) }}" class="btn btn-warning"><i class="far fa-file-pdf"></i> Preview</a>
							<a href="{{ route('partner.student.download', $data->id) }}" class="btn btn-primary"><i class="fas fa-file-download"></i> Download</a>
						</div>
					</div>
					<div class="tab-pane fade" id="penilaian" role="tabpanel" aria-labelledby="penilaian-tab">
						<div class="card-body">
							<p>Berikan penilaian kepada <i>{{ $data->name }}</i> bagaimana selama dia magang di {{ Auth::user()->name }}</p>
							{{ Form::bsTextarea(null, __('Penilaian'), 'penilaian', old('penilaian'), __('Type a message'), ['class' => 'summernote-simple', 'required' => '']) }}
							<div class="btn btn-primary btn-block">Submit</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 d-none">
		<div class="card">
			<div class="card-header"><h4>{{ __('Pencapaian Rata-rata') }}</h4></div>
			<div class="card-body">
				<div class="mb-4">
					<div class="text-small float-right font-weight-bold text-muted">101.67 Stack <small>/ </small> 90:00 Menit</div>
					<div class="font-weight-bold mb-1">Band Uang</div>
					<div class="progress" data-height="5" style="height: 5px;">
						<div class="progress-bar" role="progressbar" data-width="56%" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%;"></div>
					</div>
				</div>
				<div class="mb-4">
					<div class="text-small float-right font-weight-bold text-muted">4 Kali <small>/ </small>02:49 Menit</div>
					<div class="font-weight-bold mb-1">Isi Kaset</div>
					<div class="progress" data-height="5" style="height: 5px;">
						<div class="progress-bar" role="progressbar" data-width="64%" aria-valuenow="64" aria-valuemin="0" aria-valuemax="100" style="width: 64%;"></div>
					</div>
				</div>
				<div class="mb-4">
					<div class="text-small float-right font-weight-bold text-muted">4 Kali <small>/ </small> 02:42 Menit</div>
					<div class="font-weight-bold mb-1">Bongkar Kaset</div>
					<div class="progress" data-height="5" style="height: 5px;">
						<div class="progress-bar" role="progressbar" data-width="76%" aria-valuenow="76" aria-valuemin="0" aria-valuemax="100" style="width: 76%;"></div>
					</div>
				</div>
				<div class="mb-4">
					<div class="text-small float-right font-weight-bold text-muted">4 Kali <small>/ </small> 01:52 Menit</div>
					<div class="font-weight-bold mb-1">Pengepakan Plastik</div>
					<div class="progress" data-height="5" style="height: 5px;">
						<div class="progress-bar" role="progressbar" data-width="76%" aria-valuenow="76" aria-valuemin="0" aria-valuemax="100" style="width: 76%;"></div>
					</div>
				</div>
				<div class="mb-4">
					<div class="text-small float-right font-weight-bold text-muted">4 Kali <small>/ </small> 02:15 Menit</div>
					<div class="font-weight-bold mb-1">Pengepakan Kanvas</div>
					<div class="progress" data-height="5" style="height: 5px;">
						<div class="progress-bar" role="progressbar" data-width="85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;"></div>
					</div>
				</div>
				<div class="mb-4">
					<div class="text-small float-right font-weight-bold text-muted">4 Kali <small>/ </small> 00:52 Menit</div>
					<div class="font-weight-bold mb-1">Mengisi Kembali</div>
					<div class="progress" data-height="5" style="height: 5px;">
						<div class="progress-bar" role="progressbar" data-width="32%" aria-valuenow="32" aria-valuemin="0" aria-valuemax="100" style="width: 32%;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
        var ctx = $("#graph-1");
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["D", "I", "S", "C"],

                datasets: [
                    {
                        label: "Graph 1 - Mask Public Self",
						data: ['-1', '-2', '0', '1'],
						pointBackgroundColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
						pointBorderColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
						pointBorderWidth: 5,
                        fill: false,
                    }   
                ]
            }
        });
    </script>
    <script>
        var ctx = $("#graph-2");
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["D", "I", "S", "C"],
            
                datasets: [
                    {
                        label: "Graph 2 - Core Private Self",
						data: ['2', '1', '2', '0'],
						pointBackgroundColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
						pointBorderColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
						pointBorderWidth: 5,
                        fill: false,
                    }   
                ]
            }
        });
    </script>
    <script>	
        var ctx = $("#graph-3");
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["D", "I", "S", "C"],
            
                datasets: [
                    {
                        label: "Graph 3 - Mirror Perceived Self",
						data: ['-1', '-1', '2', '1'],
						pointBackgroundColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
						pointBorderColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
						pointBorderWidth: 5,
                        fill: false,
                    }   
                ]
            },
			options: {
				ticks: {
					stepSize: 0.5
				}

			}
        });
    </script>
@endsection