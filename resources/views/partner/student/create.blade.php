@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
        @endif

        <div class="form-container">
            <div class="card card-primary form-request">
                <div class="card-header"><h4>Form Permintaan</h4>
                    <div class="card-header-action">
                        <a href="#" class="btn btn-primary add-request"><i class="fas fa-plus"></i> Tambah Permintaan</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        {{ Form::bsSelect('col-lg-6', __('Cabang'), 'job_desk[]', $branch, old('job_desk'), __('Select'), ['placeholder' => __('Select')]) }}
                        
                        {{ Form::bsSelect('col-lg-6', __('Job Desk'), 'job_desk[]', ['CPC' => 'CPC', 'RPL' => 'RPL', 'FLM' => 'FLM', 'CIT' => 'CIT'], old('job_desk'), __('Select'), ['placeholder' => __('Select')]) }}
                        
                        {{ Form::bsText('col-lg-6', __('Kuota'), 'quota', old('quota'), __('Quota'), ['required' => '']) }}
                        
                        {{ Form::bsSelect('col-lg-6', __('Bulan'), 'month[]', ['January' => 'January', 'February' => 'February', 'March' => 'March', 'April' => 'April', 'May' => 'May'], old('month'), __('Select'), ['placeholder' => __('Select')]) }}
                        
                        {{ Form::bsSelect('col-lg-6', __('Tahun'), 'year[]', ['2020' => '2020'], old('year'), __('Select'), ['placeholder' => __('Select')]) }}
                    </div>
                </div>
            </div>
        </div>
        <a href="" class="btn btn-primary btn-block">Submit</a>
	</div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $(".add-request").on("click", function(){
            let item = $(this);
            $('select').removeClass('select2');
            let clonning = item.closest(".form-request").clone();
            clonning.appendTo(".form-container");
            clonning.find(".card-header-action").remove();  
            
            item.closest(".form-request").find('input').val('');
            item.closest(".form-request").find('select').val('');
        });
    });
</script>
@endsection