<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('home') }}">{{ config('app.shortname', 'LV') }}</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">{{ __('Dashboard') }}</li>
      <li class="{{ (request()->is('partner/home')?'active':'') }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="fa fa-home"></i> <span>{{ __('Dashboard') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Menu') }}</li>
      <li class="{{ (request()->is('partner/student/*') || request()->is('partner/student')?'active':'') }}">
        <a class="nav-link" href="{{ route('partner.student.index') }}">
          <i class="fa fa-user-graduate"></i> <span>{{ __('Siswa') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('partner/request/*') || request()->is('partner/request')?'active':'') }}">
        <a class="nav-link" href="{{ route('partner.request.index') }}">
          <i class="fa fa-users"></i> <span>{{ __('Permintaan SDM') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Logout') }}</li>
      <li>
        <a class="nav-link text-danger" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i> <span>{{ __('Logout') }}</span>
        </a>
      </li>
    </ul>
  </aside>
</div>