@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif
        <div class="alert alert-primary alert-has-icon alert-dismissible show fade">
            <div class="alert-icon"><i class="fa fa-info-circle"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>&times;</span>
                </button>
                <div class="alert-title">Help</div>
                Double Klik data yang ingin dirubah, lalu tekan enter untuk upload, atau esc untuk membatalkan edit.
            </div>
        </div>

		<div class="card">
            <div class="card-header">
                <h4 id="cabang" data-partner_branch_id="{{ $branch->id }}">
                    <span>
                    {{ $branch->name }}
                    </span> - 
                    <div class="dropdown d-inline">
                      <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" id="orders-year">{{ $years[0] }}</a>
                      <ul class="dropdown-menu dropdown-menu-sm">
                        @foreach ($years as $year)
                            <li><a href="javascript:void(0)" onclick="changeYear('{{ $year }}')" class="dropdown-item">{{ $year }}</a></li>
                            @if ($loop->last)
                            <li><a href="javascript:void(0)" onclick="changeYear('{{ $year + 1 }}')" class="dropdown-item">{{ $year + 1 }}</a></li>
                            @endif
                        @endforeach
                      </ul>
                    </div>
                </h4>
                <div class="card-header-action">
                    <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Ubah Cabang
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-start" style="width:max-content;position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                        @foreach ($branches as $cabang)
                            <a class="dropdown-item" width="100%" onclick="changeCabang('{{ $cabang->id }}', '{{ $cabang->name }}')" href="javascript:void(0)">{{ $cabang->name }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped mb-0" id='table'>
                        <thead>
                            <tr>
                                <th>Divisi</th>
                                <th>Jan</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Apr</th>
                                <th>Mei</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Agu</th>
                                <th>Sep</th>
                                <th>Okt</th>
                                <th>Nop</th>
                                <th>Des</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

	</div>
</div>
@endsection

@section('script')
<script>

function getMonthFromString(mon){
   return new Date(Date.parse(mon +" 1, 2000")).getMonth()+1
}

function changeCabang(id, name) {
    $('body').find('.card-header #cabang').data('partner_branch_id', id).find('span').text(name);
    loadData();
}

function changeYear(year) {
    $('#orders-year').html(year);
    loadData();
}

function store(element) {
    $.ajax({
        type: "post",
        url: "{{ route('partner.request.store') }}",
        data: {
            '_token' : "{{ csrf_token() }}",
            'job_desk_id' : element.closest('tr').data('jobdeskid'),
            'month' : element.closest('td').data('month'),
            'year' : $('body').find('#orders-year').text(),
            'partner_branch_id' : $('body').find('.card-header h4').data('partner_branch_id'),
            'quota' : element.val(),
        },
        success: function (response) {
            if (response.status) {
                element.closest('td').html(response.data.quota);
            }
        }
    });
}

function cancelStore(val, element) {
    element.html(val);
}

function validateData(month) {
    let year = $('body').find('#orders-year').text();
    let date = new Date();
    month = getMonthFromString(month);
    return month > (date.getMonth() + 1) && (date.getYear() + 1900) == year;
}

function inputData(element) {
    let val = element.text();
    element.find('input').val() ? val = element.find('input').val() : '';

    if ( validateData( element.data('month') ) ) {
        element.html('<input type="number" value="' + val + '" style="width:' + element.width() + '">');
        element.find('input').focus();
    }

    $('body').find('#table input').on('keyup', function(e) {
        let keyCode = e.keyCode ? e.keyCode : e.which;
        if (keyCode == 13) {
            if ($(this).val() != val) {
                store($(this));
            } else {
                cancelStore(val, $(this).closest('td'));
            }
        } else if( keyCode == 27 ){
            cancelStore(val, $(this).closest('td'));
        }
    });

    $('body').find('#table input').focusout( function() {
        if ($(this).val() != val) {
            store($(this));
        } else {
            cancelStore(val, $(this).closest('td'));
        }
    });

    return;
}

function loadData(data = null) {
    $.ajax({
        type: "post",
        url: "{{ route('partner.request.list') }}",
        data: {
            '_token' : "{{ csrf_token() }}",
            'year' : $('body').find('#orders-year').text(),
            'partner_branch_id' : $('body').find('.card-header h4').data('partner_branch_id'),
        },
        success: function (item) {

            // append data
            let jobdesks = {!! $jobdesks !!};
            let input = false;
            if (item.status) {
                let data = '';
                $.each(item.datas, function (index, datas) {
                    data += '<tr data-jobdeskid="' + index + '">';
                    $.each(datas, function (index, value) { 
                        $.inArray(index, jobdesks) !== -1
                            ? data += '<th data-month="' + index + '">' + value + '</th>'
                            : (
                                ! validateData(index)
                                    ? data += '<th data-month="' + index + '">' + value + '</th>'
                                    : data += '<td data-month="' + index + '" ondblclick="inputData($(this))" >' + value + '</td>'
                            );
                    });
                    data += '</tr>';
                });
                $('body').find('#table tbody').html(data);
            }
        }
    });
}

$(document).ready( function() {
    loadData();
});
</script>
@endsection