@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-primary">
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col" colspan="4" class="text-center">DIMENSION</th>
							</tr>
						</thead>
						<tbody>
							@for ($i=0; $i < count($dimensions['left']); $i++)
								<tr>
									<td>{{ $i+1 }}</td>
									<td>{{ $dimensions['left'][$i]['name']}}</td>
									<td>{{ $result[$dimensions['left'][$i]['abbreviation']]['value'] }}%</td>
									<td>{{ $result[$dimensions['right'][$i]['abbreviation']]['value'] }}%</td>
									<td>{{ $dimensions['right'][$i]['name']}}</td>
								</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-footer bg-whitesmoke text-center">
				<p class="h1">
				@for ($i=0; $i < count($dimensions['left']); $i++)
					@if ($result[$dimensions['left'][$i]['abbreviation']]['value'] > $result[$dimensions['right'][$i]['abbreviation']]['value'])
						{{ $dimensions['left'][$i]['abbreviation'] }}
					@else
						{{ $dimensions['right'][$i]['abbreviation'] }}
					@endif
				@endfor
				</p>
			</div>
		</div>

	</div>
</div>
@endsection