<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ $title }} | {{ config('app.name', 'Laravel') }}</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('modules/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/fontawesome/css/all.min.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <style>
        @media (min-width: 1200px) {
            .main-wrapper .confirmation-success{
                width: 60%;
            }
        }

        @media(min-width:992px) and (max-width: 1199.98px) {
            .main-wrapper .confirmation-success{
                width: 60%;
            }
        }
        @media(min-width:768px) and (max-width: 991.98px) {
            .main-wrapper .confirmation-success{
                width: 70%;
            }
        }
        @media(min-width:576px) and (max-width: 767.98px) {
            .main-wrapper .confirmation-success{
                width: 80%;
            }
        }
        @media(max-width:575.98px) {
            .main-wrapper .confirmation-success{
                width: 100%;
            }
        }
    </style>
</head>
<body class="sidebar-gone">
<div id="app">
    <section class="section">
        <div class="main-wrapper">
            <div class="container text-center" style="margin-top: 80px;">
                <img class="confirmation-success" src="{{ asset('img/svg/confirmation.svg') }}">
                <h1 class="display-4" style="color: #6777EF; font-weight:700;">Thanks for Registration!</h1>
                <p style="font-size:14px;">Registrasi Anda berhasil! Silahkan tunggu info selanjutnya yang akan dikirim melalui email.</p>
            </div>
        </div>
    </section>
</div>

    <!-- General JS Scripts -->
    <script src="{{ asset('modules/jquery.min.js') }}"></script>
    <script src="{{ asset('modules/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/stisla.js') }}"></script>

    <!-- Template JS File -->
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    </div>
</body>
</html>
