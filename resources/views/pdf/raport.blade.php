<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="charset=utf-8" />
        <title>{{ $title }}</title>
        <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }
        .page-break {
            page-break-after: always;
        }
        .container {
            font-family: Arial, Helvetica, sans-serif;
            margin: 35px 0px;
        }
        .text-center {
            text-align: center !important;
        }
        .img-profile {
            margin: 0px 40px;
            border-radius: 25px; 
            width: 150px;
        }
        .top-profile {
            margin-top: -15px;
            margin-left: 30px;
            position: absolute;
            font-weight: 400;
        }
        .title-profile {
            background-color: #0E5BA1;
            color: #ffffff;
        }
        
        .fz18 {
            font-size: 18px;
        }

        .td-50 {
            padding: 5px 40px;
            width: 250px !important;
        }

        .raport {
            padding: 1.5rem;
            margin-right: 0;
            margin-left: 0;
            border-width: .2rem;
        }
        .table {
            font-size: 14px;
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }
        .table-bordered {
            border: 1px solid #333333;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #333333;
        } 
        .table th {
            padding: 10px 0px;
        }
        .table td {
            padding: 5px 10px;
        }
        .table small {
            font-size: 12px;
        }
        </style>
    </head>
    <body>
        <div class="container">
            <img class="img-profile" src="{{ $data->photo ? public_path().'/img/avatar/'.$data->photo : 'No Photos'  }}">
            <div class="top-profile">    
                <h2><b>{{ $data->name }}</b></h2>
                <p class="fz18">{{ strtoupper($data->school->name) }}</p>
            </div>


            <div class="main-profile">
                <div class="title-profile text-center">
                    <p><b>INFORMASI</b> SISWA</p>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td class="td-50">
                                <h6>NAMA</h6>
                                <p>{{ $data->name ? $data->name : '-' }}</p>
                            </td>
                            <td class="td-50">
                                <h6>AKADEMI</h6>
                                <p>{{ $data->school->name ? strtoupper($data->school->name) : '-' }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-50">
                                <h6>NOMOR TELPON</h6>
                                <p>62 {{ $data->phone_number ? $data->phone_number : '-' }}</p>
                            </td>
                            <td class="td-50">
                                <h6>EMAIL</h6>
                                <p>{{ $data->user->email ? $data->user->email : '-' }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-50">
                                <h6>TEMPAT LAHIR</h6>
                                <p>{{ $data->place_of_birth ? $data->place_of_birth : '-' }}</p>
                            </td>
                            <td class="td-50">
                                <h6>TANGGAL LAHIR</h6>
                                <p>{{ $data->date_of_birth != '0000-00-00' ? \Carbon\Carbon::parse($data->date_of_birth)->format('d/m/Y') : '-' }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="td-50">
                                <h6>BERAT BADAN</h6>
                                <p> {{ $data->weight ? $data->weight : '-' }} </p>
                            </td>
                            <td class="td-50">
                                <h6>TINGGI BADAN</h6>
                                <p> {{ $data->weight ? $data->weight : '-' }} </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- PAGE BREAK -->
        <div class="page-break"></div>
        <!-- PAGE BREAK -->


        <!-- RAPORT -->
        <h2 class="text-center">Raport Siswa</h2>
            <div class="raport">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">{{ __('No') }}</th>
                        <th scope="col">{{ __('Skill') }}</th>
                        <th scope="col">{{ __('KKM') }}</th>
                        <th scope="col">{{ __('Pencapaian') }}</th>
                        <th scope="col">{{ __('Pencapaian Rata-rata') }}</th>
                    </tr>
                </thead>
				<tbody>
                    @php $no = 1; @endphp
                    @foreach($raport as $key)
                        <tr>
                            <th scope="row">{{ $no++ }}</th>
                            <td>{{ $key['lesson'] }}</td>
                            <td>{{ $key['kkm'] }}</td>
                            <td>{{ $key['flight_hours'] }}</td>
                            <td>{{ $key['average'] }} <small></small></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        <!-- RAPORT -->
    </body>
</html>