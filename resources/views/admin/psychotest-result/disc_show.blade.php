@extends('layouts.main')

@section('content')
<h3 class="section-title">Information Person</h3>
<table class="table table-borderless">
    <thead>
        <tr>
            <th>Name</th>
            <th>E-Mail</th>
            <th>Category</th>
            <th>Test Date</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $user['name'] }}</td>
            <td>{{ $user['email'] }}</td>
            <td>{{ $category }}</td>
            <td>{{ Carbon\Carbon::parse($result_disc[0]['created_at'])->formatLocalized('%d %B %Y')}}</td>
        </tr>
    </tbody>
</table>

<h3 class="section-title">Last Result</h3>
<table class="table table-borderless">
    <thead>
        <tr>
            <th>D</th>
            <th>I</th>
            <th>S</th>
            <th>C</th>
            <th>#</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $mostD }}</td>
            <td>{{ $mostI }}</td>
            <td>{{ $mostS }}</td>
            <td>{{ $mostC }}</td>
            <td>{{ $mostRest }}</td>
        </tr>
        <tr>
            <td>{{ $leastD }}</td>
            <td>{{ $leastI }}</td>
            <td>{{ $leastS }}</td>
            <td>{{ $leastC }}</td>
            <td>{{ $leastRest }}</td>
        </tr>
        <tr>
            <td>{{ $mostD - $leastD }}</td>
            <td>{{ $mostI - $leastI }}</td>
            <td>{{ $mostS - $leastS }}</td>
            <td>{{ $mostC - $leastC }}</td>
            <td>-</td>
        </tr>
    </tbody>
</table>

<h3 class="section-title">Graph Result</h3>
<div class="row">
    <div class="col-sm-4">
        <canvas id="graph-1" width="100%" height="100px"></canvas>
    </div>
    <div class="col-sm-4">
        <canvas id="graph-2" width="100%" height="100px"></canvas>
    </div>
    <div class="col-sm-4">
        <canvas id="graph-3" width="100%" height="100px"></canvas>
    </div>
</div>
@php
    $g = [1];
    $explode1 = [];
    $aspect = ['D', 'I', 'S', 'C'];

    foreach ($aspect as $disc) {
            foreach ($g as $no) {
                $explode1 += [$disc => [$no => explode(';', $graphs[$disc][$no][0])]];
        }
    }
    $g2 = [2];
    $explode2 = [];
    foreach ($aspect as $disc) {
            foreach ($g2 as $no) {
                $explode2 += [$disc => [$no => explode(';', $graphs2[$disc][$no][0])]];
        }
    }
    $g3 = [3];
    $explode3 = [];
    foreach ($aspect as $disc) {
        foreach ($g3 as $no) {
            $datas = [explode(';', $graphs3[$disc][$no][0]['array_index']) , explode(';', $graphs3[$disc][$no][0]['value'])];
            $i = 0;
            $array = [];
            foreach ($datas[1] as $merge) {
                $array += [$datas[0][$i] => $datas[1][$i++] ];
            }
            $explode3 += [$disc => [$no => $array]];
        }
    }            

@endphp
@section('script')
    <script>
        var ctx = $("#graph-1");
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["D", "I", "S", "C"],

                datasets: [
                    {
                        label: "Graph 1 - Mask Public Self",
                        data: [{{ $explode1['D'][1][$mostD] }}, {{ $explode1['I'][1][$mostI] }}, {{ $explode1['S'][1][$mostS] }}, {{ $explode1['C'][1][$mostC] }}],
                        pointBackgroundColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
                        pointBorderColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
                        pointBorderWidth: 8,
                        fill: false,
                    }   
                ]
            }
        });
    </script>
    <script>
        var ctx = $("#graph-2");
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["D", "I", "S", "C"],
            
                datasets: [
                    {
                        label: "Graph 2 - Core Private Self",
                        data: [{{ $explode2['D'][2][$leastD] }}, {{ $explode2['I'][2][$leastI] }}, {{ $explode2['S'][2][$leastS] }}, {{ $explode2['C'][2][$leastC] }}],
                        pointBackgroundColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
                        pointBorderColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
                        pointBorderWidth: 8,
                        fill: false,
                    }   
                ]
            }
        });
    </script>
    <script>
        var ctx = $("#graph-3");
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["D", "I", "S", "C"],
            
                datasets: [
                    {
                        label: "Graph 3 - Mirror Perceived Self",
                        data: [{{ $explode3['D'][3][$mostD - $leastD] }}, {{ $explode3['I'][3][$mostI - $leastI] }}, {{ $explode3['S'][3][$mostS - $leastS] }}, {{ $explode3['C'][3][$mostC - $leastC] }}],
                        pointBackgroundColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
                        pointBorderColor: [ "#FF6766", "#FFFF66", "#99CDFF", "#67FF9A" ],
                        pointBorderWidth: 8,
                        fill: false,
                    }   
                ]
            }
        });
    </script>
@endsection
@endsection
