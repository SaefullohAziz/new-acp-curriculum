@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif
		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="pills-mbti-tab" data-toggle="pill" href="#pills-mbti" role="tab" aria-controls="pills-mbti" aria-selected="true">MBTI Result</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-disc-tab" data-toggle="pill" href="#pills-disc" role="tab" aria-controls="pills-disc" aria-selected="false">DISC Result</a>
			</li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-mbti" role="tabpanel" aria-labelledby="pills-mbti-tab">
				<div class="card card-primary">
					<div class="card-header">
						<button class="btn btn-icon btn-secondary" data-toggle="modal" data-target="#filterModalMbti" title="{{ __('Filter') }}"><i class="fa fa-filter"></i></button>
						<button class="btn btn-icon btn-secondary" onclick="reloadTableMbti()" title="{{ __('Refresh') }}"><i class="fa fa-sync"></i></i></button>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-striped" width="100%" id="table4mbti">
								<thead>
									<tr>
										<th>{{ __('No') }}</th>
										<th>{{ __('Created At') }}</th>
										<th>{{ __('Name') }}</th>
										<th>{{ __('Category') }}</th>
										<th>{{ __('Action') }}</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-disc" role="tabpanel" aria-labelledby="pills-disc-tab">
				<div class="card card-primary">
					<div class="card-header">
						<button class="btn btn-icon btn-secondary" data-toggle="modal" data-target="#filterModalDisc" title="{{ __('Filter') }}"><i class="fa fa-filter"></i></button>
						<button class="btn btn-icon btn-secondary" onclick="reloadTableDisc()" title="{{ __('Refresh') }}"><i class="fa fa-sync"></i></i></button>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-striped" width="100%" id="table4disc">
								<thead>
									<tr>
										<th>{{ __('No') }}</th>
										<th>{{ __('Created At') }}</th>
										<th>{{ __('Name') }}</th>
										<th>{{ __('Category') }}</th>
										<th>{{ __('Action') }}</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>


@endsection()

@section('script')
<script>
	var tableMbti;
	$(document).ready(function() {
		tableMbti = $('#table4mbti').DataTable({
			processing: true,
			serverSide: true,
			"ajax": {
				"url": "{{route('psychotest-result.mbti_list')}}",
				"type": "POST",
				"data": function (d) {
				  d._token = "{{ csrf_token() }}";
		        }
			},
			columns: [
				{ data: 'DT_RowIndex', name: 'DT_RowIndex', 'searchable': false },
				{ data: 'created_at', name: 'created_at' },
				{ data: 'name', name: 'name' },
				{ data: 'category', name: 'category' },
				{ data: 'action', name: 'action', "orderable": false, 'searchable': false }
			],
			"order": [[ 1, 'desc' ]],
			"columnDefs": [
			{   
          		"targets": [ 0, -1 ], //last column
          		"orderable": false, //set not orderable
      		},
      		],
  		});
	});

	function reloadTableMbti() {
	    tableMbti.ajax.reload(null,false); //reload datatable ajax
	}

	function filter4mbti() {
		reloadTableMbti();
		$('#filter4mbti').modal('hide');
	}
</script>

<script>
	var tableDisc;
	$(document).ready(function() {
		tableDisc = $('#table4disc').DataTable({
			processing: true,
			serverSide: true,
			"ajax": {
				"url": "{{route('psychotest-result.disc_list')}}",
				"type": "POST",
				"data": function (d) {
				  d._token = "{{ csrf_token() }}";
		        }
			},
			columns: [
				{ data: 'DT_RowIndex', name: 'DT_RowIndex', 'searchable': false },
				{ data: 'created_at', name: 'created_at' },
				{ data: 'name', name: 'name' },
				{ data: 'category', name: 'category' },
				{ data: 'action', name: 'action', "orderable": false, 'searchable': false }
			],
			"order": [[ 1, 'desc' ]],
			"columnDefs": [
			{   
          		"targets": [ 0, -1 ], //last column
          		"orderable": false, //set not orderable
      		},
      		],
  		});
	});

	function reloadTableDisc() {
	    tableDisc.ajax.reload(null,false); //reload datatable ajax
	}

	function filter4Disc() {
		reloadTableDisc();
		$('#filterModalDisc').modal('hide');
	}
</script>

<!-- Modal MBTI -->
<div class="modal fade" id="filterModalMbti" tabindex="-1" role="dialog" aria-labelledby="filter4MbtiLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="filter4MbtiLabel">{{ __('Filter') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{{ Form::open(['url' => '#', 'files' => true]) }}
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
						</div>
					</div>
				</div>
				<div class="modal-footer bg-whitesmoke d-flex justify-content-center">
					{{ Form::button(__('Filter'), ['class' => 'btn btn-primary', 'onclick' => 'filter()']) }}
					{{ Form::button(__('Cancel'), ['class' => 'btn btn-secondary', ' data-dismiss' => 'modal']) }}
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>

<!-- Modal DISC -->
<div class="modal fade" id="filterModalDisc" tabindex="-1" role="dialog" aria-labelledby="filter4DiscLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="filter4DisclLabel">{{ __('Filter') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{{ Form::open(['url' => '#', 'files' => true]) }}
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
						</div>
					</div>
				</div>
				<div class="modal-footer bg-whitesmoke d-flex justify-content-center">
					{{ Form::button(__('Filter'), ['class' => 'btn btn-primary', 'onclick' => 'filter()']) }}
					{{ Form::button(__('Cancel'), ['class' => 'btn btn-secondary', ' data-dismiss' => 'modal']) }}
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endsection