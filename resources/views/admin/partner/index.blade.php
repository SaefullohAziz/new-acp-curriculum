@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-primary">
			<div class="card-header">
			@if(!auth()->user()->role->name == 'bca')
                <a href="{{ route('admin.partner.create') }}" class="btn btn-icon btn-success" title="{{ __('Create') }}"><i class="fa fa-plus"></i></a>
            @endif
				<button class="btn btn-icon btn-secondary" data-toggle="modal" data-target="#filterModal" title="{{ __('Filter') }}"><i class="fa fa-filter"></i></button>
				<button class="btn btn-icon btn-secondary" onclick="reloadTable()" title="{{ __('Refresh') }}"><i class="fa fa-sync"></i></i></button>
				<a class="btn btn-icon collapsed btn-secondary" title=" {{ __('Setting Table') }}" data-toggle="collapse" href="#showHide" role="button" aria-expanded="false" aria-controls="showHide"><i class="fa fa-cog"></i></a>
			</div>
				<div class="collapse ml-4 mb-3" id="showHide">
					<div class="row">
						<div class="col-lg-12 mb-3">
							<div class="section-title" style="margin: 0px 0px 10px 0px">{{ __('Show/Hide Column') }}</div>
							<div class="row">
								@php $no = 1; @endphp
								@foreach($columns as $column)
								<div class="custom-control custom-checkbox ml-3">
									<input type="checkbox" class="custom-control-input" id="column{{ str_replace(' ', '', $column) }}" checked>
									<label class="custom-control-label showHide" data-column="{{ $no++ }}" for="column{{ str_replace(' ', '', $column) }}">{{ $column }}</label>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped" id="table4data">
						<thead>
							<tr>
								<th>
									<div class="checkbox icheck"><label><input type="checkbox" name="selectData"></label></div>
								</th>
								<!-- <th>{{ __('Created At') }}</th> -->
								<th>{{ __('Name') }}</th>
								<th style="width: 180px">{{ __('Phone Number') }}</th>
								<th>{{ __('Address') }}</th>
								<!-- <th>{{ __('E-mail') }}</th> -->
								<!-- <th>{{ __('Action') }}</th> -->
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-footer bg-whitesmoke">
                <button class="btn btn-danger btn-sm" name="deleteData" title="{{ __('Delete') }}">{{ __('Delete') }}</button>
			</div>
		</div>

	</div>
</div>
@endsection

@section('script')
<script>
	var table;
	$(document).ready(function() {
		table = $('#table4data').DataTable({
			processing: true,
			serverSide: true,
			"ajax": {
				"url": "{{ route('admin.partner.list') }}",
				"type": "POST",
				"data": function (d) {
				  d._token = "{{ csrf_token() }}";
				  d.partners = $('select[name="partners[]"]').val();
				  d.provinces = $('select[name="provinces[]"]').val();
		          d.regencies = $('select[name="regencies[]"]').val();
		        }
			},
			columns: [
				{ data: 'DT_RowIndex', name: 'DT_RowIndex', 'searchable': false },
				// { data: 'created_at', name: 'created_at' },
				{ data: 'name', name: 'name' },
				{ data: 'phone_number', name: 'phone_number' },
				{ data: 'address', name: 'address' },
				// { data: 'email', name: 'email' },
				// { data: 'action', name: 'action', "orderable": false, 'searchable': false }
			],
			"order": [[ 1, 'desc' ]],
			"columnDefs": [
			{   
          		"targets": [ 0, -1 ], //last column
          		"orderable": false, //set not orderable
      		},
      		],
      		"drawCallback": function(settings) {
      			$('input[name="selectData"], input[name="selectedData[]"]').iCheck({
      				checkboxClass: 'icheckbox_flat-green',
      				radioClass: 'iradio_flat-green',
      				increaseArea: '20%' /* optional */
      			});
      			$('[name="selectData"]').on('ifChecked', function(event){
      				$('[name="selectedData[]"]').iCheck('check');
      			}).on('ifUnchecked', function(event){
      				$('[name="selectedData[]"]').iCheck('uncheck');
      			});
      		},
  		});

  		$('select[name="provinces[]"]').change(function(event) {
			event.preventDefault();
			var provinces = $('select[name="provinces[]"] option:selected').map(function(){
				return $(this).val();
			}).get();
			$('select[name="regencies[]"]').html('');
			if ($(this).val() != '') {
				$.ajax({
					url : "{{ route('get.regency') }}",
					type: "POST",
					dataType: "JSON",
					data: {'_token' : '{{ csrf_token() }}', 'provinces' : provinces},
					success: function(data)
					{
						$.each(data.result, function(key, value) {
							$('select[name="regencies[]"]').append('<option value="'+key+'">'+value+'</option>');
						});
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						$('select[name="regencies[]"]').html('');
					}
				});
			}
		});

		$('[name="deleteData"]').click(function(event) {
			if ($('[name="selectedData[]"]:checked').length > 0) {
				event.preventDefault();
				var selectedData = $('[name="selectedData[]"]:checked').map(function(){
					return $(this).val();
				}).get();
				swal({
			      	title: '{{ __("Are you sure want to delete this data?") }}',
			      	text: '',
			      	icon: 'warning',
			      	buttons: ['{{ __("Cancel") }}', true],
			      	dangerMode: true,
			    })
			    .then((willDelete) => {
			      	if (willDelete) {
			      		$.ajax({
							url : "{{ route('user.destroy') }}",
							type: "DELETE",
							dataType: "JSON",
							data: {"_token" : "{{ csrf_token() }}", "selectedData" : selectedData},
							success: function(data)
							{
								reloadTable();
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								if (JSON.parse(jqXHR.responseText).status) {
									swal("{{ __('Failed!') }}", '{{ __("Data cannot be deleted.") }}', "warning");
								} else {
									swal(JSON.parse(jqXHR.responseText).message, "", "error");
								}
							}
						});
			      	}
    			});
			} else {
				swal("{{ __('Please select a data..') }}", "", "warning");
			}
		});

		$('label.showHide').on('click', function(e){
            // Get the column API object
            var column = table.column( $(this).attr('data-column') );
    
            // Toggle the visibility
            column.visible( ! column.visible() );
        });

        // Reset Password
		// $('[name="resetPassword"]').click(function(event) {
		// 	if ($('[name="selectedData[]"]:checked').length > 0) {
		// 		event.preventDefault();
		// 		var selectedData = $('[name="selectedData[]"]:checked').map(function(){
		// 			return $(this).val();
		// 		}).get();
		// 		swal({
		// 	      	title: '{{ __("Are you sure want to reset this account?") }}',
		// 	      	text: '',
		// 	      	icon: 'warning',
		// 	      	buttons: ['{{ __("Cancel") }}', true],
		// 	      	dangerMode: true,
		// 	    })
		// 	    .then((willReset) => {
		// 	      	if (willReset) {
		// 	      		$.ajax({
		// 					url : "{{ route('user.reset') }}",
		// 					type: "DELETE",
		// 					dataType: "JSON",
		// 					data: {"_token" : "{{ csrf_token() }}", "selectedData" : selectedData},
		// 					success: function(data)
		// 					{
		// 						swal("{{ __('Success!') }}", '{{ __("Reseting succeed.") }}', "success");
		// 						reloadTable();
		// 					},
		// 					error: function (jqXHR, textStatus, errorThrown)
		// 					{
		// 						if (JSON.parse(jqXHR.responseText).status) {
		// 							swal("{{ __('Failed!') }}", '{{ __("Data cannot be reseted.") }}', "warning");
		// 						} else {
		// 							swal(JSON.parse(jqXHR.responseText).message, "", "error");
		// 						}
		// 					}
		// 				});
		// 	      	}
    	// 		});
		// 	} else {
		// 		swal("{{ __('Please select a data..') }}", "", "warning");
		// 	}
		// });
	});

	function reloadTable() {
	    table.ajax.reload(null,false); //reload datatable ajax
	    $('[name="selectData"]').iCheck('uncheck');
	}

	function filter() {
		reloadTable();
		$('#filterModal').modal('hide');
	}
</script>

<!-- Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="filterModallLabel">{{ __('Filter') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{{ Form::open(['route' => 'admin.school.index', 'files' => true]) }}
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							{{ Form::bsSelect('col-sm-12', 'PKT', 'partners[]', $partners, null, __('All PKT'), ['multiple' => '']) }}
							{{ Form::bsSelect('col-sm-6', 'Province', 'provinces[]', $provinces, null, __('All Province'), ['multiple' => '']) }}
							{{ Form::bsSelect('col-sm-6', 'Regency', 'regencies[]', [], null, __('All Regency'), ['multiple' => '']) }}
						</div>
					</div>
				</div>
				<div class="modal-footer bg-whitesmoke d-flex justify-content-center">
					{{ Form::button(__('Filter'), ['class' => 'btn btn-primary', 'onclick' => 'filter()']) }}
					{{ Form::button(__('Cancel'), ['class' => 'btn btn-secondary', ' data-dismiss' => 'modal']) }}
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endsection
