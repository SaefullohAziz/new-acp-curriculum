@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-primary">

			{{ Form::open(['route' => 'admin.partner.store']) }}
				<div class="card-body">
					<div class="row">
						<fieldset>
                            <div class="row">
							{{ Form::bsText('col-sm-6', __('Name'), 'name', old('name'), __('Name'), ['required' => '']) }}

							{{ Form::bsText('col-sm-6', __('Username'), 'username', old('username'), __('Username'), ['required' => '']) }}

							{{ Form::bsText('col-sm-6', __('Email'), 'email', old('email'), __('Email'), ['required' => '']) }}

							{{ Form::bsTextarea('col-sm-6', __('Address'), 'address', old('address'), __('Address'), ['' => '']) }}

							{{ Form::bsPassword('col-sm-6', __('Password'), 'password', __('Password'), ['required' => '']) }}

							{{ Form::bsPassword('col-sm-6', __('Retype Password'), 'password_confirmation', __('Retype Password'), ['required' => '']) }}
                            </div>
						</fieldset>
					</div>
				</div>
				<div class="card-footer bg-whitesmoke text-center">
					{{ Form::submit(__('Save'), ['class' => 'btn btn-primary']) }}
					{{ link_to(route('user.index'),__('Cancel'), ['class' => 'btn btn-danger']) }}
				</div>
			{{ Form::close() }}

		</div>
	</div>
</div>
@endsection