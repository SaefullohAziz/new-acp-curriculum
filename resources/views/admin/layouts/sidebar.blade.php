<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ route('home') }}">
        <!-- {{ config('app.name', 'Laravel') }} -->
        <img src="{{ asset('img/logo/bca-cma.png') }}" width="120px">
      </a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('home') }}">
        <!-- {{ config('app.shortname', 'LV') }} -->
        <img src="{{ asset('img/logo/bca-cma.png') }}" width="50px">
      </a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">{{ __('Dashboard') }}</li>
      <li class="{{ (request()->is('admin/home')?'active':'') }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="fa fa-home"></i> <span>{{ __('Home') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Menu') }}</li>
      <li class="{{ (request()->is('admin/school/*') || request()->is('admin/school')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.school.index') }}">
          <i class="fa fa-university"></i> <span>{{ __('School') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('admin/teacher/*') || request()->is('admin/teacher')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.teacher.index') }}">
          <i class="fa fa-chalkboard-teacher"></i> <span>{{ __('Fasilitator') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('admin/student/*') || request()->is('admin/student')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.student.index') }}">
          <i class="fa fa-user-graduate"></i> <span>{{ __('Student') }}</span>
        </a>
      </li>
      <!-- <li class="{{ (request()->is('admin/psychotest-result/*') || request()->is('admin/psychotest-result')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.psychotest-result.index') }}">
          <i class="fa fa-notes-medical"></i> <span>{{ __('Psychotest Result') }}</span>
        </a>
      </li>
      <li class="{{ (request()->is('admin/training/*') || request()->is('admin/training')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.training.index') }}">
          <i class="fa fa-dumbbell"></i> <span>{{ __('Training') }}</span>
        </a>
      </li> -->
      <li class="{{ (request()->is('admin/partner/*') || request()->is('admin/partner')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.partner.index') }}">
          <i class="fa fa-user-friends"></i> <span>{{ __('Partner') }}</span>
        </a>
      </li>
      <li class="menu-header">{{ __('Update') }}</li>
      <li class="{{ (request()->is('admin/update/*') || request()->is('admin/update')?'active':'') }}">
        <a class="nav-link" href="{{ route('admin.update.index') }}">
          <i class="fas fa-clipboard-list"></i> <span>{{ __('Update Data') }}</span>
        </a>
      </li>
      <!--<li class="menu-header">{{ __('Account') }}</li>
      <li class="{{ (request()->is('admin/user/*') || request()->is('admin/user')?'active':'') }}">
        <a class="nav-link" href="{{ route('user.index') }}">
          <i class="fa fa-users"></i> <span>{{ __('Users') }}</span>
        </a>
      </li> -->
      <li class="menu-header">{{ __('Logout') }}</li>
      <li>
        <a class="nav-link text-danger" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i> <span>{{ __('Logout') }}</span>
        </a>
      </li>
    </ul>
  </aside>
</div>