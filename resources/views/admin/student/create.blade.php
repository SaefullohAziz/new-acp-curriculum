@extends('layouts.main')

@section('content')
<div class="card card-primary">
    {{ Form::open(['route' => 'admin.student.store', 'files' => true]) }}
    <div class="card-body">
        <div class="row">
            <legend>{{ __('Personal Data') }}</legend>
            <div class="col-lg-6">
                <fieldset>
                    {{ Form::bsText(null, __('Name'). '*', 'name', old('name'), __('Name'), ['required' => '']) }}
                    
                    {{ Form::bsText(null, __('Identition Number').'(No. NIK / NISN)', 'identition_number', old('identition_number'), __('Identition Number')) }}
                    
                    {{ Form::bsSelect(null, __('Gender'), 'gender', ['Laki-Laki' => 'Laki-Laki', 'Perempuan' => 'Perempuan'], old('gender'), __('Select'), ['placeholder' => __('Select')]) }}
                    
                    {{ Form::bsText(null, __('Place of Birth'), 'place_of_birth', old('place_of_birth'), __('Place of Birth')) }}
                    
                    {{ Form::bsText(null, __('Date of Birth'), 'date_of_birth', old('date_of_birth'), __('Date of Birth'), ['id' => 'dateofbirth']) }}
                </fieldset>
            </div>
            <div class="col-lg-6">
                <fieldset>
                    {{ Form::bsTextAppend(null, __('Weight'), 'weight', old('weight'), __('Weight'), 'Kg', ['required' => '']) }}

                    {{ Form::bsTextAppend(null, __('Height'), 'height', old('height'), __('Height'), 'Cm', ['required' => '']) }}

                    {{ Form::bsSelect(null, __('Province'), 'province', $provinces, old('province'), __('Select'), ['placeholder' => __('Select')]) }}
        
                    {{ Form::bsSelect(null, __('City'), 'city_id', $cities, old('city_id'), __('Select'), ['placeholder' => __('Select')]) }}

                    {{ Form::bsTextarea(null, __('Address'), 'address', old('address'), __('Address')) }}
                </fieldset>
            </div>
            <legend>{{ __('Document Data') }}</legend>
            @foreach($documents as $document)
            <div class="col-lg-6">
                <fieldset>
                    {{ Form::bsBrowseFile('file', ucwords(str_replace('_', ' ', $document->name)). '*',$document->name, old($document->name), ['required' => '']) }}
                </fieldset>
            </div>
            @endforeach
            <legend>{{ __('Account Data') }}</legend>
            <div class="col-lg-6">
                <fieldset>
                    {{ Form::bsText(null, __('Username').' *', 'username', old('username'), __('Username')) }}
                    
                    {{ Form::bsText(null, __('Email').' *', 'email', old('email'), __('Email')) }}   
                </fieldset>
            </div>
            <div class="col-lg-6">
                <fieldset>
                    {{ Form::bsPassword(null, __('Password').' *', 'password', old('password'), ['required' => '']) }}
                    
                    {{ Form::bsPassword(null, __('Password Confirmation').' *', 'password_confirmation', old('password_confirmation'), ['required' => '']) }}
                </fieldset>
            </div>
        </div>
        
        <div class="card-footer bg-whitesmoke text-center">
            {{ Form::submit(__('Save'), ['name' => 'submit', 'class' => 'btn btn-primary']) }}
            {{ link_to(url()->previous(), __('Cancel'), ['class' => 'btn btn-danger']) }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
@section('script')

<script>
    $('[name="date_of_birth"]').keypress(function(e) {
        e.preventDefault();
    }).daterangepicker({
        locale: {format: 'DD-MM-YYYY'},
        singleDatePicker: true,
        showDropdowns: true,
    });
    $(document).ready(function () {
		$('select[name="province"]').change(function() {
			$('select[name="city"]').html('<option value="">Select</option>');
			if ($(this).val() != '') {
				$.ajax({
					url : "{{ route('get.city') }}",
					type: "POST",
					dataType: "JSON",
					cache: false,
					data: {'_token' : '{{ csrf_token() }}', 'province' : $(this).val()},
					success: function(data)
					{
						$.each(data.result, function(key, value) {
							$('select[name="city"]').append('<option value="'+value+'">'+value+'</option>');
						});
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						$('select[name="city"]').html('<option value="">Select</option>');
					}
				});
			}
		});
	});

    $('.file input').change(function() {
        let file = $(this)[0].files[0].name;
        $(this).prev('label').text(file);
    });
</script>
@endsection