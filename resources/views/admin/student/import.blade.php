@extends('layouts.main')
@section('style')
<style>
.dropzone {
    cursor: pointer;
    color: #999;
    font-size: 25px;
    width: 100%;
    height: auto;
    padding: 50px 0px !important;
    display: inline-block;
}

.uploads span {
    color: #3c8dbc;
}

.upload input[type="file"] {
    display: none;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">

        <div class="alert alert-primary alert-has-icon">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <div class="alert-title">Petunjuk</div>Gunakan template file excel yang sudah kami sediakan dibawah.
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4>Import File</h4>
                <div class="card-header-action"><a href="#" class="btn btn-primary" data-toggle="dropdown">Download Template</a></div>
            </div>
            <div class="card-body">
                <div class="form-group upload">
                    <label class="dropzone">
                        <i class="fa fa-cloud-upload"></i>
                        <div class="dz-default dz-message"><span>Browse file to upload</span></div>
                        <input type="file" id="fileinput" name="file"></input>
                        <span id="selected_filename">No file selected</span>
                    </label>
                </div>
                <a href="javascript:void(0)" class="btn btn-primary btn-block mt-3">Submit</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
  $('#fileinput').change(function() {
    $('#selected_filename').text($('#fileinput')[0].files[0].name);
  });
</script>
@endsection
