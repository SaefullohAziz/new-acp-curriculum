@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-primary">

			{{ Form::open(['route' => 'admin.school.store']) }}
				<div class="card-body">
					<div class="row">
						<div class="col-sm-6">
							<fieldset id="school">
								<legend>{{ __('School Data') }}</legend>
								<div class="form-group">
									<label for="input-group">{{ __('Name') }}</LAbel>
									<div class="input-group">
										<select class="custom-select col-sm-3 col-lg-2" id="school" name="school">
											<option value="SMK">SMK</option>
											<option value="SMA">SMA</option>
											<option value="MA">MA</option>
										</select>
										<select class="custom-select col-sm-4 col-lg-3" id="school-type" name="type">
											<option value="Swasta">Swasta</option>
											<option value="Negeri">Negeri</option>
										</select>
										<input type="text" class="form-control" name="name" placeholder="{{ __('School') . ' ' . __('Name') }}...">
									</div>
								</div>
						
								<div class="row">
									{{ Form::bsSelect('col-sm-6', __('Province'), 'province', $provinces, old('province'), __('Province'), ['required' => '']) }}

									{{ Form::bsSelect('col-sm-6', __('City'), 'city_id', $cities, old('city_id'), __('City'), ['required' => '']) }}
								</div>

								{{ Form::bsText(null, __('Since'), 'since', old('since'), __('Ex:') . ' 1999', ['maxlength' => '4', 'required' => '']) }}

								{{ Form::bsPhoneNumber(null, __('School Phone Number'), 'school_phone_number', old('school_phone_number'), __('School Phone Number'), ['maxlength' => '13', 'required' => '']) }}

								{{ Form::bsEmail(null, __('School E-Mail'), 'school_email', old('school_email'), __('Ex:') . ' school@school.sch.id', ['required' => '']) }}

								{{ Form::bsText(null, __('School Website (URL)'), 'school_web', old('school_web'), __('Ex:') . ' Http://school.com', ['required' => '']) }}

								{{ Form::bsTextarea(null, __('Address'), 'address', old('address'), __('Ex:') . ' Dsun.Sukahurip. 02/07', ['required' => '']) }}


								{{ Form::bsText(null, __('Total Student'), 'total_student', old('total_student'), __('Ex:') . ' 999', ['required' => '']) }}

								{{ Form::bsCheckboxList(null, __('Department'), 'department[]', $departments) }}

								{{ Form::bsSelect(null, __('ISO Certificate'), 'iso_certificate', $isoCertificates, old('iso_certificate'), __('Select'), ['placeholder' => __('Select'), 'required' => '']) }}	


							</fieldset>
						</div>
						<div class="col-sm-6">
							<fieldset id="headmaster">
								<legend>{{ __('Headmaster Data') }}</legend>
								{{ Form::bsInlineRadio(null, 'Apakah Kepala Sekolah telah mempelajari proposal BCAP?', 'proposal', ['Sudah' => 'Sudah', 'Belum' => 'Belum'], old('proposal'), ['required' => '']) }}

								{{ Form::bsText(null, __('Name'), 'headmaster_name', old('headmaster_name'), __('Ex: Drs. Eneng, M.A. M.Pd.'), ['required' => '']) }}

								{{ Form::bsEmail(null, __('Headmaster E-Mail'), 'headmaster_email', old('headmaster_email'), __('EX: headmaster@smk1.sch.id'), ['required' => '']) }}

								{{ Form::bsPhoneNumber(null, __('Phone Number'), 'headmaster_phone_number', old('headmaster_phone_number'), __('Phone Number'), ['maxlength' => '13', 'required' => '']) }}
							</fieldset>

							<fieldset id="pic">
								<legend>{{ __('PIC Data') }}</legend>

								{{ Form::bsText(null, __('NIP'), 'pic_nip', old('pic_nip'), __('Ex:') . ' 19860926', ['maxlength' => '20', 'required' => '']) }}

								{{ Form::bsSelect(null, __('Gender'), 'pic_gender', ['Pria' => 'Male', 'Wanita' => 'Female'], old('pic_gender'), __('Choose Gender..'), ['required' => '']) }}
								
								{{ Form::bsText(null, __('Name'), 'pic_name', old('pic_name'), __('Ex: Mauza, S.Pd'), ['required' => '']) }}

								{{ Form::bsEmail(null, __('E-Mail'), 'pic_email', old('pic_email'), __('EX: teacher@smk1.sch.id'), ['required' => '']) }}

								{{ Form::bsPhoneNumber(null, __('Phone Number'), 'pic_phone_number', old('pic_phone_number'), __('Phone Number'), ['maxlength' => '13', 'required' => '']) }}

								{{ Form::bsText(null, __('Date Of Birth'), 'pic_date_of_birth', old('pic_date_of_birth'), __('Choose Date'), ['required' => '']) }}

								{{ Form::bsText(null, __('Position'), 'pic_position', old('pic_position'), __('Ex:') . ' IPA Teacher', ['required' => '']) }}

								{{ Form::bsTextarea(null, __('Address'), 'pic_address', old('pic_address'), __('Ex:') . ' Dsun.Sukahurip. 02/07', ['required' => '']) }}

								{{ Form::bsFile(null, __('Photo'), 'pic_photo', old('pic_photo'), [], [__('Photo with JPG/PNG format up to 5MB.')]) }}
							</fieldset>
						</div>
					</div>
				</div>
				<div class="card-footer bg-whitesmoke text-center">
					{{ Form::submit(__('Save'), ['class' => 'btn btn-primary']) }}
					{{ link_to(route('user.index'),__('Cancel'), ['class' => 'btn btn-danger']) }}
				</div>
			{{ Form::close() }}

		</div>
	</div>
</div>
@endsection

@section('script')
<script>
	$(document).ready(function () {
		$('select[name="province"]').change(function() {
			$('select[name="city_id"]').html('<option value="">Select</option>');
			if ($(this).val() != '') {
				$.ajax({
					url : "{{ route('get.city') }}",
					type: "POST",
					dataType: "JSON",
					cache: false,
					data: {'_token' : '{{ csrf_token() }}', 'province' : $(this).val()},
					success: function(data)
					{
					$('select[name="city_id"]')
						$.each(data.result, function(key, value) {
							$('select[name="city_id"]').append('<option value="'+value+'">'+value+'</option>');
						});
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						$('select[name="city_id"]').html('<option value="">Select</option>');
					}
				});
			}
		});

		$('[name="date_of_birth"]').daterangepicker({
		locale: {format: 'DD-MM-YYYY'},
		singleDatePicker: true,
		});

		// daterangeficker
		$('[name="pic_date_of_birth"]').daterangepicker({
			locale: {format: 'DD-MM-YYYY'},
			singleDatePicker: true,
		});
	});
</script>
@endsection