@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">

		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

		<div class="card card-hero">
			<div class="card-header p-4">
				<div class="card-icon">
					<i class="fas fa-university"></i>
				</div>
				<h4>{{ $data->statuses()->latest()->first()->name }}</h4>
				<!-- <div class="card-description">Ckckck</div> -->
			</div>
			<div class="card-body p-0">
			</div>
		</div>

		<div class="card card-primary">
			<div class="card-body">
				<div class="row">
					<fieldset class="col-sm-6">
						<legend>School Data</legend>
						{{ Form::bsText('null', __('Type'), 'type', $data->type, __('Type'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('Name'), 'name', $data->name, __('Name'), ['disabled' => '']) }}
						{{ Form::bsTextarea('null', __('Address'), 'address', $data->address, __('Address'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('Province'), 'province', $data->city->province->name, __('Province'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('Regency'), 'regency', $data->city->name, __('Regency'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('Zip Code'), 'zip_code', $data->zip_code, __('Zip Code'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('Since'), 'since', $data->since, __('Since'), ['disabled' => '']) }}
						{{ Form::bsPhoneNumber('null', __('School Phone Number'), 'school_phone_number', $data->school_phone_number, __('School Phone Number'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('School Email'), 'school_email', $data->school_email, __('School Email'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('School Website (URL)'), 'school_web', $data->school_website, __('School Website'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('Total Student'), 'total_student', $data->total_student, __('Total Student'), ['disabled' => '']) }}
					</fieldset>

					<fieldset class="col-sm-6">
						<legend>Headmaster Data</legend>
						{{ Form::bsText('null', __('Headmaster Name'), 'headmaster_name', $data->headmaster_name, __('Headmaster Name'), ['disabled' => '']) }}
						{{ Form::bsPhoneNumber('null', __('Headmaster Phone Number'), 'headmaster_phone_number', $data->headmaster_phone_number, __('Headmaster Phone Number'), ['disabled' => '']) }}
						{{ Form::bsText('null', __('Headmaster Email'), 'headmaster_email', $data->headmaster_email, __('Headmaster Email'), ['disabled' => '']) }}
						
						@if(!empty($pics))
							@php $no = 1; @endphp
							@foreach($pics as $pic)
								<legend>PIC {{ $no++ }} Data</legend>
								{{ Form::bsText('null', __('PIC Name'), 'pic_name', $pic->teacher->name, __('PIC Name'), ['disabled' => '']) }}
								{{ Form::bsText('null', __('PIC Position'), 'pic_position', $pic->teacher->position, __('PIC Position'), ['disabled' => '']) }}
								{{ Form::bsPhoneNumber('null', __('PIC Phone Number'), 'pic_phone_number', $pic->teacher->phone_number, __('PIC Phone Number'), ['disabled' => '']) }}
								{{ Form::bsText('null', __('PIC Email'), 'pic_email', $pic->teacher->email, __('PIC Email'), ['disabled' => '']) }}
							@endforeach
						@endif
					</fieldset>
				</div>
			</div>
			<div class="card-footer text-center">
				<a href="{{ route('admin.school.index') }}" class="btn btn-danger">Back</a>
			</div>
		</div>

	</div>
</div>
@endsection