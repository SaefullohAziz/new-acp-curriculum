@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">
        <div id="table1" class="card card-primary">
			<div class="card-header">
                <button class="btn btn-icon btn-secondary" title="{{ __('Filter') }}" data-toggle="modal" data-target="#filterModal"><i class="fa fa-filter"></i></button>
				<button class="btn btn-icon btn-secondary" onclick="reloadTable()" title="{{ __('Refresh') }}"><i class="fa fa-sync"></i></i></button>
				<a class="btn btn-icon collapsed btn-secondary" title=" {{ __('Setting Table') }}" data-toggle="collapse" href="#showHide" role="button" aria-expanded="false" aria-controls="showHide"><i class="fa fa-cog"></i></a>
            </div>
                <div class="collapse ml-4 mb-3" id="showHide">
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <div class="section-title" style="margin: 0px 0px 10px 0px">{{ __('Show/Hide Column') }}</div>
                            <div class="row">
								@php $no = 1; @endphp
								@foreach($columns as $column)
								<div class="custom-control custom-checkbox ml-3">
									<input type="checkbox" class="custom-control-input" id="column{{ str_replace(' ', '', $column) }}" checked>
									<label class="custom-control-label showHide" data-column="{{ $no++ }}" for="column{{ str_replace(' ', '', $column) }}">{{ $column }}</label>
								</div>
								@endforeach
                            </div>
                        </div>
                    </div>
                </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped nowrap" id="table4data">
                        <thead>
                            <tr>
                                <th><div class="checkbox icheck"><label><input type="checkbox" name="selectData"></label></div></th>
                                <th>{{ __('Last Update') }}</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Generation') }}</th>
                                <th>{{ __('School') }}</th>
                                <th>{{ __('Phone Number') }}</th>
                                <th>{{ __('Place of Birth') }}</th>
                                <th>{{ __('Date of Birth') }}</th>
                                <th>{{ __('Document Progress') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="card-footer bg-whitesmoke">
				<button class="btn btn-primary btn-sm" name="moveStatus" title="{{ __('Pindah Status') }}">{{ __('Pindah Status') }}</button>
			</div>
        </div>

        <div id="table2" class="card card-primary mt-5 d-none">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped" id="table4data2">
                        <thead>
                            <tr>
                                <th><div class="checkbox icheck"><label><input type="checkbox" name="selectData2"></label></div></th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Dari Status') }}</th>
                                <th>{{ __('Ke Status') }}</th>
                                <th>{{ __('Generation') }}</th>
                                <th>{{ __('School') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    {{ Form::bsSelect(null, __('Status'), 'status', $moveStatus, old('status'), __('Select'), ['placeholder' => __('Select')]) }}
                </div>
                <div class="col-lg-4">
                    <label for="proses">{{ __('Proses') }}</label>
                    <div class="btn btn-primary btn-large btn-block btn-proses">{{ __('Proses') }}</div>
                    <div class="btn btn-primary btn-large btn-block d-none btn-confirm">{{ __('Confirm') }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    let table;
    let status;
    let selectedData = [];
    function search(data, column) {
        table.columns(column).search(data).draw();
    }
    function getRowId(tr) {
        return table.row(tr).id();
    }
	$(document).ready(function() {
		table = $('#table4data').DataTable({
			processing: true,
			serverSide: true,
			"ajax": {
				"url": "{{ route('admin.update.list') }}",
				"type": "POST",
				"data": function (d) {
		          d._token = "{{ csrf_token() }}";
				  d.student = $('select[name="student"]').val();
				  d.school = $('select[name="school"]').val();
				  d.generation = $('select[name="generation"]').val();
				  d.level_siswa = $('select[name="level_siswa"]').val();
				  d.status_siswa = $('select[name="status_siswa"]').val();
		        }
			},
			columns: [
				{ data: 'DT_RowIndex', name: 'DT_RowIndex', 'searchable': false },
				{ data: 'last_update', name: 'last_update' },
				{ data: 'name', name: 'name' },
				{ data: 'status', name: 'status' },
				{ data: 'generation', name: 'generation'},
				{ data: 'school', name: 'school' },
				{ data: 'phone_number', name: 'phone_number' },
				{ data: 'place_of_birth', name: 'place_of_birth' },
				{ data: 'date_of_birth', name: 'date_of_birth' },
				{ data: 'document_progress', name: 'document_progress' },
			],
            rowId: 'id',
			"order": [[ 1, 'desc' ]],
			"columnDefs": [
			{   
          		"targets": [ 0, -1 ], //last column
          		"orderable": false, //set not orderable
      		},
      		],
      		"drawCallback": function(settings) {
                $.each(selectedData, function (index, value) { 
                    $('#table4data').find('#' + value).iCheck('check');
                });
      			$('input[name="selectData"], input[name="selectedData[]"]').iCheck({
      				checkboxClass: 'icheckbox_flat-green',
      				radioClass: 'iradio_flat-green',
      				increaseArea: '20%' /* optional */
      			});
                
                $('input[name="selectedData[]"]').on('ifChecked', function(event) {
                    if (status !== $(this).closest('tr').find('td:nth-child(4)').text()) {
                        status = $(this).closest('tr').find('td:nth-child(4)').text();
                        search($(this).closest('tr').find('td:nth-child(4)').text(), 3);
                    }
                    console.log(selectedData);
                });

      			$('[name="selectData"]').on('ifChecked', function(event){
      				$('[name="selectedData[]"]').iCheck('check');
      			}).on('ifUnchecked', function(event){
      				$('[name="selectedData[]"]').iCheck('uncheck');
      			});
                  
      			$('input[name="selectData2"], input[name="selectedData2[]"]').iCheck({
      				checkboxClass: 'icheckbox_flat-green',
      				radioClass: 'iradio_flat-green',
      				increaseArea: '20%' /* optional */
      			});
      			$('[name="selectData2"]').on('ifChecked', function(event){
      				$('[name="selectedData2[]"]').iCheck('check');
      			}).on('ifUnchecked', function(event){
      				$('[name="selectedData2[]"]').iCheck('uncheck');
      			});

				if($("#tab-scroll").length) {
				    $("#tab-scroll").css({
				        height: 25,
				        width: 250,
				    }).niceScroll();
				}
      		},
  		});
        $('[name="moveStatus"]').click(function(event) {
            event.preventDefault();
            let selected = $('#table4data [name="selectedData[]"]:checked');
            if (selected.length > 0) {
                $('#table2').removeClass('d-none');
                selected.closest('tr').each(function(){
                    var cells = $('td', this);
                    $('#table4data2').append('<tr><td>' + cells.eq(0).html() + '</td><td>' + cells.eq(2).text() + '</td><td>' + cells.eq(3).text() + '</td><td class="toStatus">-</td><td>' + cells.eq(4).text() + '</td><td>' + cells.eq(5).text() + '</td></tr>');
                });
            } else {
                swal("{{ __('Please select a data..') }}", "", "warning");
            }
        });
        
        // Live change on column "Ke Status"
		$('select[name="status"]').change(function(e) {
            let status = $(this).children("option:selected").text();
            $('.toStatus').text(status);
        });

        // Button Validation if not Selected
        $('.btn-proses').click(function(e){
            let status = $('select[name="status"]');
            if(status[0].selectedIndex <= 0) {
                swal("{{ __('Please choose destination status..') }}", "", "warning");
            } else {
                $('.toStatus').text(status.children("option:selected").text());
                $('#table1').addClass('d-none');
                $('.btn-proses').addClass('d-none');
                $('.btn-confirm').removeClass('d-none');
            }
        });

        // Button confirmation
        $('.btn-confirm').click(function(e){
            var selectedData = $('[name="selectedData[]"]:checked').map(function(){
                return $(this).val();
            }).get();
            var statusData = $('[name="status"]').val();
            swal({
                title: '{{ __("Are you sure want to delete this data?") }}',
                text: '',
                icon: 'warning',
                buttons: ['{{ __("Cancel") }}', true],
                dangerMode: true,
            })
            .then((moveStatus) => {
			      	if (moveStatus) {
			      		$.ajax({
							url : "{{ route('admin.update.status.store') }}",
							type: "POST",
							dataType: "JSON",
							data: {"selectedData" : selectedData, "statusData" : statusData, "_token" : "{{ csrf_token() }}"},
							success: function(data)
							{
                                swal("{{ __('Success!') }}", "", "success");
                                location.reload();
							},
							error: function (jqXHR, textStatus, errorThrown)
							{
								if (JSON.parse(jqXHR.responseText).status) {
									swal("{{ __('Failed!') }}", "{{ __("Data cannot be deleted.") }}", "warning");
								} else {
									swal(JSON.parse(jqXHR.responseText).message, "", "error");
								}
							}
						});
			      	}
    			});
        });

        $('label.showHide').on('click', function(e){
            // Get the column API object
            var column = table.column( $(this).attr('data-column') );
    
            // Toggle the visibility
            column.visible( ! column.visible() );
        });
	});
    
    function reloadTable() {
        table.ajax.reload(null,false); //reload datatable ajax
        $('[name="selectData"]').iCheck('uncheck');
    }
</script>
@endsection