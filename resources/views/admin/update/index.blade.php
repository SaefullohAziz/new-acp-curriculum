@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-12">
        <div class="row">
            
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-level-up-alt"></i>
                    </div>
                    <div class="card-body">
                        <h4>{{ __('Status') }}</h4>
                        <p>{{ __('Perbarui beberapa data status siswa') }}</p>
                        <a href="{{ route('admin.update.status.index') }}" class="card-cta">{{ __('Update') }} <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>

        </div>
	</div>  
</div>
@endsection