@extends('layouts.main')

@section('content')
<!-- TOTAL SEKOLAH DAN MAP SEKOLAH -->
<div class="row">
    <div class="col-lg-4 col-md-4">
        <div class="card shadow-sm">
            <div class="card-header"><h5>{{ __('Total Sekolah') . ' (' . \App\School::count() . ')' }}</h5></div>
            <div class="card-body">
                <div id="divstatusSekolah">Mohon tunggu..</div>
                <canvas id="statusSekolah" style="height:300px; width:100%" class="d-none"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-8">
        <div class="card shadow-sm">
            <div class="card-header"><h5>{{ __('Map Sekolah') }}</h5></div>
            <div class="card-body row">
                <div class="col-lg-8">
                    <iframe src="https://www.google.com/maps/d/embed?mid=1JZebWntfpSd9MFoLsAumWWy28Me81-eA" width="100%" height="300"></iframe>
                </div>
                <div class="col-lg-4">
                    <li class="media">
                        <div class="media-body">
                            <div class="media-title"><h5>{{ __('Daftar Akademi') }}</h5></div>
                            <div id="list-scroll" tabindex="2" style="overflow: hidden; outline: none;">
                                @foreach ($academySchool as $school)
                                <div class="media-title">{{ $school }}</div>
                                @endforeach
                            </div>
                        </div>
                    </li>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="card shadow-sm">
        <div class="card-header"><h5>{{ __('Status Peserta Akademi Saat Ini') . ' (' . (int)\App\Student::count() . ' Peserta)' }}</h5></div>
            <div class="card-body">
                <div id="divaktifAlumni">Mohon tunggu..</div>
                <canvas id="aktifAlumni" style="height:300px; width:100%"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header"><h4>{{ _('Jumlah Peserta Akademi Saat Ini') }}</h4></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-sm" id="schoolAcademy">
                        <thead>
                            <tr>
                                <th scope="col">Nama Sekolah</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
        <div class="card-header"><h4>{{ _('Jumlah Peserta Kelas Intensif Saat Ini') }}</h4></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-sm" id="schoolClassIntensif">
                        <thead>
                            <tr>
                                <th scope="col">Nama Sekolah</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- TOTAL VOKASI -->
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header"><h5>Lulusan yang Belum Terserap</h5></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-sm" id="TalentPool">
                        <thead>
                            <tr>
                                <th scope="col">Sekolah</th>
                                @foreach($month_tables as $month)
                                    <th scope="col">{{ $month['bulan'] }}</th>
                                @endforeach
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header"><h5>Lulusan yang Dihasilkan</h5></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-sm" id="StudentAlocatedFinal">
                        <thead>
                            <tr>
                                <th scope="col">Sekolah</th>
                                @foreach($month_tables as $month)
                                    <th scope="col">{{ $month['bulan'] }}</th>
                                @endforeach
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card shadow-sm">
            <div class="card-header"><h4>Pencapaian Skill <small>per</small> Sekolah</h4>
                <div class="card-header-action dropdown">
                    <a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" aria-expanded="false">
                        {{ $active_lesson ? $active_lesson->name : 'skill' }}
                    </a>
                    <ul id="skillLesson" class="dropdown-menu dropdown-menu-sm dropdown-menu-right" x-placement="bottom-end" style="width:max-content; position: absolute; transform: translate3d(75px, 31px, 0px); top: 0px; left: 0px; will-change: transform; box-shadow: 0 0 10px #666;">
                        @if($active_lesson)
                            @foreach($lessons as $lesson)
                                <li data-id="{{ $lesson->id }}"><a href="javascript:void(0)" class="dropdown-item {{ $lesson->id == $active_lesson->id ? 'active' : '' }}">{{ $lesson->name }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div id="school-scroll" tabindex="2" style="overflow: hidden; outline: none;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Sekolah</th>
                                <th id="jam_terbang">Jam Terbang</th>
                                <th>Pengulangan</th>
                                <th>Pencapaian Rata-rata</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!$active_lesson)
                                <tr>
                                    <td colspan="4" class="text-center">Tidak ada data</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow-sm">
            <div class="card-header"><h5>{{ __('Total Permintaan Peserta Vokasi ('. $total_req .' Peserta)') }}</h5></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-header"><h4><small>per</small> Job Desk <small>per</small> Bulan</h4>
                            <div class="card-header-action dropdown">
                                <a href="#" data-toggle="dropdown" id="textPerPKT" class="btn btn-primary dropdown-toggle" aria-expanded="false">Semua PKT</a>
                                <ul class="dropdown-menu dropdown-menu-left" id="filterPerPKT" x-placement="bottom-end" style="position: absolute; transform: translate3d(-125px, 31px, 0px); top: 0px; left: 0px; will-change: transform; box-shadow: 0 0 10px #666;">
                                    <li><a href="javascript:void(0)" class="dropdown-item active">Semua PKT</a></li>
                                @foreach($partner_branches as $partner)
                                        <li data-id="{{ $partner->id }}"><a href="javascript:void(0)" class="dropdown-item">{{ $partner->slug }}</a></li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="card-body" id="canvasPermintaanSDMJobDesk">
                            <canvas id="permintaanSDMJobDesk" style="height:300px; width:100%"></canvas>
                        </div>    
                    </div>
                    <div class="col-lg-6 d-none">
                        <div class="card-header"><h4><small>per</small> PKT <small>per</small> Bulan</h4></div>
                        <div class="card-body">
                            <canvas id="permintaanSDMPkt" style="height:300px; width:100%"></canvas>
                        </div>    
                    </div>
                    <div class="col-lg-6">
                        <div class="card-header"><h4>{{ __('Total Peserta Vokasi Berdasarkan Status'). ' (' . (int)\App\AlocatedStudent::count() . ' Peserta)' }}</h4></div>
                        <div class="card-body">
                            <canvas id="totalStatusVokasi" style="height:300px; width:100%"></canvas>
                        </div>    
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-header">
                            <div class="card-header-action dropdown">
                                <a href="#" data-toggle="dropdown" id="textBulan" class="btn btn-primary dropdown-toggle" aria-expanded="false">{{ $active_month }}</a>
                                <ul class="dropdown-menu dropdown-menu-left" id="filterBulan" x-placement="bottom-end" style="position: absolute; transform: translate3d(-125px, 31px, 0px); top: 0px; left: 0px; will-change: transform; box-shadow: 0 0 10px #666;">
                                @foreach($months as $month)
                                        <li><a href="javascript:void(0)" class="dropdown-item {{ $month == $active_month ? 'active' : '' }}">{{ $month }}</a></li>
                                @endforeach
                                </ul>
                            </div>
                            <div class="card-header-action dropdown">
                                <a href="#" data-toggle="dropdown" id="filterDropdownJenis" class="btn btn-primary dropdown-toggle" aria-expanded="false">Total Semua</a>
                                <ul class="dropdown-menu dropdown-menu-sm dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-125px, 31px, 0px); top: 0px; left: 0px; will-change: transform; box-shadow: 0 0 10px #666;">
                                <li class="dropdown-title">Tipe Data</li>
                                <li><a href="javascript:void(0)" id="TTLDalamJawa" class="dropdown-item active">Total Semua</a></li>
                                <li><a href="javascript:void(0)" id="PKTDalamJawa" class="dropdown-item">PKT</a></li>
                                <li><a href="javascript:void(0)" id="SIMDalamJawa" class="dropdown-item">Permintaan Khusus</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body row">
                            <div class="col-lg-6" id="canvasDalamJawa">
                                <canvas id="totalDalamJawa" class="" style="height:300px; width:100%"></canvas>
                                <canvas id="pktDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>
                                <canvas id="simDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>
                            </div>
                            <div class="col-lg-6" id="canvasLuarJawa">
                                <canvas id="totalLuarJawa" style="height:300px; width:100%"></canvas>
                                <canvas id="pktLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>
                                <canvas id="simLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="row">
    <div class="col-lg-6">
        <div class="card shadow-sm">
            <div class="card-header"><h5>{{ __('Total Peserta BCA CMA') . ' (' . ((int)\App\Student::count() + 500 + 450) . ' Peserta)' }}</h5></div>
            <div class="card-body">
                <div id="divstatusSiswa">Mohon tunggu..</div>
                <canvas id="statusSiswa" style="height:300px; width:100%" class="d-none"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card shadow-sm">
            <div class="card-header"><h5>{{ __('Status Peserta Akademi Saat Ini') . ' (' . (int)\App\Student::count() . ' Peserta)' }}</h5></div>
            <div class="card-body">
                <div id="divaktifAlumni">Mohon tunggu..</div>
                <canvas id="aktifAlumni" style="height:300px; width:100%"></canvas>
            </div>
        </div>
    </div>
</div> -->

<div class="row">
    <div class="col-lg-12">
        <div class="card shadow-sm">
            <div class="card-header"><h5>{{ __('Total Vokasi Sampai Saat Ini') . ' (' . \App\AlocatedStudent::count() . ' Peserta)' }}</h5></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 mb-4">
                        <div id="divtotalVokasiJobDesk">Mohon tunggu..</div>
                        <canvas id="totalVokasiJobDesk" style="height:300px; width:100%"></canvas>
                    </div>
                    <div class="col-lg-6">
                        <div id="divtotalVokasiPkt">Mohon tunggu..</div>
                        <canvas id="totalVokasiPkt" style="height:300px; width:100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 d-none">
        <div class="card shadow-sm">
            <div class="card-header"><h5>Pencapaian Skill <small>per</small> Siswa</h5>
                <div class="card-header-action dropdown">
                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false">Sekolah</a>
                    <ul id="filterSkillSchool" tabindex="2" class="dropdown-menu dropdown-menu-sm dropdown-menu-right" x-placement="bottom-end" style="width:max-content; position: absolute; transform: translate3d(75px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: hidden; outline: none; box-shadow: 0 0 10px #666;">
                        @foreach($schools as $school)
                            <li data-id="{{ $school->id }}"><a href="javascript:void(0)" class="dropdown-item {{ $school->id == $active_student->school->id ? 'active' : '' }}">{{ $school->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="card-header-action dropdown">
                    <a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" aria-expanded="false">Siswa</a>
                    <ul id="filterSkillStudent" tabindex="2" class="dropdown-menu dropdown-menu-sm dropdown-menu-right" x-placement="bottom-end" style="width:max-content;position: absolute; transform: translate3d(75px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: hidden; outline: none; box-shadow: 0 0 10px #666;">
                        @if($students != '')
                            @foreach($students as $student)
                                <li data-id="{{ $student->id }}"><a href="javascript:void(0)" class="dropdown-item {{ $student->id == $active_student->id ? 'active' : ''}}">{{ $student->name }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <div class="card-body">
				<div id="student-scroll" tabindex="2" style="overflow: hidden; outline: none;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Jenis Skill</th>
                            <th>Jam Terbang</th>
                            <th>Pencapaian Rata-rata</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($students == '')
                            <tr>
                                <td colspan="4" class="text-center">Tidak ada data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>        
</div>

<!-- TOTAL VOKASI -->
<!-- <div class="row">
    <div class="col-lg-12">
        <div class="card shadow-sm">
            <div class="card-header"><h5>{{ __('Total Vokasi Sampai Saat Ini') . ' (' . \App\AlocatedStudent::count() . ' Peserta)' }}</h5></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 mb-4">
                        <div id="divtotalVokasiJobDesk">Mohon tunggu..</div>
                        <canvas id="totalVokasiJobDesk" style="height:300px; width:100%"></canvas>
                    </div>
                    <div class="col-lg-8">
                        <div id="divtotalVokasiPkt">Mohon tunggu..</div>
                        <canvas id="totalVokasiPkt" style="height:300px; width:100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection

@section('script')
<script>
// detect ready document
	$(document).ready(function() {

        //ajax untuk table jmlh peserta akademi
        $.ajax({
            url : "{{ route('get.schoolAcademy') }}",
            type: "POST",
            dataType: "JSON",
            cache: false,
            data: {
                '_token' : '{{ csrf_token() }}',
            },
            success: function(item)
            {
                var tbody = $("#schoolAcademy tbody");
                var tfoot = $("#schoolAcademy tfoot");
                $.each(item.data.tbody, function(key, value) {
                    tbody.append("<tr><td>"+value['nama']+"</td><td>"+value['count']+"</td></tr>");
                });
                tfoot.append("<tr style='border-bottom:2px; border-bottom-style: solid; border-bottom-color: gray;'><td colspan=100%'></td></tr><tr><td><b>Total<b></td><td><b>"+item.data.tfoot+"<b></td></tr>");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //
            }
        });

        //ajax untuk table jmlh peserta kelas intensif
        $.ajax({
            url : "{{ route('get.schoolClassIntensif') }}",
            type: "POST",
            dataType: "JSON",
            cache: false,
            data: {
                '_token' : '{{ csrf_token() }}',
            },
            success: function(item)
            {
                var tbody = $("#schoolClassIntensif tbody");
                var tfoot = $("#schoolClassIntensif tfoot");
                $.each(item.data.tbody, function(key, value) {
                    tbody.append("<tr><td>"+value['nama']+"</td><td>"+value['count']+"</td></tr>");
                });
                tfoot.append("<tr style='border-bottom:2px; border-bottom-style: solid; border-bottom-color: gray;'><td colspan=100%'></td></tr><tr><td><b>Total<b></td><td><b>"+item.data.tfoot+"<b></td></tr>");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //
            }
        });

        //ajax untuk table jmlh peserta akademi
        $.ajax({
            url : "{{ route('get.talentPool') }}",
            type: "POST",
            dataType: "JSON",
            cache: false,
            data: {
                '_token' : '{{ csrf_token() }}',
            },
            success: function(item)
            {
                //talentPool
                var talentBody = $("#TalentPool tbody");
                var talentFoot = $("#TalentPool tfoot");
                $.each(item.data.ready.tbody, function(key, value) {
                    talentBody.append("<tr><td>"+value[0]+"</td><td>"+value[1]+"</td><td>"+value[2]+"</td><td>"+value[3]+"</td><td>"+value[4]+"</td><td>"+value[5]+"</td><td>"+value[6]+"</td><td>"+value[7]+"</td></tr>");
                });
                let fReady = item.data.ready.tfoot;
                talentFoot.append("<tr style='border-bottom:2px; border-bottom-style: solid; border-bottom-color: gray;'><td colspan=100%'></td></tr><tr><td><b>Total<b></td><td><b>"+fReady[0]+"<b></td><td><b>"+fReady[1]+"<b></td><td><b>"+fReady[2]+"<b></td><td><b>"+fReady[3]+"<b></td><td><b>"+fReady[4]+"<b></td><td><b>"+fReady[5]+"<b></td><td><b>"+fReady[6]+"<b></td></tr>");

                //studentAlocated
                var alocatedBody = $("#StudentAlocatedFinal tbody");
                var alocatedFoot = $("#StudentAlocatedFinal tfoot");
                $.each(item.data.alocated.tbody, function(key, value) {
                    alocatedBody.append("<tr><td>"+value[0]+"</td><td>"+value[1]+"</td><td>"+value[2]+"</td><td>"+value[3]+"</td><td>"+value[4]+"</td><td>"+value[5]+"</td><td>"+value[6]+"</td><td>"+value[7]+"</td></tr>");
                });
                let fAlocated = item.data.alocated.tfoot;
                alocatedFoot.append("<tr style='border-bottom:2px; border-bottom-style: solid; border-bottom-color: gray;'><td colspan=100%'></td></tr><tr><td><b>Total<b></td><td><b>"+fAlocated[0]+"<b></td><td><b>"+fAlocated[1]+"<b></td><td><b>"+fAlocated[2]+"<b></td><td><b>"+fAlocated[3]+"<b></td><td><b>"+fAlocated[4]+"<b></td><td><b>"+fAlocated[5]+"<b></td><td><b>"+fAlocated[6]+"<b></td></tr>");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //
            }
        });

        $('#filterBulan li').on('click', function(){
            $('#filterBulan li a').removeClass('active');
            $(this).find('a').addClass('active');
            $(this).parent().prev().text($(this).find('a').text());
            $('#totalDalamJawa').remove();
            $('#pktDalamJawa').remove();
            $('#simDalamJawa').remove();
            $('#totalLuarJawa').remove();
            $('#pktLuarJawa').remove();
            $('#simLuarJawa').remove();

            if ($('#filterDropdownJenis').text() == 'Total Semua') {
                $('#canvasDalamJawa').append('<canvas id="totalDalamJawa" class="" style="height:300px; width:100%"></canvas>');
                $('#canvasDalamJawa').append('<canvas id="pktDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasDalamJawa').append('<canvas id="simDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="totalLuarJawa" class="" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="pktLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="simLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                loadChartJava($(this).find('a').text());
                loadChartOutJava($(this).find('a').text());
            }else if($('#filterDropdownJenis').text() == 'PKT'){
                $('#canvasDalamJawa').append('<canvas id="totalDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasDalamJawa').append('<canvas id="pktDalamJawa" class="" style="height:300px; width:100%"></canvas>');
                $('#canvasDalamJawa').append('<canvas id="simDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="totalLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="pktLuarJawa" class="" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="simLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                loadChartPKTJava($(this).find('a').text());
                loadChartPKTOutJava($(this).find('a').text());
            }else if($('#filterDropdownJenis').text() == 'Permintaan Khusus'){
                $('#canvasDalamJawa').append('<canvas id="totalDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasDalamJawa').append('<canvas id="pktDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasDalamJawa').append('<canvas id="simDalamJawa" class="" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="totalLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="pktLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
                $('#canvasLuarJawa').append('<canvas id="simLuarJawa" class="" style="height:300px; width:100%"></canvas>');
                loadChartSIMJava($(this).find('a').text());
                loadChartSIMOutJava($(this).find('a').text());
            }
            
        });

        $('#TTLDalamJawa').click(function() {
            $('#TTLDalamJawa').addClass('active');
            $('#SIMDalamJawa').removeClass('active');
            $('#PKTDalamJawa').removeClass('active');
            $('#totalDalamJawa').remove();
            $('#pktDalamJawa').remove();
            $('#simDalamJawa').remove();
            $('#canvasDalamJawa').append('<canvas id="totalDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#canvasDalamJawa').append('<canvas id="pktDalamJawa" class="" style="height:300px; width:100%"></canvas>');
            $('#canvasDalamJawa').append('<canvas id="simDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#totalLuarJawa').remove();
            $('#pktLuarJawa').remove();
            $('#simLuarJawa').remove();
            $('#canvasLuarJawa').append('<canvas id="totalLuarJawa" class="" style="height:300px; width:100%"></canvas>');
            $('#canvasLuarJawa').append('<canvas id="pktLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#canvasLuarJawa').append('<canvas id="simLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#filterDropdownJenis').text($(this).text());
            loadChartJava($('#filterBulan').find('.active').text());
            loadChartOutJava($('#filterBulan').find('.active').text());
        });

        $('#PKTDalamJawa').click(function() {
            $('#TTLDalamJawa').removeClass('active');
            $('#SIMDalamJawa').removeClass('active');
            $('#PKTDalamJawa').addClass('active');
            $('#totalDalamJawa').remove();
            $('#pktDalamJawa').remove();
            $('#simDalamJawa').remove();
            $('#canvasDalamJawa').append('<canvas id="totalDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#canvasDalamJawa').append('<canvas id="pktDalamJawa" class="" style="height:300px; width:100%"></canvas>');
            $('#canvasDalamJawa').append('<canvas id="simDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#totalLuarJawa').remove();
            $('#pktLuarJawa').remove();
            $('#simLuarJawa').remove();
            $('#canvasLuarJawa').append('<canvas id="totalLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#canvasLuarJawa').append('<canvas id="pktLuarJawa" class="" style="height:300px; width:100%"></canvas>');
            $('#canvasLuarJawa').append('<canvas id="simLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#filterDropdownJenis').text($(this).text());
            loadChartPKTJava($('#filterBulan').find('.active').text());
            loadChartPKTOutJava($('#filterBulan').find('.active').text());
        });

        $('#SIMDalamJawa').click(function() {
            $('#TTLDalamJawa').removeClass('active');
            $('#SIMDalamJawa').addClass('active');
            $('#PKTDalamJawa').removeClass('active');
            $('#totalDalamJawa').remove();
            $('#pktDalamJawa').remove();
            $('#simDalamJawa').remove();
            $('#canvasDalamJawa').append('<canvas id="totalDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#canvasDalamJawa').append('<canvas id="pktDalamJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#canvasDalamJawa').append('<canvas id="simDalamJawa" class="" style="height:300px; width:100%"></canvas>');
            $('#totalLuarJawa').remove();
            $('#pktLuarJawa').remove();
            $('#simLuarJawa').remove();
            $('#canvasLuarJawa').append('<canvas id="totalLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#canvasLuarJawa').append('<canvas id="pktLuarJawa" class="d-none" style="height:300px; width:100%"></canvas>');
            $('#canvasLuarJawa').append('<canvas id="simLuarJawa" class="" style="height:300px; width:100%"></canvas>');
            $('#filterDropdownJenis').text($(this).text());
            loadChartSIMJava($('#filterBulan').find('.active').text());
            loadChartSIMOutJava($('#filterBulan').find('.active').text());
        });

        //req per job desk
        $('#filterPerPKT li').on('click', function(){
            $('#filterPerPKT li a').removeClass('active');
            $(this).find('a').addClass('active');
            $('#permintaanSDMJobDesk').remove();
            $('#canvasPermintaanSDMJobDesk').append('<canvas id="permintaanSDMJobDesk" style="height:300px; width:100%"></canvas>');
            loadChartReqVokasi($(this).data('id'));
            $(this).parent().prev().text($(this).find('a').text());
        });
        loadChartJava();
        loadChartOutJava();
        loadChartReqVokasi();
        
        $('#skillLesson li').on('click', function(){
            $('#skillLesson li a').removeClass('active');
            $(this).find('a').addClass('active');
            schoolAchievement({ lesson_id : $(this).data('id')});
            $(this).parent().prev().text($(this).find('a').text());
            // if ($(this).find('a').text() == 'Money Ban') {
            //     let thead = $("#school-scroll thead tr");
            //     thead.find('#jam_terbang').remove();
            //     schoolAchievement();
            //     let tbody = $("#school-scroll tbody tr");
            //     tbody.find('#fligth_hour').remove();
            // }
        });

        $('#filterSkillSchool li').on('click', function(){
            $('#filterSkillSchool li a').removeClass('active');
            $(this).find('a').addClass('active');
            $('#filterSkillStudent').find('li').remove();
            $.ajax({
                url : "{{ route('get.student') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: {'_token' : '{{ csrf_token() }}', 'school_id' : $(this).data('id')},
                success: function(data)
                {
                    $.each(data.result, function(key, value) {
                        $('#filterSkillStudent').append('<li data-id="'+key+'"><a href="javascript:void(0)" class="dropdown-item">'+value+'</a></li>');
                    });
                    $('#filterSkillStudent').find('li').on('click', function(){
                        $('#filterSkillStudent li a').removeClass('active');
                        $(this).find('a').addClass('active');
                        studentAchievement({ student_id : $(this).data('id')});
                    });
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $('#filterSkillStudent li').remove();
                }
            });
        });

        $('#filterSkillStudent').find('li').on('click', function(){
            $('#filterSkillStudent li a').removeClass('active');
            $(this).find('a').addClass('active');
            studentAchievement({ student_id : $(this).data('id')});
        });

        // Showing Percentage School Data in Pie Chart
        function loadChartReqVokasi(filter = null){
            let data = {'_token' : '{{ csrf_token() }}', 'partner_id' : filter};
            $.ajax({
                url : "{{ route('get.chart') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: data,
                success: function(item)
                {
                    loadChart3('permintaanSDMJobDesk', 'bar', item.data.reqVokasi, 'Total Pemenuhan (' + item.data.countPemenuhan + ') Total Permintaan (' + item.data.countPermintaan + ')' );
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    // $('select[name="city_id"]').html('<option value="">Select</option>');
                }
            });
        }

        // Showing Percentage School Data in Pie Chart
        function loadChartJava(filter = null){
            let data = {'_token' : '{{ csrf_token() }}', 'bulan' : filter};
            $.ajax({
                url : "{{ route('get.chartReqJava') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: data,
                success: function(item)
                {
                    loadChart('totalDalamJawa', 'bar', item.data.totalDalamJawa);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    // $('select[name="city_id"]').html('<option value="">Select</option>');
                }
            });
        }

        // Showing Percentage School Data in Pie Chart
        function loadChartPKTJava(filter = null){
            let data = {'_token' : '{{ csrf_token() }}', 'bulan' : filter};
            $.ajax({
                url : "{{ route('get.chartReqJava') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: data,
                success: function(item)
                {
                    loadChart2('pktDalamJawa', 'bar', item.data.pktDalamJawa, 'Pulau Jawa (' + item.data.countTotalJawa + ')');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    // $('select[name="city_id"]').html('<option value="">Select</option>');
                }
            });
        }

        // Showing Percentage School Data in Pie Chart
        function loadChartSIMJava(filter = null){
            let data = {'_token' : '{{ csrf_token() }}', 'bulan' : filter};
            $.ajax({
                url : "{{ route('get.chartReqJava') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: data,
                success: function(item)
                {
                    loadChart2('simDalamJawa', 'bar', item.data.simDalamJawa, 'Pulau Jawa (' + item.data.countTotalJawa + ')');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    // $('select[name="city_id"]').html('<option value="">Select</option>');
                }
            });
        }

        // Showing Percentage School Data in Pie Chart
        function loadChartOutJava(filter = null){
            let data = {'_token' : '{{ csrf_token() }}', 'bulan' : filter};
            $.ajax({
                url : "{{ route('get.chartReqOutJava') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: data,
                success: function(item)
                {
                    loadChart('totalLuarJawa', 'bar', item.data.totalLuarJawa);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    // $('select[name="city_id"]').html('<option value="">Select</option>');
                }
            });
        }

        // Showing Percentage School Data in Pie Chart
        function loadChartPKTOutJava(filter = null){
            let data = {'_token' : '{{ csrf_token() }}', 'bulan' : filter};
            $.ajax({
                url : "{{ route('get.chartReqOutJava') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: data,
                success: function(item)
                {
                    loadChart2('pktLuarJawa', 'bar', item.data.pktLuarJawa, 'Luar Jawa (' + item.data.countTotalLuar + ')');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    // $('select[name="city_id"]').html('<option value="">Select</option>');
                }
            });
        }

        // Showing Percentage School Data in Pie Chart
        function loadChartSIMOutJava(filter = null){
            let data = {'_token' : '{{ csrf_token() }}', 'bulan' : filter};
            $.ajax({
                url : "{{ route('get.chartReqOutJava') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: data,
                success: function(item)
                {
                    loadChart2('simLuarJawa', 'bar', item.data.simLuarJawa, 'Luar Jawa (' + item.data.countTotalLuar + ')');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    // $('select[name="city_id"]').html('<option value="">Select</option>');
                }
            });
        }

        // Showing Percentage School Data in Pie Chart
        $.ajax({
            url : "{{ route('get.chart') }}",
            type: "POST",
            dataType: "JSON",
            cache: false,
            data: {'_token' : '{{ csrf_token() }}', 'bulan' : $('select[name="bulan"]').val()},
            success: function(item)
            {
                console.log(item.data.schooLStatuses);
                // $('#divstatusSekolah').addClass('d-none');
                // $('#statusSekolah').removeClass('d-none');
                loadChart('statusSekolah', 'pie', item.data.schooLStatuses);
                loadChart('totalVokasiJobDesk', 'polarArea', item.data.totalVokasiJobDesk);
                loadChart('totalVokasiPkt', 'polarArea', item.data.totalVokasiPkt);
                loadChart('statusSiswa', 'horizontalBar', item.data.studentStatuses, false);
                loadChart('aktifAlumni', 'pie', item.data.aktifAlumni);
                loadChart('totalStatusVokasi', 'pie', item.data.totalStatusVokasi, true, 'Total Peserta Vokasi (' + item.data.countStatusVokasi + ')');
                // loadChart('totalDalamJawa', 'bar', item.data.totalDalamJawa);
                // loadChart('totalLuarJawa', 'bar', item.data.totalLuarJawa);
                // $('#PKTDalamJawa').click(function() {
                //         loadChart2('pktDalamJawa', 'bar', item.data.pktDalamJawa, 'Pulau Jawa (' + item.data.countTotalJawa + ')');
                // });
                // $('#PKTDalamJawa').click(function() {
                //         loadChart2('pktLuarJawa', 'bar', item.data.pktLuarJawa, 'Luar Jawa (' + item.data.countTotalLuar + ')');
                // });
                // $('#SIMDalamJawa').click(function() {
                //         loadChart2('simDalamJawa', 'bar', item.data.simDalamJawa, 'Luar Jawa (' + item.data.countTotalJawa + ')');
                // });
                // $('#SIMDalamJawa').click(function() {
                //         loadChart2('simLuarJawa', 'bar', item.data.simLuarJawa, 'Luar Jawa (' + item.data.countTotalLuar + ')');
                // });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                // $('select[name="city_id"]').html('<option value="">Select</option>');
            }
        });

        schoolAchievement();
        studentAchievement();
    });

    // Showing student achievement
    function studentAchievement(filter = null){
        let data = {
                '_token' : '{{ csrf_token() }}',
            };
        data = $.extend(data, filter);
        $.ajax({
            url : "{{ route('get.studentAchievement') }}",
            type: "POST",
            dataType: "JSON",
            cache: false,
            data: data,
            success: function(item)
            {
                var table = $("#student-scroll tbody");
                table.find('tr').remove();
                $.each(item.result, function(key, value) {
                    table.append("<tr><td>"+value['lesson']+"</td><td>"+value['jam_terbang']+"</td>   <td>"+value['achievement']+"</td></tr>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //
            }
        });
    }

    // Showing school achievement
    function schoolAchievement(filter = null){
        let data = {
                '_token' : '{{ csrf_token() }}',
            };
        data = $.extend(data, filter);
        $.ajax({
            url : "{{ route('get.schoolAchievement') }}",
            type: "POST",
            dataType: "JSON",
            cache: false,
            data: data,
            success: function(item)
            {
                var table = $("#school-scroll tbody");
                table.find('tr').remove();
                $.each(item.result, function(key, value) {
                    table.append("<tr><td>"+value['school']+"</td><td id='fligth_hour'>"+value['fligth_hour']+"</td><td>"+value['loop']+"</td><td>"+value['average']+"</td></tr>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //
            }
        });
    }

    // load Chart Function
    function loadChart(id, tipe, data, legend = true, title = null) {
        $('#div'+id).addClass('d-none');
        $('#'+id).removeClass('d-none');
        let chart_params = {
            type: tipe,
            data: data,
            options: {
                legend: {
                    display: legend
                },
                responsive: true,
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var allData = data.datasets[tooltipItem.datasetIndex].data;
                            var tooltipLabel = data.labels[tooltipItem.index];
                            var tooltipData = allData[tooltipItem.index];
                            var total = 0;
                            for (var i in allData) {
                                total += allData[i];
                            }
                            var tooltipPercentage = Math.round((tooltipData / total) * 100);
                            return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                        }
                    }
                },            
            },
        };
        tipe == 'pie' ? chart_params.options.plugins = {
            datalabels: {
                formatter: (value, ctx) => {
                
                    let sum = 0;
                    let dataArr = ctx.chart.data.datasets[0].data;
                    dataArr.map(data => {
                        sum += data;
                    });
                    let percentage = (value*100 / sum).toFixed(2)+"%";
                    return percentage;
            
                },
                color: '#fff',
            }
        } : chart_params.options.plugins = {
                datalabels: {
                    display: false,
                }
            };

        title ? chart_params.options.title = {
                    display: true,
                    text: title,
                    fontSize: 16,
                } : '';
        new Chart($('#'+id), chart_params);
    }

    // load Chart Function
    function loadChart2(id, tipe, data, title = null) {
        $('#div'+id).addClass('d-none');
        $('#'+id).removeClass('d-none');
        let chart_params = {
            type: tipe,
            data: data,
            options: {
                tooltips: {
                    mode: 'label',
                },
                title: {
                    display: true,
                    text: title,
                    fontSize: 16,
                },
                scales: {
                    xAxes: [{ stacked: true }],
                    yAxes: [{ stacked: true }]
                }
            }
        };
        tipe == 'pie' ? chart_params.options.plugins = {
            datalabels: {
                formatter: (value, ctx) => {
                
                    let sum = 0;
                    let dataArr = ctx.chart.data.datasets[0].data;
                    dataArr.map(data => {
                        sum += data;
                    });
                    let percentage = (value*100 / sum).toFixed(2)+"%";
                    return percentage;
            
                },
                color: '#fff',
            }
        } : chart_params.options.plugins = {
                datalabels: {
                    display: false,
                }
            };
        new Chart($('#'+id), chart_params);
    }

    // load Chart Function
    function loadChart3(id, tipe, data, title = null) {
        let chart_params = {
            type: tipe,
            data: data,
            options: {
                responsive: true,
                tooltips: {
                    mode: 'label',
                    callbacks: {
                        title : function(tooltipItem, data) {
                            // var allData = data.datasets[tooltipItem.datasetIndex].data;
                            var tooltipLabel = tooltipItem[0].xLabel;
                            // var tooltipData = allData[tooltipItem.index];
                            // var total = 0;
                            // for (var i in allData) {
                            //     total += allData[i];
                            // }
                            // var tooltipPercentage = Math.round((tooltipData / total) * 100);
                            // return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                            return 'Total Permintaan ' + tooltipLabel[0] + ' : ' + tooltipLabel[1];
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{   
                        stacked: true,
                    },
                    {
                        id: "bar-y-axis",
                        stacked: true,
                        display: false, //optional
                    }]
                },
            }
        };
        tipe == 'pie' ? chart_params.options.plugins = {
            datalabels: {
                formatter: (value, ctx) => {
                
                    let sum = 0;
                    let dataArr = ctx.chart.data.datasets[0].data;
                    dataArr.map(data => {
                        sum += data;
                    });
                    let percentage = (value*100 / sum).toFixed(2)+"%";
                    return percentage;
            
                },
                color: '#fff',
            }
        } : chart_params.options.plugins = {
                datalabels: {
                    display: false,
                }
            };
        title ? chart_params.options.title = {
                    display: true,
                    text: title,
                    fontSize: 16,
                } : '';
        new Chart($('#'+id), chart_params);
    }

// Showing Permintaan SDM Job Desk on Stacked Bar
// let permintaanSDMJobDeskCtx = $('#permintaanSDMJobDesk');
// let permintaanSDMJobDesk = new Chart(permintaanSDMJobDeskCtx, {
//     type: 'bar',
//     data: {
//         labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
//         datasets: [
//             @foreach($reqJobs as $reqJob => $value)
//             {
//                 label: '{{ $reqJob }}',
//                 data: ['{{ $value[0] }}', '{{ $value[1] }}', '{{ $value[2] }}', '{{ $value[3] }}', '{{ $value[4] }}', '{{ $value[5] }}', '{{ $value[6] }}', '{{ $value[7] }}', '{{ $value[8] }}', '{{ $value[9] }}', '{{ $value[10] }}', '{{ $value[11] }}' ],
//                 backgroundColor: '{{ $value[12] }}',
//             },
//             @endforeach
//             {
//                 data: {{ $vokasiReqJobs }},
//                 type: 'line',
//                 label: 'Pemenuhan',
//                 fill: false,
//                 backgroundColor: "#fff",
//                 borderColor: "#3f3f3f",
//                 borderWidth: 2,
//             },
//         ]
//     },
//     options: {
//         responsive: true,
//         tooltips: {
//             mode: 'point',
//         },
//         scales: {
//             xAxes: [{
//                 stacked: true,
//             }],
//             yAxes: [{   
//                 stacked: true,
//             },
//             {
//                 id: "bar-y-axis",
//                 stacked: true,
//                 display: false, //optional
//             }]
//         }
//     }
// });


// Showing Permintaan SDM PKT on Line Chart
let permintaanSDMPktCtx = $('#permintaanSDMPkt');
let permintaanSDMPkt = new Chart(permintaanSDMPktCtx, {
    type: 'line',
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        datasets: [
            @foreach($reqPartners as $reqPartner => $value)
            {
                label: '{{ $reqPartner }}',
                data: ['{{ $value[0] }}', '{{ $value[1] }}', '{{ $value[2] }}', '{{ $value[3] }}', '{{ $value[4] }}', '{{ $value[5] }}', '{{ $value[6] }}', '{{ $value[7] }}', '{{ $value[8] }}', '{{ $value[9] }}', '{{ $value[10] }}', '{{ $value[11] }}'],
                borderWidth: 5,
                borderColor: '{{ $value[12] }}',
                backgroundColor: 'transparent',
                pointBackgroundColor: '#fff',
                pointBorderColor: '{{ $value[12] }}',
                pointRadius: 4
            },
            @endforeach
        ]
    },
    options: {
        responsive: true,
        // title: {
        //     display: true,
        //     text: 'PKT',
        //     fontSize: 16,
        // },
        legend: {
            display: true
        },
        tooltips: {
            mode: 'point',
            intersect: false,
            displayColors: true,
        },
        scales: {
            ticks: {
               stepSize: 5,
            },
            yAxes: [{
                gridLines: {
                display: false,
                drawBorder: false,
                },
            }],
            xAxes: [{
                gridLines: {
                color: '#f1f1f1',
                lineWidth: 2
                }
            }]
        },
    }
});

// Showing Permintaan TOTAL SDM Dalam Jawa on Stacked Bar
// let totalDalamJawaCtx = $('#totalDalamJawa');
// let totalDalamJawa = new Chart(totalDalamJawaCtx, {
//     type: 'bar',
//     data: {
//         labels: ["Banten", "JKT", "Jabar", "Jateng", "Jatim", "DIY"],
//         datasets: [
//             {
//                 label: 'Total',
//                 data: ['{{ $reqJavas[0] }}', '{{ $reqJavas[1] }}', '{{ $reqJavas[2] }}', '{{ $reqJavas[3] }}', '{{ $reqJavas[4] }}', '{{ $reqJavas[5] }}'],
//                 backgroundColor: 'rgba(103, 119, 239, 0.6)',
//             },        
//         ]},
//     options: {
//         legend: {
//             display:false,
//         },
// 		tooltips: {
// 			callbacks: {
// 				label: function(tooltipItem, data) {
// 					var allData = data.datasets[tooltipItem.datasetIndex].data;
// 					var tooltipLabel = data.labels[tooltipItem.index];
// 					var tooltipData = allData[tooltipItem.index];
// 					var total = 0;
// 					for (var i in allData) {
// 						total += allData[i];
// 					}
// 					var tooltipPercentage = Math.round((tooltipData / total) * 100);
// 					return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
// 				}
// 			}
// 		},
//         title: {
//             display: true,
//             text: 'Pulau Jawa (' + {{ collect($reqJavas)->sum() }} + ')',
//             fontSize: 16,
//         },
//         scales: {
//             xAxes: [{ stacked: true }],
//             yAxes: [{ stacked: true }]
//         }
//     }
// });

// Showing Permintaan SDM Dalam Jawa on Stacked Bar
// let pktDalamJawaCtx = $('#pktDalamJawa');
// let pktDalamJawa = new Chart(pktDalamJawaCtx, {
//     type: 'bar',
//     data: {
//         labels: ["Banten", "JKT", "Jabar", "Jateng", "Jatim", "DIY"],
//         datasets: [
//             @foreach($filterPartnerReqJavas as $reqPartner => $value)
//             {
//                 label: '{{ $reqPartner }}',
//                 data: ['{{ $value[0] }}', '{{ $value[1] }}', '{{ $value[2] }}', '{{ $value[3] }}', '{{ $value[4] }}', '{{ $value[5] }}'],
//                 backgroundColor: '{{ $value[6] }}',
//             },
//             @endforeach
//             // {
//             //     label: 'PKT 7',
//             //     data: [4, 5, 2, 3, 3, 3],
//             //     backgroundColor: 'rgba(238,158,54)'
//             // },
//             // {
//             //     label: 'PKT 8',
//             //     data: [4, 2, 3, 2, 4, 5],
//             //     backgroundColor: 'rgba(238,54,57)'
//             // },
        
//         ]},
//     options: {
//         title: {
//             display: true,
//             text: 'Pulau Jawa (' + {{ collect($reqJavas)->sum() }} + ')',
//             fontSize: 16,
//         },
//         scales: {
//             xAxes: [{ stacked: true }],
//             yAxes: [{ stacked: true }]
//         }
//   }
// });

// Showing Permintaan TOTAL SDM SIM Dalam Jawa on Stacked Bar
// let simDalamJawaCtx = $('#simDalamJawa');
// let simDalamJawa = new Chart(simDalamJawaCtx, {
//     type: 'bar',
//     data: {
//         labels: ["Banten", "JKT", "Jabar", "Jateng", "Jatim", "DIY"],
//         datasets: [
//             @foreach($filterReqSimJavas as $reqPartner => $value)
//             {
//                 label: '{{ $reqPartner }}',
//                 data: ['{{ $value[0] }}', '{{ $value[1] }}', '{{ $value[2] }}', '{{ $value[3] }}', '{{ $value[4] }}', '{{ $value[5] }}'],
//                 backgroundColor: '{{ $value[6] }}',
//             },
//             @endforeach       
//         ]},
//     options: {
//         legend: {
//             display:false,
//         },
//         title: {
//             display: true,
//             text: 'Pulau Jawa (' + {{ collect($reqJavas)->sum() }} + ')',
//             fontSize: 16,
//         },
//         scales: {
//             xAxes: [{ stacked: true }],
//             yAxes: [{ stacked: true }]
//         }
//     }
// });

$('#TTLLuarJawa').click(function() {
    $('#totalLuarJawa').removeClass('d-none');
    $('#simLuarJawa').addClass('d-none');
    $('#pktLuarJawa').addClass('d-none');
    $('#TTLLuarJawa').addClass('active');
    $('#SIMLuarJawa').removeClass('active');
    $('#PKTLuarJawa').removeClass('active');
});

$('#PKTLuarJawa').click(function() {
    $('#totalLuarJawa').addClass('d-none');
    $('#simLuarJawa').addClass('d-none');
    $('#pktLuarJawa').removeClass('d-none');
    $('#TTLLuarJawa').removeClass('active');
    $('#SIMLuarJawa').removeClass('active');
    $('#PKTLuarJawa').addClass('active');
});

$('#SIMLuarJawa').click(function() {
    $('#totalLuarJawa').addClass('d-none');
    $('#pktLuarJawa').addClass('d-none');
    $('#simLuarJawa').removeClass('d-none');
    $('#SIMLuarJawa').addClass('active');
    $('#PKTLuarJawa').removeClass('active');
    $('#TTLLuarJawa').removeClass('active');
});

// Showing Permintaan TOTAL SDM Luar Jawa on Stacked Bar
// let totalLuarJawaCtx = $('#totalLuarJawa');
// let totalLuarJawa = new Chart(totalLuarJawaCtx, {
//     type: 'bar',
//     data: {
//         labels: ["Kalimantan", "Maluku", "Nusa Tenggara", "Papua", "Sulawesi", "Sumatera"],
//         datasets: [
//             {
//                 label: 'Total',
//                 data: ['{{ $reqOutOfJavas[0] }}', '{{ $reqOutOfJavas[1] }}', '{{ $reqOutOfJavas[2] }}', '{{ $reqOutOfJavas[3] }}', '{{ $reqOutOfJavas[4] }}', '{{ $reqOutOfJavas[5] }}'],
//                 backgroundColor: 'rgba(103, 119, 239, 0.6)',
//             },        
//         ]},
//     options: {
//         legend: {
//             display:false,
//         },
// 		tooltips: {
// 			callbacks: {
// 				label: function(tooltipItem, data) {
// 					var allData = data.datasets[tooltipItem.datasetIndex].data;
// 					var tooltipLabel = data.labels[tooltipItem.index];
// 					var tooltipData = allData[tooltipItem.index];
// 					var total = 0;
// 					for (var i in allData) {
// 						total += allData[i];
// 					}
// 					var tooltipPercentage = Math.round((tooltipData / total) * 100);
// 					return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
// 				}
// 			}
// 		},
//         title: {
//             display: true,
//             text: 'Luar Pulau Jawa (' + {{ collect($reqOutOfJavas)->sum() }} + ')',
//             fontSize: 16,
//         },
//         scales: {
//             xAxes: [{ stacked: true }],
//             yAxes: [{ stacked: true }]
//         }
//     }
// });

// Showing Permintaan SDM Luar Jawa on Stacked Bar
// let pktLuarJawaCtx = $('#pktLuarJawa');
// let pktLuarJawa = new Chart(pktLuarJawaCtx, {
//     type: 'bar',
//     data: {
//         labels: ["Kalimantan", "Maluku", "Nusa Tenggara", "Papua", "Sulawesi", "Sumatra"],
//         datasets: [
//             @foreach($filterPartnerReqOutOfJavas as $reqPartner => $value)
//             {
//                 label: '{{ $reqPartner }}',
//                 data: ['{{ $value[0] }}', '{{ $value[1] }}', '{{ $value[2] }}', '{{ $value[3] }}', '{{ $value[4] }}', '{{ $value[5] }}'],
//                 backgroundColor: '{{ $value[6] }}',
//             },
//             @endforeach
//         ]},
//     options: {
//         title: {
//             display: true,
//             text: 'Luar Pulau Jawa (' + {{ collect($reqOutOfJavas)->sum() }} + ')',
//             fontSize: 16,
//         },
//         scales: {
//             xAxes: [{ stacked: true }],
//             yAxes: [{ stacked: true }]
//         }
//   }
// });

// Showing Permintaan TOTAL SDM SIM Dalam Jawa on Stacked Bar
// let simLuarJawaCtx = $('#simLuarJawa');
// let simLuarJawa = new Chart(simLuarJawaCtx, {
//     type: 'bar',
//     data: {
//         labels: ["Kalimantan", "Maluku", "Nusa Tenggara", "Papua", "Sulawesi", "Sumatra"],
//         datasets: [
//             @foreach($filterReqSimOutJavas as $reqPartner => $value)
//             {
//                 label: '{{ $reqPartner }}',
//                 data: ['{{ $value[0] }}', '{{ $value[1] }}', '{{ $value[2] }}', '{{ $value[3] }}', '{{ $value[4] }}', '{{ $value[5] }}'],
//                 backgroundColor: '{{ $value[6] }}',
//             },
//             @endforeach        
//         ]},
//     options: {
//         legend: {
//             display:false,
//         },
//         title: {
//             display: true,
//             text: 'Luar Pulau Jawa (' + {{ collect($reqOutOfJavas)->sum() }} + ')',
//             fontSize: 16,
//         },
//         scales: {
//             xAxes: [{ stacked: true }],
//             yAxes: [{ stacked: true }]
//         }
//     }
// });

if($("#list-scroll").length) {
    $("#list-scroll").css({
      height: 260
    }).niceScroll();
}

if($("#school-scroll").length) {
    $("#school-scroll").css({
      height: 230
    }).niceScroll();
}

if($("#student-scroll").length) {
    $("#student-scroll").css({
      height: 230
    }).niceScroll();
}

if($("#filterSkillSchool").length) {
    $("#filterSkillSchool").css({
      height: 230
    }).niceScroll();
}

if($("#filterSkillStudent").length) {
    $("#filterSkillStudent").css({
      height: 230
    }).niceScroll();
}
</script>

<!-- Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModallLabel">{{ __('Filter') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="javascript:void(0)" id="filterBulan" method="post">
            {{ csrf_field() }}
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            {{ Form::bsSelect('col-12', __('Bulan'), 'bulan', $months, null, __('Select'), ['placeholder' => __('Select')]) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke d-flex justify-content-center">
                    <button type="submit" name="filter" id="filter" class="btn btn-success">Filter</button>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-default">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection